import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  getPublicUrl(id){
    return environment.url + "/file/storage/" + id;
  }

  getPublicUrlAWS(image){
    const data = JSON.parse(image);
    return environment.publicBucketUrl + data.key;
  }
  
  async uploadForm(formData, type) {
    return await this.http.post(environment.url + '/file/upload/'+type,
      formData).toPromise();
  }


  async getBase64Image(idFile) {
    const data = JSON.parse(idFile);
    return (await this.http.post(environment.url + '/file/url', data).toPromise())['url'];
  }

  async getSignedUrl(idFile) {
    const data = JSON.parse(idFile);
    return (await this.http.post(environment.url + '/file/url', data).toPromise())['url'];
  }


  async downloadToBase64(dataImage) {
    const data = JSON.parse(dataImage);
    return new Promise(async (resolve, reject) => {
      const response = await this.http.post<Blob>(environment.url + '/file/download-as', data, { responseType: 'blob' as 'json' }).toPromise();

      let reader = new FileReader();
      reader.addEventListener("load", () => {
        resolve(reader.result);
      }, false);
      reader.readAsDataURL(response);
    })
  }
  
}
