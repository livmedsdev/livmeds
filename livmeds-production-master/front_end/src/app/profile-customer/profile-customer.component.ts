import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../user.service';
import { SessionService } from '../session.service';
import { FileService } from '../file.service';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-customer',
  templateUrl: './profile-customer.component.html',
  styleUrls: ['./profile-customer.component.scss']
})
export class ProfileCustomerComponent implements OnInit {
  public myProfile : User;

  // Security form
  public securityFormGroup;
  public errorSecurity;
  public successSecurity;

  
  // Information form
  public informationFormGroup;
  public errorInformation;
  public successInformation;

  constructor(public session: SessionService, 
    private fileService: FileService,
    public userService: UserService,
    private _ngxPicaService: NgxPicaService) { 
    this.securityFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      currentPassword: ['', Validators.compose([])],
      newPassword: ['', Validators.compose([])]
    });

    this.informationFormGroup = new FormBuilder().group({
      name: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
    });
  }

  async ngOnInit() {
    this.myProfile = {addresses:[]} as User;
    this.myProfile = await this.userService.getProfile() as User;
    console.log(this.myProfile);
    this.informationFormGroup.patchValue(this.myProfile);
  }

  
  async submitSecurityForm(){
    this.resetMessage();
    if(this.securityFormGroup.valid){
      try{
        const response : any = await this.userService.editSecurityInformation(this.securityFormGroup.value);
        this.myProfile = (await this.userService.getProfile()) as User;
        this.successSecurity = response.tradCode;
        this.securityFormGroup.reset();
      }catch(error){
        this.errorSecurity = error.error.tradCode;
      }
    }
  }

  
  async submitInformationForm(){
    this.resetMessage();
    if(this.informationFormGroup.valid){
      try{
        const response : any = await this.userService.editMyProfile(this.informationFormGroup.value);
        this.myProfile = await this.userService.getProfile() as User;
        this.successInformation = response.tradCode;
        this.informationFormGroup.patchValue(this.myProfile);
      }catch(error){
        this.errorInformation = error.error.tradCode;
      }
    }
  }
  
  resetMessage(){
    this.successSecurity = null;
    this.errorSecurity = null;
  }
  
  async onFileChanged(event){
    const selectedFile = event.target.files[0];
    const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 400, 400, {
      exifOptions:{
        forceExifOrientation: true
      },
      aspectRatio: {
      keepAspectRatio: true
    }}).toPromise();
              
    const uploadData = new FormData();
    uploadData.append('file', imageResized, selectedFile.name);
    const response : any = await this.fileService.uploadForm(uploadData, 'avatar');
    await this.userService.changeAvatar(response.idFile);
    this.myProfile = await this.userService.getProfile() as User;
  }

  
  ngOnDestroy() {
  }

}
