import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UserService } from './user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { TokenInterceptorService } from './token-interceptor.service';
import { DeliveriesComponent } from './deliveries/deliveries.component';
import { DeliveryService } from './delivery.service';
import { FileService } from './file.service';
import { AngularFireMessaging, AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { BlogComponent } from './blog/blog.component';
import { SessionService } from './session.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AddTopicComponent } from './add-topic/add-topic.component';
import { TopicComponent } from './topic/topic.component';
import { LOCALE_ID } from '@angular/core';
import { FaqComponent } from './faq/faq.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfilePharmacyComponent } from './profile-pharmacy/profile-pharmacy.component';
import { NgxPicaModule } from '@digitalascetic/ngx-pica';
import { ProfileCustomerComponent } from './profile-customer/profile-customer.component';
import { TchatComponent } from './tchat/tchat.component';
import { MessageService } from './message.service';
import { SseService } from './sse.service';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
import { ViewInvoiceComponent } from './view-invoice/view-invoice.component';
import { NotifierModule, NotifierOptions } from "angular-notifier";
import {ToasterModule} from 'angular2-toaster';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule, MatFormField, MatFormFieldModule, MatInputModule, MatButtonModule, MatCardModule, MatListModule, MatSelectionList, MatStepperModule, MatChipsModule, MatTabsModule, MatDividerModule, MatDatepickerModule} from '@angular/material';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { SignUpModalComponent } from './sign-up-modal/sign-up-modal.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ContactService } from './contact.service';
import { AddDeliveryComponent } from './add-delivery/add-delivery.component';
import { DeliveryBillComponent } from './delivery-bill/delivery-bill.component';
import { CardsComponent } from './cards/cards.component';
import { AddCardComponent } from './add-card/add-card.component';
import { DocumentsComponent } from './documents/documents.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { ModalPinValidationComponent } from './modal-pin-validation/modal-pin-validation.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CguComponent } from './cgu/cgu.component';
import { ShowImageModalComponent } from './show-image-modal/show-image-modal.component';
import { CreateDeliveryComponent } from './create-delivery/create-delivery.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AsyncClickDirective } from './directives/async-click.directive';
import { LegalMentionComponent } from './legal-mention/legal-mention.component';
import { SignUpCustomerComponent } from './sign-up-customer/sign-up-customer.component';
import { CommandComponent } from './command/command.component';
import { DocumentsCustomerComponent } from './documents-customer/documents-customer.component';
import { DocumentsPartnerComponent } from './documents-partner/documents-partner.component';
import { MapsService } from './maps.service';
import { ProductService } from './product.service';
import { PlanningDialogComponent } from './planning-dialog/planning-dialog.component';
import { NeedDocDialogComponent } from './need-doc-dialog/need-doc-dialog.component';
import { CommandsComponent } from './commands/commands.component';
import { DetailsCommandComponent } from './details-command/details-command.component';
import { TrackCommandComponent } from './track-command/track-command.component';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { ArticleFastFitnessComponent } from './article-fast-fitness/article-fast-fitness.component';
import { DevenirLivreurComponent } from './devenir-livreur/devenir-livreur.component';
import { ContactComponent } from './contact/contact.component';
import { AproposComponent } from './apropos/apropos.component';
import { DeliveriesHistoricalComponent } from './deliveries-historical/deliveries-historical.component';
import { CommandHistoricalComponent } from './command-historical/command-historical.component';

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'left',
			distance: 12
		},
		vertical: {
			position: 'bottom',
			distance: 12,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DeliveriesComponent,
    BlogComponent,
    AddTopicComponent,
    TopicComponent,
    FaqComponent,
    ProfileComponent,
    ProfilePharmacyComponent,
    ProfileCustomerComponent,
    TchatComponent,
    CreateInvoiceComponent,
    ViewInvoiceComponent,
    LoginModalComponent,
    LegalMentionComponent,
    SignUpModalComponent,
    ResetPasswordComponent,
    AddDeliveryComponent,
    DeliveryBillComponent,
    CardsComponent,
    AddCardComponent,
    DocumentsComponent,
    ModalPinValidationComponent,
    CguComponent,
    ShowImageModalComponent,
    CreateDeliveryComponent,
    SignUpComponent,
    AsyncClickDirective,
    SignUpCustomerComponent,
    CommandComponent,
    DocumentsCustomerComponent,
    DocumentsPartnerComponent,
    PlanningDialogComponent,
    NeedDocDialogComponent,
    CommandsComponent,
    DetailsCommandComponent,
    TrackCommandComponent,
    ArticleFastFitnessComponent,
    DevenirLivreurComponent,
    ContactComponent,
    AproposComponent,
    DeliveriesHistoricalComponent,
    CommandHistoricalComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    ToasterModule.forRoot(),
    NotifierModule.withConfig(customNotifierOptions),
    BrowserModule,
    NgxPicaModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(
      environment.firebase
    ),
    AngularFireMessagingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCardModule,
    NgxSpinnerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatListModule,
    MatStepperModule,
    MatChipsModule,
    MatTabsModule,
    MatDividerModule,
    MatDatepickerModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
  ],
  providers: [UserService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  },
    DeliveryService,
    FileService,
    AngularFireMessaging,
    SessionService,
    { provide: LOCALE_ID, useValue: "fr-FR" },
    MessageService,
    SseService,
    ContactService,
    MapsService,
    ProductService
  ],
  entryComponents: [LoginModalComponent, SignUpModalComponent, ModalPinValidationComponent, ShowImageModalComponent, PlanningDialogComponent, NeedDocDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
