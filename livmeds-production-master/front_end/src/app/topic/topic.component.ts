import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogService } from '../blog.service';
import { Validators, FormBuilder } from '@angular/forms';
import { FileService } from '../file.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {
  public topic;
  public messageFormGroup;
  public categories;


  constructor(private route: ActivatedRoute, private router: Router,
    private blogService: BlogService,
    public fileService: FileService) {
    this.messageFormGroup = new FormBuilder().group({
      content: ['', Validators.compose([Validators.required])]
    });
  }

  async ngOnInit() {
    this.topic = { author: {}, messages: [] , category: {}};
    this.categories = [];
    this.loadScripts();
    this.route.queryParams
      .subscribe(async params => {
        try {
          this.topic = await this.blogService.getTopic(params.id);
        } catch (error) {
          this.router.navigate(['/blog']);
        }
      });

    this.categories = await this.blogService.getCategories();
  }

  async submit() {
    if (this.messageFormGroup.valid) {
      await this.blogService.createMessage(this.topic._id, this.messageFormGroup.value.content);
      await this.refreshCommentList();
      this.messageFormGroup.reset();
    }
  }

  async refreshCommentList() {
    this.topic.messages = (await this.blogService.getMessages(this.topic._id))['data'];
  }

  private loadScripts() {
  }

  ngOnDestroy() {
  }

  setSelectedCategory(category){
    this.router.navigate(['/blog'], {queryParams: { catId: category._id, catTitle: category.title }})
  }

  setAllCategory(){
    this.router.navigate(['/blog'], {queryParams: { category: 'ALL' }})
  }

  setSearchText(event){
    this.router.navigate(['/blog'], {queryParams: { text: event.target.value }})
  }
}
