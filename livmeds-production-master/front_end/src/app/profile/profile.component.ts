import { Component, OnInit } from '@angular/core';
import { SessionService } from '../session.service';
import { UserRole } from '../enums/UserRole';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  readonly USER_ROLE = UserRole;
  constructor(public session: SessionService) { }

  ngOnInit() {
  }

}
