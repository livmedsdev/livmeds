import { Component, OnInit } from '@angular/core';
import { Topic } from '../models/Topic';
import { FormBuilder, Validators } from '@angular/forms';
import { BlogService } from '../blog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-topic',
  templateUrl: './add-topic.component.html',
  styleUrls: ['./add-topic.component.scss']
})
export class AddTopicComponent implements OnInit {
  public topicFormGroup;
  public categories;

  
  constructor(private blogService: BlogService, private router: Router) { }

  async ngOnInit() {
    this.categories = [];
    this.topicFormGroup = new FormBuilder().group({
      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      category: [null, Validators.compose([Validators.required])]
    });
    this.categories = await this.blogService.getCategories();
  }

  async submit(){
    if (this.topicFormGroup.valid) {
      try{
        const newTopic : any = await this.blogService.createTopic(this.topicFormGroup.value as Topic);
        this.router.navigate(['/blog'], {});
      }catch(error){
        console.log(error);
      }
    }
  }
}
