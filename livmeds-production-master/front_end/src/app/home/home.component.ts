import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../user.service';
import { NotificationService } from '../notification.service';
import { UserRole } from '../enums/UserRole';
import { FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../contact.service';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../session.service';
import { Router } from '@angular/router';
import { LoginModalComponent } from '../login-modal/login-modal.component';
import { MatDialog } from '@angular/material';

declare var $: any;
declare var particlesJS;

declare var Swiper;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  public user: User = {} as User;
  private isConnected;
  private title;
  public error;
  public errorRegister;
  public confirmPassword;
  public messageFormGroup;
  public isSafari = (navigator.userAgent.indexOf("Safari") > -1 
                      && navigator.userAgent.indexOf("Chrome") === -1
                      && navigator.userAgent.indexOf("Chromium") === -1
                      && navigator.userAgent.indexOf("Firefox") === -1);
  
  constructor(public userService: UserService, 
	private contactService: ContactService,
	private toastService: ToasterService,
	private translate: TranslateService,
    public session: SessionService,
	private router: Router,
    public dialog: MatDialog,
	private notificationService: NotificationService) {

    this.messageFormGroup = new FormBuilder().group({
		name: ['', Validators.compose([Validators.required])],
		mail: ['', Validators.compose([Validators.required])],
		subject: [null, Validators.compose([Validators.required])],
		message: [null, Validators.compose([Validators.required])]
	  });
   }

   async submitContact(){
	   try{
		let response : any = await this.contactService.createMessage(this.messageFormGroup.value);
		this.toastService.pop('success', 'Succès', await this.translate.get(response.tradCode).toPromise());
	   }catch(error){
		   console.log(error);
		this.toastService.pop('error', 'Erreur', 'Erreur lors de l\'envoi de votre message');
	   }
   }

  ngOnInit() {
    if((window as any).fbq) 
	{
		(window as any).fbq('trackCustom', 'Accueil');
	}
    this.loadScripts();
    this.loadJS();
    this.checkIsConnected();
    console.log(this.notificationService.currentMessage);
  }

  async submitLogin() {
    try {
      await this.userService.login(this.user);
      this.checkIsConnected();
      $('.modal').modal('hide');
      this.error = undefined;
      
    } catch (error) {
      console.log(error)
      this.error = error.error.message;
    }
  }

  async submitRegister() {
    try {
      this.user.role = (this.user.becomeDeliverer)? UserRole.ROLE_DELIVERER : UserRole.ROLE_CUSTOMER;
      await this.userService.register(this.user);
      this.checkIsConnected();
      $('.modal').modal('hide');
      this.errorRegister = undefined;
    } catch (error) {
      console.log(error)
      this.errorRegister = error.error.message;
    }
  }

  logout() {
    this.isConnected = false;
    localStorage.clear();
    this.checkIsConnected();
  }


  checkIsConnected() {
    this.isConnected = localStorage.getItem("token") !== null;
    this.title = (this.isConnected) ? 'Welcome ' + localStorage.getItem('name') : 'On vous livre vos médicaments';
  }

  loadJS() {
    
		var mySwiper = new Swiper ('.app-screenshot-slider-2', {		
			slidesPerView: 3.2,
			centeredSlides: true,
			loop: true,
        	effect: 'coverflow',
        	spaceBetween: 0,
			speed: 700,
			grabCursor: true,
			autoplay: {
			    delay: 2500,
			},
			pagination: {
			    el: '.app-screenshot-slider-2 .swiper-pagination',
			    clickable: true
			},
			coverflowEffect: {
	            rotate: 0,
	            stretch: 71,
	            depth: 160,
	            modifier: 1,
	            slideShadows : false
	        },
	        breakpoints: {
	        	1199: {
    				coverflowEffect: {
    		            stretch: 57
    		        }
	        	},
	        	992: {
    				coverflowEffect: {
    		            stretch: 50
    		        }
	        	},
	        	768: {
    				coverflowEffect: {
    		            stretch: 36
    		        }
	        	},
	        	575: {
	        		slidesPerView: 2.5,
    				coverflowEffect: {
    		            stretch: 18
    		        }
	        	},
	        	370: {
	        		slidesPerView: 2.5,
    				coverflowEffect: {
    		            stretch: 10
    		        }
	        	}
	        }
    })
    
		var mySwiper = new Swiper ('.app-screenshot-slider-3', {		
			slidesPerView: 5,
			centeredSlides: true,
			loop: true,
        	spaceBetween: 38,
			speed: 700,
			autoplay: {
			    delay: 2500,
			},
			pagination: {
			    el: '.app-screenshot-slider-3 .swiper-pagination',
			    clickable: true
			},
	        breakpoints: {
	        	1199: {
	        		slidesPerView: 4
	        	},
	        	992: {
	        		slidesPerView: 3
	        	},
	        	768: {
	        		slidesPerView: 3
	        	},
	        	575: {
	        		slidesPerView: 2,
	        		centeredSlides: false,
	        		spaceBetween: 20
	        	}
	        }
    })
    
		var mySwiper = new Swiper ('.testimonial-slider', {
			loop: true,
			slidesPerView: 3,
			spaceBetween: 30,
			speed: 700,
			autoplay: {
			    delay: 4000,
			},
			pagination: {
			    el: '.testimonial-slider .swiper-pagination',
			    clickable: true
			},
			navigation: {
		        nextEl: '.testimonial-btn-next',
		        prevEl: '.testimonial-btn-prev',
		    },
			breakpoints: {
			    767: {
			      slidesPerView: 1
			    },
			    992: {
			      	slidesPerView: 2
			    }
			}
    })
    $('a[data-rel^=lightbox-popup]').lightcase({
		swipe: true
	});
		var mySwiper = new Swiper ('.testimonial-one-item-slider', {
			loop: true,
			slidesPerView: 1,
			spaceBetween: 30,
			speed: 700,
			autoplay: {
			    delay: 4000,
			},
			pagination: {
			    el: '.testimonial-one-item-slider .swiper-pagination',
			    clickable: true
			},
			navigation: {
		        nextEl: '.testimonial-btn-next',
		        prevEl: '.testimonial-btn-prev',
		    }
    })
    
		var mySwiper = new Swiper ('.testimonial-slider-2-item', {
			loop: true,
			slidesPerView: 2,
			spaceBetween: 50,
			speed: 700,
			autoplay: {
			    delay: 4000,
			},
			pagination: {
			    el: '.testimonial-slider-2-item .swiper-pagination',
			    clickable: true
			},
			navigation: {
		        nextEl: '.testimonial-btn-next',
		        prevEl: '.testimonial-btn-prev',
		    },
			breakpoints: {
			    767: {
			      slidesPerView: 1
			    }
			}
    })
    var mySwiper = new Swiper ('.partners-slider', {
			loop: true,
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 30,
			loopFillGroupWithBlank: true,
			speed: 900,
			autoplay: {
			    delay: 4000,
			},
			pagination: {
			    el: '.partners-slider .swiper-pagination',
			    clickable: true
			},
			breakpoints: {
			    570: {
			      slidesPerView: 2,
			      slidesPerGroup: 2
			    },
			    767: {
			      	slidesPerView: 3,
					slidesPerGroup: 3
			    },
			    992: {
			      	slidesPerView: 3,
					slidesPerGroup: 3
			    }
			}
    })
    
		var mySwiper = new Swiper ('.app-screenshot-slider', {
			slidesPerView: 4,
			spaceBetween: 30,
			speed: 700,
			autoplay: {
			    delay: 2500,
			},
			pagination: {
			    el: '.app-screenshot-slider .swiper-pagination',
			    clickable: true
			},
			breakpoints: {
			    570: {
			      slidesPerView: 1
			    },
			    767: {
			      slidesPerView: 2
			    },
			    992: {
			      	slidesPerView: 3
			    }
			}
    })
    
		var mySwiper = new Swiper ('.iconic-main-slider', {
			slidesPerView: 1,
			loop: true,
			spaceBetween: 30,
			speed: 1500,
			autoplay: {
			    delay: 4000,
			},
			pagination: {
			    el: '.iconic-main-slider .swiper-pagination',
			    clickable: true
			}
    })
    
		if($('#particles').length > 0 && !this.isSafari){
			particlesJS("particles", {
			  "particles": {
			    "number": {
			      "value": 100,
			      "density": {
			        "enable": true,
			        "value_area": 1443.0708547789707
			      }
			    },
			    "color": {
			      "value": "#ffffff"
			    },
			    "shape": {
			      "type": "circle",
			      "stroke": {
			        "width": 0,
			        "color": "#000000"
			      },
			      "polygon": {
			        "nb_sides": 5
			      },
			      "image": {
			        "src": "img/github.svg",
			        "width": 100,
			        "height": 100
			      }
			    },
			    "opacity": {
			      "value": 0,
			      "random": true,
			      "anim": {
			        "enable": false,
			        "speed": 1,
			        "opacity_min": 0.1,
			        "sync": false
			      }
			    },
			    "size": {
			      "value": 3,
			      "random": true,
			      "anim": {
			        "enable": false,
			        "speed": 40,
			        "size_min": 0.1,
			        "sync": false
			      }
			    },
			    "line_linked": {
			      "enable": true,
			      "distance": 212,
			      "color": "#ffffff",
			      "opacity": 0.30464829156444934,
			      "width": 2
			    },
			    "move": {
			      "enable": true,
			      "speed": 8.017060304327615,
			      "direction": "none",
			      "random": false,
			      "straight": false,
			      "out_mode": "out",
			      "bounce": false,
			      "attract": {
			        "enable": false,
			        "rotateX": 641.3648243462092,
			        "rotateY": 1200
			      }
			    }
			  },
			  "interactivity": {
			    "detect_on": "canvas",
			    "events": {
			      "onhover": {
			        "enable": true,
			        "mode": "grab"
			      },
			      "onclick": {
			        "enable": true,
			        "mode": "repulse"
			      },
			      "resize": true
			    },
			    "modes": {
			      "grab": {
			        "distance": 400,
			        "line_linked": {
			          "opacity": 1
			        }
			      },
			      "bubble": {
			        "distance": 400,
			        "size": 40,
			        "duration": 2,
			        "opacity": 8,
			        "speed": 3
			      },
			      "repulse": {
			        "distance": 200,
			        "duration": 0.4
			      },
			      "push": {
			        "particles_nb": 4
			      },
			      "remove": {
			        "particles_nb": 2
			      }
			    }
			  },
			  "retina_detect": true
			});
		}
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    // this.dynamicScriptLoader.load(['swiper']).then(data => {
    //   // Script Loaded Successfully
    // }).catch(error => console.log(error));
  }

  ngOnDestroy(){
    // this.dynamicScriptLoader.unload(['parralax']);
  }

  doCommand(){
	  if(this.session.currentUser() === null){
		const dialogRef = this.dialog.open(LoginModalComponent, {
			width: 'auto',
			height: 'auto',
			data: {
				redirectUrl : '/command'
			}
		  });
	  
		  dialogRef.afterClosed().subscribe(async result => {
			window.scroll(0,0);
		  });
	  }else{
		  this.router.navigate(['/command']);
	  }
  }
}
