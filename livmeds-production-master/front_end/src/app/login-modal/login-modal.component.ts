import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../app.component';
import { UserService } from '../user.service';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../enums/UserRole';
import { Router } from '@angular/router';
import { SessionService } from '../session.service';
import { EventPollingService } from '../event-polling.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {
  public user: any = {};
  public showForgetPassword = false;
  error;
  constructor(
    public toasterService: ToasterService,
    public translate: TranslateService,
    public router: Router,
    public userService: UserService,
    private eventPolling: EventPollingService,
    public session: SessionService,
    public dialogRef: MatDialogRef<LoginModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    if (!this.data.redirectUrl) {
      this.data.redirectUrl = '/command';
    }
    (window as any).fbq('trackCustom', 'Connexion');
  }

  async submit() {
    if (this.user) {
      try {
        await this.userService.login(this.user);
        this.error = undefined;
        this.dialogRef.close(this.user);
        if (this.session.currentUser().status !== 'AWAITING_VALIDATION' && (this.session.currentUser().role === UserRole.ROLE_OPTICIAN
          || this.session.currentUser().role === UserRole.ROLE_VETERINARY
          || this.session.currentUser().role === UserRole.ROLE_PHARMACY)) {
          this.router.navigate(['/deliveries']);
          this.eventPolling.startRetrieveDeliveries();
        }
        else if (this.session.currentUser().status === 'AWAITING_VALIDATION' && (this.session.currentUser().role === UserRole.ROLE_OPTICIAN
          || this.session.currentUser().role === UserRole.ROLE_VETERINARY
          || this.session.currentUser().role === UserRole.ROLE_PHARMACY)) {
          this.router.navigate(['/documents']);
          this.eventPolling.startRetrieveDeliveries();
        } else if (this.session.currentUser().role === UserRole.ROLE_CUSTOMER) {
          this.router.navigate([this.data.redirectUrl]);
        }
      } catch (error) {
        console.log(error)
        this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
      }
    }
  }

  displayForgetPassword() {
    this.showForgetPassword = true;
  }

  async sendForgetPassword() {
    try {
      const response: any = await this.userService.forgetPassword(this.user.mail);
      this.toasterService.pop('success', 'Demande envoyée', await this.translate.get(response.tradCode).toPromise());
    } catch (error) {
      console.log(error);
      this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
    }
  }

  redirectToSubscribe() {
    this.router.navigate(['/sign-up-customer'], {
      queryParams: {
        redirectUrl: this.data.redirectUrl
      }
    })
    this.dialogRef.close();
  }
}
