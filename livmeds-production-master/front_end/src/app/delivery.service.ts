import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  constructor(private http: HttpClient) { }

  async getMyDeliveries(params){ 
    return await this.http.get(environment.url + "/customer/deliveries", { params: params}).toPromise();
  }

  async getDeliveriesRequested(){
    return await this.http.get(environment.url + "/merchant/deliveries").toPromise();
  }

  async get(id){
    return await this.http.get(environment.url + "/merchant/delivery/" + id).toPromise();
  }

  async validDelivery(deliveryId, params){
    return await this.http.put(environment.url + "/merchant/delivery/valid/" + deliveryId, params).toPromise();
  }

  async givePackageToDeliverer(deliveryId){
    return await this.http.put(environment.url + "/merchant/delivery/give/" + deliveryId, null).toPromise();
  }

  async refuseDelivery(deliveryId){
    return await this.http.put(environment.url + "/merchant/delivery/refuse/" + deliveryId, null).toPromise();
  }

  async create(delivery){
    const formData = new FormData();
    for(let key of Object.keys(delivery)){
      formData.append(key, JSON.stringify(delivery[key]));
    }

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.post(environment.url + "/merchant/delivery",
      formData, { headers: headers }).toPromise();
  }

  async listShops(filter){
    return await this.http.get(environment.url + "/shops", {params: filter}).toPromise();
  }


  async createByCustomer(delivery){
    const formData = new FormData();
    
    if(delivery.image){
      for(let image of delivery.image){
        let res = await fetch(image);
        let blob = await res.blob();
    
        formData.append('file', blob, "order.jpg");
      }
      delete delivery.image;
    }
    for(let key of Object.keys(delivery)){
      formData.append(key, JSON.stringify(delivery[key]));
    }

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.post(environment.url + '/customer/delivery',
      formData, { headers: headers }).toPromise();
  }

  async cancelDelivery(idDelivery){
    return await this.http.put(environment.url + "/customer/delivery/cancel/"+idDelivery, null).toPromise();
  }

  async acceptAmountForDelivery(idDelivery){
    return await this.http.put(environment.url + "/customer/delivery/valid/"+idDelivery, null).toPromise();
  }

  async refuseAmountForDelivery(idDelivery){
    return await this.http.put(environment.url + "/customer/delivery/refuse/"+idDelivery, null).toPromise();
  }
  
  async getDeliveryByIdWithCustomer(id){
    return await this.http.get(environment.url + "/customer/delivery/"+ id).toPromise();
  }

  async deliveryReceive(id, received){
    return await this.http.put(environment.url + "/customer/delivery/received/" + id, {received: received}).toPromise();
  }

  async trackDelivery(id){
    return await this.http.get(environment.url + "/customer/track/"+ id).toPromise();
  }
}
