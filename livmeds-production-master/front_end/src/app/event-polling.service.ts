import { Injectable, EventEmitter } from '@angular/core';
import { MessageService } from './message.service';
import { DeliveryService } from './delivery.service';
import { FileService } from './file.service';
import { ToasterService } from 'angular2-toaster';
import { DeliveryStatus } from './enums/DeliveryStatus';

@Injectable({
  providedIn: 'root'
})
export class EventPollingService {

  private intervals = {};
  public sendChanges = new EventEmitter();


  public intervalRetrieveDeliveries;
  public eventDeliveriesChanged = new EventEmitter();
  myDeliveries: any = [];
  private deliveriesBack: any = [];
  asyncIntervals : any;
  runAsyncInterval;
  setAsyncInterval;
  clearAsyncInterval;
  firstRefresh = true;

  constructor(
    private toasterService: ToasterService,
    private deliveryService: DeliveryService,
    private fileService: FileService,
    private messageService: MessageService) {

    this.asyncIntervals = [];

    this.runAsyncInterval = async (cb, interval, intervalIndex) => {
      await cb();
      if (this.asyncIntervals[intervalIndex]) {
        setTimeout(() => this.runAsyncInterval(cb, interval, intervalIndex), interval);
      }
    };

    this.setAsyncInterval = (cb, interval) => {
      if (cb && typeof cb === "function") {
        const intervalIndex = this.asyncIntervals.length;
        this.asyncIntervals.push(true);
        this.runAsyncInterval(cb, interval, intervalIndex);
        return intervalIndex;
      } else {
        throw new Error('Callback must be a function');
      }
    };

    this.clearAsyncInterval = (intervalIndex) => {
      if (this.asyncIntervals[intervalIndex]) {
        this.asyncIntervals[intervalIndex] = false;
      }
    };
  }

  startRetrieveDeliveries() {
    if (!this.intervalRetrieveDeliveries) {
      this.intervalRetrieveDeliveries = this.setAsyncInterval(async () => {
        try{
          await this.refreshListDeliveries();
        }catch(error){}
      }, 1000);
    }
  }

  async refreshListDeliveries() {
    let deliveries: any = await this.deliveryService.getDeliveriesRequested();

    const result = this.getChanges(this.deliveriesBack, deliveries);
    if (result !== false || result === 'NEW') {
      this.deliveriesBack = [];

      for (let delivery of deliveries) {
        this.deliveriesBack.push(Object.assign({}, delivery));
        if(delivery.image){
          if(Array.isArray(delivery.image) && delivery.image.length > 0){
            delivery.urlCertification = await this.fileService.getBase64Image(delivery.image[0])
          }else if(!Array.isArray(delivery.image))  {
            delivery.urlCertification = await this.fileService.getBase64Image(delivery.image)
          }else {
            delivery.urlCertification = undefined
          }
        }
        delivery.statusClass = this.statusClass(delivery.status);
      }

      this.myDeliveries = deliveries;
      if (result === 'NEW' && !this.firstRefresh) {
        this.updatePolling();
        this.toasterService.pop('success', 'Nouvelle demande', 'Vous avez reçu une nouvelle demande');
      }else if(this.firstRefresh){
        this.firstRefresh = false;
        this.updatePolling();
      }
    }
  }


  updatePolling() {
    for (let delivery of this.myDeliveries) {
      if ([DeliveryStatus.AWAITING_VALIDATION,
      DeliveryStatus.AWAITING_VALIDATION_AMOUNT,
      DeliveryStatus.VALID].includes(delivery.status)) {
        if (!this.alreadyExist(delivery._id, 'TO_PHARMACY')) {
          this.startPollingMsg(delivery._id, 'TO_PHARMACY');
        }
      } else {
        this.stopPollingMsg(delivery._id, 'TO_PHARMACY');
      }
    }
  }

  getChanges(oldArray, newArray) {
    let changes, i, item, j, len;

    if (JSON.stringify(oldArray) === JSON.stringify(newArray)) {
      return false;
    }

    if (oldArray.length < newArray.length) {
      debugger
      return 'NEW';
    }

    changes = [];
    for (i = j = 0, len = newArray.length; j < len; i = ++j) {
      item = newArray[i];
      if (JSON.stringify(item) !== JSON.stringify(oldArray[i])) {
        changes.push(item);
      }
    }
    return changes;
  };


  statusClass(status) {
    switch (status) {
      case 'IN_DELIVERY':
        return "blue-color";
      case 'DELIVERED':
      case 'VALID':
        return "green";
      case 'REFUSED_AMOUNT':
      case 'REFUSED':
        return "red-span";
      case 'AWAITING_VALIDATION_AMOUNT':
      case 'AWAITING_VALIDATION':
        return "yellow";
    }
    return "";
  }

  stopRetrieveDeliveries() {
    clearInterval(this.intervalRetrieveDeliveries);
    this.intervalRetrieveDeliveries = undefined;
  }

  startPollingMsg(deliveryId, typePrivateMessage) {
    const key = deliveryId + '-' + typePrivateMessage;
    this.intervals[key] = {
      firstPolling: true,
    };

    this.intervals[key].interval = setInterval(async () => {
      const privateMessages = (await this.messageService.getPrivateMessages(deliveryId, typePrivateMessage))['data'];
      if (!this.intervals[key].firstPolling) {
        const nbMessages = privateMessages.length;
        if (nbMessages > this.intervals[key].nbMessages) {
          this.sendChanges.emit({ deliveryId, typePrivateMessage, newMessages: nbMessages - this.intervals[key].nbMessages });
        }
      } else {
        this.intervals[key].firstPolling = false;
      }
      this.intervals[key].nbMessages = privateMessages.length;

    }, 1000);
  }

  stopPollingMsg(deliveryId, typePrivateMessage) {
    const key = deliveryId + '-' + typePrivateMessage;
    if (this.intervals[key]) {
      clearInterval(this.intervals[key].interval);
      delete this.intervals[key];
    }
  }

  alreadyExist(deliveryId, typePrivateMessage) {
    const key = deliveryId + '-' + typePrivateMessage;
    return this.intervals[key];
  }


  stopAllPolling() {
    for (let key of Object.keys(this.intervals)) {
      clearInterval(this.intervals[key].interval);
      delete this.intervals[key];
    }
  }

  startPolling(key, callback){
    this.intervals[key] = {
      firstPolling: true,
    };

    this.intervals[key].interval = setInterval(callback, 30000);
  }

  stopPolling(key) {
    if (this.intervals[key]) {
      clearInterval(this.intervals[key].interval);
      delete this.intervals[key];
    }
  }
}
