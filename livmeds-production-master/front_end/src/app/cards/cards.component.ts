import { Component, OnInit } from '@angular/core';
import { CardService } from '../card.service';
import { UserService } from '../user.service';
import { SessionService } from '../session.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../enums/UserRole';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
  public cards = [];

  constructor(
    private cardService: CardService,
    public session: SessionService) { }

  async ngOnInit() {
    this.cards = (await this.cardService.list())['data'];
  }

  async deleteCard(cardId){
    await this.cardService.delete(cardId)
    this.cards = (await this.cardService.list())['data'];
  }
}
