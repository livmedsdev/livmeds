import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { SseService } from '../sse.service';
import { MessageService } from '../message.service';
import { environment } from 'src/environments/environment';
import { UserRole } from '../enums/UserRole';
import { FileService } from '../file.service';

@Component({
  selector: 'app-tchat',
  templateUrl: './tchat.component.html',
  styleUrls: ['./tchat.component.scss']
})
export class TchatComponent implements OnInit, AfterViewInit {
  readonly USER_ROLE = UserRole;
  @Input() hideCloseBtn = false;
  @Input() minimized = false;
  @Input() delivery;
  @Output() onClose = new EventEmitter<any>();
  @Output() onMinimized = new EventEmitter<any>();
  public privateMessages = [];
  private privateMessagesBack = [];
  public message: string;
  private sub;
  public environment;
  public historyDiv;
  public typePrivateMessage;
  private polling;
  public count = 0;

  constructor(private fileService: FileService,
    private messageService: MessageService) {
    this.environment = environment;
  }

  async ngOnInit() {
    this.typePrivateMessage = 'TO_PHARMACY';
    this.historyDiv = document.getElementById("history");
    console.log('init');
    await this.refreshList();
    this.enablePolling();
    this.message = '';
    this.resetCount();
  }

  resetCount(){
    this.count = 0;
  }

  ngAfterViewInit(){
    this.historyDiv.scrollTop = this.historyDiv.scrollHeight;
  }

  async refreshList(){
    const privateMessages = (await this.messageService.getPrivateMessages(this.delivery._id, this.typePrivateMessage))['data'];
    if(this.getChanges(this.privateMessagesBack, privateMessages) !== false){
      this.privateMessagesBack = [];
      for(let message of privateMessages){
        this.privateMessagesBack.push(Object.assign({}, message));
        message.avatarUrl = (message.author.avatar)?await this.fileService.getBase64Image(message.author.avatar):undefined;;
      }
      this.privateMessages = privateMessages;
    }
  }


  enablePolling(){
    this.polling = setInterval(() =>{
      this.refreshList();
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.polling);
  }

  async submit(event) {
    if (event.keyCode == 13) {
      this.resetCount();
      try {
        await this.messageService.create({
          delivery: this.delivery._id,
          message: this.message,
          typePrivateMessage: this.typePrivateMessage
        });
        this.message = '';
        await this.refreshList();
        this.historyDiv.scrollTop = this.historyDiv.scrollHeight;
      } catch (error) {
        console.log(error);
      }
    }
  }

  close() {
    this.onClose.emit({});
  }

  getChanges(oldArray, newArray) {
    let changes, i, item, j, len;
    if (JSON.stringify(oldArray) === JSON.stringify(newArray)) {
      return false;
    }

    if(oldArray.length > 0 && oldArray.length < newArray.length){
      if(this.minimized){
        this.count++;
      }
      return 'NEW';
    }

    changes = [];
    for (i = j = 0, len = newArray.length; j < len; i = ++j) {
      item = newArray[i];
      if (JSON.stringify(item) !== JSON.stringify(oldArray[i])) {
        changes.push(item);
      }
    }
    return changes;
  }
  
  minimizedTchat(){
    this.minimized?this.minimized=false:this.minimized=true;
    if(!this.minimized){
      this.resetCount();
    }
    this.onMinimized.emit(this.minimized);
  }
}
