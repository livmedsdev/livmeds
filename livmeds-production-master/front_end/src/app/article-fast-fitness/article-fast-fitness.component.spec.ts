import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleFastFitnessComponent } from './article-fast-fitness.component';

describe('ArticleFastFitnessComponent', () => {
  let component: ArticleFastFitnessComponent;
  let fixture: ComponentFixture<ArticleFastFitnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleFastFitnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleFastFitnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
