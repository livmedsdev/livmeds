import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPinValidationComponent } from './modal-pin-validation.component';

describe('ModalPinValidationComponent', () => {
  let component: ModalPinValidationComponent;
  let fixture: ComponentFixture<ModalPinValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPinValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPinValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
