import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../user.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SignUpModalComponent } from '../sign-up-modal/sign-up-modal.component';
import { DialogData } from '../app.component';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SessionService } from '../session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-pin-validation',
  templateUrl: './modal-pin-validation.component.html',
  styleUrls: ['./modal-pin-validation.component.scss']
})
export class ModalPinValidationComponent implements OnInit {

  pinForm: FormGroup;

  constructor(
    private translate: TranslateService,
    private userService: UserService,
    private toasterService: ToasterService,
    private session: SessionService,
    private router: Router,
    public dialogRef: MatDialogRef<SignUpModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.pinForm = new FormBuilder().group({
      code: ['', Validators.compose([Validators.required])],
      signImage: ['', Validators.compose([Validators.required])],
      rib: ['', Validators.compose([Validators.required])],
      kbis: ['', Validators.compose([Validators.required])],
    });
    if (data.signImage) {
      this.pinForm.patchValue(
        data
      );
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

  async submit() {
    try {
      await this.userService.sendDocuments(this.pinForm.value);
      await this.userService.getProfile();

      this.toasterService.pop('success', 'Succès', await this.translate.get("Votre compte a été validé, vous pouvez maintenant utiliser pleinement l'application !").toPromise());
      this.router.navigateByUrl('/deliveries')
      this.dialogRef.close();
    } catch (error) {
      console.log(error);
      this.toasterService.pop('error', 'Erreur', 'Votre compte n\'a pas été validé');
    }
  }

}
