import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { User } from './models/User';
import { SessionService } from './session.service';
import { UserRole } from './enums/UserRole';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, public session: SessionService) { }

  async register(user: User) {
    let response = await this.http.post(environment.url + "/register", user).toPromise();
    localStorage.setItem("token", response['token']);
    await this.getProfile();
  }

  async login(user: User) {
    user.role = [UserRole.ROLE_CUSTOMER, UserRole.ROLE_PHARMACY, UserRole.ROLE_OPTICIAN, UserRole.ROLE_VETERINARY];
    let response = await this.http.post(environment.url + "/login", user).toPromise();
    localStorage.setItem("token", response['token']);
    await this.getProfile();
  }
  async getProfile() {
    const response = await this.http.get(environment.url + "/profile").toPromise();
    this.session.saveInSession(response['user']);
    return response['user'] as User;
  }


  async editSecurityInformation(params) {
    return await this.http.put(environment.url + "/security", params).toPromise();
  }

  async editMyProfile(params) {
    return await this.http.put(environment.url + "/profile", params).toPromise();
  }

  async editDocuments(data){
    const formData = new FormData();
    if(data.socialSecurityNumber && (typeof data.socialSecurityNumber === 'string' || data.socialSecurityNumber instanceof String) ){
      let res = await fetch(data.socialSecurityNumber);
      let blob = await res.blob();
  
      formData.append('socialSecurityNumber', blob, "socialSecurityNumber.jpg");
      delete data.socialSecurityNumber;
    }

    if(data.mutual && (typeof data.mutual === 'string' || data.mutual instanceof String) && (data.mutual as string).startsWith('data:image/jpeg;base64,')){
      let res = await fetch(data.mutual);
      let blob = await res.blob();
  
      formData.append('mutual', blob, "mutual.jpg");
      delete data.mutual;
    }
    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.put(environment.url + '/profile',
    this.objToForm(formData, data), { headers: headers }).toPromise();
  }

  objToForm(formData, data){
    for(let key of Object.keys(data)){
      if(['favoriteAddress'].includes(key)){
        formData.append(key, JSON.stringify(data[key]));
      } else if(['socialSecurityNumber'].includes(key) && data[key].name){
        formData.append(key, data[key], data[key].name);
      }else{
        formData.append(key, data[key]);
      }
    }
    return formData;
  }
  
  async changeAvatar(idAvatar) {
    return await this.http.put(environment.url + "/avatar", { avatar: idAvatar }).toPromise();
  }

  async updateOpeningTimes(body) {
    return await this.http.post(environment.url + "/merchant/openingTimes", body).toPromise();
  }

  getFavoriteAddress(addresses) {
    for (let address of addresses) {
      if (address.favoriteAddress) {
        return address;
      }
    }
  }

  async checkToken() {
    return await this.http.get(environment.url + '/checkToken').toPromise();
  }

  async forgetPassword(mail) {
    return await this.http.post(environment.url + "/lostPassword", {
      mail: mail
    }).toPromise();
  }


  async resetPassword(user, token, password) {
    return await this.http.post(environment.url + "/changePassword", {
      user: user,
      token: token,
      password: password
    }).toPromise();
  }

  async generateUrlContract() {
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/pdf');
    return await this.http.get(environment.url + "/user/contract", { headers: headers, responseType: 'blob' }).toPromise();
  }

  async sendDocuments(body){
    return await this.http.post(environment.url + "/documents", body).toPromise();
  }
  
  async updateDocuments(body){
    return await this.http.put(environment.url + "/documents", body).toPromise();
  }

  async sign(){
    return await this.http.put(environment.url + "/sign", {}).toPromise();
  }

  async getMyAddresses(filter) {
    return await this.http.get(environment.url + "/all-addresses", {params: filter}).toPromise();  
  }

  async addAddress(address) {
    return await this.http.post(environment.url + "/address", address).toPromise();
  }
  
  async getMyContract(){
    const response = (await this.http.get(environment.url + '/retrieve/contract').toPromise())['file'];
    return "data:application/pdf;base64," + response;
  }
}
