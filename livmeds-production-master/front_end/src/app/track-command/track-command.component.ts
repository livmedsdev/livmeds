import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { DeliveryService } from '../delivery.service';
import { EventPollingService } from '../event-polling.service';
import { MapsService } from '../maps.service';
import { UserService } from '../user.service';

declare var google;

@Component({
  selector: 'app-track-command',
  templateUrl: './track-command.component.html',
  styleUrls: ['./track-command.component.scss']
})
export class TrackCommandComponent implements OnInit {

  @ViewChild("commandTab") tbGroup: MatTabGroup;
  @ViewChild('mapTrack') mapElement: ElementRef;

  map: any;
  selfMarker = new google.maps.Marker({
    icon: "../assets/images/maps/logo_home.png",
    scaledSize: { width: 40, height: 40 },
    anchor: { x: 20, y: 20 },
  });

  merchantMarker = new google.maps.Marker({
    icon: {
      url: "../assets/images/maps/logo_pharma.png",
      scaledSize: { width: 40, height: 40 },
      anchor: { x: 20, y: 20 },
      label: 'merchant'
    }
  });

  delivererMarker  = new google.maps.Marker({
    icon: {
      url: "../assets/images/maps/logo_deliverer.png",
      scaledSize: { width: 40, height: 40 },
      anchor: { x: 20, y: 20 },
    }
  });

  delivery: any = {};
  keyInterval: string;
  messageOperation: string;

  constructor(private deliveryService: DeliveryService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private translate: TranslateService,
    private toasterService: ToasterService,
    private eventPolling: EventPollingService,
    private mapsService: MapsService) {

  }

  async ngOnInit() {
    await this.loadMap();

    this.spinner.show();
    const queryParams = this.route.snapshot.queryParams;
    try {
      this.delivery = await this.deliveryService.getDeliveryByIdWithCustomer(queryParams.command);
      this.delivery.favoriteAddress = this.userService.getFavoriteAddress(this.delivery.merchant.addresses);
      await this.updateDeliverer();
      this.keyInterval = 'customer_delivery_' + this.delivery._id
      this.eventPolling.startPolling(this.keyInterval, async() => {
        this.delivery = await this.deliveryService.getDeliveryByIdWithCustomer(queryParams.command);
        await this.updateDeliverer();
      });

      const customerPts = new google.maps.LatLng(this.delivery.address.latitude, this.delivery.address.longitude);
      this.updateMarker(this.selfMarker, customerPts);
      this.map.setCenter(customerPts);
      this.map.setZoom(13)

      const merchantAddress = this.userService.getFavoriteAddress(this.delivery.merchant.addresses);
      const pts = new google.maps.LatLng(merchantAddress.latitude, merchantAddress.longitude);
      this.updateMarker(this.merchantMarker, pts);
    } catch (error) {
      this.router.navigate(['/home'], {});
    }
    this.spinner.hide();
  }

  async loadMap() {
    try {
      let mapOptions = {
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        fullscreenControl: false
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.map.mapTypes.set('styled_map', (new Date().getHours() >= 20 || new Date().getHours() <= 8) ? this.mapsService.styles.dark : this.mapsService.styles.light);
      this.map.setMapTypeId('styled_map');

      this.map.setCenter(new google.maps.LatLng(46.6774234, 2.0960496))

    } catch (error) {
      console.log(error);
    }
  }

  ngOnDestroy(){
    this.eventPolling.stopPolling(this.keyInterval)
  }

  initMarker() {

  }

  updateMarker(marker, pts) {
    marker.setMap(this.map)
    marker.setPosition(pts)
  }

  updateProductContent() {

  }

  formatNumber(number) {
    return Number.parseFloat(number).toFixed(2);
  }

  goToContent() {
    if (!this.tbGroup || !(this.tbGroup instanceof MatTabGroup)) return;

    const tabCount = this.tbGroup._tabs.length;
    this.tbGroup.selectedIndex = (this.tbGroup.selectedIndex + 1) % tabCount;
  }

  async refuseTotalAmount() {
    this.spinner.show();
    try{
      await this.deliveryService.refuseAmountForDelivery(this.delivery._id);
      this.toasterService.pop('success', 'Succès', 'Votre refus a bien été pris en compte');
      this.spinner.hide();
    }catch(error){
      this.spinner.hide();
      console.log(error);
      this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
    }
  }

  async acceptTotalAmount() {
    this.spinner.show();
    try {
      this.logPurchase();
      await this.deliveryService.acceptAmountForDelivery(this.delivery._id);
      this.delivery = await this.deliveryService.getDeliveryByIdWithCustomer(this.delivery._id);
      this.checkOperations();
      this.toasterService.pop('success', 'Succès', 'Votre validation a bien été pris en compte');
      this.spinner.hide();
    } catch (error) {
      this.spinner.hide();
      console.log(error);
      this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
    }
  }

  checkOperations(){
    let inProgress = this.delivery.operations.filter(elem => elem.status === "IN_PROGRESS");
    let failed = this.delivery.operations.filter(elem => elem.status === "FAILED");
    if(inProgress.length > 0){
      this.messageOperation = "OPERATION_IN_PROGRESS"
    }else if(failed.length > 0){
      this.messageOperation = "OPERATION_FAILED";
    }else{
      this.messageOperation = undefined;
    }
  }

  logPurchase(){
    //todo
  }

  async updateDeliverer(){
    if(this.delivery.deliverer){ 
      const trackPos : any = await this.deliveryService.trackDelivery(this.delivery._id);
      if(trackPos.length > 0){
        const delivererPts = new google.maps.LatLng(trackPos[0].latitude, trackPos[0].longitude);
        this.updateMarker(this.delivererMarker, delivererPts);
        this.map.setCenter(delivererPts);
        this.map.setZoom(13)
      }
    }
  }

  async commandReceived(received){
    await this.deliveryService.deliveryReceive(this.delivery._id, received);
    this.delivery = await this.deliveryService.getDeliveryByIdWithCustomer(this.delivery._id);
    this.toasterService.pop('success', 'Succès', 'Votre retour a bien été pris en compte');
  }
}
