import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackCommandComponent } from './track-command.component';

describe('TrackCommandComponent', () => {
  let component: TrackCommandComponent;
  let fixture: ComponentFixture<TrackCommandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackCommandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackCommandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
