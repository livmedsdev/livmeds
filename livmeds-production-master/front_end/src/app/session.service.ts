import { Injectable } from '@angular/core';
import { User } from './models/User';
import { environment } from 'src/environments/environment';
import { FileService } from './file.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private readonly langKey = "lang"; 

  constructor(private fileService: FileService) { }

  currentUser(): User{
    if(!localStorage.getItem('token')){
      return null;
    }
    return {
      _id: localStorage.getItem('id'),
      name: localStorage.getItem('name'),
      role: localStorage.getItem('role'),
      mail: localStorage.getItem('mail'),
      avatar: localStorage.getItem('avatar'),
      status: localStorage.getItem('status'),
      lastName: localStorage.getItem('lastName'),
      phoneNumber: localStorage.getItem('phoneNumber'),
      addresses: (localStorage.getItem('addresses'))?JSON.parse(localStorage.getItem('addresses')):[],
      openingTime: (localStorage.getItem('openingTime'))?JSON.parse(localStorage.getItem('openingTime')):null,
      docSended: (localStorage.getItem('docSended') === 'true')
    } as User;
  }

  async saveInSession(data){
    localStorage.setItem("id", data._id);
    localStorage.setItem("name", data.name);
    localStorage.setItem("lastName", data.lastName);
    localStorage.setItem("role", data.role);
    localStorage.setItem("mail", data.mail);
    localStorage.setItem("status", data.status);
    localStorage.setItem("avatar", (data.avatar)? await this.fileService.getBase64Image(data.avatar):'undefined');
    localStorage.setItem("phoneNumber", data.phoneNumber);
    localStorage.setItem("addresses", JSON.stringify(data.addresses))
    localStorage.setItem("openingTime", (data.openingTime)?JSON.stringify(data.openingTime):null)
    localStorage.removeItem('docSended');
    localStorage.setItem('docSended', (data.socialSecurityNumber)?'true':'false')
  }
  
  setLang(value){
    localStorage.setItem(this.langKey, value);
  }

  setTokenFcm(value){
    localStorage.setItem('tokenFcm', value);
  }

  getTokenFcm(){
    return localStorage.getItem('tokenFcm');
  }

  setPrivateMessageReceived(delivery, typePrivateMessage, receiver){
    const label = 'nbMessage_' + delivery + '_' + typePrivateMessage + '_' + receiver;
    const nbMsg = (localStorage.getItem(label))?parseInt(localStorage.getItem(label)):0;
    localStorage.setItem(label, (nbMsg+1).toString());
  }

  getNbPrivateMessageReceived(delivery, typePrivateMessage){
    const label = 'nbMessage_' + delivery + '_' + typePrivateMessage + '_' + this.currentUser()._id;
    return (localStorage.getItem(label))?parseInt(localStorage.getItem(label)):0;
  }

  resetNbPrivateMessageReceived(delivery, typePrivateMessage){
    const label = 'nbMessage_' + delivery + '_' + typePrivateMessage + '_' + this.currentUser()._id;
    localStorage.removeItem(label);
  }

  getPreferences() {
    return {
      lang: localStorage.getItem(this.langKey)
    }
  }
}
