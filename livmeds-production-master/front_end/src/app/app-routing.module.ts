import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DeliveriesComponent } from './deliveries/deliveries.component';
import { BlogComponent } from './blog/blog.component';
import { AddTopicComponent } from './add-topic/add-topic.component';
import { TopicComponent } from './topic/topic.component';
import { FaqComponent } from './faq/faq.component';
import { ProfileComponent } from './profile/profile.component';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
import { ViewInvoiceComponent } from './view-invoice/view-invoice.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AddDeliveryComponent } from './add-delivery/add-delivery.component';
import { DeliveryBillComponent } from './delivery-bill/delivery-bill.component';
import { CardsComponent } from './cards/cards.component';
import { AddCardComponent } from './add-card/add-card.component';
import { DocumentsComponent } from './documents/documents.component';
import { CguComponent } from './cgu/cgu.component';
import { CreateDeliveryComponent } from './create-delivery/create-delivery.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LegalMentionComponent } from './legal-mention/legal-mention.component';
import { CommandComponent } from './command/command.component';
import { CommandsComponent } from './commands/commands.component';
import { TrackCommandComponent } from './track-command/track-command.component';
import { ArticleFastFitnessComponent} from './article-fast-fitness/article-fast-fitness.component';
import { DevenirLivreurComponent} from './devenir-livreur/devenir-livreur.component';
import { ContactComponent} from './contact/contact.component';
import { AproposComponent} from './apropos/apropos.component';
import { SignUpCustomerComponent} from './sign-up-customer/sign-up-customer.component';
import { DeliveriesHistoricalComponent } from './deliveries-historical/deliveries-historical.component';
import { CommandHistoricalComponent } from './command-historical/command-historical.component';
const routes: Routes = [{
  path: 'home',
  component: HomeComponent
  },
  { path: 'command-historical',component: CommandHistoricalComponent},
  { path: 'deliveries-historical', component: DeliveriesHistoricalComponent},
  { path: 'sign-up-customer', component:SignUpCustomerComponent},
  { path: 'a-propos', component:AproposComponent},
  { path: 'contact', component:ContactComponent},
  { path: 'devenir-livreur', component:DevenirLivreurComponent},
  { path: 'blog/articleFastFitness', component:ArticleFastFitnessComponent},
  { path: 'deliveries', component: DeliveriesComponent},
  { path: 'blog', component: BlogComponent, children:[]},
  { path: 'add-topic', component: AddTopicComponent},
  { path: 'topic', component: TopicComponent},
  { path: 'sign-up', component: SignUpComponent},
  { path: 'faq', component: FaqComponent},
  { path: 'profile', component: ProfileComponent},
  { path: 'delivery-invoice', component: CreateInvoiceComponent},
  { path: 'view-invoice', component: ViewInvoiceComponent},
  { path: 'reset-password', component: ResetPasswordComponent},
  { path: 'create-delivery-btob', component: AddDeliveryComponent},
  { path: 'cards', component: CardsComponent},
  { path: 'add-card', component: AddCardComponent},
  { path: 'documents', component: DocumentsComponent},
  { path: 'delivery-bill', component: DeliveryBillComponent},
  { path: 'create-delivery-customer', component: CreateDeliveryComponent},
  { path: 'cgu', component: CguComponent},
  { path: 'mentions-legales', component: LegalMentionComponent},
  { path: 'sign-up-customer', component: SignUpCustomerComponent},
  { path: 'command', component: CommandComponent},
  { path: 'commands', component: CommandsComponent},
  { path: 'track', component: TrackCommandComponent},
  { path: '', redirectTo:"/home", pathMatch:"full"},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
