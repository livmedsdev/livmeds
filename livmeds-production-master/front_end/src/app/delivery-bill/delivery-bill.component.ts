import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { environment } from 'src/environments/environment';
import { FileService } from '../file.service';
import { MatDialog } from '@angular/material';
import { ShowImageModalComponent } from '../show-image-modal/show-image-modal.component';

@Component({
  selector: 'app-delivery-bill',
  templateUrl: './delivery-bill.component.html',
  styleUrls: ['./delivery-bill.component.scss']
})
export class DeliveryBillComponent implements OnInit {
  delivery;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private fileService: FileService,
    private deliveryService: DeliveryService) { }

  async ngOnInit() {
    const queryParams = this.route.snapshot.queryParams;
    this.delivery = await this.deliveryService.get(queryParams.idDelivery);
    this.delivery.urlTagLine = (this.delivery.tagLine) ? await this.fileService.getBase64Image(this.delivery.tagLine) : undefined;
  }
}
