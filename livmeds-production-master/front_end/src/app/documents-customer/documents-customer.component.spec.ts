import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsCustomerComponent } from './documents-customer.component';

describe('DocumentsCustomerComponent', () => {
  let component: DocumentsCustomerComponent;
  let fixture: ComponentFixture<DocumentsCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
