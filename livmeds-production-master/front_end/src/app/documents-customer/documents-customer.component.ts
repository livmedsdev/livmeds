import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { FileService } from '../file.service';
import { SessionService } from '../session.service';
import { UserService } from '../user.service';
import { NgxPicaService } from '@digitalascetic/ngx-pica';

@Component({
  selector: 'app-documents-customer',
  templateUrl: './documents-customer.component.html',
  styleUrls: ['./documents-customer.component.scss']
})
export class DocumentsCustomerComponent implements OnInit {
  public profileFormGroup;
  public mutualUrl;
  public socialSecurityNumberUrl

  constructor(private spinner: NgxSpinnerService,
    public session: SessionService,
    private toasterService: ToasterService,
    private translate: TranslateService, 
    private _ngxPicaService: NgxPicaService,
    private fileService: FileService,
    public userService: UserService) { }

  async ngOnInit() {
    this.profileFormGroup = new FormBuilder().group({
      socialSecurityNumber: ['', Validators.compose([])],
      mutual: ['', Validators.compose([])],
    });

    this.spinner.show();
    const response = await this.userService.getProfile();
    for (let property in this.profileFormGroup.value) {
      if (response[property]) {
        console.log(property)
        this[property + 'Url'] = await this.fileService.getBase64Image(response[property]);
      }
    }
    this.spinner.hide();
  }

  async changeFile(event, field) {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      try {
        this.spinner.show();

        // Resized
        const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 600, 600, {
          exifOptions:{
            forceExifOrientation: true
          },
          aspectRatio: {
            keepAspectRatio: true
          }
        }).toPromise();

        let params = {};
        params[field] = imageResized;


        this.profileFormGroup.patchValue(params);
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          this[field + 'Url'] = myReader.result;
          this.spinner.hide();
        }
        myReader.readAsDataURL(imageResized);
      } catch (error) {
        console.log(error);
        this.spinner.hide();
      }
    }
  }

  async update() {
    try {
      this.spinner.show();
      await this.userService.editDocuments(this.profileFormGroup.value);
      await this.userService.getProfile();
      this.toasterService.pop('success', 'Succès', this.translate.instant("SUCCESS_UPDATE_DOCUMENTS"));
      this.spinner.hide();
    } catch (error) {
      console.log(error)
      this.toasterService.pop('error', 'Erreur', this.translate.instant(error.error.tradCode));
      this.spinner.hide();
    }
  }
}
