import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private http: HttpClient) { }

  async askAddCardIntent(){
    return await this.http.get(environment.url + '/add-card-intent').toPromise();
  }

  async list(){ 
    return await this.http.get(environment.url + "/cards").toPromise();
  }

  async setDefaultCard(cardId){
    return await this.http.put(environment.url + "/defaultCard/"+cardId, {}).toPromise();
  }

  async delete(cardId){
    return await this.http.delete(environment.url + "/card/"+cardId, {}).toPromise();
  }
}
