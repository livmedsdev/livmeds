import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  public topics: any[];
  private page: Number;
  private limit: Number;
  private nbTotal: Number;
  public selectedCategory;
  public searchText;
  public categories: Array<any>;
  
  constructor(private route: ActivatedRoute,private router: Router,private blogService: BlogService) {
  }

  async ngOnInit() {
    this.selectedCategory = null;

    const queryParams = this.route.snapshot.queryParams;
    if(queryParams.catId && queryParams.catTitle && queryParams.category !== 'ALL'){
      this.selectedCategory = {_id:queryParams.catId, title: queryParams.catTitle}
    }
    this.searchText = (queryParams.text)?queryParams.text:null;
    this.page = 0;
    this.limit = 3;
    this.loadScripts()
    this.topics = [];
    this.categories = [];
    await this.refreshList();
    this.categories = await this.blogService.getCategories() as Array<any>;
  }

  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  private loadScripts() {
  }

  ngOnDestroy() {
  }

  async refreshList() {
    const filter : any = {
      page: this.page,
      limit: this.limit
    };

    if(this.selectedCategory)
      filter.category = this.selectedCategory._id;
    
    if(this.searchText){
      filter.text = this.searchText;
    }

    const response: any = await this.blogService.getTopics(filter);
    this.topics = response.data;
    this.nbTotal = response.total;
    
    this.router.navigate([], {
      queryParams: {
        text: null,
        catId: null,
        catTitle: null
    }});
  }

  setSelectedCategory(category){
    this.selectedCategory = category;
    this.refreshList();
  }

  setAllCategory(){
    this.selectedCategory = null;
    this.refreshList();
  }

  setSearchText(event){
    this.searchText = event.target.value;
    this.refreshList();
  }
}
