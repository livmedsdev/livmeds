import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  public passwordForm;
  public categories;

  private userId;
  private token;

  
  constructor(private route: ActivatedRoute, private router: Router,
    private userService: UserService,
    private toasterService: ToasterService,
    private translate: TranslateService) { }

  async ngOnInit() {
    const queryParams = this.route.snapshot.queryParams;
    this.userId = queryParams.user;
    this.token = queryParams.token;

    this.passwordForm = new FormBuilder().group({
      password: ['', Validators.compose([Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.required])],
    });
    (window as any).fbq('trackCustom', 'Récupération du mot de passe');
  }

  async submit(){
    if (this.passwordForm.valid && this.passwordForm.value.password === this.passwordForm.value.confirmPassword) {
      try{
        const response : any = await this.userService.resetPassword(this.userId, this.token, this.passwordForm.value.password)
        this.toasterService.pop('success', 'Demande envoyée', await this.translate.get(response.tradCode).toPromise());
        this.router.navigate(['/home'], {});
      }catch(error){
        console.log(error);
        this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
      }
    }
  }
}
