import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevenirLivreurComponent } from './devenir-livreur.component';

describe('DevenirLivreurComponent', () => {
  let component: DevenirLivreurComponent;
  let fixture: ComponentFixture<DevenirLivreurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevenirLivreurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevenirLivreurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
