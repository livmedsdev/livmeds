import { SelectionModel } from '@angular/cdk/collections';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatListOption, MatSelectionList, MatStepper } from '@angular/material';
import { Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { UserRole } from '../enums/UserRole';
import { FileService } from '../file.service';
import { MapsService } from '../maps.service';
import { SessionService } from '../session.service';
import * as moment from 'moment';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CardService } from '../card.service';
import { NativeJsLibraryService } from '../native-js-library.service';
import { ProductService } from '../product.service';
import { CategoryProduct } from '../enums/CategoryProduct';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { NeedDocDialogComponent } from '../need-doc-dialog/need-doc-dialog.component';
import { PlanningDialogComponent } from '../planning-dialog/planning-dialog.component';
import { NgxPicaService } from '@digitalascetic/ngx-pica';

import { environment } from 'src/environments/environment';

declare var Stripe;
declare var google;
declare var window: any;

@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
  styleUrls: ['./command.component.scss']
})
export class CommandComponent implements OnInit {
  readonly currency = "EUR";
  readonly UserRole = UserRole;
  readonly format = 'HH:mm';

  @ViewChild("listShops")
  private selectionList: MatSelectionList;

  @ViewChild('map') mapElement: ElementRef;

  @ViewChild("address")
  public addressElement: ElementRef;

  @ViewChild("addressInformation")
  public addressElementInformation: ElementRef;

  @ViewChild("fileInputPrescription")
  public fileInputPrescription: HTMLInputElement;

  @ViewChild(MatStepper)
  private stepper: MatStepper;


  // Map
  map: any;
  selfMarker = new google.maps.Marker({
    icon: "../assets/images/maps/logo_home.png",
    scaledSize: { width: 40, height: 40 },
    anchor: { x: 20, y: 20 },
  });

  merchantMarker = new google.maps.Marker({
    icon: {
      url: "../assets/images/maps/logo_pharma.png",
      scaledSize: { width: 40, height: 40 },
      anchor: { x: 20, y: 20 },
      label: 'merchant'
    }
  });

  categorySelected: any;
  limit: any;
  searchMerchant: any;
  total: any;
  merchantList: any;
  cards = [];

  // Common
  stockedAddress;

  // First form
  public hideListAddresses = true;
  public myAddresses = [];
  txtQueryChanged: Subject<string> = new Subject<string>();
  public selectedMerchant;
  categories = [];

  // Second step
  public deliveryFormGroup;
  private iti;
  public images = [];
  private deletedImages = [];
  private newImages = [];
  indexOrder = 0;
  urlOrder: any;
  action: string;

  // Third step (shop)
  private txtProductQueryChanged: Subject<string> = new Subject<string>();
  public productList = [];
  public searchProduct;
  public categoriesProduct;
  private listProductSelected = [];
  public listProductSelectedDisplay = [];
  @ViewChild("listCategories")
  private categoriesList: MatSelectionList;
  public selectedCategory;

  public coordinates;

  private markers = [];
  private iwOpen;


  stripe = Stripe(environment.apiKeyStripe);
  card;
  private cardRegistration;
  private expirationDateYear = ['00','01','02','03','04','05','06','07','08','09',10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99];
  private  newCardFormGroup;


  constructor(
    private mapsService: MapsService,
    private session: SessionService,
    private router: Router,
    private deliveryService: DeliveryService,
    private fileService: FileService,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private toasterService: ToasterService,
    private translate: TranslateService,
    private cardService: CardService,
    private nativeJsLibrary: NativeJsLibraryService,
    private productService: ProductService,
    public dialog: MatDialog,
    private _ngxPicaService: NgxPicaService
  ) {

    this.categories = [{
      label: 'ROLE_PHARMACY',
      value: 'ROLE_PHARMACY'
    }, {
      label: 'ROLE_OPTICIAN',
      value: 'ROLE_OPTICIAN'
    }, {
      label: 'ROLE_VETERINARY',
      value: 'ROLE_VETERINARY'
    }]

  }

  async ngOnInit() {
    if (this.session.currentUser() === null) {
      this.router.navigate(['/home'], {})
      return;
    }
    this.spinner.show();
    this.merchantList = [];

    this.categoriesProduct = Object.keys(CategoryProduct);


    this.deliveryFormGroup = new FormBuilder().group({
      phoneNumber: ['', Validators.compose([Validators.required])],
      merchant: ['', Validators.compose([Validators.required])],
      addressId: ['', Validators.compose([Validators.required])],
      cardId: [null, Validators.compose([Validators.required])],
      isScheduled: [false, Validators.compose([])],
      codePromo: [null, Validators.compose([])],
      deliveryDate: [null, Validators.compose([])],
    }, { updateOn: "blur" });

    this.txtQueryChanged
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(async model => {
        await this.reloadMerchant();
        this.createMerchantMarkers()
        // api call
      });

    this.txtProductQueryChanged
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(async model => {
        this.productList = (await this.productService.getAllProduct({ text: this.searchProduct }))['data'];
        this.productList = this.productList.map((product) => {
          product.url = product.image ? this.fileService.getPublicUrlAWS(product.image) : undefined;
          return product;
        })
      });

    this.selectionList.selectedOptions = new SelectionModel<MatListOption>(false);
    this.categoriesList.selectedOptions = new SelectionModel<MatListOption>(false);
    if (!this.session.currentUser()) {
      this.router.navigateByUrl("/home", {})
    }
    this.loadMap();
    this.categorySelected = UserRole.ROLE_PHARMACY;;
    this.limit = 10;

    if (this.addressElement) {
      let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
      autocomplete.setComponentRestrictions({ 'country': ['fr'] });
      google.maps.event.addListener(autocomplete, 'place_changed', async () => {
        let place = autocomplete.getPlace();
        this.stockedAddress = this.mapsService.retrieveAddressFromPlace(place);
        this.addressElementInformation.nativeElement.value = this.stockedAddress.fullAddress;
        const pts = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        this.map.setCenter(pts)
        this.map.setZoom(13)
        this.selfMarker.setMap(this.map);
        this.selfMarker.setPosition(pts);
        this.coordinates = {
          lat: place.geometry.location.lat(),
          long: place.geometry.location.lng()
        }

        await this.reloadMerchant();
        this.createMerchantMarkers()
      });
    }
    if (this.addressElementInformation) {
      let autocomplete = new google.maps.places.Autocomplete(this.addressElementInformation.nativeElement);
      autocomplete.setComponentRestrictions({ 'country': ['fr'] });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        let place = autocomplete.getPlace();
        this.stockedAddress = this.mapsService.retrieveAddressFromPlace(place);
        this.coordinates = {
          lat: place.geometry.location.lat(),
          long: place.geometry.location.lng()
        }
        const pts = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        this.selfMarker.setMap(this.map);
        this.selfMarker.setPosition(pts);
      });
    }

    this.cards = (await this.cardService.list())['data'];
    this.myAddresses = await this.userService.getMyAddresses({}) as [];
    this.deliveryFormGroup.patchValue({
      phoneNumber: this.session.currentUser().phoneNumber
    })
    await this.reloadMerchant();
    this.spinner.hide();

    this.setupStripe();
    this.cardRegistration = (await this.cardService.askAddCardIntent())['client_secret'];
  }

  ngAfterViewInit() {
    const input = document.querySelector("#phone-input-command");
    this.nativeJsLibrary.whenAvailable('intlTelInput', () => {
      if (input) {
        this.iti = window.intlTelInput(input, {
          autoPlaceholder: "aggressive",
          utilsScript: "./assets/js/utils-8.4.6.js",
          initialCountry: "fr"
        });
      }
    })
  }

  async loadMap() {
    try {
      let mapOptions = {
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        fullscreenControl: false
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.map.mapTypes.set('styled_map', (new Date().getHours() >= 20 || new Date().getHours() <= 8) ? this.mapsService.styles.dark : this.mapsService.styles.light);
      this.map.setMapTypeId('styled_map');

      this.map.setCenter(new google.maps.LatLng(46.6774234, 2.0960496))

      navigator.geolocation.getCurrentPosition(async (position) => {
        const selfPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.coordinates = {
          lat: position.coords.latitude,
          long: position.coords.longitude
        }
        this.map.setCenter(selfPosition);
        this.map.setZoom(11);

        this.selfMarker.setMap(this.map);
        this.selfMarker.setPosition(selfPosition);

        await this.reloadMerchant();
        this.createMerchantMarkers()
      });

    } catch (error) {
      console.log(error);
    }
  }

  async categoryChange(category) {
    this.categorySelected = category.value;
    this.limit = 5;
    await this.reloadMerchant();
    this.createMerchantMarkers()
  }

  getLogoForCat(cat) {
    switch (cat) {
      case UserRole.ROLE_PHARMACY:
        return 'assets/images/pharmacy.png';
      case UserRole.ROLE_VETERINARY:
        return 'assets/images/veto.png';
      case UserRole.ROLE_OPTICIAN:
        return 'assets/images/optician.png';
    }
  }

  async onChangeShops() {
    if (this.selectionList.selectedOptions.selected[0]) {
      const merchant = this.selectionList.selectedOptions.selected[0].value;
      const address = this.userService.getFavoriteAddress(merchant.addresses);
      const pts = new google.maps.LatLng(address.latitude, address.longitude);
      this.merchantMarker.setMap(this.map);
      this.merchantMarker.setPosition(pts);
      this.map.setCenter(pts);
      this.map.setZoom(15);

      this.selectedMerchant = merchant;
      this.deliveryFormGroup.patchValue({
        merchant: merchant._id
      });
    }
  }

  async displayPlanning(event, openingTime) {
    event.stopPropagation();

    const dialog = this.dialog.open(PlanningDialogComponent, {
      data: {
        openingTime: openingTime
      },
      autoFocus: false,
    });
  }

  async reloadMerchant() {
    let filter: any = {
      category: this.categorySelected,
      limit: this.limit
    }

    if (this.coordinates) {
      filter.lat = this.coordinates.lat;
      filter.long = this.coordinates.long;
    }

    if (this.searchMerchant) {
      filter.text = this.searchMerchant;
    }
    const response = await this.deliveryService.listShops(filter);
    const newList = response['data'];
    for (let merchant of newList) {
      merchant.urlAvatar = (merchant.avatar) ? await this.fileService.getSignedUrl(merchant.avatar) : undefined;
      merchant.favoriteAddress = this.userService.getFavoriteAddress(merchant.addresses);
      merchant.isOpen = this.checkIfMerchantIsOpen(merchant.openingTime, new Date())
    }
    this.total = response['total'];
    this.merchantList = newList;
  }

  deleteMarkers() {
    for (let marker in this.markers) {
      this.markers[marker].setMap(null);
    }

    this.markers = [];
  }

  createMerchantMarkers() {
    this.deleteMarkers();
    if (this.merchantList.length > 0) {
      const bounds = new google.maps.LatLngBounds();
      if (this.selfMarker.getPosition()) {
        bounds.extend(this.selfMarker.getPosition());
      }

      console.log(this.selfMarker.getPosition())

      for (let merchant of this.merchantList) {
        let marker = new google.maps.Marker({
          icon: {
            url: "../assets/images/maps/logo_pharma.png",
            scaledSize: { width: 40, height: 40 },
            anchor: { x: 20, y: 20 },
            label: 'merchant'
          }
        });

        if(merchant && merchant.location) {
        const pts = new google.maps.LatLng(merchant.location.coordinates[1], merchant.location.coordinates[0]);
        marker.setMap(this.map);
        marker.setPosition(pts);

        this.markers.push(marker)
        bounds.extend(marker.getPosition());
        }

        // Create iw
        let infowindow = new google.maps.InfoWindow({
          content: this.generateContentWindows(merchant),
          maxWidth: 350
        });

        const handlerPlanning = (event) => this.displayPlanning(event, merchant.openingTime);
        const handlerSelectMerchant = (event) => this.selectMerchantByMap(merchant);

        google.maps.event.addListener(infowindow, 'domready', () => {
          const el = document.getElementById('btn-display-planning-' + merchant._id);
          el.removeEventListener('click', handlerPlanning)
          el.addEventListener('click', handlerPlanning);

          const elBtnSelect = document.getElementById('btn-select-merchant-' + merchant._id);
          elBtnSelect.removeEventListener('click', handlerSelectMerchant);
          elBtnSelect.addEventListener('click', handlerSelectMerchant);
        });

        google.maps.event.addListener(marker, 'click', () => {
          if (this.iwOpen ) {
            this.iwOpen.close();
          }

          this.iwOpen = infowindow;

          infowindow.open(this.map, marker);
        });

      }
      this.map.fitBounds(bounds);
    } else {
      const selfPosition = new google.maps.LatLng(this.coordinates.lat, this.coordinates.long);
      this.map.setCenter(selfPosition);
      this.map.setZoom(13)
    }
  }

  checkIfMerchantIsOpen(openingTime, targetDate) {
    if (!openingTime) {
      return undefined;
    }
    const momentTargetDate = moment(targetDate);
    const weekDay = momentTargetDate.toDate().getDay();
    const start = openingTime[weekDay + 'Start'];
    const end = openingTime[weekDay + 'End'];
    if (start && end) {
      const time: any = moment(momentTargetDate.format(this.format), this.format);
      const startMoment: any = moment(moment(start).format(this.format), this.format);
      const endMoment: any = moment(moment(end).format(this.format), this.format);


      const h = time.format('HH');
      const m = time.format('mm');

      const h1 = startMoment.format('HH');
      const m1 = startMoment.format('mm');

      const h2 = endMoment.format('HH');
      const m2 = endMoment.format('mm');

      return (h1 < h || h1 == h && m1 <= m) && (h < h2 || h == h2 && m <= m2);
    } else {
      return false;
    }
  }

  async loadMore() {
    this.spinner.show()
    this.limit += 10;
    await this.reloadMerchant();
    this.createMerchantMarkers()
    this.spinner.hide()
  }

  showListAddresses() {
    this.hideListAddresses = false;
  }

  enterNewAddress() {
    this.hideListAddresses = true;
  }

  onSearchTextChange(event) {
    this.txtQueryChanged.next(event);
  }

  fieldsRegisterFrm(field) {
    return this.deliveryFormGroup.get(field);
  }


  // Prescriptions
  async nextOrder() {
    this.indexOrder++;
    await this.refreshUrl(this.images[this.indexOrder])
  }

  async prevOrder() {
    this.indexOrder--;
    await this.refreshUrl(this.images[this.indexOrder])
  }

  async refreshUrl(url) {
    if (!url) {
      return;
    }
    if (url.startsWith("data:image/jpeg;base64,") || url.startsWith("data:image/png;base64,")) {
      this.urlOrder = url
    } else {
      this.urlOrder = await this.fileService.getSignedUrl(url);
    }
  }

  deleteImageOrder(index) {
    if (this.action === 'edit') {
      this.deletedImages.push(this.images[this.indexOrder]);
    }
    this.images.splice(index, 1);
    if (this.indexOrder > 0) {
      this.indexOrder--;
    }
    this.refreshUrl(this.images[this.indexOrder])
  }


  async changeFile(event, field) {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      try {
        this.spinner.show();
        const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 600, 600, {
          exifOptions: {
            forceExifOrientation: true
          },
          aspectRatio: {
            keepAspectRatio: true
          }
        }).toPromise();

        let params = {};
        params[field] = imageResized;
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          this.images.push(myReader.result);
          this.newImages.push(myReader.result);
          this.refreshUrl(this.images[this.indexOrder])
          this.spinner.hide();
          event.target.value = [];
        }
        myReader.readAsDataURL(imageResized);
      } catch (error) {
        this.spinner.hide();
      }
    }
  }

  // CustomerInformation
  async confirmInformation(stepper: MatStepper) {

    if (!this.stockedAddress) {
      this.toasterService.pop('warning', 'Adresse non renseignée', this.translate.instant("ERROR_ADDRESS_NOT_FILL"));
      return;
    }

    if (this.deliveryFormGroup.value.deliveryDate && (new Date(this.deliveryFormGroup.value.deliveryDate).getTime() <= new Date().getTime())) {
      this.toasterService.pop('warning', 'Date invalide', this.translate.instant("DATE_BEFORE_NOW"));
      return;
    }

    if (!this.deliveryFormGroup.value.cardId) {
      this.toasterService.pop('warning', 'Carte non sélectionnée', this.translate.instant("CARD_NOT_SELECTED"));
      return;
    }

    if (!this.session.currentUser().docSended && this.images.length > 0) {
      const dialog = this.dialog.open(NeedDocDialogComponent);
    } else {
      stepper.next()
    }

  }

  goToLastStep(stepper) {
    stepper.next();
  }

  async confirmCommand() {
    this.spinner.show();
    try {
      const data = this.deliveryFormGroup.value;
      data.addressId = this.stockedAddress;
      data.image = this.images;
      data.deletedImages = this.deletedImages;
      data.products = this.listProductSelected.map(elem => elem._id);
      data.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
      if (data.deliveryDate) {
        data.isScheduled = true;
      }
      let response: any = await this.deliveryService.createByCustomer(data);
      this.toasterService.pop('success', 'Commande validée !', this.translate.instant(response['tradCode']));
      localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(response.delivery));
      this.spinner.hide();
      this.router.navigate(['/track'], { queryParams: { command: response.delivery._id } })
    } catch (error) {
      this.toasterService.pop('error', 'Erreur lors de votre demande', this.translate.instant(error.error.tradCode));
      this.spinner.hide();
    }
  }

  // Shop
  onSearchProductTextChange(event) {
    this.txtProductQueryChanged.next(event);
  }

  addProduct(product) {
    this.listProductSelected.push(product);

    const clone = Object.assign({}, product);

    const found = this.listProductSelectedDisplay.findIndex(e => e._id === product._id);

    if (found < 0) {
      clone.quantity = 1;
      this.listProductSelectedDisplay.push(clone)
    } else {
      this.listProductSelectedDisplay[found].quantity += 1;
    }
  }

  deleteProduct(product) {
    const foundInDisplayList = this.listProductSelectedDisplay.findIndex(e => e._id === product._id);
    if (this.listProductSelectedDisplay[foundInDisplayList].quantity <= 1) {
      this.listProductSelectedDisplay.splice(foundInDisplayList, 1);
    } else {
      this.listProductSelectedDisplay[foundInDisplayList].quantity -= 1;
    }

    // Delete in backend array
    const found = this.listProductSelected.findIndex(e => e._id === product._id);
    this.listProductSelected.splice(found, 1);
  }

  async onChangeCategory() {
    if (this.categoriesList.selectedOptions.selected[0]) {
      this.selectedCategory = this.categoriesList.selectedOptions.selected[0].value.toString();
      this.productList = (await this.productService.getAllProduct({ category: this.selectedCategory }))['data'];
      this.productList = this.productList.map((product) => {
        product.url = product.image ? this.fileService.getPublicUrlAWS(product.image) : undefined;
        return product;
      })
    }
  }


  goBack(stepper: MatStepper) {
    stepper.previous();
  }

  goCustomerInformation(stepper: MatStepper) {
    if (!this.deliveryFormGroup.value.merchant) {
      this.toasterService.pop('warning', 'Boutique non sélectionnée', this.translate.instant("ERROR_MERCHANT_NOT_SELECTED"));
      return;
    }
    stepper.next();
  }

  getCardLabel(cardId) {
    for (let card of this.cards) {
      if (card.id === cardId) {
        return `XXXX XXXX XXXX ${card.card.last4} / Exp: ${card.card.exp_month} / ${card.card.exp_year}`
      }
    }
  }

  onChangeAddressAlreadyExist(event) {
    this.addressElementInformation.nativeElement.value = this.stockedAddress.fullAddress;
    const pts = new google.maps.LatLng(this.stockedAddress.latitude, this.stockedAddress.longitude);
    this.map.setCenter(pts)
    this.map.setZoom(13)
    this.selfMarker.setMap(this.map);
    this.selfMarker.setPosition(pts);
  }

  generateContentWindows(merchant) {
    return '<div id="iw-container">' +
      `<div class="iw-title">${merchant.name}</div>` +
      '<div class="iw-content">' +
      '<div class="iw-subTitle">Information</div>' +
      `<img src="${merchant.urlAvatar ? merchant.urlAvatar : 'assets/images/stand.svg'}" alt="Porcelain Factory of Vista Alegre" height="115" width="83">` +
      `<p>Téléphone: ${merchant.phoneNumber}<br>` +
      `E-Mail: ${merchant.mail}<br>` +
      `Adresse: ${merchant.favoriteAddress.fullAddress}</p>` +
      `<button id="btn-display-planning-${merchant._id}" style="margin-bottom: 5px; font-weight: 400; margin-top: 10px;" type="button"` +
      `class="btn btn-primary w-100 text-uppercase btn-accept">${this.translate.instant('PLANNING')}</button>` +
      `<button id="btn-select-merchant-${merchant._id}" style="margin-bottom: 5px; font-weight: 400; margin-top: 10px;" type="button"` +
      `class="btn btn-primary w-100 text-uppercase btn-accept">${this.translate.instant('SELECT_THIS_MERCHANT')}</button>` +
    '</div>' +
      '<div class="iw-bottom-gradient"></div>' +
      '</div>';
  }

  selectMerchantByMap(merchant){
    this.selectedMerchant = merchant;
    this.deliveryFormGroup.patchValue({
      merchant: merchant._id
    });
    this.goCustomerInformation(this.stepper);
  }



  setupStripe() {
    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    this.card = elements.create('card', { style: style });

    this.card.mount('#card-element');

    this.card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    var form = document.getElementById('payment-form');
    form.addEventListener('submit', async event => {
      event.preventDefault();

      this.spinner.show();
      const cardSetup = await this.stripe.confirmCardSetup(
        this.cardRegistration,
        {
          payment_method: {
            card: this.card,
            billing_details: {
              name: "card",
            },
          },
        });
      const errorElement = document.getElementById('card-errors');
      if (cardSetup.error) {
        this.spinner.hide();
        errorElement.textContent = cardSetup.error.message;
        this.toasterService.pop('error', 'Erreur', 'Erreur lors de l\'ajout de votre carte bancaire');
      } else {
        console.log(cardSetup);
        this.spinner.hide();
        errorElement.textContent = "";
        this.toasterService.pop('success', 'Succès', 'Carte bancaire ajoutée !');
        this.router.navigate(['/cards']);
      }
    });
  }
}
