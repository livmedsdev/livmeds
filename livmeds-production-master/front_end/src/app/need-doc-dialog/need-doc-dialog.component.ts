import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-need-doc-dialog',
  templateUrl: './need-doc-dialog.component.html',
  styleUrls: ['./need-doc-dialog.component.scss']
})
export class NeedDocDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NeedDocDialogComponent>,
    public router: Router
  ) { }

  ngOnInit() {
  }

  cancel(){
    this.dialogRef.close();
  }

  goToSendDoc(){
    this.router.navigate(["/documents"], {})
    this.dialogRef.close({});
  }
}
