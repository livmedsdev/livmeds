import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedDocDialogComponent } from './need-doc-dialog.component';

describe('NeedDocDialogComponent', () => {
  let component: NeedDocDialogComponent;
  let fixture: ComponentFixture<NeedDocDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeedDocDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedDocDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
