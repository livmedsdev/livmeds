import { TestBed } from '@angular/core/testing';

import { NativeJsLibraryService } from './native-js-library.service';

describe('NativeJsLibraryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NativeJsLibraryService = TestBed.get(NativeJsLibraryService);
    expect(service).toBeTruthy();
  });
});
