import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Topic } from './models/Topic';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient, public session: SessionService) { }

  async getTopics(filter){
    return await this.http.get(environment.url + '/customer/topics',{
      params: filter
    }).toPromise();
  }

  async getTopic(topicId){
    return await this.http.get(environment.url + '/customer/topic/'+ topicId).toPromise();
  }

  async getMessages(topicId){
    return await this.http.get(environment.url + '/customer/messages/'+topicId).toPromise();
  }
  
  async createTopic(topic: Topic){
    return await this.http.post(environment.url + '/customer/topic', topic).toPromise();
  }

  async createMessage(topic : String, content : String){
    return await this.http.post(environment.url + '/customer/message', {topic, content}).toPromise();
  }

  async getCategories(){
    return await this.http.get(environment.url + '/customer/categories').toPromise();
  }
}
