export class User {
    _id: string;
    avatar: string;
    lastName: string;
    mail: string;
    name: string;
    role: any;
    password: string;
    becomeDeliverer: boolean;
    favoriteAddress: string;
    phoneNumber: string;
    status: string;
    addresses: Array<any>;
    openingTime: any;
    docSended: boolean;
}