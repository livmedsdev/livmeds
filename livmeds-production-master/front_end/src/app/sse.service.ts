import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SseService {

  constructor(private _ngZone: NgZone) { }


  private getEventSource(url: string) : EventSource{
    return new EventSource(url);
  }

  public getServerSentEvent(path: string, params: {}, eventName: string) : Observable<any> {
    const url = new URL(environment.url + path);
    url.searchParams.append('authorization', localStorage.getItem('token'));
    for (const property in params) {
      url.searchParams.append(property, params[property]);
    }

    return Observable.create(observer =>{
      const eventSource = this.getEventSource(url.toString());
      console.log(eventSource);

      eventSource.addEventListener(eventName, (event) => {
        this._ngZone.run(() =>{
          observer.next(JSON.parse(event['data']));
        });
      })


      eventSource.onopen = (event) => {
        console.log('connection open');
      };

      eventSource.onerror = error =>{
        console.log(error);
        console.log('error');
        this._ngZone.run(() =>{
          observer.error(error);
        });
      }
    })
  }
}
