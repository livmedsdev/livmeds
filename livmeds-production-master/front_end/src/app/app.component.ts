import { Component, ElementRef, ViewChild, Inject } from '@angular/core';
import { User } from './models/User';
import { UserService } from './user.service';
import { SessionService } from './session.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { UserRole } from './enums/UserRole';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { SignUpModalComponent } from './sign-up-modal/sign-up-modal.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from './notification.service';
import { EventPollingService } from './event-polling.service';

declare var $: any;
declare var google;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly USER_ROLE = UserRole;
  public user: User = { role: null } as User;
  private isConnected;
  private title;
  public error;
  public errorRegister;
  public confirmPassword;
  private address;

  public config: ToasterConfig =
    new ToasterConfig({
      positionClass: 'toast-bottom-left',
      tapToDismiss: true,
      timeout: 0
    });

  public categories;

  @ViewChild("address")
  public addressElement: ElementRef;

  @ViewChild("addressPharmacy")
  public addressPharmacyElement: ElementRef;

  constructor(public userService: UserService,
    public session: SessionService,
    private toasterService: ToasterService,
    private eventPolling: EventPollingService,
    public dialog: MatDialog,
    public notificationService: NotificationService,
    public translate: TranslateService,
    public router: Router) {
    translate.setDefaultLang('fr');
    translate.use('fr');
    registerLocaleData(localeFr, 'fr');

    if (this.session.getPreferences().lang === undefined || this.session.getPreferences().lang === null) {
      this.session.setLang('fr');
    }

    this.categories = [{
      label: 'ROLE_PHARMACY',
      value: 'ROLE_PHARMACY'
    }, {
      label: 'ROLE_OPTICIAN',
      value: 'ROLE_OPTICIAN'
    }, {
      label: 'ROLE_VETERINARY',
      value: 'ROLE_VETERINARY'
    }]
  }

  async ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    try {
      if (localStorage.getItem('token')) {
        const checkToken = (await this.userService.checkToken())['valid'];
        if (this.session.currentUser() && checkToken === false) {
          this.logout();
        } else if (this.session.currentUser()
          && [UserRole.ROLE_PHARMACY, UserRole.ROLE_OPTICIAN, UserRole.ROLE_VETERINARY].includes(this.session.currentUser().role)) {
          this.eventPolling.startRetrieveDeliveries();
        }
      }
    } catch (error) {
      debugger
      if (this.session.currentUser()) {
        this.logout();
      }
    }
    // this.notificationService.requestPermission();
    // this.notificationService.receiveMessage();
  }

  async submitRegisterPharmacy() {
  }

  logout() {
    this.isConnected = false;
    localStorage.clear();
    this.router.navigate(['/home']);
    this.eventPolling.stopRetrieveDeliveries();
  }


  ngAfterViewInit() {
    if ($('header.header').length > 0) {
      var myNavBar = {

        flagAdd: true,

        elements: [],

        init: function (elements) {
          this.elements = elements;
        },

        add: function () {
          if (this.flagAdd) {
            for (var i = 0; i < this.elements.length; i++) {
              document.getElementById(this.elements[i]).className += " sticky-nav";
            }
            this.flagAdd = false;
          }
        },

        remove: function () {
          for (var i = 0; i < this.elements.length; i++) {
            document.getElementById(this.elements[i]).className =
              document.getElementById(this.elements[i]).className.replace(/(?:^|\s)sticky-nav(?!\S)/g, '');
          }
          this.flagAdd = true;
        }

      };
      /**
       * Init the object. Pass the object the array of elements
       * that we want to change when the scroll goes down
       */
      myNavBar.init([
        "main-nav"
      ]);

      /**
       * Function that manage the direction
       * of the scroll
       */
      const offSetManager = () => {

        var yOffset = 0;
        var currYOffSet = window.pageYOffset;
        
        if (yOffset < currYOffSet) {
          myNavBar.add();
        } else if (currYOffSet == yOffset) {
          myNavBar.remove();
        }
      }
      /**
       * bind to the document scroll detection
       */
      window.onscroll = function (e) {
        offSetManager();
      }

      /**
       * We have to do a first detectation of offset because the page
       * could be load with scroll down set.
       */
      offSetManager();
    }
  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  resetFields() {
    this.user = {} as User;
    this.confirmPassword = null;
    this.addressElement.nativeElement.value = null;
    this.addressPharmacyElement.nativeElement.value = null;
  }

  openLoginModal() {
    const dialogRef = this.dialog.open(LoginModalComponent, {
      width: 'auto',
      height: 'auto',
      data: {}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');

    });
  }

  openSignUpModal(userRole) {
    let data: any = {};
    if (userRole) {
      data.role = userRole;
    }
    const dialogRef = this.dialog.open(SignUpModalComponent, {
      width: 'auto',
      height: 'auto',
      data: data
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log('The dialog was closed');
      this.user = result;

    });
  }

  changeLanguage(lang) {
    this.session.setLang(lang);
    this.translate.use(lang);
  }
}

export interface DialogData {
  role: String,
  redirectUrl: String,
}