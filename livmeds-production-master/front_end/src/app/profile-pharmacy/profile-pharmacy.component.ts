import { Component, OnInit } from '@angular/core';
import { SessionService } from '../session.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { User } from '../models/User';
import { FileService } from '../file.service';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

declare var window: any;

@Component({
  selector: 'app-profile-pharmacy',
  templateUrl: './profile-pharmacy.component.html',
  styleUrls: ['./profile-pharmacy.component.scss']
})
export class ProfilePharmacyComponent implements OnInit {
  public myProfile : User;

  // Security form
  public securityFormGroup;
  public errorSecurity;
  public successSecurity;

  
  // Information form
  public informationFormGroup;
  public errorInformation;
  public successInformation;

  // Opening time
  public openingTimesForm;
  public days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
  public openingTimes;
  public checkValid = false;

  private iti;
  
  constructor(public session: SessionService, 
    public fileService: FileService,
    public userService: UserService, 
    private toasterService: ToasterService,
    private translate: TranslateService,
    private _ngxPicaService: NgxPicaService) { 
    this.securityFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      currentPassword: ['', Validators.compose([])],
      newPassword: ['', Validators.compose([])]
    });

    this.informationFormGroup = new FormBuilder().group({
      name: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])]
    });

    this.openingTimesForm = new FormBuilder().group({
      '0Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '0End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '1Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '1End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '2Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '2End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '3Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '3End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '4Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '4End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '5Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '5End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '6Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '6End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '7Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '7End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])]
    }, { updateOn: "blur" });

    let openingTime = this.session.currentUser().openingTime;
    openingTime = this.dateToString(openingTime);
    if(openingTime){
      this.openingTimesForm.patchValue(openingTime);
      if(openingTime['0Start']){
        this.openingTimesForm.patchValue({
          '7Start': openingTime['0Start']
        })
      }
      if(openingTime['0End']){
        this.openingTimesForm.patchValue({
          '7End': openingTime['0End']
        })
      }
    }
    this.openingTimes = this.openingTimesForm.value;
  }

  dateToString(values){
    const newValues = {};
    for(let index in values){
      if(values[index] && values[index] !== ''){
        const date = new Date();
        date.setHours(values[index].split(':')[0]);
        date.setMinutes(values[index].split(':')[1])
        newValues[index] = moment(values[index]).format('HH:mm');
      }else{
        newValues[index] = values[index];
      }
    }
    return newValues;
  }

  fieldsRegisterFrm(field) {
    return this.openingTimesForm.get(field);
  }

  async ngOnInit() {
    this.myProfile = {addresses:[]} as User;
    this.myProfile = await this.userService.getProfile() as User;
    console.log(this.myProfile);
    this.informationFormGroup.patchValue(this.myProfile);
    this.myProfile.openingTime['7Start'] = this.myProfile.openingTime['0Start'];
    this.myProfile.openingTime['7End'] = this.myProfile.openingTime['0End']
    this.openingTimes = this.myProfile.openingTime;
  }

  
  async submitSecurityForm(){
    this.resetMessage();
    if(this.securityFormGroup.valid){
      try{
        const response : any = await this.userService.editSecurityInformation(this.securityFormGroup.value);
        this.myProfile = await this.userService.getProfile() as User;
        this.successSecurity = response.tradCode;
        this.securityFormGroup.reset();
      }catch(error){
        this.errorSecurity = error.error.tradCode;
      }
    }
  }

  ngAfterViewInit() {

    const input = document.querySelector("#phone-input-profile");
    if(input) {
        this.iti = window.intlTelInput(input, {
            autoPlaceholder: "aggressive",
            utilsScript: "./assets/js/utils-8.4.6.js",
            initialCountry: "fr"
        });
    }
  }
  
  async submitInformationForm(){
    this.resetMessage();
    if(this.informationFormGroup.valid){
      try{
        let params = this.informationFormGroup.value;
        params.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
        const response : any = await this.userService.editMyProfile(this.informationFormGroup.value);
        this.myProfile = await this.userService.getProfile() as User;
        this.successInformation = response.tradCode;
        this.informationFormGroup.patchValue(this.myProfile);
      }catch(error){
        this.errorInformation = error.error.tradCode;
      }
    }
  }
  
  resetMessage(){
    this.successSecurity = null;
    this.errorSecurity = null;
  }
  
  async onFileChanged(event){
    const selectedFile = event.target.files[0];
    const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 400, 400, {
      exifOptions:{
        forceExifOrientation: true
      },
      aspectRatio: {
      keepAspectRatio: true
    }}).toPromise();
              
    const uploadData = new FormData();
    uploadData.append('file', imageResized, selectedFile.name);
    const response : any = await this.fileService.uploadForm(uploadData, 'avatar');
    await this.userService.changeAvatar(response.idFile);
    this.myProfile = await this.userService.getProfile() as User;
  }

  async submitOpeningTime(){
    if(!this.openingTimesForm.valid){
      this.validateAllFormFields(this.openingTimesForm);
      return;
    }
    console.log(this.openingTimesForm.value)
    try {
      let params = this.openingTimesForm.value;
      params['0Start'] = params['7Start']
      params['0End'] = params['7End']
      params = this.convertValueToDate(params);
      await this.userService.updateOpeningTimes(params);
      this.myProfile = await this.userService.getProfile() as User;
      this.openingTimes = params;
      this.toasterService.pop('success', 'Succès', await this.translate.get("Les horaires de votre boutique ont été mis à jour!").toPromise());
    } catch (error) {
      console.log(error);
      this.toasterService.pop('error', 'Erreur', 'Erreur lors de la modification de vos horaires');
    }
  }

  convertValueToDate(values){
    const newValues = {};
    for(let index in values){
      if(values[index] && values[index] !== ''){
        const date = new Date();
        date.setHours(values[index].split(':')[0]);
        date.setMinutes(values[index].split(':')[1])
        newValues[index] = date;
      }else{
        newValues[index] = values[index];
      }
    }
    return newValues;
  }

  
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
    this.checkValid = true;
  }

}
