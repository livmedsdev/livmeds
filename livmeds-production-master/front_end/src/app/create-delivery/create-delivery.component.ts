import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { CardService } from '../card.service';
import { DeliveryService } from '../delivery.service';
import { PackageSize } from '../enums/PackageSize';
import { FileService } from '../file.service';
import { UserService } from '../user.service';

declare var window: any;
declare var google;

@Component({
  selector: 'app-create-delivery',
  templateUrl: './create-delivery.component.html',
  styleUrls: ['./create-delivery.component.scss']
})
export class CreateDeliveryComponent implements OnInit {

  public productFormGroup;
  public products: any[] = [];
  public totalAmountWithoutFee;
  public deliveryFormGroup;
  public cards: any = [];
  public myAddresses = [];
  public hideListAddresses = true;
  @ViewChild("address")
  public addressElement: ElementRef;
  public checkValid = false;

  private iti;

  constructor(private route: ActivatedRoute,
    private fileService: FileService,
    private translate: TranslateService,
    private toasterService: ToasterService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public dialog: MatDialog,
    private cardService: CardService,
    private userService: UserService,
    private deliveryService: DeliveryService) { }

  async ngOnInit() {
    this.productFormGroup = new FormBuilder().group({
      description: ['', Validators.compose([Validators.required])],
      quantity: ['', Validators.compose([Validators.required])],
      unitAmount: ['', Validators.compose([Validators.required])]
    });

    this.deliveryFormGroup = new FormBuilder().group({
      nameCustomer: ['', Validators.compose([Validators.required])],
      lastnameCustomer: ['', Validators.compose([Validators.required])],
      phoneNumberCustomer: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      cardId: [null, Validators.compose([Validators.required])],
      isForCustomer: [true, Validators.compose([])],
      codePromo: [null, Validators.compose([])],
    }, { updateOn: "blur" });
    if (this.addressElement) {
      let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
      autocomplete.setComponentRestrictions({ 'country': ['fr'] });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        let place = autocomplete.getPlace();
        this.deliveryFormGroup.patchValue({
          address: this.retrieveAddressFromPlace(place)
        });
      });
    }
    this.cards = (await this.cardService.list())['data'];
    this.myAddresses = await this.userService.getMyAddresses({}) as [];
  }


  ngAfterViewInit() {
    const input = document.querySelector("#phone-input-create-delivery");
    if (input) {
      this.iti = window.intlTelInput(input, {
        autoPlaceholder: "aggressive",
        utilsScript: "./assets/js/utils-8.4.6.js",
        initialCountry: "fr"
      });
    }
  }


  async submit() {
    if(!this.deliveryFormGroup.valid || this.products.length === 0){
      this.validateAllFormFields(this.deliveryFormGroup);
      return;
    }
    this.spinner.show();
    let delivery = this.deliveryFormGroup.value;
    delivery.phoneNumberCustomer = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
    delivery.products = this.products;
    try {
      const response = await this.deliveryService.create(delivery);
      this.toasterService.pop('success', 'Succès', 'Votre demande de livraison pour ce client a bien été créée');
      this.spinner.hide();
      this.router.navigate(['/deliveries']);
    } catch (error) {
      console.log(error)
      this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
      this.spinner.hide();
    }
  }

  fieldsRegisterFrm(field) {
    return this.deliveryFormGroup.get(field);
  }

  addProduct() {
    this.products.push(this.productFormGroup.value);
    this.productFormGroup.reset();
    this.calculateTotalAmount();
  }

  calculateTotalAmount() {
    this.totalAmountWithoutFee = 0;
    for (let product of this.products) {
      this.totalAmountWithoutFee += (product.quantity * product.unitAmount)
    }
    this.totalAmountWithoutFee = this.totalAmountWithoutFee.toFixed(2);
  }

  deleteProduct(product) {
    this.products.splice(this.products.indexOf(product), 1);
    this.calculateTotalAmount();
  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components

    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  showListAddresses() {
    this.hideListAddresses = false;
  }

  enterNewAddress() {
    this.hideListAddresses = true;
  }


  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
    this.checkValid = true;
  }
}
