import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { UserRole } from '../enums/UserRole';
import { SessionService } from '../session.service';
import { UserService } from '../user.service';
import scriptjs from 'scriptjs';
import { NgxSpinnerService } from 'ngx-spinner';

declare var window: any;
declare var $: any;
declare var google;
declare var AppleID;

@Component({
  selector: 'app-sign-up-customer',
  templateUrl: './sign-up-customer.component.html',
  styleUrls: ['./sign-up-customer.component.scss']
})
export class SignUpCustomerComponent implements OnInit {

  public registerFormGroup;
  public registerSocialFormGroup;
  public confirmPassword;
  public address;
  public checkValid = false;
  public i = 0;

  public categories = [{
    label: 'ROLE_PHARMACY',
    value: 'ROLE_PHARMACY'
  }, {
    label: 'ROLE_OPTICIAN',
    value: 'ROLE_OPTICIAN'
  }, {
    label: 'ROLE_VETERINARY',
    value: 'ROLE_VETERINARY'
  }]


  @ViewChild("address")
  public addressElement: ElementRef;

  @ViewChild("addressPharmacy")
  public addressPharmacyElement: ElementRef;

  private iti;

  private redirectUrl;


  private readonly appleScriptUrl: string = 'https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js';
  private ready: Promise<boolean> = new Promise(resolve => {
    if (typeof window !== 'undefined') {
      scriptjs(this.appleScriptUrl, () => resolve(true));
    } else {
      resolve(false);
    }
  });


  constructor(
    private translate: TranslateService,
    private userService: UserService,
    public session: SessionService,
    private spinner: NgxSpinnerService,
    public router: Router,
    private route: ActivatedRoute,
    private toasterService: ToasterService) {

    this.registerFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      passwordConfirm: ['', Validators.compose([Validators.required])],
      favoriteAddressDisplay: [null, Validators.compose([Validators.required])],
      favoriteAddress: [null, Validators.compose([Validators.required])],
      compAddress: ['', Validators.compose([])],
      referralCodeReferrer: ['', Validators.compose([])]
    },
      { updateOn: "blur" });


    this.registerSocialFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      favoriteAddressDisplay: [null, Validators.compose([Validators.required])],
      favoriteAddress: [null, Validators.compose([Validators.required])],
      userIdFb: ['', Validators.compose([])],
      accessTokenFb: ['', Validators.compose([])],
      identityToken: ['', Validators.compose([])],
      authorizationCode: ['', Validators.compose([])],
      compAddress: ['', Validators.compose([])],
      referralCodeReferrer: ['', Validators.compose([])]
    },
      { updateOn: "blur" });


    this.ready.then(isReady => {
      if (isReady) {
        AppleID.auth.init({
          clientId: 'livmeds.com',
          scope: "name email",
          redirectURI: `${window.location.origin}/sign-up-customer?tp=true`,
          state: "website"
        });
      }
    });
  }

  ngOnInit() {
    this.redirectUrl = '/home'
    this.route.queryParams.subscribe(param => {
      if (param.redirectUrl) {
        this.redirectUrl = param.redirectUrl;
      }
    });

    (window as any).fbq('trackCustom', 'Affichage_formulaire_Inscription_Client');
    // if(this.infoFb.connectionType){
    //   this.registerSocialFormGroup.patchValue(this.infoFb);
    //   console.log('register social');
    //   const inputSocial = document.querySelector("#phone-input-sign-up-customer-social");
    //   if(inputSocial) {
    //       this.iti = window.intlTelInput(inputSocial, {
    //           autoPlaceholder: "aggressive",
    //           utilsScript: "./assets/js/utils-8.4.6.js",
    //           initialCountry: "fr"
    //       });
    //   }
    //   console.log(this.registerSocialFormGroup.value);
    // }else{
    this.registerFormGroup.reset();
    // }
  }

  fieldsRegisterFrm(field) {
    return this.registerFormGroup.get(field);
  }

  fieldsRegisterSocialFrm(field) {
    return this.registerSocialFormGroup.get(field);
  }

  ngAfterViewInit() {

    this.whenAvailable('intlTelInput', () => {
      const input = document.querySelector("#phone-input-sign-up-customer");
      if (input) {
        this.iti = window.intlTelInput(input, {
          autoPlaceholder: "aggressive",
          utilsScript: "./assets/js/utils-8.4.6.js",
          initialCountry: "fr"
        });
      }
    });

    this.whenDefined("google", () => {
      if (this.addressElement) {
        let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
        autocomplete.setComponentRestrictions({ 'country': ['fr'] });
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
          let place = autocomplete.getPlace();
          this.registerFormGroup.patchValue({ favoriteAddress: this.retrieveAddressFromPlace(place) });
        });
      }

      if (this.addressPharmacyElement) {
        let autocompletePharmacy = new google.maps.places.Autocomplete(this.addressPharmacyElement.nativeElement);
        autocompletePharmacy.setComponentRestrictions({ 'country': ['fr'] });
        google.maps.event.addListener(autocompletePharmacy, 'place_changed', () => {
          let place = autocompletePharmacy.getPlace();
          this.registerFormGroup.patchValue({ favoriteAddress: this.retrieveAddressFromPlace(place) });
        });
      }
    })
  }


  async submit() {
    debugger
    if (!this.registerFormGroup.valid) {
      this.validateAllFormFields(this.registerFormGroup);
      return;
    }

    if (this.registerFormGroup.valid) {
      this.spinner.show()
      try {
        let user = this.registerFormGroup.value;
        user.favoriteAddress = JSON.stringify(user.favoriteAddress)
        user.role = UserRole.ROLE_CUSTOMER;
        user.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
        user.connectionType = 'MAIL';
        await this.userService.register(user);
        this.spinner.hide()
        this.router.navigate([this.redirectUrl]);
      } catch (error) {
        this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
        this.spinner.hide()
      }
    }

  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  whenDefined(variable, callback) {
    const interval = 10; // ms
    window.setTimeout(() => {
      if (google) {
        callback(google);
      } else {
        this.whenDefined(variable, callback);
      }
    }, interval);
  }

  whenAvailable(name, callback) {
    const interval = 10; // ms
    window.setTimeout(() => {
      if (window[name]) {
        callback(window[name]);
      } else {
        this.whenAvailable(name, callback);
      }
    }, interval);
  }


  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
    this.checkValid = true;
  }

  signUpApple() {
    AppleID.auth.signIn();
  }
}
