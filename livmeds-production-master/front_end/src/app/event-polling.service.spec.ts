import { TestBed } from '@angular/core/testing';

import { EventPollingService } from './event-polling.service';

describe('EventPollingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventPollingService = TestBed.get(EventPollingService);
    expect(service).toBeTruthy();
  });
});
