import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }

  async create(privateMessage){
    return await this.http.post(environment.url + "/merchant/privateMessage", privateMessage).toPromise();
  }

  async getPrivateMessages(idDelivery, typePrivateMessage){ 
    return await this.http.get(environment.url + "/merchant/privateMessages/"+ idDelivery, {params:{
      typePrivateMessage: typePrivateMessage
    }}).toPromise();
  }

}
