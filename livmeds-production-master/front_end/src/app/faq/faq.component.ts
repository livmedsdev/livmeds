import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  questions : any = {};
  constructor() {
    this.questions.delivery = [{
      question: 'Comment laisser un avis et un pourboire à mon livreur ?',
      answer: 'Il est tout à fait possible de laisser un avis à votre livreur une fois votre commande livrée. Vous pourrez ainsi le féliciter pour son professionnalisme, ou encore sa rapidité. Vous pouvez accompagnez cette note, d’un pourboire en guise de bonus. '
    },{
      question: 'Puis-je me faire livrer des médicaments classés dangereux ?',
      answer: 'Non, tous les médicaments ne sont pas éligibles, certaines classes de médicaments ne peuvent être livrées, notamment les médicaments dits stupéfiants. Cette catégorie ne peut être délivrée qu’en présence d’un professionnel de santé. '
    },{
      question: 'Est-il possible de se faire livrer des produits sans ordonnance ?',
      answer: 'Oui. Il est possible de vous faire livrer tous types de produits parapharmaceutiques via notre rubrique E-shop en plus de vos médicaments sur ordonnance.'
    },{
      question: 'Quels sont nos délais et nos horaires de livraison ?',
      answer: 'Nous vous garantissons un délai de livraison assuré en moins de 30 minutes 24h/24h 7j/7j grâce aux pharmacies de garde partenaires.'
    },{
      question: 'Un médicament commandé n\'est-il pas disponible dans ma pharmacie favorite que se passe-t-il ?',
      answer: 'Si votre médicament où votre produit parapharmaceutique n’est plus disponible au sein de votre pharmacie, vous recevrez une notification de refus du pharmacien afin de vous inviter à choisir une pharmacie ayant l’article ou le médicament disponible.'
    },{
      question: 'De quelle manière sont transportés vos médicaments ?',
      answer: 'Les médicaments sont transportés dans des sacs hermétiques sous scellés permettant ainsi la confidentialité de la livraison de vos médicaments. Le pharmacien sera le seul interlocuteur connaissant le contenu de votre commande !'
    },{
      question: 'De quelle manière se déplacent nos livreurs ?',
      answer: 'Nos coursiers se déplacent en vélo afin de respecter l’image et les valeurs environnementales que nous donnons à notre projet.'
    },{
      question: 'Puis-je annuler ma livraison ?',
      answer: 'Vous pouvez annuler votre livraison en signalant un problème directement sur l’application ou en nous joignant par téléphone. Cependant, une fois que le livreur a récupéré votre commande, la livraison n’est plus'
    },{
      question: 'Comment suivre ma livraison ?',
      answer: 'Il vous sera possible de suivre votre livreur en temps réel grâce à notre service de géolocalisation. Vous pourrez également le joindre directement via l’application au cours de la livraison en cas de problème.'
    },{
      question: 'Puis-je me faire livrer les médicaments de mon animal domestique ?',
      answer: 'Vous pouvez vous faire livrer les médicaments et les soins disponibles dans vos pharmacies pour vos animaux.'
    },{
      question: 'Que se passe-t-il si je ne réponds pas au livreur ?',
      answer: ' Le livreur a l’obligation de vous attendre jusqu’à 10 minutes, au-delà il aura le choix de patienter plus longtemps ou de retourner la commande à la pharmacie ce qui vous sera facturé.'
    }]
  this.questions.myData = [
    {
      question: 'Comment sont conservées mes données de santé ?',
      answer: 'Vos données de santé sont stockées chez un hébergeur de données agréé afin de garantir la sécurité et le cryptage de vos informations personnelles.'
    },{
      question: 'Puis-je demander conseil à mon pharmacien ou mon praticien ?',
      answer: 'Il est possible de chatter avec votre professionnel de santé via le chat de notre application qui vous permettra ainsi de pouvoir demander tous les conseils dont vous avez besoin.'
    }]

  this.questions.payment = [{
      question: 'Quels sont les coûts d’une livraison ?',
      answer: 'Chaque livraison effectuée sur un rayon de 3km entre le domicile et la pharmacie choisie est facturée 9,60€. Chaque kilomètre supplémentaire est facturé 1€ de plus qui reviendra directement au livreur.'
    },{
      question: 'Puis-je m’abonner ? ',
      answer: 'Il vous sera également possible de choisir des abonnements de livraison afin de vous permettre d’économiser vos courses !'
    },{
      question: 'Est-il possible de se faire rembourser les frais de livraison de médicament ?',
      answer: 'Nous avons négocié pour vous auprès des mutuelles et assurance, une prise en charge de vos frais de médicaments “hors médicament non-remboursés”'
    },{
      question: 'Comment puis-je payer ?',
      answer:'Vous pouvez régler avec votre carte bancaire en ligne où bien avec Apple Pay si vous vous êtes inscrits avec un compte Apple.'
    }];

    this.questions.help = [{
      question: 'Que faire en cas de question ou problème sur l’application ?',
      answer: 'Si vous rencontrez un problème lors d’une commande, vous pouvez utiliser le bouton « signaler un problème » disponible sur l’application. Vous avez aussi la possibilité de chatter avec un téléconseiller directement en ligne sur notre site internet ou joindre notre service d’assistance téléphonique au 04 83 43 27 33.'
    }]

  }

  ngOnInit() {
    (window as any).fbq('trackCustom', 'FAQ');
  }

  scroll(el: String) {
    $('html,body').animate({
      scrollTop: $("#"+el).offset().top - 200
   }, 800);
  }

}
