import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DeliveryService } from '../delivery.service';
import { PackageSize } from '../enums/PackageSize';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../user.service';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { CardService } from '../card.service';

declare var $: any;
declare var google;

@Component({
  selector: 'app-add-delivery',
  templateUrl: './add-delivery.component.html',
  styleUrls: ['./add-delivery.component.scss']
})
export class AddDeliveryComponent implements OnInit {

  @ViewChild("address")
  public addressElement: ElementRef;
  public deliveryFormGroup;
  public cards : any = [];
  public myAddresses = [];
  public hideListAddresses = true;

  constructor(private deliveryService: DeliveryService,
    private translate: TranslateService,
    private toasterService: ToasterService,
    private router: Router,
    private cardService: CardService,
    private userService: UserService
    ) { 
    this.deliveryFormGroup = new FormBuilder().group({
      bigPackage: [false, Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      nbPackage: ['', Validators.compose([Validators.required])],
      cardId: [null, Validators.compose([Validators.required])]
    });
  }

  async ngOnInit() {
    if(this.addressElement){
      let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
      autocomplete.setComponentRestrictions({'country': ['fr']});
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        let place = autocomplete.getPlace();
        this.deliveryFormGroup.patchValue({
          address: this.retrieveAddressFromPlace(place)
        });
      });
    }
    this.cards = (await this.cardService.list())['data'];
    this.myAddresses = await this.userService.getMyAddresses({}) as [];
  }

  async submit(){
    let delivery = this.deliveryFormGroup.value;
    delivery.sizePackage = (delivery.bigPackage)?PackageSize["50X50"]:PackageSize.SMALL;
    try{
      const response = await this.deliveryService.create(delivery);
      this.toasterService.pop('success', 'Succès', 'Votre demande de livraison a bien été créée');
      this.router.navigate(['/deliveries']);
    } catch (error) {
      console.log(error)
      this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
    }
  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components

    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  showListAddresses(){
    this.hideListAddresses = false;
  }

  enterNewAddress(){
    this.hideListAddresses = true;
  }
}
