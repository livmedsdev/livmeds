import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FileService } from '../file.service';
import { MatDialog } from '@angular/material';
import { ShowImageModalComponent } from '../show-image-modal/show-image-modal.component';

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.scss']
})
export class CreateInvoiceComponent implements OnInit {
  public delivery : any = {
    image: []
  };
  public productFormGroup;
  public products: any[] = [];
  public totalAmountWithoutFee;
  public deliveryChat;
  public indexOrder;
  public url;

  constructor(private route: ActivatedRoute,
    private fileService: FileService,
    private router: Router,
    public dialog: MatDialog,
    private deliveryService: DeliveryService) {
      this.productFormGroup = new FormBuilder().group({
        description: ['', Validators.compose([Validators.required])],
        comment: ['', Validators.compose([])],
        quantity: ['', Validators.compose([Validators.required])],
        unitAmount: ['', Validators.compose([Validators.required])]
      });
    }

  async ngOnInit() {
    this.indexOrder = 0;
    this.calculateTotalAmount();
    const queryParams = this.route.snapshot.queryParams;
    this.delivery = await this.deliveryService.get(queryParams.idDelivery);
    if(this.delivery.image && !Array.isArray(this.delivery.image)){
      this.delivery.image = [this.delivery.image]
    }
    for(const productPara of this.delivery.productsSelected){
      if (this.products.filter(e => e._id === productPara.pres_id).length > 0) {
        const index = this.products.findIndex(e => e._id === productPara.pres_id);
        this.products[index].quantity += 1;
      }else{
        this.products.push({
          _id: productPara.pres_id,
          description: productPara.presentation,
          image: productPara.image,
          quantity: 1,
          unitAmount : productPara.prix_moyen ? productPara.prix_moyen/100 : productPara.prix/100
        })
        this.calculateTotalAmount();
      }
    }
    this.deliveryChat = this.delivery;
    this.url = await this.fileService.getBase64Image(this.delivery.image[this.indexOrder]);
  }

  addProduct(){
    this.products.push(this.productFormGroup.value);
        console.log(this.products);
        console.log(this.delivery);
        console.log(this.delivery.productsSelected);
    this.productFormGroup.reset();
    this.calculateTotalAmount();
  }

  async validDelivery(){
    try{

      console.log(this.products);
      let response = await this.deliveryService.validDelivery(this.delivery._id, {products : this.products});
      this.router.navigate(['/deliveries']);
    }catch(error){
      console.error(error)
    }
  }

  calculateTotalAmount(){
    this.totalAmountWithoutFee=0;
    for(let product of this.products){
      if(!product.status || product.status !== 'DELETED'){
        this.totalAmountWithoutFee += (product.quantity * product.unitAmount)
      }
    }
    this.totalAmountWithoutFee = this.totalAmountWithoutFee.toFixed(2);
  }

  deleteProduct(product){
    if(product._id){
      this.products[this.products.indexOf(product)].status = 'DELETED';
    }else{
      this.products.splice(this.products.indexOf(product), 1 );
    }
    this.calculateTotalAmount();
  }

  editAmountProduct(product, event){
    const index = this.products.findIndex(e => e._id === product._id);
    this.products[index].unitAmount = event.target.value;
    this.calculateTotalAmount();
            console.log(this.products);
            console.log(this.delivery);
            console.log(this.delivery.productsSelected);
  }

  async seeImage(title, imageData) {
    if (imageData) {
      const dialogRef = this.dialog.open(ShowImageModalComponent, {
        width: 'auto',
        height: 'auto',
        data: {
          url: await this.fileService.getSignedUrl(imageData),
          title: title
        }
      });

      dialogRef.afterClosed().subscribe(async result => {
      });
    }
  }

  async nextOrder(){
    this.indexOrder++;
    this.url  =  await this.fileService.getSignedUrl(this.delivery.image[this.indexOrder]);
  }

  async prevOrder(){
    this.indexOrder--;
    this.url  =  await this.fileService.getSignedUrl(this.delivery.image[this.indexOrder]);
  }
}
