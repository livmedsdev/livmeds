import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SessionService } from '../session.service';
import { UserRole } from '../enums/UserRole';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { FileService } from '../file.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { SignUpModalComponent } from '../sign-up-modal/sign-up-modal.component';
import { ModalPinValidationComponent } from '../modal-pin-validation/modal-pin-validation.component';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent {

  constructor(public session: SessionService) {
  }
}
