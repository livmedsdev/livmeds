import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session.service';
import { Router } from '@angular/router';
import { SseService } from '../sse.service';
import { NotifierService } from 'angular-notifier';
import { ToasterService } from 'angular2-toaster';
import { FileService } from '../file.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ShowImageModalComponent } from '../show-image-modal/show-image-modal.component';
import { MatDialog } from '@angular/material';
import { EventPollingService } from '../event-polling.service';

@Component({
  selector: 'app-deliveries-historical',
  templateUrl: './deliveries-historical.component.html',
  styleUrls: ['./deliveries-historical.component.scss']
})
export class DeliveriesHistoricalComponent implements OnInit {
   deliveryChat;
    isMinimized = false;
    private subNew;
    private subRefreshList;
    private pollingRefreshList;
    private subNewMessage;
    private count = {};
    private audio;

    constructor(
      private fileService: FileService,
      private router: Router,
      public dialog: MatDialog,
      public eventPolling: EventPollingService,
      private deliveryService: DeliveryService, public sessionService: SessionService) {

        this.audio = new Audio();
        this.audio.src = "../../../assets/sound/notification.mp3";
        this.audio.load();
    }

    async ngOnInit() {
      this.subNewMessage = this.eventPolling.sendChanges.subscribe((data) => {
        const key = data.deliveryId + '-' + data.typePrivateMessage;
        console.log(data);
        if (!this.deliveryChat || this.deliveryChat._id !== data.deliveryId) {
          if (this.count[key]) {
            this.count[key] += data.newMessages;
          } else {
            this.count[key] = data.newMessages;
          }
          this.audio.play();
        }
      });
      this.eventPolling.updatePolling();
    }

    async validDelivery(id) {
      this.router.navigate(['delivery-invoice'], {
        queryParams: {
          idDelivery: id
        }
      })
    }

    async viewInvoice(id) {
      this.router.navigate(['view-invoice'], {
        queryParams: {
          idDelivery: id
        }
      })
    }

    async viewDeliveryBill(id) {
      this.router.navigate(['delivery-bill'], {
        queryParams: {
          idDelivery: id
        }
      })
    }

    async refuseDelivery(id) {
      const response = await this.deliveryService.refuseDelivery(id);
    }


    ngOnDestroy() {
      clearInterval(this.pollingRefreshList);
      this.eventPolling.stopAllPolling();
    }

    openChat(delivery) {
      this.deliveryChat = delivery;
      this.count[delivery._id + '-TO_PHARMACY'] = 0;
    }

    closeChat() {
      this.deliveryChat = null;
    }


    minimized(data) {
      this.isMinimized = data;
    }

    async givePackage(deliveryId) {
      try {
        await this.deliveryService.givePackageToDeliverer(deliveryId);
      } catch (error) {
        console.log(error)
      }
    }



    async seeImage(title, imageData) {

      if(imageData && ((Array.isArray(imageData) && imageData.length > 0) || !Array.isArray(imageData))){
        const dialogRef = this.dialog.open(ShowImageModalComponent, {
          width: 'auto',
          height: 'auto',
          data: {
            // url: await this.fileService.getSignedUrl(imageData),
            imageData: (Array.isArray(imageData))?imageData:[imageData],
            title: title
          }
        });

        dialogRef.afterClosed().subscribe(async result => {
        });
      }
    }

  }

  export interface ImageDataForModal {
    title: String,
    url: String,
    imageData: String
  }
