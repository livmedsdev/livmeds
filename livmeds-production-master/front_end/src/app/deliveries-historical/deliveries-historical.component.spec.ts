import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveriesHistoricalComponent } from './deliveries-historical.component';

describe('DeliveriesHistoricalComponent', () => {
  let component: DeliveriesHistoricalComponent;
  let fixture: ComponentFixture<DeliveriesHistoricalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveriesHistoricalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveriesHistoricalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
