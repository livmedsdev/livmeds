import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NativeJsLibraryService {

  constructor() { }


  whenDefined(variable, callback) {
    const interval = 10; // ms
    window.setTimeout(() => {
      if (variable) {
        callback(variable);
      } else {
        this.whenDefined(variable, callback);
      }
    }, interval);
  }

  whenAvailable(name, callback) {
    const interval = 10; // ms
    window.setTimeout(() => {
      if (window[name]) {
        callback(window[name]);
      } else {
        this.whenAvailable(name, callback);
      }
    }, interval);
  }
}
