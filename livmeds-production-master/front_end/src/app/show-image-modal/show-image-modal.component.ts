import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ImageDataForModal } from '../deliveries/deliveries.component';
import { FileService } from '../file.service';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

@Component({
  selector: 'app-show-image-modal',
  templateUrl: './show-image-modal.component.html',
  styleUrls: ['./show-image-modal.component.scss']
})
export class ShowImageModalComponent implements OnInit {
  public url;
  public  indexOrder;

  constructor(
    public dialogRef: MatDialogRef<ShowImageModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ImageDataForModal,
    public fileService: FileService) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  async ngOnInit() {
    this.indexOrder = 0;
    this.url =  await this.fileService.getSignedUrl(this.data.imageData[this.indexOrder]);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async download(imageData) {
    const url = await this.fileService.downloadToBase64(imageData)

    let docDefinition = {
      content: [
        {
          image: url,
          style: "center"
        }
      ],

      styles: {
        center:
        {
          alignment: 'center'
        }

      }
    };
    pdfMake.createPdf(docDefinition).download();
  }

  async nextOrder(){
    this.indexOrder++;
    this.url  =  await this.fileService.getSignedUrl(this.data.imageData[this.indexOrder]);
  }

  async prevOrder(){
    this.indexOrder--;
    this.url  =  await this.fileService.getSignedUrl(this.data.imageData[this.indexOrder]);
  }
}
