import { Component, OnInit } from '@angular/core';
import { CardService } from '../card.service';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';

declare var Stripe;

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent implements OnInit {
  stripe = Stripe(environment.apiKeyStripe);
  card;
  private cardRegistration;
  private expirationDateYear = ['00','01','02','03','04','05','06','07','08','09',10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99];
  private  newCardFormGroup;

  constructor(
    private spinner: NgxSpinnerService,
    private cardService: CardService,
    private toasterService: ToasterService,
    private router: Router,
    private translate: TranslateService) { }

  async ngOnInit() {
    this.setupStripe();
    this.cardRegistration = (await this.cardService.askAddCardIntent())['client_secret'];
  }

  setupStripe() {
    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    this.card = elements.create('card', { style: style });

    this.card.mount('#card-element');

    this.card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    var form = document.getElementById('payment-form');
    form.addEventListener('submit', async event => {
      event.preventDefault();

      this.spinner.show();
      const cardSetup = await this.stripe.confirmCardSetup(
        this.cardRegistration,
        {
          payment_method: {
            card: this.card,
            billing_details: {
              name: "card",
            },
          },
        });
      const errorElement = document.getElementById('card-errors');
      if (cardSetup.error) {
        this.spinner.hide();
        errorElement.textContent = cardSetup.error.message;
        this.toasterService.pop('error', 'Erreur', 'Erreur lors de l\'ajout de votre carte bancaire');
      } else {
        console.log(cardSetup);
        this.spinner.hide();
        errorElement.textContent = "";
        this.toasterService.pop('success', 'Succès', 'Carte bancaire ajoutée !');
        this.router.navigate(['/cards']);
      }
    });
  }

}
