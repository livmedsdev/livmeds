
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PlanningDialogData } from '../interface/PlanningDialogData';
import { NeedDocDialogComponent } from '../need-doc-dialog/need-doc-dialog.component';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-planning-dialog',
  templateUrl: './planning-dialog.component.html',
  styleUrls: ['./planning-dialog.component.scss']
})
export class PlanningDialogComponent implements OnInit {
  readonly format = 'HH:mm';

  public lines: string[];

  constructor(
    public dialogRef: MatDialogRef<PlanningDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PlanningDialogData,
    private translate: TranslateService
    ) { }

  ngOnInit() {
    this.lines = [];
    const dayTrad = ['MONDAY', "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"]
    const dayPlanning = [undefined, undefined, undefined, undefined, undefined, undefined, undefined]

    if (this.data.openingTime) {

      const startSunday = this.data.openingTime['0Start'];
      const endSunday = this.data.openingTime['0End'];

      if (startSunday || endSunday) {
        dayPlanning[6] = { start: startSunday, end: endSunday };
      }

      for (let i = 1; i < 7; i++) {
        const start = this.data.openingTime[i + 'Start'];
        const end = this.data.openingTime[i + 'End'];
        if (start || end) {
          dayPlanning[i - 1] = { start, end };
        }
      }
    }

    for (let day in dayTrad) {
      let line = this.translate.instant(dayTrad[day]) + ': ';
      if (!dayPlanning[day]) {
        line += this.translate.instant('CLOSE');
      } else {
        line += ((dayPlanning[day].start) ? moment(dayPlanning[day].start).format(this.format) : this.translate.instant('NOT_FILLED')) + '-';
        line += ((dayPlanning[day].start) ? moment(dayPlanning[day].end).format(this.format) : this.translate.get('NOT_FILLED'));
      }
      this.lines.push(line);
    }
  }

  return(){
    this.dialogRef.close();
  }

}
