import { Injectable } from '@angular/core';

declare var google;

@Injectable({
  providedIn: 'root'
})
export class MapsService {
  
  public styles= {
    "light": new google.maps.StyledMapType(
      [
      {
        "featureType": "landscape",
        "stylers": [
          {
            "color": "#eef0f9"
          }
        ]
      },
      {
        "featureType": "landscape.natural.landcover",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#bde6c3"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi.business",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#a3aabf"
          }
        ]
      },
      {
        "featureType": "transit",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#b3d9fe"
          }
        ]
      }
    ]),
    "dark":new google.maps.StyledMapType(
      [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#212121"
          }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#212121"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#757575"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.country",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#40444f"
          }
        ]
      },
      {
        "featureType": "poi",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#181818"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#1b1b1b"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#2c2c2c"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#8a8a8a"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#292d38"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#20222b"
          }
        ]
      },
      {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#4e4e4e"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "transit",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#415ba3"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#3d3d3d"
          }
        ]
      }
    ])
  }

  constructor() { }

  renderPath(response, map, stepsInMemory) {
    console.log(response);
    for (let step of stepsInMemory) {
      step.setMap(null);
    }
    stepsInMemory = [];
    let bounds = new google.maps.LatLngBounds();
    let lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 4
    };
    let polylineOptions = [{
      strokeColor: '#D8418F',
      strokeWeight: 5
    }, {
      strokeColor: '#D8418F',
      strokeWeight: 5
    }];
    let legs = response.routes[0].legs;
    for (let i = 0; i < legs.length; i++) {
      let steps = legs[i].steps;
      for (let j = 0; j < steps.length; j++) {
        let nextSegment = steps[j].path;
        let stepPolyline = new google.maps.Polyline(i > 0 ? polylineOptions[1] : polylineOptions[0]);
        for (let k = 0; k < nextSegment.length; k++) {
          stepPolyline.getPath().push(nextSegment[k]);
          bounds.extend(nextSegment[k]);
        }
        stepPolyline.setMap(map);
        stepsInMemory.push(stepPolyline);
      }
    }
    return stepsInMemory;
  }

  renderPathWithoutMemory(response, map) {
    let bounds = new google.maps.LatLngBounds();
    let lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 4
    };
    let polylineOptions = [{
      strokeColor: '#66bb6a',
      strokeWeight: 5
    }, {
      strokeColor: '#66bb6a',
      strokeWeight: 5
    }];
    let legs = response.routes[0].legs;
    for (let i = 0; i < legs.length; i++) {
      let steps = legs[i].steps;
      for (let j = 0; j < steps.length; j++) {
        let nextSegment = steps[j].path;
        let stepPolyline = new google.maps.Polyline(i > 0 ? polylineOptions[1] : polylineOptions[0]);
        for (let k = 0; k < nextSegment.length; k++) {
          stepPolyline.getPath().push(nextSegment[k]);
          bounds.extend(nextSegment[k]);
        }
        stepPolyline.setMap(map);
      }
    }
  }


  retrieveAddressFromPlace(place) {
    const addressComponents = place.address_components

    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }


  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }
}
