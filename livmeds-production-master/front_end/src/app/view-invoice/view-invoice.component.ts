import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { FormBuilder, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FileService } from '../file.service';
import { MatDialog } from '@angular/material';
import { ShowImageModalComponent } from '../show-image-modal/show-image-modal.component';

@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.scss']
})
export class ViewInvoiceComponent implements OnInit {
  public delivery: any = {};
  public productFormGroup;
  public products: any[] = [];
  public totalAmountWithoutFee;

  // Orders
  public indexOrder;
  public url;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private fileService: FileService,
    public dialog: MatDialog,
    private deliveryService: DeliveryService) {
  }

  async ngOnInit() {
    this.indexOrder = 0;
    this.calculateTotalAmount();
    const queryParams = this.route.snapshot.queryParams;
    this.delivery = await this.deliveryService.get(queryParams.idDelivery);
    this.products = this.delivery.invoice.products
    this.calculateTotalAmount();
    if(this.delivery.image && this.delivery.image.length > 0){
      this.url = await this.fileService.getBase64Image(this.delivery.image[this.indexOrder]);
    }
  }

  addProduct() {
    this.products.push(this.productFormGroup.value);
    this.productFormGroup.reset();
    this.calculateTotalAmount();
  }

  calculateTotalAmount() {
    this.totalAmountWithoutFee = 0;
    for (let product of this.products) {
      if (!product.status || product.status !== 'DELETED') {
        this.totalAmountWithoutFee += (product.quantity * product.unitAmount)
      }
    }
    this.totalAmountWithoutFee = this.totalAmountWithoutFee.toFixed(2);
  }

  deleteProduct(product) {
    this.products.splice(this.products.indexOf(product), 1);
  }

  async seeImage(title, imageData) {
    if (imageData) {
      const dialogRef = this.dialog.open(ShowImageModalComponent, {
        width: 'auto',
        height: 'auto',
        data: {
          url: await this.fileService.getSignedUrl(imageData),
          title: title
        }
      });

      dialogRef.afterClosed().subscribe(async result => {
      });
    }
  }

  async nextOrder(){
    this.indexOrder++;
    this.url  =  await this.fileService.getSignedUrl(this.delivery.image[this.indexOrder]);
  }

  async prevOrder(){
    this.indexOrder--;
    this.url  =  await this.fileService.getSignedUrl(this.delivery.image[this.indexOrder]);
  }
}
