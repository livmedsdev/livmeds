import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs';

declare var messaging: any;

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  
  currentMessage = new BehaviorSubject(null);

  constructor(private angularFireMessaging: AngularFireMessaging) {
    // this.angularFireMessaging.messaging.subscribe(
    //   (_messaging) => {
    //     _messaging.onMessage = _messaging.onMessage.bind(_messaging);
    //     _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
    //     // _messaging.setBackgroundMessageHandler = _messaging.setBackgroundMessageHandler.bind(_messaging);

    //     // _messaging.setBackgroundMessageHandler((payload) =>{
    //     //   console.log("back new message received. ", payload);
    //     // })
    //   }
    // )

    this.angularFireMessaging.messaging.subscribe(
      (_messaging: any) => {
      // _messaging.onMessage = _messaging.onMessage.bind(_messaging);
      _messaging._next = (payload: any) => {
      console.log(payload);
      };
      _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
      );
   }

   requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
        this.currentMessage.next(payload);
      })
  }
}
