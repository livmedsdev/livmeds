import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandHistoricalComponent } from './command-historical.component';

describe('CommandHistoricalComponent', () => {
  let component: CommandHistoricalComponent;
  let fixture: ComponentFixture<CommandHistoricalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandHistoricalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandHistoricalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
