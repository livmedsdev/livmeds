import { Component, OnInit, ViewChild } from '@angular/core';
import { MatList, MatSelectionList } from '@angular/material';
import { DeliveryService } from '../delivery.service';
import { UserService } from '../user.service';

import * as moment from 'moment';
import { SessionService } from '../session.service';
import { FileService } from '../file.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-command-historical',
  templateUrl: './command-historical.component.html',
  styleUrls: ['./command-historical.component.scss']
})
export class CommandHistoricalComponent implements OnInit {

  @ViewChild("commandsList")
  private selectionList: MatList;

  commandsList: any = [];
  total: any;
  limit: any;
  page: any;

  constructor(private deliveryService: DeliveryService,
    private session: SessionService,
    private spinner: NgxSpinnerService,
    private fileService: FileService,
    private toasterService: ToasterService,
    private translate: TranslateService,
    private router: Router,
    private userService: UserService) { }

  async ngOnInit() {
    moment.locale(this.session.getPreferences().lang);
    this.limit = 5;
    this.page = 0;
    await this.refreshList();
  }

  async refreshAllList(){
    let limit = this.limit;
    limit = (this.page+1)*this.limit

    let resp: any = await this.deliveryService.getMyDeliveries({page: 0, limit});
    this.total = resp.total;
    const data = [];
    for(let delivery of resp.data){
      delivery.merchantFavoriteAddress = this.userService.getFavoriteAddress(delivery.merchant.addresses);
      delivery.formatDate = moment(delivery.insertAt).format('Do MMM YYYY, HH:mm');
      const utcDate = moment.utc(delivery.deliveryDate).toDate();
      delivery.formatDeliveryDate = moment(utcDate).format('Do MMM YYYY, HH:mm');
      delivery.urlAvatar = (delivery.merchant.avatar) ? await this.fileService.getSignedUrl(delivery.merchant.avatar) : undefined;

      data.push(delivery);
    }

    this.commandsList = data;
  }

  async refreshList(){
    let resp: any = await this.deliveryService.getMyDeliveries({page: this.page, limit: this.limit});
    this.total = resp.total;
    const data = [];
    for(let delivery of resp.data){
      delivery.merchantFavoriteAddress = this.userService.getFavoriteAddress(delivery.merchant.addresses);
      delivery.formatDate = moment(delivery.insertAt).format('Do MMM YYYY, HH:mm');
      const utcDate = moment.utc(delivery.deliveryDate).toDate();
      delivery.formatDeliveryDate = moment(utcDate).format('Do MMM YYYY, HH:mm');
      delivery.urlAvatar = (delivery.merchant.avatar) ? await this.fileService.getSignedUrl(delivery.merchant.avatar) : undefined;

      data.push(delivery);
    }

    this.commandsList = this.commandsList.concat(data);
  }

  async loadMore() {
    this.spinner.show()
    this.page++;
    await this.refreshList();
    this.spinner.hide()
  }

  async cancel(delivery){
    try{
      const response : any = await this.deliveryService.cancelDelivery(delivery._id);
      this.toasterService.pop('success', 'Succès', this.translate.instant(response.tradCode));
      this.refreshAllList();
    }catch(error){
      this.toasterService.pop('error', 'Erreur lors de votre demande', this.translate.instant(error.error.tradCode));
    }
  }

  async showOnMap(delivery){
    this.router.navigate(['/track'], {queryParams:{command: delivery._id}})
  }
}
