import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsPartnerComponent } from './documents-partner.component';

describe('DocumentsPartnerComponent', () => {
  let component: DocumentsPartnerComponent;
  let fixture: ComponentFixture<DocumentsPartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsPartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
