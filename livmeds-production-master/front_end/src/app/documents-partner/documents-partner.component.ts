import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'angular2-toaster';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { FileService } from '../file.service';
import { SessionService } from '../session.service';
import { UserService } from '../user.service';

declare var SignaturePad;
@Component({
  selector: 'app-documents-partner',
  templateUrl: './documents-partner.component.html',
  styleUrls: ['./documents-partner.component.scss']
})
export class DocumentsPartnerComponent implements OnInit {
  documentsFormGroup;
  ribUrl;
  kbisUrl;
  ciFrontUrl;
  ciBackUrl;
  criminalRecordUrl;
  contract;
  signaturePad;
  env;

  @ViewChild('canvasElement') canvasElement: ElementRef;

  constructor(public session: SessionService,
    public dialog: MatDialog,
    private fileService: FileService,
    private spinner: NgxSpinnerService,
    private toasterService: ToasterService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer,
    private router: Router,
    public userService: UserService) {
    this.documentsFormGroup = new FormBuilder().group({
      rib: ['', Validators.compose([Validators.required])],
      kbis: ['', Validators.compose([Validators.required])],
    });
    this.env = environment.testingApi;
  }


  async ngOnInit() {
    this.spinner.show();
    // if (this.session.currentUser().status === 'ACTIVE') {
      const response = await this.userService.getProfile();
      this.documentsFormGroup.patchValue(response);
      for (let property in this.documentsFormGroup.value) {
        if (response[property]) {
          console.log(property)
          this[property + 'Url'] = await this.fileService.getBase64Image(response[property]);
        }
      }
    //   if (!environment.testingApi) {
    //     try {
    //       this.contract = this.sanitizer.bypassSecurityTrustResourceUrl(await this.userService.getMyContract());
    //     } catch (error) {
    //       this.contract = null;
    //     }
    //   }
    // } else {
      this.contract = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(await this.userService.generateUrlContract()));
    // }

    this.spinner.hide();
  }

  ngAfterViewInit() {

    let canvas = this.canvasElement.nativeElement;
    this.signaturePad = new SignaturePad(canvas);
    let ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    this.signaturePad.clear(); // otherwise isEmpty() might return incorrect value
  }

  async changeFile(event, tradCode, field) {
    const selectedFile = event.target.files[0];

    if (selectedFile) {
      try {
        this.spinner.show();
        const uploadData = new FormData();
        uploadData.append('file', selectedFile, selectedFile.name);
        let response = await this.fileService.uploadForm(uploadData, 'document');
        let params = {};
        params[field] = response['idFile']
        this.documentsFormGroup.patchValue(params);
        this[field + 'Url'] = await this.fileService.getBase64Image(response['idFile']);
        this.spinner.hide();
      } catch (error) {
        console.log(error);
        this.spinner.hide();
      }
    }
  }

  async sign() {
    if (this.documentsFormGroup.valid) {
      try {
        this.spinner.show();
        await this.userService.sign();
        let data = this.documentsFormGroup.value;
        data.signImage = this.signaturePad.toDataURL();
        await this.userService.sendDocuments(data);
        await this.userService.getProfile();
        this.spinner.hide();

        this.toasterService.pop('success', 'Succès', await this.translate.get("Votre compte a été validé, vous pouvez maintenant utiliser pleinement l'application !").toPromise());
        this.router.navigateByUrl('/deliveries')
        // const dialogRef = this.dialog.open(ModalPinValidationComponent, {
        //   width: 'auto',
        //   height: 'auto',
        //   data
        // });

        // dialogRef.afterClosed().subscribe(async result => {
        //   console.log('The dialog was closed');

        // });
      } catch (error) {
        console.log(error);
        this.spinner.hide();
        this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
      }
    }
  }

  clearCanvas() {
    this.canvasElement.nativeElement.width = this.canvasElement.nativeElement.width;
  }

  async update() {
    if (this.documentsFormGroup.valid) {
      try {
        const params = this.documentsFormGroup.value;
        await this.userService.updateDocuments(params);
        await this.userService.getProfile();
        this.toasterService.pop('success', 'Succès', await this.translate.get('SUCCESS_UPDATE_DOCUMENTS').toPromise());
      } catch (error) {
        console.log(error);
        this.toasterService.pop('error', 'Erreur', await this.translate.get('FAIL_UPDATE_DOCUMENTS').toPromise());
      }
    }
  }
}
