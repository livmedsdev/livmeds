import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public messageFormGroup;

  constructor(
  private contactService: ContactService,
  private toastService: ToasterService,
  private translate: TranslateService) {
      this.messageFormGroup = new FormBuilder().group({
  		name: ['', Validators.compose([Validators.required])],
  		mail: ['', Validators.compose([Validators.required])],
  		subject: [null, Validators.compose([Validators.required])],
  		message: [null, Validators.compose([Validators.required])]
  	  });
  	  }

  ngOnInit() {
  }

     async submitContact(){
  	   try{
  		let response : any = await this.contactService.createMessage(this.messageFormGroup.value);
  		this.toastService.pop('success', 'Succès', await this.translate.get(response.tradCode).toPromise());
  	   }catch(error){
  		   console.log(error);
  		this.toastService.pop('error', 'Erreur', 'Erreur lors de l\'envoi de votre message');
  	   }
     }

}
