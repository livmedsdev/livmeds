import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginModalComponent } from '../login-modal/login-modal.component';
import { DialogData } from '../app.component';
import { UserRole } from '../enums/UserRole';
import { UserService } from '../user.service';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../session.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

declare var window: any;
declare var $: any;
declare var google;

@Component({
  selector: 'app-sign-up-modal',
  templateUrl: './sign-up-modal.component.html',
  styleUrls: ['./sign-up-modal.component.scss']
})
export class SignUpModalComponent implements OnInit {

  public registerFormGroup;
  public confirmPassword;
  public address;
  public role;

  public categories = [{
    label: 'ROLE_PHARMACY',
    value: 'ROLE_PHARMACY'
  }, {
    label: 'ROLE_OPTICIAN',
    value: 'ROLE_OPTICIAN'
  }, {
    label: 'ROLE_VETERINARY',
    value: 'ROLE_VETERINARY'
  }]

  @ViewChild("address")
  public addressElement: ElementRef;

  @ViewChild("addressPharmacy")
  public addressPharmacyElement: ElementRef;

  private iti;

  constructor(
    private translate: TranslateService,
    private userService: UserService,
    public session: SessionService,
    public router: Router,
    private toasterService: ToasterService,
    public dialogRef: MatDialogRef<SignUpModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    (window as any).fbq('trackCustom', 'Inscription marchand');
    this.role = this.data.role;

    this.registerFormGroup = new FormBuilder().group({
      mail: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      phoneNumber: [null, Validators.compose([Validators.required])],
      siret: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      passwordConfirm: [null, Validators.compose([Validators.required])],
      responsibleLastname: [null, Validators.compose([Validators.required])],
      responsibleName: [null, Validators.compose([Validators.required])],
      role: [null, Validators.compose([Validators.required])],
      favoriteAddressDisplay: [null, Validators.compose([Validators.required])],
      favoriteAddress: [null, Validators.compose([Validators.required])]
    },
      { updateOn: "blur" });
  }

  fieldsRegisterFrm(field) {
    return this.registerFormGroup.get(field);
  }

  ngAfterViewInit() {

    const input = document.querySelector("#phone-input");
    if (input) {
      this.iti = window.intlTelInput(input, {
        autoPlaceholder: "aggressive",
        utilsScript: "./assets/js/utils-8.4.6.js",
        initialCountry: "fr"
      });
    }

    if (this.addressElement) {
      let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
      autocomplete.setComponentRestrictions({ 'country': ['fr'] });
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        let place = autocomplete.getPlace();
        this.registerFormGroup.patchValue({ favoriteAddress: this.retrieveAddressFromPlace(place) });
      });
    }

    if (this.addressPharmacyElement) {
      let autocompletePharmacy = new google.maps.places.Autocomplete(this.addressPharmacyElement.nativeElement);
      autocompletePharmacy.setComponentRestrictions({ 'country': ['fr'] });
      google.maps.event.addListener(autocompletePharmacy, 'place_changed', () => {
        let place = autocompletePharmacy.getPlace();
        this.registerFormGroup.patchValue({ favoriteAddress: this.retrieveAddressFromPlace(place) });
      });
    }
  }

  async becomeDeliverer(event) {
    // this.user.role = (this.user.becomeDeliverer) ? UserRole.ROLE_DELIVERER : UserRole.ROLE_CUSTOMER
  }

  async submit() {
    // if (this.role) {
    //   this.user.role = (this.user.becomeDeliverer) ? UserRole.ROLE_DELIVERER : UserRole.ROLE_CUSTOMER
    // }
    if (this.registerFormGroup.valid) {
      try {
        this.registerFormGroup.patchValue({
          phoneNumber: this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164)
        })
        await this.userService.register(this.registerFormGroup.value);
        $('.modal').modal('hide');
        this.toasterService.pop('success', 'Compte créé avec succès !', 'Votre compte a bien été créé');
        this.dialogRef.close(this.registerFormGroup.value);

        if (this.session.currentUser().status === 'AWAITING_VALIDATION' && (this.session.currentUser().role === UserRole.ROLE_OPTICIAN
          || this.session.currentUser().role === UserRole.ROLE_VETERINARY
          || this.session.currentUser().role === UserRole.ROLE_PHARMACY)) {
          this.router.navigate(['/documents']);
        }
      } catch (error) {
        console.log(error)
        this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
      }
    }else{
      console.log(this.registerFormGroup.errors)
      this.toasterService.pop('error', 'Erreur', await this.translate.get("INVALID_PARAMS").toPromise());
    }
    // if (this.user) {
    //   if (this.user.role !== UserRole.ROLE_DELIVERER || this.user.role !== UserRole.ROLE_CUSTOMER) {

    // } else {
    //   try {
    //     await this.userService.register(this.user);
    //     $('.modal').modal('hide');
    //     this.toasterService.pop('success', 'Compte créé avec succès !', 'Votre compte a bien été créé');
    //     this.dialogRef.close(this.user);
    //   } catch (error) {
    //     console.log(error)
    //     this.toasterService.pop('error', 'Erreur', await this.translate.get(error.error.tradCode).toPromise());
    //   }
    // }

  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }
}
