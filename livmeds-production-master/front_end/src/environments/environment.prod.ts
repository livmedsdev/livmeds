export const environment = {
  production: true,
  testingApi: false,
  url: "__URL_ENV_PROD__",
  firebase: {
    apiKey: "AIzaSyASgLidxwDZBhCLby0w4rq4QYryAMJzl5w",
    authDomain: "livmeds-196da.firebaseapp.com",
    databaseURL: "https://livmeds-196da.firebaseio.com",
    projectId: "livmeds-196da",
    storageBucket: "livmeds-196da.appspot.com",
    messagingSenderId: "290515115712",
    appId: "1:290515115712:web:00434ebe01cefa8c4e1725",
    measurementId: "G-1JGGJGXGDE"
  },
  apiKeyStripe: '__KEY_STRIPE__',
  publicBucketUrl: 'https://livmeds-public.s3.eu-west-3.amazonaws.com/',
};
