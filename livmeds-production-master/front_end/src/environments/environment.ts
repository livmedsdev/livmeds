// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  testingApi: true,
  //url: "http://192.168.1.74:3001"
  //url: "http://192.168.43.52:3001"
  //url: "http://localhost:3002",
  url: "https://api-livmeds.1000geeks.com",
  // url: "https://livmeds.com:8443",
  //url: "http://localhost:3002",
  //  url: "https://test.livmeds.com/api",
  // url: "https://livmeds.com:8443",
  //url: "http://localhost:3002",
   //  url: "https://test.livmeds.com/api",
  // url: "https://api.livmeds.com",
  // url: "https://delimeds.ddns.net:8443",
  firebase: {
    apiKey: "AIzaSyASgLidxwDZBhCLby0w4rq4QYryAMJzl5w",
    authDomain: "livmeds-196da.firebaseapp.com",
    databaseURL: "https://livmeds-196da.firebaseio.com",
    projectId: "livmeds-196da",
    storageBucket: "livmeds-196da.appspot.com",
    messagingSenderId: "290515115712",
    appId: "1:290515115712:web:00434ebe01cefa8c4e1725",
    measurementId: "G-1JGGJGXGDE"
  },
  apiKeyStripe: 'pk_test_74ZT4NrrWbM0vHoY24RGG08i00rUAZAKSn',
  publicBucketUrl: 'https://livmeds-public.s3.eu-west-3.amazonaws.com/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
