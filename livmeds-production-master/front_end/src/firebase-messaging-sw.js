// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.14.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  apiKey: "AIzaSyASgLidxwDZBhCLby0w4rq4QYryAMJzl5w",
  authDomain: "livmeds-196da.firebaseapp.com",
  databaseURL: "https://livmeds-196da.firebaseio.com",
  projectId: "livmeds-196da",
  storageBucket: "livmeds-196da.appspot.com",
  messagingSenderId: "290515115712",
  appId: "1:290515115712:web:00434ebe01cefa8c4e1725",
  measurementId: "G-1JGGJGXGDE"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();