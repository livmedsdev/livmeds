upstream backend_api{
    server external-api.livmeds.com:8443;
}

server {
	listen 80 default_server;
	server_name _;

    location / {
	    return 301 https://$host$request_uri;
    }
}

server {
    listen 443 http2 ssl default_server;
    server_name  localhost;
    ssl_certificate /etc/nginx/certs/local_server.pem;
    ssl_certificate_key /etc/nginx/certs/local_server_key.pem;
    
    charset UTF-8;
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
    add_header Content-Security-Policy "base-uri 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://apis.google.com https://js.stripe.com https://maps.google.com https://maps.googleapis.com https://connect.facebook.net https://www.facebook.com https://www.googletagmanager.com https://client.crisp.chat https://sc-static.net https://www.googleadservices.com https://www.google-analytics.com https://googleads.g.doubleclick.net https://consent.cookiebot.com https://consentcdn.cookiebot.com https://appleid.cdn-apple.com;";

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;
    gzip_min_length 500;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html?$query_string;
        http2_push_preload on;

        http2_push /assets/css/style.css;
        http2_push /assets/css/responsive.css;
        http2_push /assets/css/animate.css;
    }
}

server {
    listen 8443 ssl;
    server_name  localhost;
    ssl_certificate /etc/nginx/certs/local_server.pem;
    ssl_certificate_key /etc/nginx/certs/local_server_key.pem;

    location / {
        proxy_pass https://backend_api;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}