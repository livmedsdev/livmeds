#!/bin/sh
set -e

echo $API_URL
url="$(echo "$API_URL" | sed 's/\//\\\//g')"
echo $url
echo "s/__URL_ENV_PROD__/$url"
sed -i -e "s/__URL_ENV_PROD__/$url/g" /usr/share/nginx/html/main*.js > /usr/share/nginx/html/main*.js
sed -i -e "s/__KEY_STRIPE__/$KEY_STRIPE/g" /usr/share/nginx/html/main*.js > /usr/share/nginx/html/main*.js