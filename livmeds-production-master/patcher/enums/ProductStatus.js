const PromoCodeStatus = {
    ACTIVE: "ACTIVE",
    DELETED: "DELETED"
};

module.exports = PromoCodeStatus;