const PaymentStatus = {
    SUCCESS: "SUCCESS",
    IN_PROGRESS: "IN_PROGRESS",
    FAILED: "FAILED"
};

module.exports = PaymentStatus;