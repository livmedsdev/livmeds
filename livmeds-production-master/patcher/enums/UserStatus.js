const UserStatus = {
    ACTIVE: "ACTIVE",
    AWAITING_VALIDATION: "AWAITING_VALIDATION",
    BLOCKED: "BLOCKED",
    DELETED: "DELETED"
};

module.exports = UserStatus;