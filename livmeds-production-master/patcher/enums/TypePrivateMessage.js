const TypePrivateMessage = {
    TO_PHARMACY: "TO_PHARMACY",
    TO_DELIVERER: "TO_DELIVERER"
};

module.exports = TypePrivateMessage;