const BonusStatus = {
    ENABLED: "ENABLED",
    DISABLED: "DISABLED"
};

module.exports = BonusStatus;