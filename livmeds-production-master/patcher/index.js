require('dotenv').config()
const mongoose = require('mongoose');
const fs = require('fs');

console.log("PATCHER SERVICE V1")

const configBdd = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    user: process.env.USER_DB,
    pass: process.env.PASSWORD_DB,
    dbName: process.env.NAME_DB
};

let connect;

if (process.env.NODE_ENV === 'PROD') {
    connect = mongoose.connect(
        'mongodb+srv://' + process.env.HOST_DB + '?retryWrites=true&w=majority', configBdd)
}else {
   connect = mongoose.connect('mongodb://' + process.env.USER_DB + ':' + process.env.PASSWORD_DB +
    '@' + process.env.HOST_DB + ':' + process.env.PORT_DB + '/' + process.env.NAME_DB
    + '?authSource=admin', configBdd)
}

connect.then(async () => {
    console.log("connected")
    try{
        const PatcherService = require('./service/PatcherService');
        const patcherService = new PatcherService();
    
        // await patcherService.trimUserInfo();

        // await patcherService.addLocationForMerchant();

        await patcherService.updateCustomerStripe();

        console.log("---------")
        console.log("FINI")
    }catch(error){
        console.log(error)
        process.exit(1);
    }
    process.exit(0);
})
    .catch((error) => {
        console.log("ERROR with database connection : " + error)
        process.exit(1);
    });
