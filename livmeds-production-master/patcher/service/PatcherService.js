const models = require('../models/index.js');
const stripe = require('stripe')(process.env.STRIPE_API_KEY);

// Class contains logic to create default data in database
module.exports = class {

    async addLocationForMerchant() {
        const users = await models.User.find({ role: { $in: ["ROLE_OPTICIAN", "ROLE_PHARMACY", "ROLE_VETERINARY"] } })
            .populate("addresses")
            .lean()
            .exec();
        console.log("IN PROGRESS ADD LOCATION...")
        for (let user of users) {
            const addr = this.getFavoriteAddress(user.addresses);
            if (addr) {
                const location = { type: 'Point', coordinates: [addr.longitude, addr.latitude] }
                await models.User.updateOne({ _id: user._id }, { location: location }).exec();
            }
        }

        console.log("FINISH ADD LOCATION !")
    }

    async updateCustomerStripe() {
        const users = await models.User.find()
            .populate('addresses')
            .lean()
            .exec();

        console.log("IN PROGRESS UPDATE CUSTOMER STRIPE...")
        for (let user of users) {
            let name = user.lastName + ' ' + user.name;
            if (["ROLE_OPTICIAN", "ROLE_PHARMACY", "ROLE_VETERINARY"].includes(user.role)) {
                name = user.name;
            }

            if (!user.customerIdPayment) {
                continue;
            }

            const addr = this.getFavoriteAddress(user.addresses);
            try{

                if (addr) {
                    await stripe.customers.update(
                        user.customerIdPayment,
                        {
                            email: user.mail,
                            address: {
                                city: addr.city,
                                line1: addr.fullAddress
                            },
                            name: name,
                            phone: user.phoneNumber,
                            metadata: {
                                role: user.role
                            },
                            shipping: {
                                address: {
                                    city: addr.city,
                                    line1: addr.fullAddress
                                },
                                name: name,
                                phone: user.phoneNumber,
                            }
                        }
                    );
                } else {
                    await stripe.customers.update(
                        user.customerIdPayment,
                        {
                            email: user.mail,
                            name: name,
                            phone: user.phoneNumber,
                            metadata: {
                                role: user.role
                            }
                        }
                    );
                }
            }catch(error){
                console.log(user.mail + '\n');
            }

            await new Promise(resolve => setTimeout(resolve, 100));
        }

        console.log("FINISH UPDATE CUSTOMER STRIPE !")
    }


    getFavoriteAddress(addresses) {
        for (let address of addresses) {
            if (address.favoriteAddress) {
                return address;
            }
        }
        return null;
    }

    async trimUserInfo() {
        console.log("IN PROGRESS TRIM USER INFO...")
        const users = await models.User.find({}).exec();
        let nb = 0;
        for (let user of users) {
            const params = {};
            // Name
            params.name = this.formatString(user.name);
            params.name = this.firstCharUpperCase(params.name);

            // Mail
            params.mail = this.formatString(user.mail);

            // Lastname
            if (user.lastName) {
                params.lastName = this.formatString(user.lastName);
                params.lastName = this.firstCharUpperCase(params.lastName);
            }

            // responsibleName
            if (user.responsibleName) {
                params.responsibleName = this.formatString(user.responsibleName)
                params.responsibleName = this.firstCharUpperCase(params.responsibleName);
            }

            // responsibleLastname
            if (user.responsibleLastname) {
                params.responsibleLastname = this.formatString(user.responsibleLastname);
                params.responsibleLastname = this.firstCharUpperCase(params.responsibleLastname);
            }


            // console.log("Avant");
            // console.log(user)
            // console.log("Après");
            // console.log(params)
            // console.log("------")
            try {
                await models.User.updateOne({ _id: user._id }, params).exec();
            } catch (error) {
                console.log(error);
                console.log("NB : " + nb);
                return;
            }
            nb++;
        }
        console.log("FINISH TRIM USER INFO !")
    }

    formatString(str) {
        if (str && (typeof str === "string")) {
            return str.trim();
        } else {
            return str;
        }
    }

    firstCharUpperCase(str) {
        if (str && (typeof str === "string")) {
            return this.capitalize(str)
        } else {
            return str;
        }
    }

    capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}