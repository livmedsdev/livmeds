require('dotenv').config()
const defaultData = require('./default-data');
const mongoose = require('mongoose');
console.log(process.env.USER_DB)

const configBdd = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    user: process.env.USER_DB,
    pass: process.env.PASSWORD_DB,
    dbName: process.env.NAME_DB
};

mongoose.connect('mongodb://' + process.env.USER_DB + ':' + process.env.PASSWORD_DB +
    '@' + process.env.HOST_DB + ':' + process.env.PORT_DB + '/' + process.env.NAME_DB
    + '?authSource=admin', configBdd)
    .then(() => {
        console.log("connected")
        defaultData.clear().then(async () => {
            console.log("Start initialization")
            await defaultData.save();
            console.log("Database initialized");
            process.exit(0);
        });
    })
    .catch((error) => {
        console.log("ERROR with database connection : " + error)
        process.exit(1);
    });
