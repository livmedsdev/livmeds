const models = require('./models/index.js');
const defaultData = require('./default-data.json');
const bcrypt = require('bcrypt');
const voucher_codes = require('voucher-code-generator');

const PaymentService = require('../back_end/app/utils/PaymentService');

// Class contains logic to create default data in database
module.exports = {
    clear: () => {
        let listPromise = [];
        Object.keys(models).forEach((key, index) => {
            listPromise.push(models[key].deleteMany({}));
        });

        return Promise.all(listPromise);
    },
    save: async () => {
        for (let config of defaultData.configs) {
            let obj = new models.Config(config);
            await obj.save();
        }
        for (let product of defaultData.products) {
            let productObj = new models.Product(product);
            await productObj.save();
        }

        for (let category of defaultData.categories) {
            let categoryObj = new models.Category(category);
            await categoryObj.save();
        }

        for(let type of defaultData.typesDeliverer){
            const typeObj = new models.TypeDeliverer(type);
            await typeObj.save();
        }


        let addresses = {};
        for (let user of defaultData.addresses) {
            let addressObj = new models.Address(user);
            let addressCreated = await addressObj.save();
            addresses[addressObj._id] = addressCreated;
        }
        let listPromiseUser = [];
        for (let user of defaultData.users) {
            let userObj = new models.User(user);
            // userObj.referralCode = (voucher_codes.generate({
            //     length: 6,
            //     count: 1,
            //     charset: voucher_codes.charset("alphabetic"),
            //     prefix: "PARRAIN-",
            // }))[0].toUpperCase();
            userObj.addresses = [];
            listPromiseUser.push(new Promise(async (resolve, reject) => {
                userObj.password = await bcrypt.hash(userObj.password, parseInt(process.env.SALTROUNDS));
                for (let address of user.addresses) {
                    userObj.addresses.push(addresses[address]);
                }

                const paymentService = new PaymentService();
                const userCreated = await paymentService.createUser(userObj);
                userObj.customerIdPayment = (userCreated)?userCreated.id:null;
                const userInDb = await userObj.save();
                await paymentService.updateUserWithoutAddr(userInDb);
                

                resolve();
            }))
        }
        await Promise.all(listPromiseUser);
    }
}