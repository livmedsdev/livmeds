// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //url: "http://192.168.1.74:3001"
  //url: "http://192.168.1.23:3001"
  // url: "http://localhost:3002",
  //url: "http://localhost:8080",
  //url: "https://test.livmeds.com/api",
  url: "https://api-livmeds.1000geeks.com",
  //url: "https://api.livmeds.com",
  // url: 'http://10.0.2.2:8080',
  // url: "https://livmeds.com:8443",
  urlFront: "https://livmeds.com",
  // emulator Android
  // url: "http://10.0.2.2:3002",
  apiKeyStripe: 'pk_test_74ZT4NrrWbM0vHoY24RGG08i00rUAZAKSn',
  publicBucketUrl: 'https://livmeds-public.s3.eu-west-3.amazonaws.com/',
  iosAppId: '1510261512',
  devKeyIosAppFlyer: 'wPDRJMwCreSuVXSiNcgqMm',
  cometChatAppId: '189189345be260af',
  cometChatRegion: 'eu'
};

/*s
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
