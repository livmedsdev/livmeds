import { Component, ViewChild, ElementRef, enableProdMode } from '@angular/core';

import { Platform, NavController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MainEventService } from './main-event.service';
import { UserService } from './user.service';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { SessionService } from './session.service';
import { FileService } from './file.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { UserRole } from './enums/UserRole';
import { User } from './models/User';
import { Camera, CameraSource, CameraResultType, ImageOptions } from '@capacitor/camera';
import * as jsonFr from '../assets/i18n/fr.json';
import { DeliveryService } from './delivery.service';
import { Appsflyer } from '@ionic-native/appsflyer/ngx';
import { environment } from 'src/environments/environment';
import { AppVersion } from '@ionic-native/app-version/ngx';
import * as compareVersions from 'compare-versions'
import { Facebook } from '@ionic-native/facebook/ngx';
import { Device } from '@ionic-native/device/ngx';
import { CometChat } from "@cometchat-pro/cordova-ionic-chat"
import { SplashScreen } from '@capacitor/splash-screen';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { TchatService } from './tchat.service';
import { FirebaseAnalytics } from "@capacitor-community/firebase-analytics";

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  readonly UserRole = UserRole;
  readonly currency = 'EUR';
  hideTopBar: boolean;
  hideModal: boolean = true;
  subscribeDisplayTopBar;
  subHideModalLoader;
  subHideSidebar;
  subChangeTitle;
  subChangeRole;
  subHideMenuButton;
  subSetBackButtonUrl;
  mail;
  name;
  title;
  routerLinkAddress;
  backUrl = null;
  role;
  hideMenuButton;

  // Wait customer
  subWaitCustomer;
  subStopCounter;
  intervalWaitCustomer;
  timeLeft: number = 600;
  minuteLeft = 10;
  secondLeft;

  @ViewChild('sideBarMenu') sideBarMenuElement: ElementRef;

  constructor(
    private appVersion: AppVersion,
    private appsflyer: Appsflyer,
    private platform: Platform,
    private deliveryService: DeliveryService,
    public router: NavController,
    private _eventEmiter: MainEventService,
    private userService: UserService,
    private translate: TranslateService,
    public session: SessionService,
    private device: Device,
    private routerAngular: Router,
    private fileService: FileService,
    private backgroundMode: BackgroundMode,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private facebook: Facebook
  ) {
    this.translate.setTranslation('fr', jsonFr, true);
    registerLocaleData(localeFr, 'fr');
    this.initializeApp();
  }

  async ngOnInit() {
    console.log("onInit")
    this.logInfo();
    console.log("onInit - PLATFORM - READY")
    const versions: any = await this.userService.getVersions();
    let validVersion = true;
    this.session.setAppVersion(await this.appVersion.getVersionNumber());
    if (this.platform.is('ios')) {
      validVersion = compareVersions(versions.versionIosMin, await this.appVersion.getVersionNumber()) !== 1
    } else if (this.platform.is('android')) {
      validVersion = compareVersions(versions.versionAndroidMin, await this.appVersion.getVersionNumber()) !== 1
    }

    if (!validVersion) {
      const alert = await this.alertController.create({
        header: 'La version de votre application est obsolète. Veuillez mettre à jour votre version en allant sur le store de votre mobile.',
        buttons: [
          {
            text: this.translate.instant('GO_TO_STORE'),
            handler: () => {
              if (this.platform.is('ios')) {
                window.open('itms-apps://itunes.apple.com/app/id1510261512', '_system');
              }
              else if (this.platform.is('android')) {
                window.open('market://details?id=com.livmeds', '_system');
              }
              return false;
            }
          }
        ],
        backdropDismiss: false
      });
      alert.present();
    }
    // });

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.setDefaultLang(event.lang); // <----- Why resetting the default on language change?
    });

    if (this.session.getPreferences().enableTutorial === undefined || this.session.getPreferences().enableTutorial === null) {
      this.session.setEnableTutorial('true');
    }

    if (this.session.getPreferences().lang === undefined || this.session.getPreferences().lang === null) {
      this.session.setLang('fr');
    }
    this.translate.setDefaultLang('fr');
    this.translate.use('fr');

    this.role = localStorage.getItem('role');
  }

  async initializeApp() {
    this.subscribeDisplayTopBar = this._eventEmiter.hideTopBar.subscribe(data => {
      this.hideTopBar = data;
      console.log("TOP BAR: change to : " + data)
    });
    this.subHideModalLoader = this._eventEmiter.hideModalLoader.subscribe(data => {
      this.hideModal = data
      console.log("MODAL : change to : " + data)
    });
    this.subChangeTitle = this._eventEmiter.title.subscribe(data => this.title = data);
    this.subChangeRole = this._eventEmiter.roleChange.subscribe(data => this.role = data);
    this.subHideMenuButton = this._eventEmiter.hideMenuButton.subscribe(data => this.hideMenuButton = data);
    this.subHideSidebar = this._eventEmiter.hideSideBar.subscribe(data => data ? this.closeSideBarMenu() : this.openSideBarMenu());
    this.subSetBackButtonUrl = this._eventEmiter.backButtonUrl.subscribe(data => data !== null ? this.backUrl = data : this.backUrl = null)
    this.subWaitCustomer = this._eventEmiter.startWaitCustomer.subscribe(data => {
      this.intervalWaitCustomer = setInterval(() => {
        if (this.timeLeft > 0) {
          this.timeLeft--;
          console.log(this.timeLeft)
          this.minuteLeft = Math.floor(this.timeLeft / 60);
          this.secondLeft = this.timeLeft - (this.minuteLeft * 60);
          this._eventEmiter.sendCounter({
            minute: this.minuteLeft,
            second: this.secondLeft
          })
        } else {
          this._eventEmiter.sendFinishWaitCustomer();
          this.timeLeft = 600;
          this.minuteLeft = 10;
          clearInterval(this.intervalWaitCustomer);
        }
      }, 1000)
    });

    this.subStopCounter = this._eventEmiter.stopCounter.subscribe(data => {
      this.timeLeft = 600;
      this.minuteLeft = 10;
      clearInterval(this.intervalWaitCustomer);
    })
    this.backgroundMode.enable();
    FirebaseAnalytics.setCollectionEnabled({enabled: true});

    this.appsflyer.initSdk({
      isDebug: false,
      devKey: environment.devKeyIosAppFlyer,
      appId: environment.iosAppId,
      waitForATTUserAuthorization: 10
    });


    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: Token) => {
        // alert('Push registration success, token: ' + token.value);
        this.session.setTokenFcm(token.value);
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        // alert('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      async (notification: PushNotificationSchema) => {
        // alert('Push received: ' + JSON.stringify(notification));
        const data = notification.data;
        if (data.notificationType === 'NEW_MESSAGES') {
          if (!this.routerAngular.url.includes("tchat")) {
            this.session.setPrivateMessageReceived(data.delivery, data.typePrivateMessage, data.receiver);
            this._eventEmiter.sendNewMessageEvent(data);
          }
        }
        if (data.notifcationType === 'MERCHANT_VALIDATED_AMOUNT') {
          if (data.id) {
            try {
              const delivery = await this.deliveryService.getDeliveryByIdWithCustomer(data.id);
              localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(delivery));
            } catch (error) {
              console.log(error);
            }
          }
        }
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      async (notification: ActionPerformed) => {
        // alert('Push action performed: ' + JSON.stringify(notification));
        const data = notification.notification.data;
        if (data.notificationType === 'NEW_MESSAGES') {
          if (!this.routerAngular.url.includes("tchat")) {
            this.session.setPrivateMessageReceived(data.delivery, data.typePrivateMessage, data.receiver);
            this._eventEmiter.sendNewMessageEvent(data);
          }
        }
        if (data.notifcationType === 'MERCHANT_VALIDATED_AMOUNT') {
          if (data.id) {
            try {
              const delivery = await this.deliveryService.getDeliveryByIdWithCustomer(data.id);
              localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(delivery));
            } catch (error) {
              console.log(error);
            }
          }
        }
      }
    );


    PushNotifications.requestPermissions().then(result => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    await this.session.setDeviceUuid();

    // let appSetting = new CometChat.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(environment.cometChatRegion).build();
    // await CometChat.init(environment.cometChatAppId, appSetting);

    if (localStorage.getItem("token")) {
      const role = localStorage.getItem('role')
      try {
        await this.userService.checkToken();
        if (role === UserRole.ROLE_CUSTOMER) {
          // await this.tchatService.setEventListener();
          SplashScreen.hide();
          this.router.navigateRoot('/home-customer');
        } else if (role === UserRole.ROLE_DELIVERER) {
          SplashScreen.hide();
          this.router.navigateRoot('/home-deliverer');
        }
      } catch (error) {
        SplashScreen.hide();
        this.logout();
        console.log('Session expired');
        this.router.navigateRoot('/home-customer-sign');
      }
    } else {
      console.log("NO TOKEN => GO CHOICE")
      this.router.navigateRoot('/home-customer-sign');
    }
  }


  openSideBarMenu() {
    switch (this.session.currentUser().role) {
      case UserRole.ROLE_NURSE:
      case UserRole.ROLE_DOCTOR:
      case UserRole.ROLE_CUSTOMER:
        this.routerLinkAddress = '/home-customer';
        break;
      case UserRole.ROLE_DELIVERER:
        this.routerLinkAddress = '/home-deliverer';
        break;
    }
    let main_menu = $('.main-menu');
    main_menu.toggleClass('hidden-soft');
    $(this.sideBarMenuElement.nativeElement).toggleClass('closed');
    if ($(this.sideBarMenuElement.nativeElement).hasClass('closed')) {
      main_menu.css('display', 'flex').animate({
        width: "toggle"
      }, 300);
    }
    else {
      this.hideModal = false;
      main_menu.css('display', 'flex').hide().animate({
        width: "toggle"
      }, 300);
    }
    // $('#load .fa-spin').hide();
    // $('#load').css('visibility', 'visible');
    this.mail = localStorage.getItem('mail');
    this.name = localStorage.getItem('name');
  }

  closeSideBarMenu() {
    let main_menu = $('.main-menu');
    main_menu.hide().css('display', 'flex').animate({
      width: "toggle"
    }, 300);
    $('.menu-open').addClass('closed');
    this.hideModal = true;
    // $('#load .fa-spin').show();
    // $('#load').css('visibility', 'hidden');
  }


  displayTopBar(event) {
    this.hideTopBar = event.display;
  }

  ngOnDestroy() {
    this.subscribeDisplayTopBar.unsubscribe();
  }

  logout() {
    this.userService.logout();
  }

  async changeAvatar() {
    const loading = await this.loadingController.create({
      message: 'Envoi de votre photo de profil...'
    });
    await loading.present();

    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: false,
      width: 600,
      height: 600,
      correctOrientation: true,
      source: CameraSource.Prompt,
      resultType: CameraResultType.DataUrl,
    } as ImageOptions);

    const base64Image = image.dataUrl;
    const response: any = await this.fileService.upload(base64Image, 'avatar');
    await this.userService.changeAvatar(response.idFile);
    await this.userService.getProfile();
    loading.dismiss();
  }

  hideMenu() {
    let main_menu = $('.main-menu');
    if (main_menu.css('display') !== 'none') {
      this.closeSideBarMenu();
    }
  }


  logInfo() {
    this.platform.ready().then(() => {
      console.log("READ_LOG_INFO")
      const eventName = "DEVICE_INFO";
      const params = {};
      try {
        params['modele'] = this.device.model;
        params['plateforme'] = this.device.platform;
        params['version OS'] = this.device.version;
        this.appsflyer.logEvent(eventName, params);

        params[this.facebook.EVENTS.EVENT_PARAM_CURRENCY] = this.currency;
        console.log(params)
        this.facebook.logEvent(eventName, params);
      } catch (error) {
        console.log(error);
      }
    })
  }

  async call(number) {
    window.open(`tel:${number}`, '_system');
  }
}
