import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { UserService } from '../user.service';
import { FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { ActivatedRoute } from '@angular/router';
import { FileService } from '../file.service';
import { NotificationService } from '../notification.service';

declare var window: any;

@Component({
  selector: 'app-new-customer',
  templateUrl: './new-customer.page.html',
  styleUrls: ['./new-customer.page.scss'],
})
export class NewCustomerPage implements OnInit {
  public address;
  private label;
  public registerFormGroup;
  public actualCustomer;
  public title;
  public socialSecurityNumberUrl;
  public mutualUrl;
  private iti;

  constructor(private eventEmitter: MainEventService,
    private route: ActivatedRoute,
    public loadingController: LoadingController,
    private translate: TranslateService,
    private fileService: FileService,
    private router: NavController,
    private notificationService: NotificationService,
    private customerService: CustomerService) {

    this.registerFormGroup = new FormBuilder().group({
      name: ['', Validators.compose([Validators.required])],
      lastname: ['', Validators.compose([Validators.required])],
      socialSecurityNumber: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      mutual: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.address = null;
    this.label = null;
    this.eventEmitter.setBackButtonUrl('/manage-customer');
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    const input = document.querySelector("#phone-input-new-customer");
    if (input) {
      this.iti = window.intlTelInput(input, {
        autoPlaceholder: "aggressive",
        utilsScript: "./assets/js/utils-8.4.6.js",
        initialCountry: "fr"
      });
      this.eventEmitter.sendEventToHideModalLoader(true);
    }

    const id = this.route.snapshot.queryParams.id;
    if (id) {
      this.title = 'TITLE_EDIT_CUSTOMER';
      this.eventEmitter.sendEventToChangeTitle(await this.translate.get(this.title).toPromise());
      this.actualCustomer = await this.customerService.get(id);
      this.registerFormGroup.patchValue(this.actualCustomer);
      this.address = this.actualCustomer.address.fullAddress;
      this.initImages();
    } else {
      this.actualCustomer = null;
      this.title = 'TITLE_ADD_NEW_CUSTOMER';
      this.eventEmitter.sendEventToChangeTitle(await this.translate.get(this.title).toPromise());
    }
  }

  async submit() {
    console.log(this.registerFormGroup.valid)
    const value = this.registerFormGroup.value;
    value.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);

    const loading = await this.loadingController.create({
      message: "Envoi des informations..."
    });
    if (this.registerFormGroup.valid) {
      try {
        await loading.present();
        if (this.actualCustomer) {
          await this.customerService.edit(this.actualCustomer._id, value);
        } else {
          await this.customerService.create(value);
        }

        await loading.dismiss();
        this.router.navigateBack('/manage-customer');
      } catch (error) {
        console.log(error);
        await loading.dismiss();
        await this.notificationService.showToast({
          message: await this.translate.get(error.error.tradCode).toPromise(),
          duration: 3000,
          color: 'danger',
          position: 'bottom'
        });
      }
    }
  }

  setAddress(data) {
    this.registerFormGroup.patchValue({
      address: data
    })
  }


  async changeFile(event, tradCode, field) {
    const selectedFile = event.target.files[0];
    const loading = await this.loadingController.create({
      message: await this.translate.get(tradCode).toPromise()
    });

    if (selectedFile) {
      try {
        await loading.present();
        let params = {};
        params[field] = selectedFile;
        this.registerFormGroup.patchValue(params);
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          this[field + 'Url'] = myReader.result;
          loading.dismiss();
        }
        myReader.readAsDataURL(selectedFile);
      } catch (error) {
        console.log(error);
        loading.dismiss();
      }
    }
  }
  async initImages() {
    try {
      this.mutualUrl = await this.fileService.getSignedUrl(this.actualCustomer.mutual);
      this.socialSecurityNumberUrl = await this.fileService.getSignedUrl(this.actualCustomer.socialSecurityNumber);
    } catch (error) {
      console.debug(error);
      // No problem
    }
  }
}
