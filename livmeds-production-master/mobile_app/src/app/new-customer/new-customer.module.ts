import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewCustomerPage } from './new-customer.page';
import { TranslateModule } from '@ngx-translate/core';
import { AddressInputComponentModule } from '../address-input/address-input.component.module';

const routes: Routes = [
  {
    path: '',
    component: NewCustomerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
    AddressInputComponentModule
  ],
  declarations: [NewCustomerPage]
})
export class NewCustomerPageModule {}
