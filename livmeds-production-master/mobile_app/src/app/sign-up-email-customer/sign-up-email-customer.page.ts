import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastController, NavController, LoadingController, Platform } from '@ionic/angular';
import { UserService } from '../user.service';
import { Address } from '../models/Address';
import { MainEventService } from '../main-event.service';
import { UserRole } from '../enums/UserRole';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { environment } from 'src/environments/environment';
import { NotificationService } from '../notification.service';
import { Appsflyer } from '@ionic-native/appsflyer/ngx';
import { Camera, CameraSource, CameraResultType, ImageOptions } from '@capacitor/camera';

import { SignInWithApple } from '@ionic-native/sign-in-with-apple';
import { Facebook } from '@ionic-native/facebook/ngx';
import {IonSlides } from '@ionic/angular'
declare var window: any;

@Component({
  selector: 'app-sign-up-email-customer',
  templateUrl: './sign-up-email-customer.page.html',
  styleUrls: ['./sign-up-email-customer.page.scss'],
})
export class SignUpEmailCustomerPage implements OnInit {
  public registerFormGroup: FormGroup;
  public registerSocialFormGroup: FormGroup;
  public address: Address;
  public socialSecurityNumberUrl;
  public mutualUrl;
  private iti;
  private options;
  public infoFb;

  @ViewChild('passwordsBox')
  passwordsBox: ElementRef;

  constructor(private userService: UserService,
    public platform: Platform,
    private fb: Facebook,
    private iab: InAppBrowser,
    private router: NavController,
    private route: ActivatedRoute,
    private appsflyer: Appsflyer,
    private translate: TranslateService,
    public loadingController: LoadingController,
    private eventEmitter: MainEventService,
    private notificationService: NotificationService) {
    this.registerFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      passwordConfirm: ['', Validators.compose([Validators.required])],
      compAddress: ['', Validators.compose([])],
      referralCodeReferrer: ['', Validators.compose([])]
    },
      { updateOn: "blur" });


    this.registerSocialFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      userIdFb: ['', Validators.compose([])],
      accessTokenFb: ['', Validators.compose([])],
      identityToken: ['', Validators.compose([])],
      authorizationCode: ['', Validators.compose([])],
      compAddress: ['', Validators.compose([])],
      referralCodeReferrer: ['', Validators.compose([])]
    },
      { updateOn: "blur" });
  }

  fieldsRegisterFrm(field) {
    return this.registerFormGroup.get(field);
  }

  fieldsRegisterSocialFrm(field) {
    return this.registerSocialFormGroup.get(field);
  }

  ngOnInit() {
    this.address = null;
    this.infoFb = {};
    this.appsflyer.logEvent('Affichage_formulaire_Inscription_Client'.toUpperCase(), {});
  }


  async ionViewWillEnter() {
    this.infoFb = this.route.snapshot.queryParams;
    console.log(this.infoFb);
    if (this.infoFb.connectionType) {
      this.registerSocialFormGroup.patchValue(this.infoFb);
      console.log('register social');
      const inputSocial = document.querySelector("#phone-input-sign-up-customer-social");
      if (inputSocial) {
        this.iti = window.intlTelInput(inputSocial, {
          autoPlaceholder: "aggressive",
          utilsScript: "./assets/js/utils-8.4.6.js",
          initialCountry: "fr"
        });
      }
      console.log(this.registerSocialFormGroup.value);
    } else {
      this.registerFormGroup.reset();
      const input = document.querySelector("#phone-input-sign-up-customer");
      if (input) {
        this.iti = window.intlTelInput(input, {
          autoPlaceholder: "aggressive",
          utilsScript: "./assets/js/utils-8.4.6.js",
          initialCountry: "fr"
        });
      }
    }
    this.eventEmitter.setBackButtonUrl('/sign-up-option');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('SIGN_UP').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(true);
    this.eventEmitter.sendEventToHideModalLoader(true);
  }

  async submit() {
    console.log("click");
    if (this.address !== null && (this.registerFormGroup.valid || this.registerSocialFormGroup.valid)) {
      const loading = await this.loadingController.create({
        message: await this.translate.get('REGISTER_IN_PROGRESS').toPromise()
      });
      try {
        await loading.present();
        let user = this.infoFb.connectionType ? this.registerSocialFormGroup.value : this.registerFormGroup.value;
        user.favoriteAddress = this.address;
        user.role = UserRole.ROLE_CUSTOMER;
        user.connectionType = this.infoFb.connectionType;
        user.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
        user.connectionType = this.infoFb.connectionType ? this.infoFb.connectionType : 'MAIL';
        await this.userService.register(user);
        await this.userService.sendNotificationToken();
        this.appsflyer.logEvent(('Inscription_Client_Reussie_avec_' + user.connectionType).toUpperCase(), {});
        await loading.dismiss();
        this.router.navigateRoot(['home-customer', {}]);
      } catch (error) {
        await this.notificationService.showToast({
          message: await this.translate.get(error.error.tradCode).toPromise(),
          duration: 5000,
          color: 'danger'
        });
        await loading.dismiss();
      }
    }
  }

  setAddress(data) {
    console.log("setAddress")
    console.log(data);
    this.address = data;
  }


  async changeFile(event, tradCode, field, formGroup) {
    const selectedFile = event.target.files[0];
    const loading = await this.loadingController.create({
      message: await this.translate.get(tradCode).toPromise()
    });

    if (selectedFile) {
      try {
        await loading.present();
        let params = {};
        params[field] = selectedFile;
        formGroup.patchValue(params);
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          this[field + 'Url'] = myReader.result;
          loading.dismiss();
        }
        myReader.readAsDataURL(selectedFile);
      } catch (error) {
        console.log(error);
        loading.dismiss();
      }
    }
  }


  async takeMutual(formGroup) {
    const loading = await this.loadingController.create({
      message: await this.translate.get('UPLOAD_MUTUAL').toPromise()
    });
    await loading.present();

    try {
      const image = await Camera.getPhoto({
        quality: 60,
        allowEditing: false,
        width: 600,
        height: 600,
        correctOrientation: true,
        source: CameraSource.Camera,
        resultType: CameraResultType.DataUrl,
      } as ImageOptions);

      const base64Image = image.dataUrl;

      this.mutualUrl = base64Image;
      formGroup.patchValue({
        mutual: base64Image
      });
      await loading.dismiss();
    } catch (error) {
      await loading.dismiss();
    }
  }

  async takeSecurityNumber(formGroup) {
    const loading = await this.loadingController.create({
      message: await this.translate.get('UPLOAD_SOCIAL_SECURITY').toPromise()
    });
    await loading.present();
    
    try {
      const image = await Camera.getPhoto({
        quality: 60,
        allowEditing: false,
        width: 600,
        height: 600,
        correctOrientation: true,
        source: CameraSource.Camera,
        resultType: CameraResultType.DataUrl,
      } as ImageOptions);

      const base64Image = image.dataUrl;
      this.socialSecurityNumberUrl = base64Image;
      formGroup.patchValue({
        socialSecurityNumber: base64Image
      });
      await loading.dismiss();

    } catch (error) {
      await loading.dismiss();
    }
  }


  openCgu() {
    const browser = this.iab.create(environment.urlFront + '/cgu', '_system');
  }


  /* add */
    async signUpFacebook() {
      const loading = await this.loadingController.create({
        message: await this.translate.get('RETRIEVE_INFORMATION').toPromise()
      });
      await loading.present();
      try {
        await this.platform.ready();
        const response = await this.fb.login(['public_profile', 'email']);
        console.log(response);
        if (response.status === 'connected') {
          const userIdFb = response.authResponse.userID;
          const accessTokenFb = response.authResponse.accessToken;
          const userInfo = await this.userService.getInfoFacebook(accessTokenFb);
          const params = userInfo;
          params['userIdFb'] = userIdFb;
          params['accessTokenFb'] = accessTokenFb;
          params['connectionType'] = 'FACEBOOK';

          await loading.dismiss();
          this.router.navigateForward('/sign-up-email-customer', {
            queryParams: params
          })
        } else {
          throw 'Not connected'
        }
      } catch (error) {
        console.log(error);
        await this.notificationService.showToast({
          message: 'Erreur lors de la récupération des informations',
          duration: 5000,
          color: 'danger'
        });
        await loading.dismiss();
      }
    }

    async signUpApple() {
      const loading = await this.loadingController.create({
        message: await this.translate.get('RETRIEVE_INFORMATION').toPromise()
      });
      await loading.present();
      try {
        await this.platform.ready();
        const response = await SignInWithApple.signin( { requestedScopes: [0, 1] });
        const responseApi : any = await this.userService.storeInformation({
          userId: response.user,
          data:{
            name: response.fullName.givenName,
            lastName: response.fullName.familyName,
            mail: response.email,
          },
          raw: response
        });
        console.log(response);

          const params = {
            name: responseApi.name,
            lastName: responseApi.lastName,
            mail: responseApi.mail,
            userId: response.user,
            identityToken: response.identityToken,
            authorizationCode: response.authorizationCode,
            connectionType: 'APPLE'
          };
          await loading.dismiss();
          this.router.navigateForward('/sign-up-email-customer', {
            queryParams: params
          })
      } catch (error) {
        console.log(error);
        await this.notificationService.showToast({
          message: 'Erreur lors de la récupération des informations',
          duration: 5000,
          color: 'danger'
        });
        await loading.dismiss();
      }
    }

    @ViewChild('slides', {static:false}) slides: IonSlides;
    slideNext(){
     this.slides.slideNext();
    }
    slidePrev(){
           this.slides.slidePrev();
    }
}
