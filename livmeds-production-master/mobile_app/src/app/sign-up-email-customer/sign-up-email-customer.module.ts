import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SignUpEmailCustomerPage } from './sign-up-email-customer.page';
import { PasswordFieldComponent } from '../password-field/password-field.component';
import { PasswordFieldComponentModule } from '../password-field/password-field.component.module';
import { AddressInputComponentModule } from '../address-input/address-input.component.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: SignUpEmailCustomerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordFieldComponentModule,
    AddressInputComponentModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [SignUpEmailCustomerPage]
})
export class SignUpEmailCustomerPageModule {}
