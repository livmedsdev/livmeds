import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NavController, AlertController, IonRouterOutlet, LoadingController, ToastController, Platform, IonInfiniteScroll, IonContent } from '@ionic/angular';
import { DeliveryService } from '../delivery.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainEventService } from '../main-event.service';
import { FileService } from '../file.service';
import { UserService } from '../user.service';
import { PositionService } from '../position.service';
import { MapsService } from '../maps.service';
import { TranslateService } from '@ngx-translate/core';
import { ProductService } from '../product.service';
import { CategoryProduct } from '../models/CategoryProduct';
import { DeliveryStatus } from '../enums/DeliveryStatus';
import { UserRole } from '../enums/UserRole';
import { SessionService } from '../session.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CustomerService } from '../customer.service';
import { CardService } from '../card.service';
import { EventPollingService } from '../event-polling.service';
import { FormBuilder, Validators } from '@angular/forms';
import { SupportTicketsService } from '../support-tickets.service';
import { NotificationService } from '../notification.service';
import * as moment from 'moment';
import { Appsflyer } from '@ionic-native/appsflyer/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraSource, CameraResultType, ImageOptions } from '@capacitor/camera';
import { DomSanitizer } from '@angular/platform-browser';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { FirebaseAnalytics } from "@capacitor-community/firebase-analytics";
import { SplashScreen } from '@capacitor/splash-screen';

import {IonSlides } from '@ionic/angular'
import { ActionSheet, ActionSheetButtonStyle } from '@capacitor/action-sheet';
import { LocationService } from '../location.service';

declare var $: any;
declare var google;
declare var togglePaymentOptionsContainer;
declare var window: any;

@Component({
  selector: 'app-home-customer',
  templateUrl: './home-customer.page.html',
  styleUrls: ['./home-customer.page.scss'],
})
export class HomeCustomerPage implements OnInit {
  readonly currency = "EUR";
  readonly UserRole = UserRole;
  readonly format = 'HH:mm';
  readonly stepsConstant = {
    STEP_CREATE_DELIVERY_REQUEST: 'STEP_CREATE_DELIVERY_REQUEST',
    STEP_CHANGE_PAYMENT_INFORMATION: 'STEP_CHANGE_PAYMENT_INFORMATION'
  }

  readonly cameraSource = CameraSource;

  stepInMemory;
  hideFirstStepCreateDeliveryRequest: boolean;
  hideWishContainer: boolean;
  hideWishInput: boolean;
  hideBtnReturn: boolean;
  hideWishInformation: boolean;
  hideShoppingCart: boolean;
  wishContainer: boolean;
  hidePaymentOption: boolean;
  hidePharmacyInformation: boolean;
  hideRequestDelivery: boolean;
  merchantActiveOnMap;
  delivererActiveOnMap;
  isRecoveredFirstTime: boolean;
  hideDelivererInformation;
  mapLoaded: boolean;
  selectCategory: boolean;
  selectProduct: boolean;
  showDetailsInvoice: boolean;
  alertRefusedByDeliverer;
  action;
  cards = [];
  rate: Number;
  displayRate: boolean;
  typeParameterActive;

  private iti;
  private markers = [];
  private merchantMarkers = [];
  private iwOpen;

  public delivery; Delivery = {
    address: {},
    merchant: { _id: "", name: "" },
    invoice: {},
    image: [],
    deletedImages: [],
    newImages: []
  };
  // private options: CameraOptions;
  public merchantList;
  public productList;
  public listProductSelected;
  public listProductSelectedDisplay = [];
  public selectedProduct;
  public messageOperation;

  deliveryBackup;

  //Marker
  private delivererListMarkers: any = [];
  private merchantMarker;
  public productListPharmanity: any = [];
  public productPharmanity: any = [];

  //Marker informations
  customerMarker;
  merchantPosition;
  customerPosition;
  directionsDisplay;
  selfMarker;
  polling;
  pollingPosition;
  steps;


  alert;
  alertCancelled;
  alertRefused;
  alertReturnPackage;
  categoriesProduct;

  // Shops
  displayShops: boolean = false;
  categorySelected;
  categories = [];
  limit = 5;
  total = 0;
  searchMerchant;
  txtQueryChanged: Subject<string> = new Subject<string>();
  searchProduct;
  txtProductQueryChanged: Subject<string> = new Subject<string>();

  // Customer
  displayCustomers: boolean = false;
  customers = [];
  limitCustomer = 5;
  totalCustomer = 0;
  searchCustomer;
  txtQueryCustomerChanged: Subject<string> = new Subject<string>();
  Arr = Array; //Array type captured in a variable

  // Addresses
  displayAddresses: boolean = false;
  addresses = [];
  limitAddress = 5;
  totalAddress = 0;
  searchAddress;
  txtQueryAddressChanged: Subject<string> = new Subject<string>();

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  @ViewChild('sideBarMenu') sideBarMenuElement: ElementRef;

  subGetDeliveryInformation;
  subGetDeliverers;
  subPrivateMessages;
  subRedrawMap;
  count;
  firstTimeCheckStep = true;
  firstTimeCheckStepDeliverer = true;


  // support
  showSendMessageToSupport;
  formSendMessage;
  myTickets = [];
  limitTicket = 5;
  totalTicket = 0;

  // process
  inProcess = false;
  cleanMaps = false;
  intervalCleanMaps;
  stopDeliverers = false;

  // List orders
  displayListOrder = false;
  indexOrder = 0;
  urlOrder;


   //Parmanity
       public ref_category;
       public  par_codeId;
       public page = 1 ;
       public currentPage;
       public par_category;
       public ph_id = 0;
        displayProductDetail: boolean = false;
        public detailProduct;

          limitProduct = 40;
          totalProduct = 0;

  private coordinates;

  public scrollStyleShop = "scroll"

  @ViewChild('slides', {static:false}) slides: IonSlides;

  constructor(
    public fileService: FileService,
    public location: LocationService, 
    private mapsService: MapsService,
    private geolocation: Geolocation,
    private _eventEmitter: MainEventService,
    private platform: Platform,
    public sanitizer: DomSanitizer,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    private deliveryService: DeliveryService,
    private notificationService: NotificationService,
    private router: NavController,
    private translate: TranslateService,
    private customerService: CustomerService,
    public alertController: AlertController,
    public session: SessionService,
    private cardService: CardService,
    public productService: ProductService,
    private positionService: PositionService,
    private eventPolling: EventPollingService,
    private supportService: SupportTicketsService,
    private facebook: Facebook,
    private document: DocumentViewer,
    private appsflyer: Appsflyer,
    public userService: UserService) {
    this.selectProduct = false;
    this.selectCategory = false;
    this.hideShoppingCart = true;
    this.showDetailsInvoice = false;
    this.showSendMessageToSupport = false;
    //this.categoriesProduct = Object.keys(CategoryProduct);

    this.categories = [{
      label: 'ROLE_PHARMACY',
      value: 'ROLE_PHARMACY'
    }, {
      label: 'ROLE_OPTICIAN',
      value: 'ROLE_OPTICIAN'
    }, {
      label: 'ROLE_VETERINARY',
      value: 'ROLE_VETERINARY'
    }]


    this.formSendMessage = new FormBuilder().group({
      subject: ['', Validators.compose([Validators.required])],
      message: ['', Validators.compose([Validators.required])]
    });

    this.categorySelected = UserRole.ROLE_PHARMACY;


    this.txtQueryChanged
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(model => {
        this.reloadMerchant();
        // api call
      });


/*
    this.txtProductQueryChanged
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(async model => {
        this.productList = (await this.productService.getAllProduct({ text: this.searchProduct }))['data'];
        this.productList = this.productList.map((product) => {
          product.url = product.image ? this.fileService.getPublicUrl(product.image) : undefined;
          return product;
        })
        if (this.searchProduct && this.searchProduct != "") {
          this.scrollStyleShop = "initial";
        } else {
          this.scrollStyleShop = "scroll";
        }
      });
*/

    this.txtProductQueryChanged
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(async model => {

        const responseProduct_2=  await this.deliveryService.getPharmanityProductByName(this.searchProduct );
         //console.log("recherche par nom : ");console.log(responseProduct_2);


         this.productList = responseProduct_2;


        if (this.searchProduct && this.searchProduct != "") {
          this.scrollStyleShop = "initial";
        } else {
          this.scrollStyleShop = "scroll";
        }
      });

    this.txtQueryCustomerChanged
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(model => {
        this.reloadCustomer();
        // api call
      });


    this.txtQueryAddressChanged
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(model => {
        this.reloadCustomer();
        // api call
      });
  }


  async ngOnInit() {
    this._eventEmitter.sendEventToHideModalLoader(false);
    this.listProductSelected = []
    this._eventEmitter.sendEventToChangeRole(localStorage.getItem('role'));
    this.delivererActiveOnMap = {
      address: {}
    };
    this.merchantActiveOnMap = {
      favoriteAddress: {}
    };
    this.delivery = {
      address: null,
      merchant: null,
      invoice: {},
      image: [],
      deletedImages: [],
      newImages: []
    };
    this.addresses = [];
    this.hideFirstStepCreateDeliveryRequest = true;
    this.hideWishInput = true;
    this.hideWishContainer = true;
    this.hideBtnReturn = true;
    this.hideWishInformation = true;
    this.hidePaymentOption = true;
    this.hidePharmacyInformation = true;
    this.hideDelivererInformation = true;
    this.hideRequestDelivery = false;

    await this.createAlerts();

    await this.cardService.setupStripe('#card-element-create-delivery', 'card-errors-create-delivery', 'payment-form-create-delivery',
      async () => {
        this.cards = (await this.cardService.list())['data'];
        await this.cardService.reloadCardRegistration();
      }, () => { });

      this.getPharmaCategory('ref_medi');
  }

  async createAlerts() {

    this.alertRefused = await this.alertController.create({
      backdropDismiss: false,
      header: await this.translate.get('DELIVERY_REFUSED').toPromise(),
      message: await this.translate.get('DELIVERY_REFUSED_DESCRIPTION').toPromise(),
      buttons: [{
        text: await this.translate.get('BTN_OKAY').toPromise(),
        handler: async () => {
          await this.resetMaps();
        }
      }
      ]
    });

    this.alertRefusedByDeliverer = await this.alertController.create({
      backdropDismiss: false,
      header: await this.translate.get('DELIVERY_REFUSED').toPromise(),
      message: await this.translate.get('DELIVERY_REFUSED_DESCRIPTION_BY_DELIVERER').toPromise(),
      buttons: [{
        text: await this.translate.get('BTN_OKAY').toPromise(),
        handler: async () => {
          await this.resetMaps();
        }
      }
      ]
    });

    this.alertReturnPackage = await this.alertController.create({
      backdropDismiss: false,
      header: await this.translate.get('PACKAGE_RETURNED').toPromise(),
      message: await this.translate.get('PACKAGE_RETURNED_MESSAGE').toPromise(),
      buttons: [{
        text: await this.translate.get('BTN_OKAY').toPromise(),
        handler: async () => {
          await this.resetMaps();
        }
      }
      ]
    });

    this.alert = await this.alertController.create({
      backdropDismiss: false,
      header: await this.translate.get('PACKAGE_DELIVERED').toPromise(),
      message: await this.translate.get('DESCRIPTION_PACKAGE_DELIVERED').toPromise(),
      buttons: [{
        text: await this.translate.get('NOT_RECEIVED').toPromise(),
        handler: async () => {
          await this.deliveryService.deliveryReceive(this.delivery._id, false);
          await this.resetMaps();
          this.hideDelivererInformation = true;
          this.displayRate = true;
        }
      }, {
        text: await this.translate.get('RECEIVED').toPromise(),
        handler: async () => {
          const loading = await this.loadingController.create({
            message: await this.translate.get('SEND_RESULT_AND_VALID_PAYMENT').toPromise()
          });
          loading.present();
          await this.deliveryService.deliveryReceive(this.delivery._id, true);
          await this.resetMaps();
          this.hideDelivererInformation = true;
          this.displayRate = true;
          loading.dismiss();
        }
      }
      ]
    });

    this.alertCancelled = await this.alertController.create({
      backdropDismiss: false,
      header: await this.translate.get('ALERT_DELIVERY_CANCELLED').toPromise(),
      message: await this.translate.get('DESCRIPTION_DELIVERY_CANCELLED').toPromise(),
      buttons: [{
        text: await this.translate.get('BTN_OKAY').toPromise(),
        handler: async () => {
          await this.resetMaps();
        }
      }
      ]
    });

  }

  async ionViewWillEnter() {
    this._eventEmitter.sendEventToHideMenuButton(false);
    this._eventEmitter.setBackButtonUrl(null);
    this._eventEmitter.sendEventToChangeTitle(this.translate.instant('TITLE_MAPS'))
    this._eventEmitter.sendEventToHideTopBar(false);
    this._eventEmitter.sendEventToHideModalLoader(false);
    this.initSubscribe();
    this.cleanMaps = false;
    this.urlOrder = undefined;
    await this.location.askToTurnOnGPS();
    await this.location.requestGPSPermission();
    await this.location.checkGPSPermission();
    await this.loadMap();
    await this.cardService.reloadCardRegistration();
    this.displayRate = false;
    const input = document.querySelector("#phone-input-home-customer");
    if (input && !this.iti) {
      this.iti = window.intlTelInput(input, {
        autoPlaceholder: "aggressive",
        utilsScript: "./assets/js/utils-8.4.6.js",
        initialCountry: "fr"
      });
    }
    this.cards = (await this.cardService.list())['data'];
    await this.reloadMerchant(false, false);
    this.action = 'create';
    this.steps = [];
    this.initMarker();
    this.createMerchantMarkers();
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true,
      preserveViewport: true
    });
    this.directionsDisplay.setOptions({
      polylineOptions: {
        strokeColor: '#66bb6a',
        strokeWeight: 5,
        strokeOpacity: 0
      }
    });
    this.merchantActiveOnMap = {
      favoriteAddress: {}
    };
    this.delivererActiveOnMap = {
      address: {},
      _id: null
    };

    this.delivererListMarkers = [];
    this.stopDeliverers = false;

    const roleAuthorizedToGetListCustomer = [UserRole.ROLE_DOCTOR, UserRole.ROLE_NURSE];
    if (roleAuthorizedToGetListCustomer.includes(this.session.currentUser().role)) {
      await this.reloadCustomer();
    }
    await this.reloadAddresses();
    if (this.mapLoaded) {
      this.checkDeliveryInMemory();
    }
    this._eventEmitter.sendEventToHideModalLoader(true);
    SplashScreen.hide();

    this.action = this.route.snapshot.queryParams.action;
    if (this.action) {
      switch (this.action) {
        case 'edit':
          const id = this.route.snapshot.queryParams.id;
          this.hideFirstStepCreateDeliveryRequest = false;
          this.wishContainer = true;
          this.hideRequestDelivery = true;
          this.delivery = await this.deliveryService.getDeliveryByIdWithCustomer(id);
          this.delivery.addressId = this.delivery.address;
          this.delivery.isScheduled = (this.delivery.status === DeliveryStatus.SCHEDULED);
          this.delivery.deletedImages = [];
          this.delivery.newImages = [];
          let sliderElem = document.getElementById("isScheduled") as HTMLIonCheckboxElement;
          sliderElem.checked = this.delivery.isScheduled;
          this.action = "edit";
          this.selectCard({ id: this.delivery.cardId });
          break;
      }
    }
  }

  async reloadMerchant(pagination = true, enableFilter = true) {
    let filter: any = {
      category: this.categorySelected,
    }

    if (pagination) {
      filter.limit = this.limit;
    }

    if (this.coordinates) {
      filter.lat = this.coordinates.lat;
      filter.long = this.coordinates.long;
    }

    if (this.searchMerchant && enableFilter && this.searchMerchant !== "") {
      filter.text = this.searchMerchant;
      delete filter.lat;
      delete filter.long;
    }


    const response = await this.deliveryService.listShops(filter);
    const newList = response['data'];
    for (let merchant of newList) {
      merchant.urlAvatar = (merchant.avatar) ? await this.fileService.getSignedUrl(merchant.avatar) : undefined;
      merchant.favoriteAddress = this.userService.getFavoriteAddress(merchant.addresses);
      merchant.isOpen = this.checkIfMerchantIsOpen(merchant.openingTime, new Date())
    }
    this.total = response['total'];
    this.merchantList = newList;



    const merchantPharmanity = await this.deliveryService.getPharmanity();
    //console.log("pharmanity : ");console.log(response2);


    for( let indexPharmacy in this.merchantList)
    {
            for(let indexPharmanity in merchantPharmanity){
                if(!this.merchantList[indexPharmacy].ph_id){
                    if(this.merchantList[indexPharmacy].favoriteAddress.city == merchantPharmanity[indexPharmanity].commune
                        && this.merchantList[indexPharmacy].name.toLowerCase() ==  merchantPharmanity[indexPharmanity].raison_sociale.toLowerCase() )
                     {
                        this.merchantList[indexPharmacy] = merchantPharmanity[indexPharmanity];
                     }
                     /*
                     else if(filter.text == undefined) {
                       this.merchantList.push(merchantPharmanity[indexPharmanity]);
                     }
                     */
                 }

           }
    }

  }

  checkIfMerchantIsOpen(openingTime, targetDate) {
    if (!openingTime) {
      return undefined;
    }
    const momentTargetDate = moment(targetDate);
    const weekDay = momentTargetDate.toDate().getDay();
    const start = openingTime[weekDay + 'Start'];
    const end = openingTime[weekDay + 'End'];
    if (start && end) {
      const time: any = moment(momentTargetDate.format(this.format), this.format);
      const startMoment: any = moment(moment(start).format(this.format), this.format);
      const endMoment: any = moment(moment(end).format(this.format), this.format);


      const h = time.format('HH');
      const m = time.format('mm');

      const h1 = startMoment.format('HH');
      const m1 = startMoment.format('mm');

      const h2 = endMoment.format('HH');
      const m2 = endMoment.format('mm');

      return (h1 < h || h1 == h && m1 <= m) && (h < h2 || h == h2 && m <= m2);

      // return time.isBetween(startMoment, endMoment, undefined, "[]");

    } else {
      return false;
    }
  }

  async displayPlanning(event, openingTime) {

    event.stopPropagation();
    const dayTrad = ['MONDAY', "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"]
    const dayPlanning = [undefined, undefined, undefined, undefined, undefined, undefined, undefined]
    let text = "Horaires:<br>";

    if (openingTime) {

      const startSunday = openingTime['0Start'];
      const endSunday = openingTime['0End'];

      if (startSunday || endSunday) {
        dayPlanning[6] = { start: startSunday, end: endSunday };
      }

      for (let i = 1; i < 7; i++) {
        const start = openingTime[i + 'Start'];
        const end = openingTime[i + 'End'];
        if (start || end) {
          dayPlanning[i - 1] = { start, end };
        }
      }
    }

    for (let day in dayTrad) {
      text += await this.translate.get(dayTrad[day]).toPromise() + ': ';
      if (!dayPlanning[day]) {
        text += await this.translate.get('CLOSE').toPromise() + '<br>';
      } else {
        text += ((dayPlanning[day].start) ? moment(dayPlanning[day].start).format(this.format) : await this.translate.get('NOT_FILLED').toPromise()) + '-';
        text += ((dayPlanning[day].start) ? moment(dayPlanning[day].end).format(this.format) : await this.translate.get('NOT_FILLED').toPromise()) + '<br>';
      }
    }

    const alert = await this.alertController.create({
      backdropDismiss: false,
      header: await this.translate.get('PLANNING').toPromise(),
      message: text,
      buttons: [{
        text: await this.translate.get('BTN_OKAY').toPromise(),
        handler: async () => {
        }
      }
      ]
    });
    alert.present();
  }


  async reloadCustomer() {
    let filter: any = {
      limit: this.limitCustomer
    }

    if (this.searchCustomer) {
      filter.text = this.searchCustomer;
    }
    const response = await this.customerService.list(filter);
    this.customers = response['data'];
    this.total = response['total'];
  }



  async reloadAddresses() {
    let filter: any = {
      limit: this.limitAddress
    }

    if (this.searchAddress) {
      filter.text = this.searchAddress;
    }
    const response = await this.userService.getMyAddresses(filter);
    this.addresses = response['data'];
    this.total = response['total'];
  }

  async loadMap() {
    try {
      let mapOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        fullscreenControl: false
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.map.mapTypes.set('styled_map', (new Date().getHours() >= 20 || new Date().getHours() <= 8) ? this.mapsService.styles.dark : this.mapsService.styles.light);
      this.map.setMapTypeId('styled_map');
      this.map.setCenter(new google.maps.LatLng(46.6774234, 2.0960496));
      this.map.setZoom(5);

      try {
        // await this.platform.ready();
        let position = await this.geolocation.getCurrentPosition();
        const selfPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.setCenter(selfPosition);
        this.map.setZoom(13);
        this.coordinates = {
          lat: position.coords.latitude,
          long: position.coords.longitude,
        }
      } catch (error) {
        console.log("error to retrieve geolocation")
        console.log(error)
      }

      google.maps.event.addListenerOnce(this.map, 'idle', async () => {
        this.mapLoaded = true;
      });

      google.maps.event.addListener(this.map, "click", (event) => {
        if (this.iwOpen) {
          this.iwOpen.close()
        }
      });

    } catch (error) {
      console.log(error);
    }
  }

  firstStepCreateDeliveryRequest(merchant = undefined) {
    this.delivery = {
      address: undefined,
      merchant: merchant,
      phoneNumber: this.session.currentUser().phoneNumber,
      invoice: {},
      image: [],
      deletedImages: [],
      newImages: []
    };

    if (this.session.currentUser().defaultCardId && this.session.currentUser().defaultCardId !== "undefined") {
      this.selectCard({ id: this.session.currentUser().defaultCardId });
    } else {
      if (this.cards.length > 0) {
        this.selectCard({ id: this.cards[0].id });
      }
    }

    this.hideFirstStepCreateDeliveryRequest = false;
    this.wishContainer = true;
    this.cleanMaps = false;
    this.displayShop();
  }

  displayWishContainer(display) {
    this.scrollStyleShop = "scroll";
    this.wishContainer = $('.ride-options-bottom').height();
    $('.wish-container').height('100%');
    this.hideWishContainer = !display;
    this.hideFirstStepCreateDeliveryRequest = display;
    this.hideBtnReturn = true;
    this.hideWishInformation = false;
    this.selectCategory = true;
  }

  async displayProducts(category) {
    this.scrollStyleShop = "initial";
    this.productList = (await this.productService.getAllProduct({ category }))['data'];
    this.productList = this.productList.map((product) => {
      product.url = product.image ? this.fileService.getPublicUrl(product.image) : undefined;
      return product;
    })
    this.selectCategory = false;
    this.selectProduct = true;
  }

  displayWishInput() {
    this.hideFirstStepCreateDeliveryRequest = false;
    this.hideWishContainer = false;
    this.hideBtnReturn = false;
    this.hideWishInput = false;
    this.hideWishInformation = true;
  }

  returnFirstStep() {
    if (this.stepInMemory === this.stepsConstant.STEP_CREATE_DELIVERY_REQUEST) {
      this.hideFirstStepCreateDeliveryRequest = false;
      this.hideWishContainer = true;
      this.hideWishInput = true;
      this.hideWishInformation = true;
      this.hideBtnReturn = true;
      this.hidePaymentOption = true;
    } else if (this.stepInMemory === this.stepsConstant.STEP_CHANGE_PAYMENT_INFORMATION) {
      this.hidePharmacyInformation = false;
      this.hidePaymentOption = true;
    }
    this.stepInMemory = undefined;
  }

  async displayPaymentOption() {
    this.hideFirstStepCreateDeliveryRequest = true;
    this.hidePaymentOption = false;
    this.stepInMemory = this.stepsConstant.STEP_CREATE_DELIVERY_REQUEST;
  }

  async selectCard(cardSelected) {
    for (let card of this.cards) {
      if (cardSelected.id === card.id) {
        card.selected = true;
      } else {
        card.selected = false;
      }
    }
    this.delivery.cardId = cardSelected.id;
    if (this.stepInMemory === this.stepsConstant.STEP_CHANGE_PAYMENT_INFORMATION) {
      await this.deliveryService.changeCard(this.delivery._id, cardSelected.id);
    }
  }

  async changeFile(event, tradCode) {
    const selectedFile = event.target.files[0];
    const loading = await this.loadingController.create({
      message: await this.translate.get(tradCode).toPromise()
    });

    if (selectedFile) {
      try {
        await loading.present();
        const myReader: FileReader = new FileReader();
        myReader.onloadend = (e) => {
          let base64Image: any = myReader.result;
          this.delivery.image.push(base64Image);
          this.delivery.newImages.push(base64Image);
          this.refreshUrl(this.delivery.image[this.indexOrder])
          event.target.value = [];
          loading.dismiss();
        }
        myReader.readAsDataURL(selectedFile);
      } catch (error) {
        console.log(error);
        loading.dismiss();
      }
    }
  }

  async takePicture() {
    const loading = await this.loadingController.create({
      message: 'Envoi du reçu...'
    });
    await loading.present();

    try {
      const image = await Camera.getPhoto({
        quality: 60,
        allowEditing: false,
        width: 600,
        height: 600,
        correctOrientation: true,
        source: CameraSource.Prompt,
        resultType: CameraResultType.DataUrl,
      } as ImageOptions);

      const base64Image = image.dataUrl;
      this.delivery.image.push(base64Image);
      this.delivery.newImages.push(base64Image);
      await loading.dismiss();
      await this.notificationService.showToast({
        message: await this.translate.get('CERTIFICATION_UPLOADED').toPromise(),
        duration: 5000,
        color: 'success',
        position: 'top'
      });
      this.refreshUrl(this.delivery.image[this.indexOrder])
    } catch (error) {
      await loading.dismiss();
      await this.notificationService.showToast({
        message: await this.translate.get('ERROR_CERTIFICATION_UPLOADED').toPromise(),
        duration: 5000,
        color: 'danger',
        position: 'top'
      });
    }
  }

  async checkout() {
    if (!this.delivery.merchant) {
      await this.notificationService.showToast({
        message: await this.translate.get('MERCHANT_NOT_CHOOSE').toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'top'
      });
      return;
    }

    if (!this.delivery.addressId) {
      await this.notificationService.showToast({
        message: await this.translate.get('ADRESS_NOT_CHOOSE').toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'top'
      });
      return;
    }

    if (new Date(this.delivery.deliveryDate).getTime() <= new Date().getTime()) {
      await this.notificationService.showToast({
        message: await this.translate.get('DATE_BEFORE_NOW').toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'top'
      });
      return;
    }

    if (!this.delivery.cardId) {
      await this.notificationService.showToast({
        message: await this.translate.get('CARD_NOT_SELECTED').toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'top'
      });
      return;
    }

    this.hideRequestDelivery = true;
    const loading = await this.loadingController.create({
      message: 'Envoi de votre demande...'
    });

    if (!this.session.currentUser().docSended && this.delivery.image.length > 0) {
      const alert = await this.alertController.create({
        header: this.translate.instant("DOC_NOT_SENDED"),
        buttons: [
          {
            text: this.translate.instant('BTN_CANCEL'),
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              // do nothing
            }
          }, {
            text: this.translate.instant('BTN_GO_TO_SEND_DOC'),
            handler: () => {
              this.router.navigateForward("/profile", {})
            }
          }
        ]
      });
      alert.present();
    } else {
      const alert = await this.alertController.create({
        header: 'Si vous avez un code promotionnel, vous pouvez le saisir ci-dessous. Sinon veuillez valider en laissant le chant vide.',
        inputs: [
          {
            name: 'code',
            type: 'text',
            placeholder: this.translate.instant('CODE_PROMOTION')
          }
        ],
        buttons: [
          {
            text: this.translate.instant('BTN_CANCEL'),
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              // do nothing
            }
          }, {
            text: this.translate.instant('BTN_SUBMIT'),
            handler: async (data) => {

              await loading.present();
              this.delivery.codePromo = data.code;
              try {
                this.delivery.products = this.listProductSelected;//.map(elem => elem.pres_id);
                let response: any;
                if (this.action === 'edit') {
                  response = await this.deliveryService.update(this.delivery, this.delivery.newImages);
                } else{
                  response = await this.deliveryService.create(this.delivery);
                }
                await loading.dismiss();
                await this.notificationService.showToast({
                  message: await this.translate.get(response['tradCode']).toPromise(),
                  duration: 3000,
                  color: 'success',
                  position: 'top'
                })

                if (!this.delivery.isScheduled) {
                  this.action = 'create';
                  localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(response.delivery));
                  this.stopDeliverers = true;
                  this.eventPolling.eventStopGetDeliverers.emit({})
                  this.cleanDeliverers();
                  this.cleanMerchantMarkers();
                  this.initDeliveryMap(response.delivery);
                } else {
                  this.hideFirstStepCreateDeliveryRequest = true;
                  this.wishContainer = true;
                  this.hideRequestDelivery = false;
                  let sliderElem = document.getElementById("isScheduled") as HTMLIonCheckboxElement;
                  sliderElem.checked = false;
                  await this.closeDelivery();
                }

                this.displayShops = false;
              } catch (error) {
                console.log(error)
                await loading.dismiss();
                await this.notificationService.showToast({
                  message: await this.translate.get(error.error.tradCode).toPromise(),
                  duration: 5000,
                  color: 'danger',
                  position: 'top'
                });
              }
            }
          }
        ]
      });
      alert.present();
    }
  }

  addMarkerMerchant(merchant) {
    this.merchantActiveOnMap = merchant;
    this.merchantMarker = new google.maps.Marker({
      icon: {
        url: "../assets/images/maps/logo_pharma.png",
        scaledSize: { width: 40, height: 40 },
        anchor: { x: 20, y: 20 },
        label: 'merchant'
      }
    });
    const favoriteAddress = this.userService.getFavoriteAddress(merchant.addresses);
    merchant.favoriteAddress = favoriteAddress;

    let latLng = new google.maps.LatLng(favoriteAddress.latitude, favoriteAddress.longitude);
    this.merchantMarker.setPosition(latLng);
    this.merchantMarker.setMap(this.map);
    this.markers.push(this.merchantMarker);
    this.merchantPosition = latLng;
    this.map.setCenter(latLng);
    this.hidePharmacyInformation = false;

    google.maps.event.addListener(this.merchantMarker, 'click', () => {
      this.hidePharmacyInformation = false;
    });
  }

  initSubscribe() {
    this.subGetDeliveryInformation = this.eventPolling.eventGetDeliveryInformationCustomer.subscribe(async delivery => {
      if (!this.inProcess) {
        this.inProcess = true;
        this.deliveryBackup = Object.assign({}, this.delivery);
        this.delivery = delivery;
        this.checkOperations();
        this.saveLastDelivery(delivery);

        this.checkStep();
        this.count = this.session.getNbPrivateMessageReceived(this.delivery._id, this.typeParameterActive);

        if (this.delivery.status === DeliveryStatus.IN_DELIVERY) {
          this.stopDeliverers = true;
          this.eventPolling.eventStopGetDeliverers.emit({})
          this.cleanDeliverers();

          // init deliverer
          if (!this.delivererActiveOnMap._id) {
            console.log(this.delivery.deliverer)
            this.delivererActiveOnMap = this.delivery.deliverer;
          }
          this.eventPolling.eventStartRedrawMapCustomer.emit({});
        } else if (this.delivery.status === DeliveryStatus.RETURN_PACKAGE) {
          await this.alertReturnPackage.present();
        }
        else if (this.delivery.status === DeliveryStatus.DELIVERED) {
          this.alert.present();
          this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});
        }
        else if (this.delivery.status === DeliveryStatus.REFUSED) {
          this.alertRefused.present();
          this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});
        }
        else if (this.delivery.status === DeliveryStatus.REFUSED_BY_DELIVERER) {
          this.alertRefusedByDeliverer.present();
          this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});
          await this.resetMaps();
          this.delivery = {
            address: null,
            merchant: null,
            invoice: {},
            image: [],
            deletedImages: [],
            newImages: []
          };
        } else if (this.delivery.status === DeliveryStatus.REFUSED_AMOUNT) {
          this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});
          await this.resetMaps();
          this.delivery = {
            address: null,
            merchant: null,
            invoice: {},
            image: [],
            deletedImages: [],
            newImages: []
          };
        } else if (this.delivery.status === DeliveryStatus.CANCELLED) {
          this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});
          this.alertCancelled.present();
        } else if (this.delivery.status === DeliveryStatus.NOT_RECEIVED) {
          this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});
          await this.resetMaps();
          this.delivery = {
            address: null,
            merchant: null,
            invoice: {},
            image: [],
            deletedImages: [],
            newImages: []
          };
        }
        if (this.cleanMaps) {
          this.delivery = {
            address: null,
            merchant: null,
            invoice: {},
            image: [],
            deletedImages: [],
            newImages: []
          };
          this.cleanSteps();
        }
        this.inProcess = false;
      }
    });

    this.subRedrawMap = this.eventPolling.eventRedrawMapCustomer.subscribe(async () => {
      console.log("REDRAW");
      if (this.delivery._id && this.delivery.status === DeliveryStatus.IN_DELIVERY) {
        let track: any = await this.deliveryService.trackDelivery(this.delivery._id);
        if (track !== null && track.length > 0) {
          const positionDeliverer = new google.maps.LatLng(track[0].latitude, track[0].longitude);
          this.selfMarker.setPosition(positionDeliverer);
          // await this.computeDirection(positionDeliverer);
        }
      }
    })

    this.subGetDeliverers = this.eventPolling.eventGetDeliverers.subscribe(async (data) => {
      if (!this.stopDeliverers) {
        for (let deliverer of data.deliverers as any[]) {
          if (this.delivererListMarkers[deliverer._id]) {
            if (deliverer.positions.length > 0) {
              let latLng = new google.maps.LatLng(deliverer.positions[0].latitude, deliverer.positions[0].longitude);
              this.delivererListMarkers[deliverer._id].setPosition(latLng);
            } else {
              this.delivererListMarkers.delete(deliverer._id);
            }
          } else {
            if (deliverer.positions.length > 0) {
              console.log("CREATE MARKER")
              const marker = new google.maps.Marker({
                icon: {
                  url: "../assets/images/maps/logo_deliverer.png",
                  scaledSize: { width: 40, height: 40 },
                  anchor: { x: 20, y: 20 },
                }
              });
              this.delivererListMarkers[deliverer._id] = marker;
              let latLng = new google.maps.LatLng(deliverer.positions[0].latitude, deliverer.positions[0].longitude);
              this.delivererListMarkers[deliverer._id].setPosition(latLng);
              this.delivererListMarkers[deliverer._id].setMap(this.map);
            }
          }
        }
      } else {
        this.cleanDeliverers();
      }
    })

    this.subPrivateMessages = this._eventEmitter.newMessageReceived.subscribe((data) => {
      this.count = this.session.getNbPrivateMessageReceived(this.delivery._id, this.typeParameterActive);
    })
  }

  checkStep() {
    if (this.firstTimeCheckStep && this.delivery.packageGivenToDeliverer === false) {
      this.typeParameterActive = 'TO_PHARMACY';
      this.firstTimeCheckStep = false
    } else if (this.firstTimeCheckStepDeliverer && this.delivery.packageGivenToDeliverer && this.delivery.recoveredPackageByDeliverer) {
      this.typeParameterActive = 'TO_DELIVERER';
      this.firstTimeCheckStepDeliverer = false
    }
  }

  computeDirection(data) {
    if (!this.cleanMaps) {
      console.log(data);
      return new Promise<void>((resolve, reject) => {
        let directionsService = new google.maps.DirectionsService;

        let waypoints = [{ location: this.merchantPosition }];
        if (this.delivery.recoveredPackageByDeliverer) {
          waypoints = [];
          if (!this.isRecoveredFirstTime) {
            this.isRecoveredFirstTime = true;
            this.hidePharmacyInformation = true;
            this.hideDelivererInformation = false;
          }
        }

        //Add direction
        const directionParameters = {
          origin: data,
          destination: this.customerPosition,
          travelMode: google.maps.TravelMode['DRIVING'],
          waypoints: waypoints
        };
        console.log(directionParameters);
        directionsService.route(directionParameters, (result, status) => {
          if (status == 'OK') {
            this.directionsDisplay.setDirections(result);
            this.steps = this.mapsService.renderPath(result, this.map, this.steps);
            resolve();
          } else {
            console.warn(status);
            reject(status);
          }
        });
      });
    }
  }

  hidePositionDeliverers() {
    for (let deliverer of this.delivererListMarkers) {
      deliverer.setMap(this.map);
      this.markers.push(deliverer);
    }
  }

  async centerMap() {
    let track: any = await this.deliveryService.trackDelivery(this.delivery._id);
    if (track !== null && track.length > 0) {
      const positionDeliverer = new google.maps.LatLng(track[0].latitude, track[0].longitude);
      this.map.setCenter(positionDeliverer);
    }
  }

  async initDeliveryMap(delivery) {
    this.cleanDeliverers();
    this.cleanMaps = false;
    this.isRecoveredFirstTime = false;
    this.delivery = delivery;
    this.addMarkerMerchant(this.delivery.merchant);
    this.hideFirstStepCreateDeliveryRequest = true;
    this.hideRequestDelivery = true;

    this.hidePositionDeliverers();
    this.cleanMerchantMarkers();
    this.customerPosition = new google.maps.LatLng(this.delivery.latitude, this.delivery.longitude);
    this.customerMarker.setPosition(this.customerPosition)
    this.customerMarker.setMap(this.map);
    this.markers.push(this.customerMarker);
    this.selfMarker.setMap(this.map);
    this.markers.push(this.selfMarker);
    this.directionsDisplay.setMap(this.map);

    if (this.delivery.status === DeliveryStatus.IN_DELIVERY) {
      this.stopDeliverers = true;
      this.eventPolling.eventStopGetDeliverers.emit({})
      let track: any = await this.deliveryService.trackDelivery(this.delivery._id);
      if (track !== null && track.length > 0) {
        const positionDeliverer = new google.maps.LatLng(track[0].latitude, track[0].longitude);
        this.selfMarker.setPosition(positionDeliverer);
        // await this.computeDirection(positionDeliverer);
      }

      if (this.delivererActiveOnMap._id === null) {
        console.log(this.delivery.deliverer)
        this.delivererActiveOnMap = this.delivery.deliverer;
      }
    } else if (this.delivery.status === DeliveryStatus.RETURN_PACKAGE) {
      await this.alertReturnPackage.present();
    }
    else if (this.delivery.status === DeliveryStatus.DELIVERED) {
      this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({ delivery: this.delivery });
      await this.alert.present();
    }
    else if (this.delivery.status === DeliveryStatus.REFUSED) {
      await this.alertRefused.present();
    }
    else if (this.delivery.status === DeliveryStatus.REFUSED_BY_DELIVERER) {
      await this.alertRefusedByDeliverer.present();
    }

    this.eventPolling.eventStartGetDeliveryInformationCustomer.emit({ delivery: this.delivery });
  }

  ionViewWillLeave() {
    this.cleanMaps = false;
    // Delivery informations
    this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({})
    if (this.subGetDeliveryInformation) {
      this.subGetDeliveryInformation.unsubscribe();
      this.subGetDeliveryInformation = undefined;
    }

    // Redraw map
    this.eventPolling.eventStopRedrawMapCustomer.emit({})
    if (this.subRedrawMap) {
      this.subRedrawMap.unsubscribe();
      this.subRedrawMap = undefined;
    }

    // Deliverers
    this.eventPolling.eventStopGetDeliverers.emit({})
    if (this.subGetDeliverers) {
      this.subGetDeliverers.unsubscribe();
      this.subGetDeliverers = undefined;
    }
    // Tchat
    this.eventPolling.eventStopTchat.emit({})
    if (this.subPrivateMessages) {
      this.subPrivateMessages.unsubscribe();
      this.subPrivateMessages = undefined;
    }
  }

  cleanSteps() {
    for (let step of this.steps) {
      step.setMap(null);
    }
    this.steps = []
  }

  cleanMarkers() {
    for (let marker of this.markers) {
      marker.setMap(null);
    }
    this.markers = []
    this.cleanDeliverers();
  }

  initMarker() {
    this.selfMarker = new google.maps.Marker({
      icon: {
        url: "../assets/images/maps/logo_deliverer.png",
        scaledSize: { width: 40, height: 40 },
        anchor: { x: 20, y: 20 },
      }
    });
    this.merchantMarker = new google.maps.Marker({
      url: "../assets/images/consultation.png",
      scaledSize: new google.maps.Size(2, 2),
      anchor: { x: 1, y: 1 },
    });

    this.customerMarker = new google.maps.Marker({
      icon: "../assets/images/maps/logo_home.png",
      scaledSize: { width: 40, height: 40 },
      anchor: { x: 20, y: 20 },
    });

    const selfPosition = new google.maps.LatLng(this.coordinates.lat, this.coordinates.long);
    this.customerMarker.setPosition(selfPosition)
    this.customerMarker.setMap(this.map)
  }

  showChat(type) {
    this.count = 0;
    this.session.resetNbPrivateMessageReceived(this.delivery._id, type);
    this.router.navigateForward("/tchat", {
      queryParams: {
        authorType: 'customer',
        delivery: this.delivery._id,
        typePrivateMessage: type
      }
    })
  }

  async refuseTotalAmount() {
    // REFUSE amount
    const loading = await this.loadingController.create({
      message: await this.translate.get('REFUSE_IN_PROGRESS').toPromise()
    });
    await loading.present();
    try {
      await this.deliveryService.refuseAmountForDelivery(this.delivery._id);
      this.logRefuse();
      await this.resetMaps();
      await loading.dismiss();
    } catch (error) {
      await loading.dismiss();
      console.log(error);
      await this.notificationService.showToast({
        message: await this.translate.get(error.error.tradCode).toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'bottom'
      });
    }
  }

  async acceptTotalAmount() {
    const loading = await this.loadingController.create({
      message: await this.translate.get('VALID_DELIVERY_BY_YOU').toPromise()
    });
    await loading.present();
    try {
      this.logPurchase();
      await this.deliveryService.acceptAmountForDelivery(this.delivery._id);
      this.delivery = await this.deliveryService.getDeliveryByIdWithCustomer(this.delivery._id);
      this.checkOperations();
      this.saveLastDelivery(this.delivery);
      await this.notificationService.showToast({
        message: await this.translate.get("YOU_HAVE_VALIDED_AMOUNT").toPromise(),
        duration: 3000,
        color: 'success',
        position: 'bottom'
      });
      await loading.dismiss();
    } catch (error) {
      await loading.dismiss();
      console.log(error);
      await this.notificationService.showToast({
        message: await this.translate.get(error.error.tradCode).toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'bottom'
      });
    }
  }

  logPurchase() {
    this.platform.ready().then(() => {
      try {
        this.appsflyer.logEvent('af_purchase', {
          "af_revenue": this.delivery.invoice.amountCustomer,
          "af_currency": this.currency
        });

        const params = {};
        params[this.facebook.EVENTS.EVENT_PARAM_CURRENCY] = this.currency;
        this.facebook.logEvent(this.facebook.EVENTS.EVENT_NAME_PURCHASED, params, this.delivery.invoice.amountCustomer);
        FirebaseAnalytics.logEvent({
          name: "purchase",
          params: {
            transaction_id: this.delivery._id,
            currency: this.currency,
            value: this.delivery.invoice.amountCustomer
          },
        });


      } catch (error) {
        console.log(error);
      }
    })
  }

  logRefuse() {
    this.platform.ready().then(() => {
      try {
        const eventName = "REFUS_DU_MONTANT";

        this.appsflyer.logEvent(eventName, {
          "af_revenue": this.delivery.invoice.amountCustomer,
          "af_currency": this.currency
        });

        const params = {};
        params[this.facebook.EVENTS.EVENT_PARAM_CURRENCY] = this.currency;
        this.facebook.logEvent(eventName, params, -this.delivery.invoice.amountCustomer);
      } catch (error) {
        console.log(error);
      }
    });
  }

  ionViewDidLeave() {
    this.eventPolling.eventStopGetDeliverers.emit({})
  }

  addProduct(product) {
    this.listProductSelected.push(product);

    // Informations displayed
    const clone = Object.assign({}, product);

    const found = this.listProductSelectedDisplay.findIndex(e => e.pres_id === product.pres_id);

    if (found < 0) {
      clone.quantity = 1;
      this.listProductSelectedDisplay.push(clone)
    } else {
      this.listProductSelectedDisplay[found].quantity ++;
    }

    // Notify user
    this.notificationService.showToast({
      message: this.translate.instant('PRODUCT_ADDED_IN_SHOPPING_CART'),
      duration: 2000,
      color: 'success',
      position: 'bottom'
    });

    console.log(this.listProductSelectedDisplay);
  }

  returnToCategoriesList() {
    this.selectProduct = false;
    this.selectCategory = true;
    this.scrollStyleShop = "scroll";
  }

  returnToRequest() {
    this.hideWishContainer = true;
    this.hideFirstStepCreateDeliveryRequest = false;
  }

  removeProduct(product) {
    const foundInDisplayList = this.listProductSelectedDisplay.findIndex(e => e._id === product._id);
    if (this.listProductSelectedDisplay[foundInDisplayList].quantity <= 1) {
      this.listProductSelectedDisplay.splice(foundInDisplayList, 1);
    } else {
      this.listProductSelectedDisplay[foundInDisplayList].quantity -= 1;
    }

    // Delete in backend array
    const found = this.listProductSelected.findIndex(e => e._id === product._id);
    this.listProductSelected.splice(found, 1);

        this.notificationService.showToast({
          message: this.translate.instant('PRODUCT_DELETE_IN_SHOPPING_CART'),
          duration: 2000,
          color: 'success',
          position: 'bottom'
        });
  }

  saveLastDelivery(lastDelivery) {
    localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(lastDelivery));
  }

  checkDeliveryInMemory() {
    // Check if exist last delivery in memory
    let lastDelivery = localStorage.getItem('lastDelivery_' + this.session.currentUser()._id)
    if (lastDelivery) {
      this.initDeliveryMap(JSON.parse(lastDelivery));
    } else {
      this.eventPolling.eventStartGetDeliverers.emit({});
    }
  }

  showShoppingCart(show) {
    if (!this.hideWishContainer) {
      this.hideWishContainer = show;
    }
    this.hideShoppingCart = !show;
    this.hideFirstStepCreateDeliveryRequest = show;
  }

  displayInvoice(show) {
    this.showDetailsInvoice = show;
    this.hidePharmacyInformation = show;
    if (show) {
      this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});
    } else {
      this.eventPolling.eventStartGetDeliveryInformationCustomer.emit({ delivery: this.delivery });
    }
  }

  categoryChange(category) {
    console.log(category);
    this.categorySelected = category.value;
    this.limit = 5;
    this.reloadMerchant();
  }

  displayMoreMerchant() {
    this.limit += 5
    this.reloadMerchant();
  }

  displayMoreAddresses() {
    this.limitAddress += 5
    this.reloadAddresses();
  }

  selectMerchant(merchant,ph_id) {
    this.delivery.merchant = merchant;

    if(ph_id != 0 )
    {
        this.ph_id = ph_id;
    }
    //this.displayShops = false;
  }

  async displayShop() {
    const loading = await this.loadingController.create({
      message: this.translate.instant('LOADING')
    });
    await loading.present();
    await this.reloadMerchant();
    await loading.dismiss();
    this.displayShops = true;
    this.slides.lockSwipes(true);
  }

  onSearchTextChange(event) {
    this.txtQueryChanged.next(event);
  }


  onSearchProductTextChange(event) {
    this.txtProductQueryChanged.next(event);
  }

  selectCustomer(customer) {
    this.delivery.customerOfMerchant = customer;
    this.delivery.addressId = customer.address._id;
    this.displayCustomers = false;
  }

  onSearchCustomerTextChange(event) {
    this.txtQueryCustomerChanged.next(event);
  }

  displayMoreCustomer() {
    this.limit += 5
    this.reloadCustomer();
  }


  selectAddress(address) {
    this.delivery.addressId = address;
    this.displayAddresses = false;
  }

  onSearchAddressTextChange(event) {
    this.txtQueryAddressChanged.next(event);
  }


  getLogoForCat(cat) {
    switch (cat) {
      case UserRole.ROLE_PHARMACY:
        return 'assets/images/pharmacy.png';
      case UserRole.ROLE_VETERINARY:
        return 'assets/images/veto.png';
      case UserRole.ROLE_OPTICIAN:
        return 'assets/images/optician.png';
    }
  }

  resetForm() {
    let sliderElem = document.getElementById("isScheduled") as HTMLIonCheckboxElement;
    sliderElem.checked = false;
    this.delivery = {
      address: undefined,
      merchant: undefined,
      invoice: {},
      image: [],
      deletedImages: [],
      newImages: []
    };
    this.action = 'create';
  }

  async call(number) {
    window.open(`tel:${number}`, '_system');
  }

  setScore(score) {
    for (let i = 1; i <= score; i++) {
      $('.star-' + i).removeClass("font-weight-light")
    }

    for (let i = score + 1; i <= 5; i++) {
      $('.star-' + i).addClass("font-weight-light")
    }
    this.rate = score;
  }

  async submitRate() {
    try {
      await this.deliveryService.rate({
        value: this.rate,
        delivery: this.delivery._id
      });
      await this.notificationService.showToast({
        message: await this.translate.get('THANKS_FOR_YOUR_RATE').toPromise(),
        duration: 2000,
        color: 'success',
        position: 'top'
      });
      this.displayRate = false;
      this.delivery = {
        address: null,
        merchant: null,
        invoice: {},
        image: [],
        deletedImages: [],
        newImages: []
      };
    } catch (error) {
      console.log(error);
      await this.notificationService.showToast({
        message: await this.translate.get(error.tradCode).toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'bottom'
      });
    }
  }

  noRate() {
    this.displayRate = false;
  }

  async resetMaps() {
    this.cleanMaps = true;
    this.listProductSelected = [];
    this.hideRequestDelivery = false;
    this.hideDelivererInformation = true;
    this.firstTimeCheckStep = true;
    this.firstTimeCheckStepDeliverer = true;
    this.customerMarker.setMap(null);
    this.selfMarker.setMap(null);
    this.directionsDisplay.setMap(null);
    this.merchantMarker.setMap(null);
    this.eventPolling.eventStopTchat.emit({})
    this.eventPolling.eventStopGetDeliverers.emit({})
    this.pollingPosition = undefined;
    this.cleanMarkers();
    this.initMarker();
    await this.reloadMerchant(false, false);
    this.createMerchantMarkers();
    this.hidePharmacyInformation = true;
    this.stopDeliverers = false;
    this.eventPolling.eventStartGetDeliverers.emit({});
    localStorage.removeItem('lastDelivery_' + this.session.currentUser()._id);
    await this.createAlerts();
    this.cards = this.cards.map((card) => {
      card.selected = false
      return card;
    })
    this.cleanSteps();
  }

  async closeDelivery() {
    await this.resetMaps();
    this.eventPolling.eventStopGetDeliveryInformationCustomer.emit({});

    this.intervalCleanMaps = setInterval(() => {
      if (this.cleanMaps) {
        this.delivery = {
          address: null,
          merchant: null,
          invoice: {},
          image: [],
          deletedImages: [],
          newImages: []
        };

        this.cleanSteps();
      } else {
        clearInterval(this.intervalCleanMaps)
      }

    }, 100);

  }

  formatNumber(number) {
    return Number.parseFloat(number).toFixed(2);
  }

  async contactSupport(show) {
    this.showSendMessageToSupport = show;
    this.reloadTickets();
  }

  async reloadTickets() {
    let filter: any = {
      limit: this.limitTicket,
      delivery: this.delivery._id
    }
    const response = await this.supportService.list(filter);
    this.myTickets = response['data'];
    this.totalTicket = response['total'];
  }

  displayMoreTickets() {
    this.limitTicket += 5
    this.reloadTickets();
  }

  async submitSupport() {
    if (this.formSendMessage.valid) {
      try {
        const body = this.formSendMessage.value;
        body.delivery = this.delivery._id;
        await this.supportService.create(body);
        this.showSendMessageToSupport = false;
        await this.notificationService.showToast({
          message: await this.translate.get('SUCCESS_SEND_SUPPORT').toPromise(),
          duration: 5000,
          color: 'success'
        });
        this.formSendMessage.reset();
      } catch (error) {
        await this.notificationService.showToast({
          message: await this.translate.get(error.error.tradCode).toPromise(),
          duration: 5000,
          color: 'danger'
        });
      }
    } else {
      await this.notificationService.showToast({
        message: 'Un de vos champs est invalide',
        duration: 5000,
        color: 'danger'
      });
    }
  }


  increaseBox(increase, targetId, showElement, hideElement) {
    if (increase) {
      $('#' + targetId).show();
    } else {
      $('#' + targetId).hide();
    }
    $('#' + showElement).show();
    $('#' + hideElement).hide();
  }

  cleanDeliverers() {
    for (let marker in this.delivererListMarkers) {
      this.delivererListMarkers[marker].setMap(null);
    }
  }

  async showMessageScheduled(event) {
    this.delivery.isScheduled = event.target.checked;

    if (event.target.checked === true) {
      const confirmScheduled = await this.alertController.create({
        backdropDismiss: false,
        header: await this.translate.get('TITLE_CONFIRM_SCHEDULED').toPromise(),
        message: await this.translate.get('DESCRIPTION_CONFIRM_SCHEDULED').toPromise(),
        buttons: [{
          text: await this.translate.get('BTN_OKAY').toPromise(),
          handler: async () => { }
        }
        ]
      });
      await confirmScheduled.present();
    }else{
      delete this.delivery.deliveryDate;
    }
  }

  displayToChangeCard() {
    this.hidePaymentOption = false;
    this.hidePharmacyInformation = true;
    this.stepInMemory = this.stepsConstant.STEP_CHANGE_PAYMENT_INFORMATION;
    this.selectCard({ id: this.delivery.cardId });
  }

  checkOperations() {
    let inProgress = this.delivery.operations.filter(elem => elem.status === "IN_PROGRESS");
    let failed = this.delivery.operations.filter(elem => elem.status === "FAILED");
    if (inProgress.length > 0) {
      this.messageOperation = "OPERATION_IN_PROGRESS"
    } else if (failed.length > 0) {
      this.messageOperation = "OPERATION_FAILED";
    } else {
      this.messageOperation = undefined;
    }
  }

  async displayListOrders() {
    this.displayListOrder = true
    this.indexOrder = 0;
    await this.refreshUrl(this.delivery.image[this.indexOrder])
  }

  async nextOrder() {
    this.indexOrder++;
    await this.refreshUrl(this.delivery.image[this.indexOrder])
  }

  async prevOrder() {
    this.indexOrder--;
    await this.refreshUrl(this.delivery.image[this.indexOrder])
  }

  async refreshUrl(url) {
    if (!url) {
      return;
    }
    if (url.startsWith("data:image/jpeg;base64,") || url.startsWith("data:image/png;base64,") || url.startsWith("data:application/pdf;base64,")) {
      this.urlOrder = {
        type: url.startsWith("data:application/pdf;base64,") ? 'PDF' : 'IMAGE',
        content: url,
      }
    } else {
      const data = JSON.parse(url);
      if (data.key.endsWith(".pdf")) {
        this.urlOrder = {
          type: 'PDF',
          content: await this.fileService.getPdfBlob(url)
        }
      } else {
        this.urlOrder = {
          type: 'IMAGE',
          content: await this.fileService.getSignedUrl(url)
        }
      }
    }
  }

  async openPdf(base64) {

    const fileName = "preview-pdf.pdf";

    const result = await Filesystem.writeFile({
      path: fileName,
      data: base64,
      directory: Directory.Data
    });

    const getUriResult = await Filesystem.getUri({
      directory: Directory.Data,
      path: fileName
    });

    const options: DocumentViewerOptions = {
      title: 'Preview'
    };

    const path = getUriResult.uri;
    this.document.viewDocument(path, 'application/pdf', options)
  }

  deleteImageOrder(index) {
    if (this.action === 'edit') {
      this.delivery.deletedImages.push(this.delivery.image[this.indexOrder]);
    }
    this.delivery.image.splice(index, 1);
    if (this.indexOrder > 0) {
      this.indexOrder--;
    }
    this.refreshUrl(this.delivery.image[this.indexOrder])
  }


  @ViewChild(IonContent, { static: false }) content: IonContent;
  displaybuttonGoToTop = false;

  async scrollToLabel(label) {
    const visible = await this.isVisible("topList");
    console.log(visible);

    var titleELe = document.getElementById("topList");
    this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
  }

  isVisible(label): Promise<any> {
    const domElement = document.getElementById(label)
    return new Promise(resolve => {
      const o = new IntersectionObserver(([entry]) => {
        resolve(entry.intersectionRatio === 1);
        o.disconnect();
      });
      o.observe(domElement);
    });
  }

  isInViewport(label) {
    const el = document.getElementById(label)
    const rect = el.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  logScrolling(event) {
    this.displaybuttonGoToTop = !this.isInViewport('topList')
  }

  cleanMerchantMarkers() {
    for (let marker in this.merchantMarkers) {
      this.merchantMarkers[marker].setMap(null);
    }

    this.merchantMarkers = [];
  }

  createMerchantMarkers() {
    this.cleanMerchantMarkers();
    if (this.merchantList.length > 0) {
      const bounds = new google.maps.LatLngBounds();
      if (this.customerMarker.getPosition()) {
        bounds.extend(this.customerMarker.getPosition());
      }

      for (let merchant of this.merchantList) {
        let marker = new google.maps.Marker({
          icon: {
            url: "../assets/images/maps/logo_pharma.png",
            scaledSize: { width: 40, height: 20 },
            anchor: { x: 20, y: 20 },
            label: 'merchant'
          }
        });

        var pts;

        if(merchant['lat/10000'] && merchant['lng/10000'])
        {
            pts = new google.maps.LatLng(merchant['lat/10000'], merchant['lng/10000']);
        }else
        {
             pts = new google.maps.LatLng(merchant.location.coordinates[1], merchant.location.coordinates[0]);
        }

        //const pts = new google.maps.LatLng(merchant.location.coordinates[1], merchant.location.coordinates[0]);
        marker.setMap(this.map);
        marker.setPosition(pts);

        this.merchantMarkers.push(marker)
        bounds.extend(marker.getPosition());
        

        // Create iw
        let infowindow = new google.maps.InfoWindow({
          content: this.mapsService.generateContentWindows(merchant),
          maxWidth: 350
        });

        const handlerPlanning = (event) => this.displayPlanning(event, merchant.openingTime);
        const handlerSelectMerchant = (event) => this.selectMerchantByMap(merchant);

        google.maps.event.addListener(infowindow, 'domready', () => {
          const el = document.getElementById('btn-display-planning-' + merchant._id);
          if (el !== null) {
            el.removeEventListener('click', handlerPlanning)
            el.addEventListener('click', handlerPlanning);
          }

          const elBtnSelect = document.getElementById('btn-select-merchant-' + merchant._id);
          elBtnSelect.removeEventListener('click', handlerSelectMerchant)
          elBtnSelect.addEventListener('click', handlerSelectMerchant);
        });

        google.maps.event.addListener(marker, 'click', () => {
          if (this.iwOpen) {
            this.iwOpen.close();
          }

          this.iwOpen = infowindow;

          infowindow.open(this.map, marker);
        });

      }
      this.map.fitBounds(bounds);
    } else {
      const selfPosition = new google.maps.LatLng(this.coordinates.lat, this.coordinates.long);
      this.map.setCenter(selfPosition);
      this.map.setZoom(13)
    }
  }

  selectMerchantByMap(merchant) {
    this.firstStepCreateDeliveryRequest(merchant)

    if (this.iwOpen) {
      this.iwOpen.close();
      delete this.iwOpen;
    }
  }

  async centerMapNotInDelivery() {
    const selfPosition = new google.maps.LatLng(this.coordinates.lat, this.coordinates.long);
    this.map.setCenter(selfPosition);
    this.map.setZoom(13);
  }

  clearSearch() {
    this.scrollStyleShop = "scroll";
  }

  base64ToArrayBuffer(base64) {
    base64 = base64.replace('data:application/pdf;base64,', '');
    let binary_string = base64.replace(/\\n/g, '');
    binary_string = window.atob(base64);
    let len = binary_string.length;
    let bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

  ionSlideDidChange(event){
    this.slides.getActiveIndex().then(index => {
          console.log(index)
         if(index == 3 )
         {
            this.displayWishContainer(true);
            this.returnToCategoriesList();
         }
      })
  }
  slideNext()
   {
        this.slides.slideNext();
   }

    slidePrev()
   {
           this.slides.slidePrev();
   }
   

    deleteProduct(product)
   {
     const foundInDisplayList = this.listProductSelectedDisplay.findIndex(e => e._id === product._id);
     var quantity = this.listProductSelectedDisplay[foundInDisplayList].quantity
     for(var i=0; i < quantity ;i++)
     {
         if (this.listProductSelectedDisplay[foundInDisplayList].quantity <= 1) {
               this.listProductSelectedDisplay.splice(foundInDisplayList, 1);
         } else {
               this.listProductSelectedDisplay[foundInDisplayList].quantity -= 1;
         }


     // Delete in backend array
     const found = this.listProductSelected.findIndex(e => e._id === product._id);
     this.listProductSelected.splice(found, 1);

         this.notificationService.showToast({
           message: this.translate.instant('PRODUCT_DELETE_IN_SHOPPING_CART'),
           duration: 2000,
           color: 'success',
           position: 'bottom'
         });
         }
   }

  async choosePrescription(fileInputPrescription) {
    const result = await ActionSheet.showActions({
      title: this.translate.instant('ADD_JUSTIFICATION_FILE'),
      message: 'Sélectionner le type de fichier de votre ordonnance',
      options: [
        {
          title: 'Image',
        },
        {
          title: 'PDF',
        },
      ],
    });

    console.log('Action Sheet result:', result);

    switch(result.index){
      case 0:
        this.takePicture();
        break;
      case 1:
        fileInputPrescription.click();
        break;
    }
  }




      async getPharmaCategory(params)
      {
            const responseCategory =  await this.deliveryService.getPharmanityCategory()
            //console.log("category");
            console.log(responseCategory);

            this.categoriesProduct = responseCategory[params];
            this.ref_category = params;
      }



    async getProduct(id)
    {
          this.par_codeId = id;
          var responseProduct;
          var ref = this.ref_category == 'ref_medi' ? 'medi' : 'para';
          if(this.ph_id ==  0){
           responseProduct =  await this.deliveryService.getPharmanityProduct(id,ref,this.page );}
          else{
           responseProduct =  await this.deliveryService.getPharmanityProductByPharma(id,ref,this.page,this.ph_id);}

          //console.log("product");
          console.log(responseProduct);

          this.productList =responseProduct['products'];
          this.currentPage = this.page;
          this.par_category = ref;

           this.totalProduct = responseProduct['total'].total;
    }

    async displayMoreProduct() {
      this.limitProduct += 40
        var responseProduct;

        this.currentPage = this.currentPage  + 1;

        if(this.ph_id ==  0){
            responseProduct =   await this.deliveryService.getPharmanityProduct(this.par_codeId,this.par_category ,this.currentPage );}
        else{
            responseProduct =  await this.deliveryService.getPharmanityProductByPharma(this.par_codeId,this.par_category ,this.currentPage,this.ph_id);}

         responseProduct = responseProduct['products'];

         for(let product in responseProduct)
         {
            this.productList.push(responseProduct[product]);
         }
    }

/*
     async Page(symbol)
    {
          if(symbol == '+'){
              this.currentPage = this.currentPage  + 1;}
              else if( this.currentPage > 1) {
              this.currentPage = this.currentPage - 1;}

              var responseProduct;

                if(this.ph_id ==  0){
                         responseProduct =   await this.deliveryService.getPharmanityProduct(this.par_codeId,this.par_category ,this.currentPage );}
                        else{
                         responseProduct =  await this.deliveryService.getPharmanityProductByPharma(this.par_codeId,this.par_category ,this.currentPage,this.ph_id);}

          //console.log("product");
          //console.log(responseProduct);

          this.productList = responseProduct['products'];
    }
    */

    public  html;

    async detailProductById(id)
    {
        this.displayProductDetail = true;
        this.detailProduct =   await this.deliveryService.getPharmanityProductById(id);
        console.log(this.detailProduct);
        this.html = this.detailProduct.content;
        console.log(this.html);

    }

}
