import { Address } from './Address';

export class User {
    _id: string;
    avatar: string;
    lastName: string;
    mail: string;
    name: string;
    role: any;
    password: string;
    becomeDeliverer: boolean;
    favoriteAddress: Address;
    phoneNumber: string;
    status: string;
    addresses: Array<any>;
    typeDeliverer: String;
    defaultCardId: string;
    openingTime: any;
    mutual: any;
    connectionType: string;
    socialToken: string; 
    referralCode: string;
    docSended: boolean;
}