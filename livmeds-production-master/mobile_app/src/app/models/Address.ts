export class Address {
    address: String;
    city: String;
    postalCode: String;
    country: String;
    fullAddress: String;
    latitude: String;
    longitude: String;
}