import { User } from './User';
import { Address } from './Address';

export class Delivery {
    phoneNumber: String;
    customer : string;
    deliverer : string;
    merchant : string;
    address : Address;
    image: String;
    checkDeliverySuccessfullyByDeliverer: Boolean;
    checkDeliverySuccessfullyByCustomer: Boolean;
    latitude: Number;
    longitude: Number;
}