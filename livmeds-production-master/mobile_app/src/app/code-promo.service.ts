import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CodePromoService {

  constructor(private http: HttpClient) { }

  async generateReferralCode(){
    return await this.http.post(environment.url + "/customer/referral-code", {}).toPromise();
  }
}
