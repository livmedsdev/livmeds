import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { MainEventService } from './main-event.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SupportTicketsService {

  constructor(private http: HttpClient) { }

  async create(message){
    return await this.http.post(environment.url + "/customer/support-tickets", message).toPromise();
  }


  async list(filter){
    return await this.http.get(environment.url + "/customer/support-tickets", {params: filter}).toPromise();
  }

}
