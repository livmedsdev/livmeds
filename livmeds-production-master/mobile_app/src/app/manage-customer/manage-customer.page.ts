import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../user.service';
import { CustomerService } from '../customer.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-manage-customer',
  templateUrl: './manage-customer.page.html',
  styleUrls: ['./manage-customer.page.scss'],
})
export class ManageCustomerPage implements OnInit {
  customers;

  // Search
  txtQueryChanged: Subject<string> = new Subject<string>();
  limit = 5;
  total = 0;
  search;

  constructor(private eventEmitter : MainEventService,
    private translate: TranslateService, private customerService: CustomerService) { 
      this.txtQueryChanged
        .pipe(debounceTime(500), distinctUntilChanged())
        .subscribe(model => {
          this.refreshList();
          // api call
        });
      }

  ngOnInit() {
    this.customers = [];
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_MANAGE_CUSTOMER').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.eventEmitter.sendEventToHideModalLoader(false);
    await this.refreshList();
    this.eventEmitter.sendEventToHideModalLoader(true);
  }

  onSearchTextChange(event) {
    this.txtQueryChanged.next(event);
  }

  async refreshList(){
    let filter: any = {
      limit: this.limit
    }

    if (this.search) {
      filter.text = this.search;
    }
    const response = await this.customerService.list(filter);
    this.customers = response['data'];
    this.total = response['total'];
  }


  displayMore() {
    this.limit += 5
    this.refreshList();
  }
}
