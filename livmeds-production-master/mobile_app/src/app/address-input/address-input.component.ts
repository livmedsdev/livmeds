import { Component, OnInit, ElementRef, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { Address } from '../models/Address';

declare var $: any;
declare var google;

@Component({
  selector: 'app-address-input',
  templateUrl: './address-input.component.html',
  styleUrls: ['./address-input.component.scss'],
})
export class AddressInputComponent implements OnInit {

  @Input("required") required;

  invalid = false;

  ngOnInit(): void {
  }

  @ViewChild("address")
  public addressElement: ElementRef;
  public address: Address;

  @Output("onChange")
  private onChange;

  @Input()
  public defaultValue;

  constructor() {
    this.onChange = new EventEmitter();
  }

  ngAfterViewInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    autocomplete.setComponentRestrictions({ 'country': ['fr'] });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      let place = autocomplete.getPlace();
      this.address = this.retrieveAddressFromPlace(place);
      this.onChange.emit(this.address);
      this.validate(this.addressElement.nativeElement.value);
      this.addressElement.nativeElement.focus();
      this.addressElement.nativeElement.blur();
    });
  }

  ionViewWillEnter() {

  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components

    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    } as Address;
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }


  validate(value) {
    if (this.required === 'true' && (value === '' || !this.address)) {
      this.invalid = true;
    } else {
      this.invalid = false;
    }
  }

  @Input('checkValid') set checkValid(value: boolean) {
    if(this.addressElement){
      this.validate(this.addressElement.nativeElement.value);
    }
  }
}
