import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainEventService {
  
  hideTopBar = new EventEmitter();
  hideModalLoader = new EventEmitter();
  title = new EventEmitter();
  roleChange = new EventEmitter();
  hideSideBar = new EventEmitter();
  backButtonUrl = new EventEmitter();
  hideMenuButton = new EventEmitter();
  startWaitCustomer = new EventEmitter();
  sendCounterWaitCustomer = new EventEmitter();
  finishWaitCustomer = new EventEmitter();
  stopCounter = new EventEmitter();

  newMessageReceived = new EventEmitter();

  constructor() { }

  sendEventToHideTopBar(data: boolean) {
    this.hideTopBar.emit(data);
  }

  sendEventToHideModalLoader(data: boolean) {
    this.hideModalLoader.emit(data);
  }

  sendEventToChangeTitle(title: String){ 
    this.title.emit(title);
  }

  sendEventToChangeRole(role: String){
    this.roleChange.emit(role);
  }

  sendEventToHideSidebar(data){
    this.hideSideBar.emit(data);
  }

  setBackButtonUrl(data){
    this.backButtonUrl.emit(data);
  }

  sendEventToHideMenuButton(data){
    this.hideMenuButton.emit(data);
  }

  sendStartWaitCustomer(){
    this.startWaitCustomer.emit({});
  }

  sendCounter(data){
    this.sendCounterWaitCustomer.emit(data);
  }

  sendFinishWaitCustomer(data = {}){
    this.finishWaitCustomer.emit(data);
  }

  sendStopCounter(){
    this.stopCounter.emit({});
  }

  sendNewMessageEvent(data = {}){
    this.newMessageReceived.emit(data);
  }
}
