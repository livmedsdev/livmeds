import { Injectable } from '@angular/core';
import { ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  toast = null;

  constructor(public toastController: ToastController) {

  }

  async showToast(data:any){
      this.toast ? this.toast.dismiss() : false;
      this.toast = await this.toastController.create(data);
      this.toast.present();
  }
}
