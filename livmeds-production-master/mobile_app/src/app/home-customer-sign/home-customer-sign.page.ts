import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { SplashScreen } from '@capacitor/splash-screen';

@Component({
  selector: 'app-home-customer-sign',
  templateUrl: './home-customer-sign.page.html',
  styleUrls: ['./home-customer-sign.page.scss'],
})
export class HomeCustomerSignPage implements OnInit {

  constructor( private eventEmitter: MainEventService,private translate: TranslateService,) { }

    async ionViewWillEnter() {
      this.eventEmitter.setBackButtonUrl('/choice');
      this.eventEmitter.sendEventToChangeTitle(await this.translate.get('DISCOVER').toPromise());
      this.eventEmitter.sendEventToHideTopBar(true);
      this.eventEmitter.sendEventToHideMenuButton(true);
      this.eventEmitter.sendEventToHideModalLoader(true);
    }


  ngOnInit() {
    SplashScreen.hide();
  }

}
