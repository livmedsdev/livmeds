import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { UserService } from '../user.service';
import { SignInWithApple } from '@ionic-native/sign-in-with-apple';
import { User } from '../models/User';
import { UserRole } from '../enums/UserRole';
import { NotificationService } from '../notification.service';
import { parseTemplate } from '@angular/compiler';
import { TchatService } from '../tchat.service';
import { SessionService } from '../session.service';

declare var $: any;

@Component({
  selector: 'app-sign-up-option',
  templateUrl: './sign-up-option.page.html',
  styleUrls: ['./sign-up-option.page.scss'],
})
export class SignUpOptionPage implements OnInit {
  hideModal: boolean;

  constructor(private fb: Facebook,
    public platform: Platform,
    private router: NavController,
    private userService: UserService,
    private eventEmitter: MainEventService,
    public loadingController: LoadingController,
    private notificationService: NotificationService,
    private session: SessionService,
    private tchatService: TchatService,
    private translate: TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.eventEmitter.setBackButtonUrl('/home-customer-sign');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('SIGN_UP').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(true);
  }

  async signUpFacebook() {
    const loading = await this.loadingController.create({
      message: await this.translate.get('RETRIEVE_INFORMATION').toPromise()
    });
    await loading.present();
    try {
      await this.platform.ready();
      const response = await this.fb.login(['public_profile', 'email']);
      console.log(response);
      if (response.status === 'connected') {
        const userIdFb = response.authResponse.userID;
        const accessTokenFb = response.authResponse.accessToken;
        const userInfo = await this.userService.getInfoFacebook(accessTokenFb);
        const params = userInfo;
        params['userIdFb'] = userIdFb;
        params['accessTokenFb'] = accessTokenFb;
        params['connectionType'] = 'FACEBOOK';

        await loading.dismiss();
        this.router.navigateForward('/sign-up-email-customer', {
          queryParams: params
        })
      } else {
        throw 'Not connected'
      }
    } catch (error) {
      console.log(error);
      await this.notificationService.showToast({
        message: 'Erreur lors de la récupération des informations',
        duration: 5000,
        color: 'danger'
      });
      await loading.dismiss();
    }
  }

  async signUpApple() {
    const loading = await this.loadingController.create({
      message: await this.translate.get('RETRIEVE_INFORMATION').toPromise()
    });
    await loading.present();
    try {
      await this.platform.ready();
      const response = await SignInWithApple.signin({ requestedScopes: [0, 1] });
      const responseApi: any = await this.userService.storeInformation({
        userId: response.user,
        data: {
          name: response.fullName.givenName,
          lastName: response.fullName.familyName,
          mail: response.email,
        },
        raw: response
      });
      console.log(response);

      const params = {
        name: responseApi.name,
        lastName: responseApi.lastName,
        mail: responseApi.mail,
        userId: response.user,
        identityToken: response.identityToken,
        authorizationCode: response.authorizationCode,
        connectionType: 'APPLE'
      };
      await loading.dismiss();
      this.router.navigateForward('/sign-up-email-customer', {
        queryParams: params
      })
    } catch (error) {
      console.log(error);
      await this.notificationService.showToast({
        message: 'Erreur lors de la récupération des informations',
        duration: 5000,
        color: 'danger'
      });
      await loading.dismiss();
    }
  }
}
