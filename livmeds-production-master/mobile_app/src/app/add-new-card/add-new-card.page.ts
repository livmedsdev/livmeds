import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { CardService } from '../card.service';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { NotificationService } from '../notification.service';

declare var Stripe;

@Component({
  selector: 'app-add-new-card',
  templateUrl: './add-new-card.page.html',
  styleUrls: ['./add-new-card.page.scss'],
})
export class AddNewCardPage implements OnInit {
  stripe = Stripe(environment.apiKeyStripe);
  card: any;

  constructor(private eventEmitter: MainEventService,
    private cardService: CardService,
    private router: NavController,
    private translate: TranslateService) { }

  ngOnInit() {
    this.eventEmitter.sendEventToHideModalLoader(false);
  }

  async ionViewWillEnter() {
    this.eventEmitter.setBackButtonUrl('/add-credit-card');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_ADD_NEW_CARD').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    await this.cardService.reloadCardRegistration();
    this.cardService.setupStripe('#card-element', 'card-errors', 'payment-form',
    async () =>{
      this.router.pop();
    }, () =>{});
    this.eventEmitter.sendEventToHideModalLoader(true)
  }

}
