import { Component, OnInit, Input, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { UserService } from '../user.service';
import { DeliveryService } from '../delivery.service';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

import * as moment from 'moment';
import { MapsService } from '../maps.service';
import { IonInfiniteScroll, NavController, ToastController } from '@ionic/angular';
import { SessionService } from '../session.service';
import { NotificationService } from '../notification.service';
declare var google;

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  public deliveries = [];

  public page: number;
  public limit: number = 5;
  public nbTotal: number;

  constructor(private userService: UserService,
    private notificationService: NotificationService,
    private translate: TranslateService,
    private mapsService: MapsService,
    private session: SessionService,
    private router: NavController,
    private deliveryService: DeliveryService,
    private eventEmitter: MainEventService) { }

  async ngOnInit() {
    this.deliveries = [];
  }

  async ionViewWillEnter() {
    this.eventEmitter.setBackButtonUrl('/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_HISTORY').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.eventEmitter.sendEventToHideModalLoader(false);
    this.page = 0;
    this.deliveries = [];
    this.refreshList();
    this.eventEmitter.sendEventToHideModalLoader(true);

  }

  async initMaps(delivery) {
    const mapsElement = document.getElementById("map_" + delivery._id);
    console.log(mapsElement)
    let maps;
    try {
      const latlng = new google.maps.LatLng(39.305, -76.617);
      let mapOptions = {
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        fullscreenControl: false,
        center: latlng
      }

      maps = new google.maps.Map(mapsElement, mapOptions);

      let directionsService = new google.maps.DirectionsService;
      let latLng = new google.maps.LatLng(delivery.merchantFavoriteAddress.latitude, delivery.merchantFavoriteAddress.longitude);
      let latLngCustomer = new google.maps.LatLng(delivery.address.latitude, delivery.address.longitude);

      //Add direction
      const directionParameters = {
        origin: latLng,
        destination: latLngCustomer,
        travelMode: google.maps.TravelMode['DRIVING']
      };
      const directionsDisplay = new google.maps.DirectionsRenderer({
      });
      directionsDisplay.setOptions({
        polylineOptions: {
          strokeColor: '#66bb6a',
          strokeWeight: 5,
          strokeOpacity: 0
        }
      });
      console.log(directionParameters);
      directionsService.route(directionParameters, (result, status) => {
        if (status == 'OK') {
          directionsDisplay.setDirections(result);
          this.mapsService.renderPathWithoutMemory(result, maps);
        }
      });
      directionsDisplay.setMap(maps);

    } catch (error) {
      console.log(error);
    }
    return maps;
  }

  showOnMap(delivery) {
    localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(delivery));
    this.router.navigateRoot('/home-customer');
  }

  async cancel(delivery){
    try{
      const response : any = await this.deliveryService.cancelDelivery(delivery._id);
      await this.notificationService.showToast({
        message: await this.translate.get(response.tradCode).toPromise(),
        duration: 3000,
        color: 'success'
      });
      this.refreshAllList();
    }catch(error){
      await this.notificationService.showToast({
        message: error.error.tradCode,
        duration: 3000,
        color: 'danger'
      });
    }
  }

  async refreshList(){
    let resp: any = await this.deliveryService.getMyDeliveries({page: this.page, limit: this.limit});
    this.nbTotal = resp.total;

    moment.locale(this.session.getPreferences().lang);
    const data = resp.data.map((delivery) => {
      delivery.merchantFavoriteAddress = this.userService.getFavoriteAddress(delivery.merchant.addresses);
      delivery.formatDate = moment(delivery.insertAt).format('Do MMM YYYY, HH:mm');
      const utcDate = moment.utc(delivery.deliveryDate).toDate();
      delivery.formatDeliveryDate = moment(utcDate).format('Do MMM YYYY, HH:mm');

      return delivery;
    });

    this.deliveries = this.deliveries.concat(data);
    const intervalInitMaps = setInterval(() => {
      if (data.length > 0) {
        data.map((delivery) => {
          this.initMaps(delivery)
          return delivery;
        });
        clearInterval(intervalInitMaps);
      }
    }, 500)
  }

  async edit(delivery){
    this.router.navigateForward('/home-customer', {
      queryParams: {
        action: 'edit',
        id: delivery._id
      }
    })
  }

  async showDetails(delivery){
    this.router.navigateForward('/delivery-details', {
      queryParams: {
        id: delivery._id
      }
    })
  }


  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  async loadData(event) {
    this.page++;
    await this.refreshList();

    event.target.complete();
    if((this.page*this.limit) > this.nbTotal){
      event.target.disabled = true;
    }
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  async refreshAllList(){
    let limit = this.limit;
    limit = (this.page+1)*this.limit

    let resp: any = await this.deliveryService.getMyDeliveries({page: 0, limit: limit});
    this.nbTotal = resp.total;

    moment.locale(this.session.getPreferences().lang);
    const data = resp.data.map((delivery) => {
      delivery.merchantFavoriteAddress = this.userService.getFavoriteAddress(delivery.merchant.addresses);
      delivery.formatDate = moment(delivery.insertAt).format('Do MMM YYYY, HH:mm');
      const utcDate = moment.utc(delivery.deliveryDate).toDate();
      delivery.formatDeliveryDate = moment(utcDate).format('Do MMM YYYY, HH:mm');

      return delivery;
    });

    this.deliveries = data;
    const intervalInitMaps = setInterval(() => {
      if (data.length > 0) {
        data.map((delivery) => {
          this.initMaps(delivery)
          return delivery;
        });
        clearInterval(intervalInitMaps);
      }
    }, 500)
  }
}
