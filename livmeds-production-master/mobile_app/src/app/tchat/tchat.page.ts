import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { MessageService } from '../message.service';
import { SessionService } from '../session.service';
import { FileService } from '../file.service';
import { UserRole } from '../enums/UserRole';
import { File } from '@ionic-native/file/ngx';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { EventPollingService } from '../event-polling.service';

@Component({
  selector: 'app-tchat',
  templateUrl: './tchat.page.html',
  styleUrls: ['./tchat.page.scss'],
})
export class TchatPage {
  private delivery;
  public privateMessages: any = [];
  public message: String;
  private typePrivateMessage;
  private authorType;
  public recordInProgress : boolean = false;
  public audioPath;
  public destAvatar;

  subRefreshTchat;

  constructor(private _eventEmitter: MainEventService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private messageService: MessageService,
    private deliveryService: DeliveryService, 
    private fileIonic: File,
    // private fileTransfer: FileTransfer,
    public fileService: FileService,
    private eventPolling: EventPollingService,
    public session: SessionService) { }

  async ionViewWillEnter() {
    this.initSubs();
    const params = this.route.snapshot.queryParams;
    this.typePrivateMessage = params.typePrivateMessage;
    this.authorType = params.authorType;
    if (this.authorType === 'customer') {
      this.delivery = await this.deliveryService.getDeliveryByIdWithCustomer(params.delivery);
      if(this.typePrivateMessage === 'TO_PHARMACY'){
        this.destAvatar = (this.delivery.merchant.avatar)?await this.fileService.getSignedUrl(this.delivery.merchant.avatar):undefined;
      }else if(this.typePrivateMessage === 'TO_DELIVERER'){
        this.destAvatar = (this.delivery.deliverer.avatar)?await this.fileService.getSignedUrl(this.delivery.deliverer.avatar):undefined;
      }
    } else {
      this.delivery = await this.deliveryService.getDeliveryById(params.delivery);
      if(this.typePrivateMessage === 'TO_DELIVERER'){
        this.destAvatar = (this.delivery.customer.avatar)?await this.fileService.getSignedUrl(this.delivery.customer.avatar):undefined;
      }
    }
    await this.eventPolling.refreshListTchat({
      authorType: this.authorType,
      delivery: this.delivery,
      typePrivateMessage: this.typePrivateMessage
    });
    this.eventPolling.eventStartTchat.emit({
      authorType: this.authorType,
      delivery: this.delivery,
      typePrivateMessage: this.typePrivateMessage
    });
    this.message = '';
    this._eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/home-deliverer' : '/home-customer');
    this._eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_CHAT').toPromise());
    this._eventEmitter.sendEventToHideMenuButton(false);
    this._eventEmitter.sendEventToHideTopBar(false);
    this._eventEmitter.sendEventToHideModalLoader(true);
  }

  initSubs(){
    this.subRefreshTchat = this.eventPolling.eventRefreshTchat.subscribe((data) =>{
      if(data.changes !== false){
        this.privateMessages = data.messages;
      }
    });
  }

  ionViewDidLeave() {
    if(this.subRefreshTchat){
      this.subRefreshTchat.unsubscribe();
      this.subRefreshTchat = undefined;
    }
    this._eventEmitter.sendEventToHideModalLoader(true);
    this.eventPolling.eventStopTchat.emit();
  }

  async sendMessage() {
    try {
      await this.messageService.create(this.authorType, {
        delivery: this.delivery._id,
        message: this.message,
        typePrivateMessage: this.typePrivateMessage
      });
      await this.eventPolling.refreshListTchat({
        authorType: this.authorType,
        delivery: this.delivery,
        typePrivateMessage: this.typePrivateMessage
      });
      this.message = '';
    } catch (error) {
      console.log(error);
    }
  }

  // startRecord() {
  //   this.recordInProgress = true;
  //   this.audioPath=this.fileIonic.tempDirectory;
  //   this.fileIonic.createFile(this.audioPath, 'my_file.mp3', true).then(() => {
  //     this.file = this.media.create(this.fileIonic.tempDirectory.replace(/^file:\/\//, '') + 'my_file.mp3');
  //     this.file.startRecord();
  //   });
  //   console.log('start');
  // }

  // async stopRecord() {
  //   this.recordInProgress = false;
  //   console.log('stop');
  //   this.file.stopRecord();
  //   const response = await this.fileService.uploadFile(this.audioPath +  'my_file.mp3','record.mp3');
  //   console.log(response);

  //   const resp = JSON.parse(response.response);

  //   try {
  //     await this.messageService.create(this.authorType, {
  //       delivery: this.delivery._id,
  //       audioFile: resp['idFile'],
  //       typePrivateMessage: this.typePrivateMessage
  //     });
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }


  // async startAudioMessage(message){
  //   const localPath = this.fileIonic.dataDirectory + message.audioFile + '.mp3';
  //   const url = await this.fileService.downloadLocally(localPath, message.audioFile);
  //   const file: MediaObject = this.media.create(url.replace(/^file:\/\//, ''));
  //   file.play();
  // }
}
