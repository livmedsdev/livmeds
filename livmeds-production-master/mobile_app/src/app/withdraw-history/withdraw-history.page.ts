import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

declare var config: any;
declare var config1: any;
declare var config2: any;
declare var config3: any;
declare var Chart: any;
declare var $: any;

@Component({
  selector: 'app-withdraw-history',
  templateUrl: './withdraw-history.page.html',
  styleUrls: ['./withdraw-history.page.scss'],
})
export class WithdrawHistoryPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
    let win = window as any;
    var ctxCanvas = document.getElementById('canvas') as HTMLCanvasElement;
    let ctx = ctxCanvas.getContext('2d');
    win.myLine = new Chart(ctx, config);
    var ctxCanvas1 = document.getElementById('canvas1') as HTMLCanvasElement;
    let ctx1 = ctxCanvas1.getContext('2d');
    win.myLine = new Chart(ctx1, config1);
    var ctxCanvas2 = document.getElementById('canvas2') as HTMLCanvasElement;
    let ctx2 = ctxCanvas2.getContext('2d');
    win.myLine = new Chart(ctx2, config2);
    var ctxCanvas3 = document.getElementById('canvas3') as HTMLCanvasElement;
    let ctx3 = ctxCanvas3.getContext('2d');
    win.myLine = new Chart(ctx3, config3);


    if ($('.slider-container').length > 0) {
      $('.slider-container').owlCarousel({
        autoPlay: true,
        autoPlayInterval: 10,
        responsiveClass: true,
        merge: true,
        responsive: {
          678: {
            mergeFit: true
          },
          1000: {
            mergeFit: false
          }
        },
        items: 2,
        itemsDesktop: [1199, 2],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 2],
        itemsTabletSmall: false,
        itemsMobile: [479, 2]
      });
    }
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/wallet');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_WITHDRAW_HISTORY').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }
}
