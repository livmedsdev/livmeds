import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../enums/UserRole';
import { CardService } from '../card.service';
import { SessionService } from '../session.service';
import { UserService } from '../user.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-add-credit-card',
  templateUrl: './add-credit-card.page.html',
  styleUrls: ['./add-credit-card.page.scss'],
})
export class AddCreditCardPage implements OnInit {
  public cards;


  constructor(private eventEmitter: MainEventService,
    private cardService: CardService,
    private userService: UserService,
    public session: SessionService,
    private loadingController: LoadingController,
     private translate : TranslateService) { }

  ngOnInit() {
    this.eventEmitter.sendEventToHideModalLoader(false);
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/add-payout-methods' :'/payment-method');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_MY_CREDIT_CARDS').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.eventEmitter.sendEventToHideModalLoader(false);
    this.cards = (await this.cardService.list())['data'];
    this.eventEmitter.sendEventToHideModalLoader(true);
  }

  async setDefaultCard(cardId){
    await this.cardService.setDefaultCard(cardId);
    await this.userService.getProfile();
    
  }

  async deleteCard(card){
    const loading = await this.loadingController.create({
      message: await this.translate.get('DELETE_CARD_IN_PROGRESS').toPromise()
    });
    try{
      await loading.present();
      await this.cardService.delete(card.id);
      this.cards = (await this.cardService.list())['data'];
      await loading.dismiss();
    }catch(error){
      console.log(error);
      await loading.dismiss();
    }
  }
}
