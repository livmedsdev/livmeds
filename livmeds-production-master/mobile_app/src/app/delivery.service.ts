import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  constructor(private http: HttpClient) { }

  async create(delivery){
    const formData = new FormData();
    
    if(delivery.image){
      for(let image of delivery.image){
        let res = await fetch(image);
        let blob = await res.blob();
    
        formData.append('file', blob, "order.jpg");
      }
      delete delivery.image;
      delete delivery.newImages;
    }
    for(let key of Object.keys(delivery)){
      formData.append(key, JSON.stringify(delivery[key]));
    }

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.post(environment.url + '/customer/delivery',
      formData, { headers: headers }).toPromise();
  }

  async update(delivery, newImages){
    const formData = new FormData();
    
    if(newImages.length > 0){
      for(let image of newImages){
        let res = await fetch(image);
        let blob = await res.blob();
    
        formData.append('file', blob, "order.jpg");
      }
      delete delivery.newImages;
    }

    if(delivery.image){
      delete delivery.image;
    }

    for(let key of Object.keys(delivery)){
      formData.append(key, JSON.stringify(delivery[key]));
    }

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.put(environment.url + "/customer/delivery/edit/"+delivery._id,
      formData, { headers: headers }).toPromise();
  }

  async getMyDeliveries(params){ 
    return await this.http.get(environment.url + "/customer/deliveries", { params: params}).toPromise();
  }

    async getPharmanity() {
      return await this.http.get(environment.url + "/customer/pharmanity").toPromise();
    }

       async getPharmanityCategory() {
      return await this.http.get(environment.url + "/customer/pharmanity/category").toPromise();
    }

    async getPharmanityProduct(idCategory,params,page) {

    return await this.http.get(environment.url + "/customer/pharmanity/category/products/"+idCategory+"/"+params+"/"+page).toPromise();
    }

      async getPharmanityProductByPharma(idCategory,params,page,ph_id) {

    return await this.http.get(environment.url + "/customer/pharmanity/category/products/"+idCategory+"/"+params+"/"+page+"/"+ph_id).toPromise();
    }
      async getPharmanityProductById(id) {

        return await this.http.get(environment.url + "/customer/pharmanity/category/products/"+id).toPromise();
    }

      async getPharmanityProductByName(name) {

    return await this.http.get(environment.url + "/customer/pharmanity/category/productsByName/"+name).toPromise();
    }

  async cancelDelivery(idDelivery){
    return await this.http.put(environment.url + "/customer/delivery/cancel/"+idDelivery, null).toPromise();
  }

  async acceptAmountForDelivery(idDelivery){
    return await this.http.put(environment.url + "/customer/delivery/valid/"+idDelivery, null).toPromise();
  }

  async refuseAmountForDelivery(idDelivery){
    return await this.http.put(environment.url + "/customer/delivery/refuse/"+idDelivery, null).toPromise();
  }

  async getDeliveriesAvailable(){
    return await this.http.get(environment.url + "/deliverer/deliveries/valid").toPromise();
  }

  async acceptThisDelivery(id, accept: boolean){
    return await this.http.put(environment.url + "/deliverer/delivery/accept/" + id, null).toPromise();
  }

  async getDeliveryById(id){
    return await this.http.get(environment.url + "/deliverer/delivery/"+ id).toPromise();
  }


  async listMyDeliveriesByDeliverer(params){
    return await this.http.get(environment.url + "/deliverer/deliveries", { params: params}).toPromise();
  }

  async getStats(){
    return await this.http.get(environment.url + "/deliverer/stats", {}).toPromise();
  }

  async getDeliveryByIdWithCustomer(id){
    return await this.http.get(environment.url + "/customer/delivery/"+ id).toPromise();
  }

  async addTrack(id, data){
    return await this.http.post(environment.url + "/deliverer/track/"+ id, data).toPromise();
  }

  async trackDelivery(id){
    return await this.http.get(environment.url + "/customer/track/"+ id).toPromise();
  }

  async recoveredPackageByDeliverer(id){
    return await this.http.put(environment.url + "/deliverer/delivery/recovered/"+ id, null).toPromise();
  }

  async reachDestination(id){
    return await this.http.put(environment.url + "/deliverer/delivery/reachDestination/"+ id, null).toPromise();
  }

  async returnPackage(id){
    return await this.http.put(environment.url + "/deliverer/delivery/returnPackage/"+ id, null).toPromise();
  }


  async deliveredPackageByDeliverer(id, body){
    return await this.http.put(environment.url + "/deliverer/delivery/delivered/"+ id, body).toPromise();
  }

  async deliveryReceive(id, received){
    return await this.http.put(environment.url + "/customer/delivery/received/" + id, {received: received}).toPromise();
  }

  async refuseThisDelivery(id){
    return await this.http.put(environment.url + "/deliverer/delivery/refuse/" + id, null).toPromise();
  }

  async listShops(filter){
    return await this.http.get(environment.url + "/shops", {params: filter}).toPromise();
  }

  async getMyDeliveryInProgress(){
    return (await this.http.get(environment.url + "/deliverer/in-delivery", {}).toPromise())['delivery'];
  }

  async rate(body){
    return await this.http.post(environment.url + "/customer/rate", body).toPromise();
  }

  async changeCard(deliveryId, cardId){
    return await this.http.put(environment.url + `/customer/delivery/card/${deliveryId}`, {cardId}).toPromise();
  }
}
