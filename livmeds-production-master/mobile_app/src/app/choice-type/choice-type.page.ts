import { Component, OnInit } from '@angular/core';
import { Appsflyer } from '@ionic-native/appsflyer/ngx';
import { Device } from '@ionic-native/device/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Platform } from '@ionic/angular';
import { MainEventService } from '../main-event.service';
import { FirebaseAnalytics } from "@capacitor-community/firebase-analytics";
import { SplashScreen } from '@capacitor/splash-screen';

@Component({
  selector: 'app-choice-type',
  templateUrl: './choice-type.page.html',
  styleUrls: ['./choice-type.page.scss'],
})
export class ChoiceTypePage implements OnInit {

  constructor(private eventEmitter: MainEventService,
    private appsflyer: Appsflyer,
    private platform: Platform,
    private device: Device,
    private facebook: Facebook
  ) { }

  ngOnInit() {
      FirebaseAnalytics.setScreenName({
        screenName: "menu",
        nameOverride: "Menu",
      });
      this.appsflyer.logEvent('SCREEN_VIEW', {
        "name": "Menu"
      });
  }

  ionViewWillEnter() {
    this.eventEmitter.setBackButtonUrl(null);
    this.eventEmitter.sendEventToHideTopBar(true);
    const interval_id = window.setInterval("", 9999);
    for (let i = 1; i < interval_id; i++) {
      window.clearInterval(i);
    }
    this.eventEmitter.sendEventToHideModalLoader(true);
    SplashScreen.hide();
  }
}
