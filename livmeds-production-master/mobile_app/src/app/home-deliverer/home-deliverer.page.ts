import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { NavController, AlertController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { PositionService } from '../position.service';
import { MapsService } from '../maps.service';
import { UserService } from '../user.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../session.service';
import { DeliveryStatus } from '../enums/DeliveryStatus';
import { FileService } from '../file.service';
import { EventPollingService } from '../event-polling.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { NotificationService } from '../notification.service';
import { registerPlugin } from "@capacitor/core";
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SplashScreen } from '@capacitor/splash-screen';
const BackgroundGeolocation: any = registerPlugin("BackgroundGeolocation");

declare var $: any;
declare var showMapRequestPin: any;
declare var hideMapRequestPin: any;
declare var google;
declare var renderDirectionsPolylines: any;
declare var Math: any;

declare var window: any;
declare var requestAnimFrame: any;

@Component({
  selector: 'app-home-deliverer',
  templateUrl: './home-deliverer.page.html',
  styleUrls: ['./home-deliverer.page.scss'],
})
export class HomeDelivererPage implements OnInit {

  @ViewChild('mapDeliverer') mapElement: ElementRef;

  @ViewChild('canvasElement') canvasElement: ElementRef;

  map: any;
  selfMarker;
  watchSubscribePosition;
  intervalToGetDelivery;
  intervalToRefreshDelivery;
  deliveryReceipt;

  //Marker informations
  merchantMarker;
  customerMarker;
  merchantPosition;
  customerPosition;
  directionsDisplay;
  steps;

  //hide variable
  hideNotifNewRequest;
  hidePickupNotification;
  hideDropOff;
  hidePickUp;
  hideOfflineNotif;
  hideReachCustomer;
  hideReturnPackage;
  isEnableCountdown;
  hideDeliveryBill;

  eventAlreadyInitialize;
  lastPositionChanged;

  timeLeft: number = 600;
  interval;
  private intervalWaitCustomer;
  private subFinishCounter;
  private subCheckStatus;
  minuteLeft = 10;
  secondLeft = 0;

  sliderElem;
  hideAwaitingValidationAccount;
  mapLoaded;

  subGetDeliveriesValid;
  subRefreshDelivery;
  subPrivateMessages;
  count;
  firstTimeCheckStep = true;

  promiseComputeDirection;

  private lastUpdateTime;
  private minFrequency = 3 * 1000;

  watcherId: string;

  constructor(public userService: UserService,
    private notificationService: NotificationService,
    private platform: Platform,
    private geolocation: Geolocation,
    private _eventEmitter: MainEventService,
    private positionService: PositionService,
    private mapsService: MapsService,
    public loadingController: LoadingController,
    private translate: TranslateService,
    private clipboard: Clipboard,
    private deliveryService: DeliveryService,
    private router: NavController,
    public session: SessionService,
    private eventPolling: EventPollingService,
    private fileService: FileService,
    public alertController: AlertController) {

  }

  async ngOnInit() {
    this.mapLoaded = false;
    this.hideAwaitingValidationAccount = true;
    this.deliveryReceipt = { _id: null, customerOfMerchant: {}, customer: {}, merchant: {}, address: {}, merchantFavoriteAddress: {}, invoice: {} };
    this.selfMarker = new google.maps.Marker({
      icon: {
        url: "../assets/images/maps/logo_deliverer.png",
        scaledSize: { width: 40, height: 40 },
        anchor: { x: 20, y: 20 },
      }
    });
    this.isEnableCountdown = false;
  }

  async ngAfterViewInit() {
    window.requestAnimFrame = (function () {
      return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimaitonFrame ||
        function (callback) {
          window.setTimeout(callback, 1000 / 60);
        };
    })();

    let canvas = this.canvasElement.nativeElement;
    let ctx = canvas.getContext("2d");
    ctx.strokeStyle = "#222222";
    ctx.lineWidth = 4;

    let drawing = false;
    let mousePos = {
      x: 0,
      y: 0
    };
    let lastPos = mousePos;

    canvas.addEventListener("mousedown", function (e) {
      drawing = true;
      lastPos = getMousePos(canvas, e);
    }, false);

    canvas.addEventListener("mouseup", function (e) {
      drawing = false;
    }, false);

    canvas.addEventListener("mousemove", function (e) {
      mousePos = getMousePos(canvas, e);
    }, false);

    // Add touch event support for mobile
    canvas.addEventListener("touchstart", function (e) {

    }, false);

    canvas.addEventListener("touchmove", function (e) {
      var touch = e.touches[0];
      var me = new MouseEvent("mousemove", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(me);
    }, false);

    canvas.addEventListener("touchstart", function (e) {
      mousePos = getTouchPos(canvas, e);
      var touch = e.touches[0];
      var me = new MouseEvent("mousedown", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(me);
    }, false);

    canvas.addEventListener("touchend", function (e) {
      var me = new MouseEvent("mouseup", {});
      canvas.dispatchEvent(me);
    }, false);

    function getMousePos(canvasDom, mouseEvent) {
      var rect = canvasDom.getBoundingClientRect();
      return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
      }
    }

    function getTouchPos(canvasDom, touchEvent) {
      var rect = canvasDom.getBoundingClientRect();
      return {
        x: touchEvent.touches[0].clientX - rect.left,
        y: touchEvent.touches[0].clientY - rect.top
      }
    }

    function renderCanvas() {
      if (drawing) {
        ctx.moveTo(lastPos.x, lastPos.y);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
        lastPos = mousePos;
      }
    }

    // Prevent scrolling when touching the canvas
    document.body.addEventListener("touchstart", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchend", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchmove", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);

    (function drawLoop() {
      requestAnimFrame(drawLoop);
      renderCanvas();
    })();

    await this.initEvent();

  }

  clearCanvas() {
    this.canvasElement.nativeElement.width = this.canvasElement.nativeElement.width;
  }

  async ionViewWillEnter() {
    this._eventEmitter.sendEventToChangeRole(localStorage.getItem('role'));
    this._eventEmitter.sendEventToHideModalLoader(false);
    this._eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_MAPS').toPromise());
    this._eventEmitter.sendEventToHideTopBar(false);
    this._eventEmitter.sendEventToHideMenuButton(false);
    this._eventEmitter.setBackButtonUrl(null);
    this.hideOfflineNotif = false;
    this.setStatusDeliverer();
    this.firstTimeCheckStep = true;
    this.initSubs();
    this.hideNotifNewRequest = true;
    this.hidePickupNotification = true;
    this.hideDropOff = true;
    this.hidePickUp = true;
    this.hideReachCustomer = true;
    this.hideReturnPackage = true;
    this.hideDeliveryBill = true;
    await this.loadMap();
    if (this.session.currentUser().status === 'AWAITING_VALIDATION') {
      this.hideAwaitingValidationAccount = false;
    }

    this.merchantMarker = new google.maps.Marker({
      icon: "../assets/images/maps/logo_pharma.png",
      scaledSize: { width: 40, height: 40 },
      anchor: { x: 20, y: 20 },
    });

    this.customerMarker = new google.maps.Marker({
      icon: "../assets/images/maps/logo_home.png",
      scaledSize: { width: 50, height: 50 },
      anchor: { x: 25, y: 25 },
    });
    this.steps = [];
    this.deliveryReceipt = { _id: null, customerOfMerchant: {}, customer: {}, merchant: {}, address: {}, merchantFavoriteAddress: {}, invoice: {} };

    const myDelivery = await this.deliveryService.getMyDeliveryInProgress();
    const lastDeliveryInMemory = localStorage.getItem('lastDelivery_' + this.session.currentUser()._id);
    if ((myDelivery && !lastDeliveryInMemory)
      || (myDelivery && lastDeliveryInMemory && lastDeliveryInMemory['_id'] !== myDelivery._id)) {
      if (myDelivery.merchant) {
        myDelivery.merchantFavoriteAddress = this.userService.getFavoriteAddress(myDelivery.merchant.addresses);
      } else if (myDelivery.partner) {
        myDelivery.merchantFavoriteAddress = myDelivery.partner.address;
      }
      localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(myDelivery));
    }
    await this.checkStatusDeliverer();
    SplashScreen.hide();
  }

  async loadMap() {
    try {
      let mapOptions = {
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        fullscreenControl: false,
      }
      console.log(this.mapElement.nativeElement);

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.map.mapTypes.set('styled_map', (new Date().getHours() >= 20 || new Date().getHours() <= 8) ? this.mapsService.styles.dark : this.mapsService.styles.light);
      this.map.setMapTypeId('styled_map');

      console.log("styled maps");
      google.maps.event.addListenerOnce(this.map, 'idle', async () => {
        console.log('loaded');
        this.mapLoaded = true;
      })


    } catch (error) {
      console.log(error);
    }
  }

  initEvent() {
    if (!this.eventAlreadyInitialize) {
      this.eventAlreadyInitialize = true;
      $('.change-request-status .slider').on('click', this.clickSlider.bind(this));
    }
  }

  async clickSlider(event) {
    this.stopIntervalToGetDeliveriesValid();
    this.hideOfflineNotif = !this.hideOfflineNotif;
    $('.request-ride-container').addClass('hidden');
    if (!this.hideOfflineNotif) {
      localStorage.setItem('statusDelivererer', 'false');
      this.selfMarker.setMap(null);
      $('.new-request-notification').addClass('hidden');
      $('.meters-left-450').addClass('hidden');
      $('.meters-left-50').addClass('hidden');
      $('.new-request').addClass('hidden');
      this._eventEmitter.sendEventToChangeTitle(await this.translate.get('STATUS_OFFLINE').toPromise());
      this.stopWatch()
      await this.userService.setOnline(false);
    }
    else {
      localStorage.setItem('statusDelivererer', 'true');
      await this.userService.setOnline(true);
      await this.setOnlineDeliverer();
    }
  }

  async setOnlineDeliverer() {
    console.log("wait position")
    let position = await this.internalGetCurrentPosition();
    const selfPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    this.selfMarker.setPosition(selfPosition);
    console.log("finish wait position")
    this.selfMarker.setMap(this.map);
    this.map.setCenter(selfPosition);
    if (!localStorage.getItem('lastDelivery_' + this.session.currentUser()._id)) {
      this.eventPolling.eventStartGetDeliveriesValid.emit({});
    }

    this._eventEmitter.sendEventToChangeTitle(await this.translate.get('STATUS_ONLINE').toPromise());

    this._eventEmitter.sendEventToHideModalLoader(true);
    if (this.watcherId) {
      return;
    }
    this.initWatch();
  }

  stopWatch() {
    if (this.watcherId) {
      BackgroundGeolocation.removeWatcher({
        id: this.watcherId
      });
      delete this.watcherId;
    }
  }

  initWatch() {

    BackgroundGeolocation.addWatcher(
      {
        // If the "backgroundMessage" option is defined, the watcher will
        // provide location updates whether the app is in the background or the
        // foreground. If it is not defined, location updates are only
        // guaranteed in the foreground. This is true on both platforms.

        // On Android, a notification must be shown to continue receiving
        // location updates in the background. This option specifies the text of
        // that notification.
        backgroundMessage: "Cancel to prevent battery drain.",

        // The title of the notification mentioned above. Defaults to "Using
        // your location".
        backgroundTitle: "Tracking You.",

        // Whether permissions should be requested from the user automatically,
        // if they are not already granted. Defaults to "true".
        requestPermissions: true,

        // If "true", stale locations may be delivered while the device
        // obtains a GPS fix. You are responsible for checking the "time"
        // property. If "false", locations are guaranteed to be up to date.
        // Defaults to "false".
        stale: false,

        // The minimum number of metres between subsequent locations. Defaults
        // to 0.
        distanceFilter: 50
      }, async (location) => {

        const now = new Date();
        if (this.lastUpdateTime && now.getTime() - this.lastUpdateTime.getTime() < this.minFrequency) {
          console.log("Ignoring position update");
          return;
        }
        this.lastUpdateTime = now;

        // do something with position
        if (!this.promiseComputeDirection) {
          this.promiseComputeDirection = true;
          try {
            let latLng = new google.maps.LatLng(location.latitude, location.longitude);
            this.selfMarker.setPosition(latLng);
            this.map.setCenter(latLng);
            if (this.deliveryReceipt !== null && this.deliveryReceipt.status === DeliveryStatus.IN_DELIVERY) {
              await this.computeDirection(location.latitude, location.longitude);
              this.deliveryService.addTrack(this.deliveryReceipt._id, { latitude: location.latitude, longitude: location.longitude });
            } else {
              this.positionService.sendPosition(location.latitude, location.longitude);
            }
          } catch (error) { }
          this.promiseComputeDirection = false;
          console.log('FINISH')
        }

      }).then((watchId) => {
        // When a watcher is no longer needed, it should be removed by calling
        // 'removeWatcher' with an object containing its ID.
        this.watcherId = watchId
      });
  }

  initSubs() {
    this.subGetDeliveriesValid = this.eventPolling.eventGetDeliveriesValid.subscribe((data) => {
      const myDelivery = data.deliveriesInProgress;
      const lastDeliveryInMemory = localStorage.getItem('lastDelivery_' + this.session.currentUser()._id);
      if ((myDelivery && !lastDeliveryInMemory)
        || (myDelivery && lastDeliveryInMemory && lastDeliveryInMemory['_id'] !== myDelivery._id)) {
        if (myDelivery.merchant) {
          myDelivery.merchantFavoriteAddress = this.userService.getFavoriteAddress(myDelivery.merchant.addresses);
        } else if (myDelivery.partner) {
          myDelivery.merchantFavoriteAddress = myDelivery.partner.address;
        }
        this.stopIntervalToGetDeliveriesValid();
        localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(myDelivery));
        this.initDeliveryMap(myDelivery);
      } else {
        const deliveries = data.deliveriesValid;
        if (deliveries.length > 0 && deliveries[0].status === DeliveryStatus.VALID) {
          this.deliveryReceipt = deliveries[0];
          if (this.deliveryReceipt.merchant) {
            this.deliveryReceipt.merchantFavoriteAddress = this.userService.getFavoriteAddress(this.deliveryReceipt.merchant.addresses);
          } else if (this.deliveryReceipt.partner) {
            this.deliveryReceipt.merchantFavoriteAddress = this.deliveryReceipt.partner.address;
          }
          this.stopIntervalToGetDeliveriesValid();
          this.hideNotifNewRequest = false;
        }
      }
    })

    this.subRefreshDelivery = this.eventPolling.eventRefreshDeliveryDeliverer.subscribe(async (delivery) => {
      if (delivery === 'error') {
        this.eventPolling.eventStopRefreshDeliveryDeliverer.emit({});
        this.eventPolling.eventStopCheckStatusDelivery.emit({});
        const alertCancelled = await this.alertController.create({
          backdropDismiss: false,
          header: await this.translate.get('ALERT_DELIVERY').toPromise(),
          message: await this.translate.get('DELIVERY_REASSIGNED_OR_EXPIRED').toPromise(),
          buttons: [{
            text: await this.translate.get('BTN_OKAY').toPromise(),
            handler: () => {
              this.resetMap();
            }
          }
          ]
        });
        alertCancelled.present();
        return;
      }
      if (delivery.status === DeliveryStatus.CANCELLED) {
        this.eventPolling.eventStopRefreshDeliveryDeliverer.emit({});
        this.eventPolling.eventStopCheckStatusDelivery.emit({});
        const alertCancelled = await this.alertController.create({
          backdropDismiss: false,
          header: await this.translate.get('ALERT_DELIVERY_CANCELLED').toPromise(),
          message: await this.translate.get('DESCRIPTION_DELIVERY_CANCELLED').toPromise(),
          buttons: [{
            text: await this.translate.get('BTN_OKAY').toPromise(),
            handler: async () => {
              this.resetMap();
            }
          }
          ]
        });
        alertCancelled.present();
        return;
      }
      if (!this.deliveryReceipt.packageGivenToDeliverer) {
        this.deliveryReceipt = delivery;
        if (this.deliveryReceipt.merchant) {
          this.deliveryReceipt.merchantFavoriteAddress = this.userService.getFavoriteAddress(this.deliveryReceipt.merchant.addresses);
        } else if (this.deliveryReceipt.partner) {
          this.deliveryReceipt.merchantFavoriteAddress = this.deliveryReceipt.partner.address;
        }
        if (this.deliveryReceipt.packageGivenToDeliverer) {
          localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(this.deliveryReceipt));
          this.eventPolling.eventStopRefreshDeliveryDeliverer.emit({});
        }
      } else {
        if (this.firstTimeCheckStep) {
          this.firstTimeCheckStep = false
        }
        this.eventPolling.eventStopRefreshDeliveryDeliverer.emit({});
      }
    })

    this.subCheckStatus = this.eventPolling.eventCheckStatusDelivery.subscribe(async (delivery) => {
      if (delivery.status === DeliveryStatus.CANCELLED) {
        this.eventPolling.eventStopRefreshDeliveryDeliverer.emit({});
        this.eventPolling.eventStopCheckStatusDelivery.emit({});
        const alertCancelled = await this.alertController.create({
          backdropDismiss: false,
          header: await this.translate.get('ALERT_DELIVERY_CANCELLED').toPromise(),
          message: await this.translate.get('DESCRIPTION_DELIVERY_CANCELLED').toPromise(),
          buttons: [{
            text: await this.translate.get('BTN_OKAY').toPromise(),
            handler: async () => {
              this.resetMap();
            }
          }
          ]
        });
        alertCancelled.present();
      } else if (delivery.status === DeliveryStatus.DELIVERED) {
        this.eventPolling.eventStopRefreshDeliveryDeliverer.emit({});
        this.eventPolling.eventStopCheckStatusDelivery.emit({});
        const alertCancelled = await this.alertController.create({
          backdropDismiss: false,
          header: await this.translate.get('ALERT').toPromise(),
          message: await this.translate.get('DELIVERED_PACKAGE').toPromise(),
          buttons: [{
            text: await this.translate.get('BTN_OKAY').toPromise(),
            handler: async () => {
              this.resetMap();
            }
          }
          ]
        });
        alertCancelled.present();
      }
    })

    this.count = 0;
    this.subPrivateMessages = this._eventEmitter.newMessageReceived.subscribe((data) => {
      this.count = this.session.getNbPrivateMessageReceived(this.deliveryReceipt._id, 'TO_DELIVERER');
    })
  }

  startIntervalToRefreshDelivery() {
    this.eventPolling.eventStartRefreshDeliveryDeliverer.emit(this.deliveryReceipt);
  }

  stopIntervalToGetDeliveriesValid() {
    this.eventPolling.eventStopGetDeliveriesValid.emit({});
  }

  resetMap() {
    this.directionsDisplay.setMap(null);
    this.merchantMarker.setMap(null);
    this.customerMarker.setMap(null);
    this._eventEmitter.sendStopCounter();
    this.isEnableCountdown = false;
    this.firstTimeCheckStep = true;
    this.cleanSteps();
    this.timeLeft = 600;
    this.hidePickupNotification = true;
    this.hideDeliveryBill = true;
    localStorage.removeItem('lastDelivery_' + this.session.currentUser()._id);
    this.deliveryReceipt = { _id: null, customerOfMerchant: {}, customer: {}, merchant: {}, address: {}, merchantFavoriteAddress: {}, invoice: {} };
    this.eventPolling.eventStartGetDeliveriesValid.emit({});
    this.eventPolling.eventStopCheckStatusDelivery.emit({});
  }

  async ionViewDidLeave() {

    this.eventPolling.eventStopGetDeliveriesValid.emit({});
    this.eventPolling.eventStopRefreshDeliveryDeliverer.emit({});


    if (this.subGetDeliveriesValid) {
      this.subGetDeliveriesValid.unsubscribe();
      this.subGetDeliveriesValid = undefined;
    }

    if (this.subCheckStatus) {
      this.subCheckStatus.unsubscribe();
      this.subCheckStatus = undefined;
    }

    if (this.subRefreshDelivery) {
      this.subRefreshDelivery.unsubscribe();
      this.subRefreshDelivery = undefined;
    }

    if (this.subPrivateMessages) {
      this.subPrivateMessages.unsubscribe();
      this.subPrivateMessages = undefined;
    }

    this.stopWatch();

    this.isEnableCountdown = false;

    this.selfMarker.setMap(null);
  }

  async acceptThisDelivery() {
    const loading = await this.loadingController.create({
      message: await this.translate.get('SEND_YOUR_RESPONSE').toPromise()
    });
    await loading.present();
    try {
      let response = await this.deliveryService.acceptThisDelivery(this.deliveryReceipt._id, true);
      if (response[0].merchant) {
        response[0].merchantFavoriteAddress = this.userService.getFavoriteAddress(response[0].merchant.addresses);
      } else if (response[0].partner) {
        response[0].merchantFavoriteAddress = response[0].partner.address;
      }
      localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(response[0]));
      await loading.dismiss();
      await this.initDeliveryMap(response[0])

      const position = {
        coords: {
          latitude: this.selfMarker.getPosition().lat(),
          longitude: this.selfMarker.getPosition().lng()
        }
      }
      if (!this.promiseComputeDirection) {
        this.promiseComputeDirection = true;
        try {
          await this.computeDirection(position.coords.latitude, position.coords.longitude);
        } catch (error) { }
        this.promiseComputeDirection = false;
      }

      await this.deliveryService.addTrack(this.deliveryReceipt._id, { latitude: position.coords.latitude, longitude: position.coords.longitude });
      this.eventPolling.eventStartCheckStatusDelivery.emit(this.deliveryReceipt);

    } catch (error) {
      await this.notificationService.showToast({
        message: await this.translate.get(error.error.tradCode).toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'bottom'
      });
      this.hideNotifNewRequest = true;
      this.hidePickupNotification = true;
      this.hideDropOff = true;
      this.hidePickUp = true;
      this.hideReachCustomer = true;
      this.hideReturnPackage = true;
      this.deliveryReceipt = { _id: null, customerOfMerchant: {}, customer: {}, merchant: {}, address: {}, merchantFavoriteAddress: {}, invoice: {} };
      this.eventPolling.eventStartGetDeliveriesValid.emit({});
      await loading.dismiss();
    }
  }

  async computeDirection(lat, long) {
    console.log('RERENDER')
    return new Promise((resolve, reject) => {
      let directionsService = new google.maps.DirectionsService;

      console.log('COMPUTE')
      let waypoints = [{ location: this.merchantPosition }];
      if (this.deliveryReceipt.recoveredPackageByDeliverer) {
        waypoints = [];
      }

      //Add direction
      const directionParameters = {
        origin: new google.maps.LatLng(lat, long),
        destination: this.customerPosition,
        travelMode: google.maps.TravelMode['DRIVING'],
        waypoints: waypoints
      };
      console.log(directionParameters);
      directionsService.route(directionParameters, (result, status) => {
        if (status == 'OK') {
          this.directionsDisplay.setDirections(result);
          this.steps = this.mapsService.renderPath(result, this.map, this.steps);
          resolve();
        } else {
          console.warn(status);
          reject(status);
        }
      });
    });
  }


  async recoveredPackage() {
    await this.deliveryService.recoveredPackageByDeliverer(this.deliveryReceipt._id);
    this.deliveryReceipt = await this.deliveryService.getDeliveryById(this.deliveryReceipt._id);
    if (this.deliveryReceipt.merchant) {
      this.deliveryReceipt.merchantFavoriteAddress = this.userService.getFavoriteAddress(this.deliveryReceipt.merchant.addresses);
    } else if (this.deliveryReceipt.partner) {
      this.deliveryReceipt.merchantFavoriteAddress = this.deliveryReceipt.partner.address;
    }
    localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(this.deliveryReceipt));
    this.hideDropOff = true;
    this.hidePickUp = true;
    this.hideReachCustomer = false;

    const position = {
      coords: {
        latitude: this.selfMarker.getPosition().lat(),
        longitude: this.selfMarker.getPosition().lng()
      }
    }
    if (!this.promiseComputeDirection) {
      this.promiseComputeDirection = true;
      try {
        await this.computeDirection(position.coords.latitude, position.coords.longitude);
      } catch (error) { }
      this.promiseComputeDirection = false;
    }
    console.log("send track")
    await this.deliveryService.addTrack(this.deliveryReceipt._id, { latitude: position.coords.latitude, longitude: position.coords.longitude });
  }

  async reachCustomer() {
    await this.deliveryService.reachDestination(this.deliveryReceipt._id);
    this.deliveryReceipt = await this.deliveryService.getDeliveryById(this.deliveryReceipt._id);
    // Update addr
    if (this.deliveryReceipt.merchant) {
      this.deliveryReceipt.merchantFavoriteAddress = this.userService.getFavoriteAddress(this.deliveryReceipt.merchant.addresses);
    } else if (this.deliveryReceipt.partner) {
      this.deliveryReceipt.merchantFavoriteAddress = this.deliveryReceipt.partner.address;
    }
    localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(this.deliveryReceipt));
    this.hideDropOff = false;
    this.hidePickUp = true;
    this.hideReachCustomer = true;
  }

  async returnPackage() {
    await this.deliveryService.returnPackage(this.deliveryReceipt._id);
    this.deliveryReceipt = await this.deliveryService.getDeliveryById(this.deliveryReceipt._id);
    if (this.deliveryReceipt.merchant) {
      this.deliveryReceipt.merchantFavoriteAddress = this.userService.getFavoriteAddress(this.deliveryReceipt.merchant.addresses);
    } else if (this.deliveryReceipt.partner) {
      this.deliveryReceipt.merchantFavoriteAddress = this.deliveryReceipt.partner.address;
    }
    localStorage.setItem('lastDelivery_' + this.session.currentUser()._id, JSON.stringify(this.deliveryReceipt));
    this.hideDropOff = true;
    this.hidePickUp = true;
    this.hideReachCustomer = true;
    localStorage.removeItem('lastDelivery_' + this.session.currentUser()._id);
    this.deliveryReceipt = { _id: null, customerOfMerchant: {}, customer: {}, merchant: {}, address: {}, merchantFavoriteAddress: {}, invoice: {} };
    this.hidePickupNotification = true;
    await this.notificationService.showToast({
      message: "Veuillez retourner le colis à la pharmacie dans les plus bref délais",
      duration: 10000,
      color: 'success',
      position: 'top'
    });
    this.directionsDisplay.setMap(null);
    this.merchantMarker.setMap(null);
    this.customerMarker.setMap(null);
    this._eventEmitter.sendStopCounter();
    this.isEnableCountdown = false;
    this.cleanSteps();
    this.timeLeft = 600;

    this.eventPolling.eventStartGetDeliveriesValid.emit({});
  }

  waitCustomer() {
    this.isEnableCountdown = true;
    this.intervalWaitCustomer = this._eventEmitter.sendCounterWaitCustomer.subscribe(data => {
      this.minuteLeft = data.minute;
      this.secondLeft = data.second;
    })
    this.subFinishCounter = this._eventEmitter.finishWaitCustomer.subscribe(data => {
      this.hideReturnPackage = false;
    })
    this._eventEmitter.sendStartWaitCustomer();
  }

  async deliveredPackage() {
    const loading = await this.loadingController.create({
      message: await this.translate.get('VALID_DELIVERED_PACKAGE').toPromise()
    });
    await loading.present();
    try {
      let body: any = {};
      if (this.deliveryReceipt.isBtoB || this.deliveryReceipt.isForCustomer) {
        const response = await this.fileService.upload(this.canvasElement.nativeElement.toDataURL(), 'receipt');
        const image = response['idFile'];
        body.tagLine = image;
      }
      await this.deliveryService.deliveredPackageByDeliverer(this.deliveryReceipt._id, body);
      localStorage.removeItem('lastDelivery_' + this.session.currentUser()._id);
      this.deliveryReceipt = { _id: null, customerOfMerchant: {}, customer: {}, merchant: {}, address: {}, merchantFavoriteAddress: {}, invoice: {} };
      this.hidePickupNotification = true;
      this.hideDeliveryBill = true;
      this.clearCanvas();
      this.directionsDisplay.setMap(null);
      this.merchantMarker.setMap(null);
      this.customerMarker.setMap(null);
      this._eventEmitter.sendStopCounter();
      this.isEnableCountdown = false;
      this.firstTimeCheckStep = true;
      this.cleanSteps();
      this.timeLeft = 600;

      this.eventPolling.eventStartGetDeliveriesValid.emit({});

      await this.notificationService.showToast({
        message: await this.translate.get('DELIVERY_FINISHED_FROM_DELIVERER').toPromise(),
        duration: 4000,
        color: 'success',
        position: 'top'
      });
      await loading.dismiss();
    } catch (error) {
      await this.notificationService.showToast({
        message: await this.translate.get(error.error.tradCode).toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'top'
      });
      await loading.dismiss();
    }
  }



  async initDeliveryMap(delivery) {
    console.log("INIT DELIVERY MAP")
    console.log(delivery);
    this.deliveryReceipt = delivery;
    this.count = this.session.getNbPrivateMessageReceived(this.deliveryReceipt._id, 'TO_DELIVERER');
    this.startIntervalToRefreshDelivery();

    let favoriteAddress;
    if (this.deliveryReceipt.merchant) {
      favoriteAddress = this.userService.getFavoriteAddress(this.deliveryReceipt.merchant.addresses);
    } else if (this.deliveryReceipt.partner) {
      favoriteAddress = this.deliveryReceipt.partner.address;
    }
    this.merchantPosition = new google.maps.LatLng(favoriteAddress.latitude, favoriteAddress.longitude);
    this.merchantMarker.setPosition(this.merchantPosition);
    this.customerPosition = new google.maps.LatLng(this.deliveryReceipt.latitude, this.deliveryReceipt.longitude);
    this.customerMarker.setPosition(this.customerPosition)
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      preserveViewport: true,
      suppressMarkers: true
    });
    this.directionsDisplay.setOptions({
      polylineOptions: {
        strokeColor: '#66bb6a',
        strokeWeight: 5,
        strokeOpacity: 0
      }
    });
    this.directionsDisplay.setMap(this.map);
    this.merchantMarker.setMap(this.map);
    this.customerMarker.setMap(this.map);

    console.log(this.deliveryReceipt);
    console.log(this.deliveryReceipt);
    this.hideNotifNewRequest = true;
    this.hidePickupNotification = false;
    if (this.deliveryReceipt.recoveredPackageByDeliverer) {
      if (this.deliveryReceipt.delivererReachDestination) {
        this.hideDropOff = false;
        this.hidePickUp = true;
        this.hideReachCustomer = true;
      } else {
        this.hideDropOff = true;
        this.hidePickUp = true;
        this.hideReachCustomer = false;
      }
    } else {
      this.hideDropOff = true;
      this.hidePickUp = false;
    }

    let position = await this.internalGetCurrentPosition();
    if (!this.promiseComputeDirection) {
      this.promiseComputeDirection = true;
      try {
        this.computeDirection(position.coords.latitude, position.coords.longitude);
      } catch (error) { }
      this.promiseComputeDirection = false;
    }
  }

  showChat(type) {
    this.count = 0;
    this.session.resetNbPrivateMessageReceived(this.deliveryReceipt._id, type);
    this.router.navigateForward("/tchat", {
      queryParams: {
        authorType: 'deliverer',
        delivery: this.deliveryReceipt._id,
        typePrivateMessage: type
      }
    })
  }

  cleanSteps() {
    for (let step of this.steps) {
      step.setMap(null);
    }
    this.steps = []
  }

  async checkStatusDeliverer() {
    let lastDelivery = localStorage.getItem('lastDelivery_' + this.session.currentUser()._id);
    const status = localStorage.getItem('statusDelivererer');
    if (status === 'true' || lastDelivery) {
      await this.setOnlineDeliverer();
      this.sliderElem.checked = true;
      this.hideOfflineNotif = true;
      this._eventEmitter.sendEventToHideModalLoader(true);
      if (this.deliveryReceipt._id) {
        this.eventPolling.eventStartCheckStatusDelivery.emit(this.deliveryReceipt);
      }
    } else {
      console.log("wait position")
      let position = await this.internalGetCurrentPosition();
      const selfPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      this.map.setCenter(selfPosition);
      console.log("position found")
      this._eventEmitter.sendEventToHideModalLoader(true);
    }

    if (lastDelivery) {
      this.stopIntervalToGetDeliveriesValid();
      await this.initDeliveryMap(JSON.parse(lastDelivery));
    }
  }

  setStatusDeliverer() {
    this.sliderElem = document.getElementById("statusDeliverer") as HTMLIonCheckboxElement;
    this.sliderElem.checked = false;
    let lastDelivery = localStorage.getItem('lastDelivery_' + this.session.currentUser()._id);
    const status = localStorage.getItem('statusDelivererer');
    if (status === 'true' || lastDelivery) {
      this.sliderElem.checked = true;
      this.hideOfflineNotif = true;
    }
  }

  async refuseThisDelivery() {
    const loading = await this.loadingController.create({
      message: await this.translate.get('SEND_YOUR_RESPONSE').toPromise()
    });
    await loading.present();
    try {
      await this.deliveryService.refuseThisDelivery(this.deliveryReceipt._id);
    } catch (error) {
      await this.notificationService.showToast({
        message: await this.translate.get(error.error.tradCode).toPromise(),
        duration: 3000,
        color: 'danger',
        position: 'bottom'
      });
    }
    this.hideNotifNewRequest = true;
    this.hidePickupNotification = true;
    this.hideDropOff = true;
    this.hidePickUp = true;
    this.hideReachCustomer = true;
    this.hideReturnPackage = true;
    this.deliveryReceipt = { _id: null, customerOfMerchant: {}, customer: {}, merchant: {}, address: {}, merchantFavoriteAddress: {}, invoice: {} };
    this.eventPolling.eventStartGetDeliveriesValid.emit({});
    await loading.dismiss();
  }

  enableCountdown() {
    this.waitCustomer();
  }

  async centerMap() {
    let position = await this.internalGetCurrentPosition();
    const selfPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    this.map.setCenter(selfPosition);
  }

  async internalGetCurrentPosition() {
    return await this.geolocation.getCurrentPosition();
  };

  async call(number) {
    window.open(`tel:${number}`, '_system');
  }

  async copyAddress(adresse) {
    await this.clipboard.copy(adresse);
    await this.notificationService.showToast({
      message: "Adresse copiée !",
      duration: 1000,
      color: 'success',
      position: 'bottom'
    });
  }
}

