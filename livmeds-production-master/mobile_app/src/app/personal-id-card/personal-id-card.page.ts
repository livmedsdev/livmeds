import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-personal-id-card',
  templateUrl: './personal-id-card.page.html',
  styleUrls: ['./personal-id-card.page.scss'],
})
export class PersonalIdCardPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/deliverer-registration');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_PERSONAL_ID_CARD').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }
}
