import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/User';
import { NgForm, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { MainEventService } from '../main-event.service';
import { UserRole } from '../enums/UserRole';
import { TranslateService } from '@ngx-translate/core';
import { TypeDeliverer } from '../enums/TypeDeliverer';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session.service';
import { NotificationService } from '../notification.service';

declare var window: any;

@Component({
  selector: 'app-login-deliverer',
  templateUrl: './login-deliverer.page.html',
  styleUrls: ['./login-deliverer.page.scss'],
})
export class LoginDelivererPage implements OnInit {
  private rememberMeOption = false;

  public loginFormGroup: FormGroup;
  public registerFormGroup: FormGroup;
  private address;
  public typesDeliverer = [];
  private iti;
  public checkValid = false;

  constructor(private userService: UserService,
    private router: NavController,
    private eventEmitter: MainEventService,
    private translate: TranslateService,
    private session: SessionService,
    public loadingController: LoadingController,
    private iab: InAppBrowser,
    public notification: NotificationService) {
    this.loginFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });

    this.registerFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      siret: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      passwordConfirm: ['', Validators.compose([Validators.required])],
      typeDeliverer: ['', Validators.compose([Validators.required])],
      favoriteAddress: [false, Validators.compose([Validators.required])]
    },
      { updateOn: "blur" });
  }

  fieldsRegisterFrm(field) {
    return this.registerFormGroup.get(field);
  }

  async ngOnInit() {
    this.loginFormGroup.controls['mail'].setValue(localStorage.getItem('mail'));
    this.loginFormGroup.controls['password'].setValue(localStorage.getItem('pass'));
    this.typesDeliverer = await this.userService.listTypesDeliverer() as [];
  }

  async ionViewWillEnter() {
    this.eventEmitter.setBackButtonUrl('/choice');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_LOGIN').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(true);
    const input = document.querySelector("#phone-input-login-deliverer");
    if (input) {
      this.iti = window.intlTelInput(input, {
        autoPlaceholder: "aggressive",
        utilsScript: "./assets/js/utils-8.4.6.js",
        initialCountry: "fr"
      });
    }
    (document.getElementById('loginPasswordDeliverer') as any).value = localStorage.getItem('pass');
  }

  async submit() {
      if(this.loginFormGroup.valid) {
      const loading = await this.loadingController.create({
        message: await this.translate.get('CONNECT_IN_PROGRESS').toPromise()
      });
      await loading.present();
      try {
        const loginParams: User = this.loginFormGroup.value as User;
        loginParams.role = UserRole.ROLE_DELIVERER;
        loginParams.connectionType = "MAIL";
        loginParams.favoriteAddress = this.address;
        await this.userService.login(loginParams);
        await this.userService.sendNotificationToken();
        if (this.rememberMeOption) {
          this.session.setPasswordInMemory(loginParams.password);
        } else {
          localStorage.removeItem('pass');
        }
        await loading.dismiss();
        this.router.navigateRoot(['home-deliverer', {}]);
      } catch (error) {
        await this.notification.showToast({
          message: await this.translate.get(error.error.tradCode).toPromise(),
          duration: 3000,
          color: 'danger',
          position: 'bottom'
        });
        await loading.dismiss();
      }
    } else {
      await this.notification.showToast({
        message: 'Un de vos champs est invalide',
        duration: 5000,
        color: 'danger'
      });
    }
  }

  async submitRegister() {
    console.log(this.registerFormGroup.errors)
    console.log(this.registerFormGroup.value)
    if (this.registerFormGroup.valid) {
      try {
        let user = this.registerFormGroup.value as User;
        user.role = UserRole.ROLE_DELIVERER;
        user.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
        await this.userService.register(user);
        await this.userService.sendNotificationToken();
        this.router.navigateRoot(['home-deliverer', {}]);
      } catch (error) {
        await this.notification.showToast({
          message: await this.translate.get(error.error.tradCode).toPromise(),
          duration: 3000,
          color: 'danger'
        });
      }
    } else {
      this.validateAllFormFields(this.registerFormGroup);
      await this.notification.showToast({
        message: 'Un de vos champs est invalide',
        duration: 5000,
        color: 'danger'
      });
    }
  }

  lostPassword() {
    this.router.navigateForward('/reset-password', {
      queryParams: {
        backPage: "/login-deliverer"
      }
    })
  }

  setAddress(data) {
    this.registerFormGroup.patchValue({
      favoriteAddress: data
    });
  }

  openCgu() {
    const browser = this.iab.create(environment.urlFront + '/cgu', '_system');
  }

  rememberMe(value) {
    this.rememberMeOption = value;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
    this.checkValid = true;
  }

  changeType(value) {
    this.registerFormGroup.patchValue({
      typeDeliverer: value
    });
  }
}
