import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginDelivererPage } from './login-deliverer.page';
import { PasswordFieldComponentModule } from '../password-field/password-field.component.module';
import { TranslateModule } from '@ngx-translate/core';
import { AddressInputComponent } from '../address-input/address-input.component';
import { AddressInputComponentModule } from '../address-input/address-input.component.module';

const routes: Routes = [
  {
    path: '',
    component: LoginDelivererPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    PasswordFieldComponentModule,
    TranslateModule,
    AddressInputComponentModule
  ],
  declarations: [LoginDelivererPage]
})
export class LoginDelivererPageModule {}
