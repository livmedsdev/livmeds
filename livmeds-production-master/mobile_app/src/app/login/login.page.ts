import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/User';
import { NgForm, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { MainEventService } from '../main-event.service';
import { UserRole } from '../enums/UserRole';
import { TranslateService } from '@ngx-translate/core';
import { Facebook } from '@ionic-native/facebook/ngx';
import { SessionService } from '../session.service';
import { NotificationService } from '../notification.service';
import { TchatService } from '../tchat.service';
import { SignInWithApple } from '@ionic-native/sign-in-with-apple';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private rememberMeOption = false;

  public loginFormGroup: FormGroup;
  constructor(private userService: UserService,
    public  platform: Platform,
    private router: NavController,
    private eventEmitter: MainEventService,
    public loadingController: LoadingController,
    private fb: Facebook,
    private translate: TranslateService,
    private tchatService: TchatService,
    private session: SessionService,
    private notification: NotificationService) {
    this.loginFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.loginFormGroup.controls['mail'].setValue(localStorage.getItem('mail'));
    this.loginFormGroup.controls['password'].setValue(localStorage.getItem('pass'));
  }

  async ionViewWillEnter() {
    this.eventEmitter.setBackButtonUrl('/home-customer-sign');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('SIGN_IN').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(true);
    (document.getElementById('loginPassword') as any).value = localStorage.getItem('pass');
  }

  async submit() {
    if (this.loginFormGroup.valid) {
      const loading = await this.loadingController.create({
        message: await this.translate.get('CONNECT_IN_PROGRESS').toPromise()
      });
      await loading.present();
      try {
        const loginParams: User = this.loginFormGroup.value as User;
        loginParams.role = UserRole.ROLE_CUSTOMER;
        loginParams.connectionType = "MAIL";
        await this.userService.login(loginParams);
        await this.userService.sendNotificationToken();
        if (this.rememberMeOption) {
          this.session.setPasswordInMemory(loginParams.password);
        } else {
          localStorage.removeItem('pass');
        }

        // await this.tchatService.setEventListener();
        await loading.dismiss();
        this.router.navigateRoot(['home-customer', {}]);
      } catch (error) {
        await this.notification.showToast({
          message: await this.translate.get(error.error.tradCode).toPromise(),
          duration: 3000,
          color: 'danger',
          position: 'bottom'
        });
        await loading.dismiss();
      }
    } else {
      this.notification.showToast({
        message: 'Un de vos champs est invalide',
        duration: 5000,
        color: 'danger'
      });
    }
  }

  lostPassword() {
    this.router.navigateForward('/reset-password', {
      queryParams: {
        backPage: "/login-customer"
      }
    })
  }


  rememberMe(value) {
    this.rememberMeOption = value;
  }


    async loginFacebook() {
      const loading = await this.loadingController.create({
        message: await this.translate.get('CONNECT_IN_PROGRESS').toPromise()
      });
      await loading.present();
      try {
        await this.platform.ready();
        const response = await this.fb.login(['public_profile', 'email']);
        console.log(response);
        if (response.status === 'connected') {
          // const userIdFb = response.authResponse.userID;
          const accessTokenFb = response.authResponse.accessToken;
          await this.userService.login({
            role: UserRole.ROLE_CUSTOMER,
            connectionType: 'FACEBOOK',
            socialToken: accessTokenFb
          } as User);
          await this.userService.sendNotificationToken();
          await loading.dismiss();
          this.router.navigateRoot(['home-customer', {}]);
        } else {
          throw 'Not connected'
        }
      } catch (error) {
        console.log(error);
        await this.notification.showToast({
          message: 'Vous n\'avez pas de compte avec ce compte Facebook',
          duration: 5000,
          color: 'danger'
        });
        await loading.dismiss();
      }
    }

    async loginApple() {
      const loading = await this.loadingController.create({
        message: await this.translate.get('CONNECT_IN_PROGRESS').toPromise()
      });
      await loading.present();
      try {
        await this.platform.ready();
        const response = await SignInWithApple.signin( { requestedScopes: [0, 1] });
        console.log(response);
        await this.userService.login({
          role: UserRole.ROLE_CUSTOMER,
          connectionType: 'APPLE',
          socialToken: response.identityToken
        } as User);
        await this.userService.sendNotificationToken();
        await loading.dismiss();
        this.router.navigateRoot(['home-customer', {}]);

      } catch (error) {
        console.log(error);
        await this.notification.showToast({
          message: 'Vous n\'avez pas de compte avec ce compte Apple',
          duration: 5000,
          color: 'danger'
        });
        await loading.dismiss();
      }
    }
}
