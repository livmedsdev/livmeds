import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';
import { PasswordFieldComponent } from '../password-field/password-field.component';
import { PasswordFieldComponentModule } from '../password-field/password-field.component.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PasswordFieldComponentModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}