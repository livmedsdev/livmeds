import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { CardService } from '../card.service';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.page.html',
  styleUrls: ['./payment-method.page.scss'],
})
export class PaymentMethodPage implements OnInit {
  constructor(private eventEmitter: MainEventService, 
    private translate : TranslateService) { }

  ngOnInit() {
    this.eventEmitter.sendEventToHideModalLoader(false);
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_PAYMENT_METHOD').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.eventEmitter.sendEventToHideModalLoader(true);
  }
}
