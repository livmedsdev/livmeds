import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-deliverer-registration',
  templateUrl: './deliverer-registration.page.html',
  styleUrls: ['./deliverer-registration.page.scss'],
})
export class DelivererRegistrationPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/home-deliverer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_INFORMATION_DELIVERER').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }
}
