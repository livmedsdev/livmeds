import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewAddressPage } from './new-address.page';
import { TranslateModule } from '@ngx-translate/core';
import { AddressInputComponentModule } from '../address-input/address-input.component.module';

const routes: Routes = [
  {
    path: '',
    component: NewAddressPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    AddressInputComponentModule
  ],
  declarations: [NewAddressPage]
})
export class NewAddressPageModule {}
