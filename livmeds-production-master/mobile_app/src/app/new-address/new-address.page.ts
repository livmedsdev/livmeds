import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { NavController } from '@ionic/angular';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-address',
  templateUrl: './new-address.page.html',
  styleUrls: ['./new-address.page.scss'],
})
export class NewAddressPage implements OnInit {
  private address;
  public label;
  public compAddress
  returnToMap : boolean = false;
  constructor(private eventEmitter : MainEventService,
    private route: ActivatedRoute,
    private translate: TranslateService, private router: NavController, private userService: UserService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.returnToMap = (this.route.snapshot.queryParams.returnToMap)?this.route.snapshot.queryParams.returnToMap as boolean:false;
    this.address = null;
    this.compAddress = null;
    this.label = null;
    this.eventEmitter.setBackButtonUrl((this.returnToMap)?'/home-customer':'/home-address');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_ADD_NEW_ADDRESS').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }

  async submit(){
    if(this.address){
      if(this.compAddress){
        this.address.compAddress = this.compAddress;
      }
      await this.userService.addAddress(this.address)
      if(this.returnToMap){
        this.router.pop();
      }else{
        this.router.navigateRoot('/home-address');
      }
    }
  }

  setAddress(data){
    this.address = data;
  }
}
