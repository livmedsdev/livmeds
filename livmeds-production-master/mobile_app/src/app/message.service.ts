import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { MainEventService } from './main-event.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }

  async create(authorType, privateMessage){
    return await this.http.post(environment.url + "/"+authorType+"/privateMessage", privateMessage).toPromise();
  }

  async getPrivateMessages(authorType, idDelivery, typePrivateMessage){ 
    return await this.http.get(environment.url + "/"+authorType+"/privateMessages/"+ idDelivery, {
      params:{
        typePrivateMessage: typePrivateMessage
      }
    }).toPromise();
  }

}
