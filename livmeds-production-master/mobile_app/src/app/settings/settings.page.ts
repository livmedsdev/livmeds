import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MainEventService } from '../main-event.service';
import { UserRole } from '../enums/UserRole';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  public enabledTutorial;

  constructor(private eventEmitter : MainEventService,
    private session: SessionService,
    private translate: TranslateService) { }

  ngOnInit() {
    
  }

  async ionViewWillEnter(){
    let sliderElem = document.getElementById("enableTutorial") as HTMLIonCheckboxElement;
    sliderElem.checked = (this.session.getPreferences().enableTutorial === 'true');
    this.eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/home-deliverer' :'/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_SETTINGS').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }

  async changeLanguage(lang){
    this.session.setLang(lang);
    this.translate.use(lang);
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_SETTINGS').toPromise());
  }

  changeEnableTutorial(value){
    console.log(value);
    this.session.setEnableTutorial(value);
  }
}
