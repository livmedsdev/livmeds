import { Injectable } from '@angular/core';
import { User } from './models/User';
import { environment } from 'src/environments/environment';
import { FileService } from './file.service';
import { AlertController, Platform } from '@ionic/angular';
// import { Device } from '@ionic-native/device/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from './notification.service';
import { Device } from '@capacitor/device';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private readonly enableTutorialKey = "enableTutorial";
  private readonly langKey = "lang";
  private readonly tchatTokenKey = "tchatToken"
  
  constructor(private fileService: FileService,
    // private device: Device,
    private androidPermissions: AndroidPermissions,
    public notification: NotificationService,
    private translate: TranslateService,
    private platform: Platform) {
  }

  getDeviceUuid() {
    return localStorage.getItem('deviceUuid');
  }

  async setDeviceUuid() {
    // if (!localStorage.getItem('deviceUuid')) {
    //   if (this.platform.is('ios')) {
    //     localStorage.setItem('deviceUuid', await this.uniqueDeviceID.get());
    //   } else {
    //     const { hasPermission } = await this.androidPermissions.checkPermission(
    //       this.androidPermissions.PERMISSION.READ_PHONE_STATE
    //     );
    //     if (!hasPermission) {
    //       const result = await this.androidPermissions.requestPermission(
    //         this.androidPermissions.PERMISSION.READ_PHONE_STATE
    //       );
    //       console.log(result);

    //       if (!result.hasPermission) {
    //         throw new Error('Permissions required');
    //       }
    //     }
    //     const deviceId = this.device.uuid;
    //     if (deviceId === 'null' ||  deviceId === null) {
    //       await this.notification.showToast({
    //         message: await this.translate.get("ERROR_TO_RETRIEVE_UUID").toPromise(),
    //         duration: 10000,
    //         color: 'danger',
    //         position: 'bottom'
    //       });
    //     } else {
    //       localStorage.setItem('deviceUuid', deviceId);
    //     }
    //   }
    // }
    localStorage.setItem('deviceUuid', await (await Device.getId()).uuid);
  }

  currentUser(): User {
    if (!localStorage.getItem('token')) {
      return null;
    }
    return {
      _id: localStorage.getItem('id'),
      name: localStorage.getItem('name'),
      role: localStorage.getItem('role'),
      mail: localStorage.getItem('mail'),
      avatar: localStorage.getItem('avatar'),
      lastName: localStorage.getItem('lastName'),
      phoneNumber: localStorage.getItem('phoneNumber'),
      addresses: (localStorage.getItem('addresses')) ? JSON.parse(localStorage.getItem('addresses')) : [],
      status: localStorage.getItem('status'),
      defaultCardId: localStorage.getItem('defaultCardId'),
      connectionType: localStorage.getItem('connectionType'),
      referralCode: localStorage.getItem('referralCode'),
      docSended: (localStorage.getItem('docSended') === 'true')
    } as User;
  }

  async saveInSession(data) {
    localStorage.setItem("id", data._id);
    localStorage.setItem("name", data.name);
    localStorage.setItem("lastName", data.lastName);
    localStorage.setItem("role", data.role);
    localStorage.setItem("mail", data.mail);
    localStorage.setItem("avatar", (data.avatar) ? await this.fileService.getSignedUrl(data.avatar) : 'undefined');
    localStorage.setItem("phoneNumber", data.phoneNumber);
    localStorage.setItem("addresses", JSON.stringify(data.addresses))
    localStorage.setItem('status', data.status);
    localStorage.setItem('defaultCardId', data.defaultCardId);
    localStorage.setItem('connectionType', data.connectionType);
    localStorage.setItem('referralCode', data.referralCode);
    localStorage.removeItem('docSended');
    localStorage.setItem('docSended', (data.socialSecurityNumber)?'true':'false')
  }

  getPreferences() {
    return {
      enableTutorial: localStorage.getItem(this.enableTutorialKey),
      lang: localStorage.getItem(this.langKey)
    }
  }

  setEnableTutorial(value) {
    localStorage.setItem(this.enableTutorialKey, value);
  }

  setLang(value) {
    localStorage.setItem(this.langKey, value);
  }

  setPasswordInMemory(value) {
    localStorage.setItem('pass', value);
  }

  setTokenFcm(value) {
    localStorage.setItem('tokenFcm', value);
  }

  getTokenFcm() {
    return localStorage.getItem('tokenFcm');
  }

  setPrivateMessageReceived(delivery, typePrivateMessage, receiver) {
    const label = 'nbMessage_' + delivery + '_' + typePrivateMessage + '_' + receiver;
    const nbMsg = (localStorage.getItem(label)) ? parseInt(localStorage.getItem(label)) : 0;
    localStorage.setItem(label, (nbMsg + 1).toString());
  }

  getNbPrivateMessageReceived(delivery, typePrivateMessage) {
    const label = 'nbMessage_' + delivery + '_' + typePrivateMessage + '_' + this.currentUser()._id;
    return (localStorage.getItem(label)) ? parseInt(localStorage.getItem(label)) : 0;
  }

  resetNbPrivateMessageReceived(delivery, typePrivateMessage) {
    const label = 'nbMessage_' + delivery + '_' + typePrivateMessage + '_' + this.currentUser()._id;
    localStorage.removeItem(label);
  }
  
  async setAppVersion(appVersion){
    localStorage.setItem('appVersion', appVersion);
  }

  getAppVersion(){
    return localStorage.getItem('appVersion');
  }

  setTchatToken(value){
    localStorage.setItem(this.tchatTokenKey, value);
  }
  
  getTchatToken() {
    return localStorage.getItem(this.tchatTokenKey);
  }
}
