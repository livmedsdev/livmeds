import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-apply-promo',
  templateUrl: './apply-promo.page.html',
  styleUrls: ['./apply-promo.page.scss'],
})
export class ApplyPromoPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_PROMO_CODE').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }

  refreshList(){
    
  }

  async submit(){

  }
}
