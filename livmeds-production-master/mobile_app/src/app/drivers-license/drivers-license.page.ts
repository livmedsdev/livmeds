import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MainEventService } from '../main-event.service';

@Component({
  selector: 'app-drivers-license',
  templateUrl: './drivers-license.page.html',
  styleUrls: ['./drivers-license.page.scss'],
})
export class DriversLicensePage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/deliverer-registration');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_DELIVERER_LICENSE').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }

}
