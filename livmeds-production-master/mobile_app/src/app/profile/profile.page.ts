import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { FileService } from '../file.service';
import { SessionService } from '../session.service';
import { UserService } from '../user.service';
import { UserRole } from '../enums/UserRole';
import { LoadingController, ToastController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { TypeDeliverer } from '../enums/TypeDeliverer';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { environment } from 'src/environments/environment';
import { NotificationService } from '../notification.service';
import { CodePromoService } from '../code-promo.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Camera, CameraSource, CameraResultType, ImageOptions } from '@capacitor/camera';

declare var Stripe;
declare var window: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {
  stripe;

  bankAccountForm;
  bankAccountOtherDataForm;

  public profileFormGroup;
  public profile;
  sliderElem;
  public socialSecurityNumberUrl;
  public mutualUrl;
  public typesDeliverer = [];
  private iti;

  constructor(private eventEmitter: MainEventService, private translate: TranslateService,
    public session: SessionService,
    private userService: UserService,
    private notificationService: NotificationService,
    private iab: InAppBrowser,
    private loadingController: LoadingController,
    private codePromoServie: CodePromoService,
    private socialSharing: SocialSharing,
    private fileService: FileService) {

    this.initForm();

  }

  ngOnInit(){
    const input = document.querySelector("#phone-input-profile");
    if(input) {
        this.iti = window.intlTelInput(input, {
            autoPlaceholder: "aggressive",
            utilsScript: "./assets/js/utils-8.4.6.js",
            initialCountry: "fr"
        });
    }
  }

  async ionViewWillEnter() {
    this.eventEmitter.sendEventToHideModalLoader(false);
    this.profile = (await this.userService.getProfile())['user'];
    this.typesDeliverer = await this.userService.listTypesDeliverer() as [];
    await this.initForm();
    this.eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/home-deliverer' : '/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_PROFILE').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.eventEmitter.sendEventToHideModalLoader(true);
    this.initImages();
  }

  async changeAvatar() {
    const loading = await this.loadingController.create({
      message: 'Envoi de votre photo de profil...'
    });
    await loading.present();

    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: false,
      width: 600,
      height: 600,
      correctOrientation: true,
      source: CameraSource.Prompt,
      resultType: CameraResultType.DataUrl,
    } as ImageOptions);

    const base64Image = image.dataUrl;
    const response: any = await this.fileService.upload(base64Image, 'avatar');
    await this.userService.changeAvatar(response.idFile);
    this.profile = (await this.userService.getProfile())['user'];
    loading.dismiss();
  }

  async chooseFile(tradCode, field){
    const loading = await this.loadingController.create({
      message: this.translate.instant(tradCode)
    });
    await loading.present();
    
    try{
      const image = await Camera.getPhoto({
        quality: 60,
        allowEditing: false,
        width: 600,
        height: 600,
        correctOrientation: true,
        source: CameraSource.Prompt,
        resultType: CameraResultType.DataUrl,
      } as ImageOptions);
  
      const base64Image = image.dataUrl;
      this[field + 'Url'] = base64Image;
  
      const params = {};
      params[field] = base64Image;
      this.profileFormGroup.patchValue(params);
    }catch(error){
      console.log(error)
    }

    loading.dismiss();
  }

  async changeFile(event, tradCode, field) {
    const selectedFile = event.target.files[0];
    const loading = await this.loadingController.create({
      message: await this.translate.get(tradCode).toPromise()
    });

    if (selectedFile) {
      try {
        await loading.present();
        let params = {};
        params[field] = selectedFile;
        this.profileFormGroup.patchValue(params);
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          this[field + 'Url'] = myReader.result;
          loading.dismiss();
        }
        myReader.readAsDataURL(selectedFile);
      } catch (error) {
        console.log(error);
        loading.dismiss();
      }
    }
  }


  async submit() {
    const loading = await this.loadingController.create({
      message: await this.translate.get("SEND_PROFILE_INFORMATION").toPromise()
    });

    try {
      await loading.present();
      let body = Object.assign({}, this.profileFormGroup.value);
      delete body.socialSecurityNumber;
      delete body.mutual;
      body.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
      await this.userService.editSecurity(body);
      await this.userService.editProfile(this.profileFormGroup.value);
      this.profile = (await this.userService.getProfile())['user'];
      await this.notificationService.showToast({
        message: await this.translate.get('SUCCESS_UPDATE_PROFILE').toPromise(),
        duration: 2000,
        color: 'success',
        position: 'bottom'
      });
      await loading.dismiss();
    } catch (error) {
      console.log(error)
      await this.notificationService.showToast({
        message: await this.translate.get('FAILED_UPDATE_PROFILE').toPromise(),
        duration: 2000,
        color: 'danger',
        position: 'top'
      });
      await loading.dismiss();
    }
  }

  async initForm() {
    this.bankAccountForm = new FormBuilder().group({
      account_number: ['', Validators.compose([Validators.required])],
      account_holder_name: ['', Validators.compose([Validators.required])],
      country: ['FR', Validators.compose([])],
      currency: ['eur', Validators.compose([])],
    });

    this.bankAccountOtherDataForm = new FormBuilder().group({
      tax_id: ['', Validators.compose([Validators.required])],
      companyDocFront: ['', Validators.compose([Validators.required])],
      companyDocBack: ['', Validators.compose([Validators.required])],
      personDocFront: ['', Validators.compose([Validators.required])],
      personDocBack: ['', Validators.compose([Validators.required])],
      dobDay: ['', Validators.compose([Validators.required])],
      dobMonth: ['', Validators.compose([Validators.required])],
      dobYear: ['', Validators.compose([Validators.required])],
      payoutDay: ['28', Validators.compose([])],
    });

    switch (this.session.currentUser().role) {
      case UserRole.ROLE_CUSTOMER:
        this.profileFormGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          socialSecurityNumber: ['', Validators.compose([])],
          mutual: ['', Validators.compose([])],
          currentPassword: ['', Validators.compose([])],
          newPassword: ['', Validators.compose([])]
        });
        break;
      case UserRole.ROLE_DELIVERER:
        this.profileFormGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          currentPassword: ['', Validators.compose([])],
          newPassword: ['', Validators.compose([])],
          typeDeliverer: ['', Validators.compose([Validators.required])]
        });
        break;
      case UserRole.ROLE_DOCTOR:
      case UserRole.ROLE_NURSE:
        this.profileFormGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          siret: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          currentPassword: ['', Validators.compose([])],
          newPassword: ['', Validators.compose([])],
        });
        break;
    }
    if(this.profile){
      this.profileFormGroup.patchValue(this.profile);
    }

    if(this.profile.accountId){
      let account : any = await this.userService.getAccount();
      this.bankAccountOtherDataForm.patchValue({
        payoutDay: String(account.data.payoutDay),
        dobDay: account.data.responsibleDob.day,
        dobMonth: account.data.responsibleDob.month,
        dobYear: account.data.responsibleDob.year
      })
    }
  }

  async initImages(){
    try{
      if(this.profile.mutual){
        this.mutualUrl = await this.fileService.getSignedUrl(this.profile.mutual);
      }
      if(this.profile.socialSecurityNumber){
        this.socialSecurityNumberUrl = await this.fileService.getSignedUrl(this.profile.socialSecurityNumber);
      }
    }catch(error){
      console.debug(error);
      // No problem
    }
  }

  openCgu(){
    const browser = this.iab.create(environment.urlFront + '/cgu', '_system');
  }

  async generateReferralCode(){
    await this.codePromoServie.generateReferralCode();
    this.profile = (await this.userService.getProfile())['user'];
  }

  shareReferralCode(){
    this.socialSharing.share('LivMed\'s -- Mon code de parrainage est ' + this.session.currentUser().referralCode + '. Pour l\'utiliser, installer l\'application LivMed\s disponible sur les stores Android et iOS.');
  }


  async choosePhoto(field) {
    const loading = await this.loadingController.create({
      message: 'En attente de votre choix...'
    });
    await loading.present();

    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: false,
      width: 600,
      height: 600,
      correctOrientation: true,
      source: CameraSource.Camera,
      resultType: CameraResultType.DataUrl,
    } as ImageOptions);

    const base64Image = image.dataUrl;

    let res = await fetch(base64Image);
    let blob = await res.blob();
    const params = {};
    params[field] = blob;
    this.bankAccountOtherDataForm.patchValue(params);

    loading.dismiss();
  }


  async addBankAccount() {
    this.initStripe();
    if(!this.bankAccountForm.valid && !this.bankAccountOtherDataForm.valid){
      await this.notificationService.showToast({
        message: this.translate.instant('Veuillez remplir tous les champs du formulaire'),
        duration: 2000,
        color: 'danger',
        position: 'top'
      });
      return;
    }
    this.checkPayoutDay(this.bankAccountOtherDataForm.value.payoutDay);
    const loading = await this.loadingController.create({
      message: 'Envoi de vos informations...'
    });
    await loading.present();
    try {
      const result = await this.stripe.createToken('bank_account', this.bankAccountForm.value);
      console.log(result);

      let data = {
        bankAccountToken: result.token.id,
        taxId: this.bankAccountOtherDataForm.value.tax_id,
        companyDocFront: this.bankAccountOtherDataForm.value.companyDocFront,
        companyDocBack: this.bankAccountOtherDataForm.value.companyDocBack,
        personDocFront: this.bankAccountOtherDataForm.value.personDocFront,
        personDocBack: this.bankAccountOtherDataForm.value.personDocBack,
        dob: JSON.stringify({
          day: this.bankAccountOtherDataForm.value.dobDay,
          month: this.bankAccountOtherDataForm.value.dobMonth,
          year: this.bankAccountOtherDataForm.value.dobYear,
        }),
        payoutDay: this.bankAccountOtherDataForm.value.payoutDay
      };
      await this.userService.createStripeAccount(data)
      await this.notificationService.showToast({
        message: this.translate.instant('SUCCESS_CREATE_BANK_ACCOUNT'),
        duration: 2000,
        color: 'success',
        position: 'bottom'
      });
      
      this.profile = (await this.userService.getProfile())['user'];
    } catch (error) {
      console.log(error);
      await this.notificationService.showToast({
        message: this.translate.instant('Erreur lors de l\'envoi de vos informations bancaires'),
        duration: 2000,
        color: 'danger',
        position: 'top'
      });
    }
    await loading.dismiss();
  }

  checkPayoutDay(number: string){
    const nb = Number(number);
    if(nb <= 0 && nb > 31){
      throw "INVALID_PAYOUT_DAY";
    }
  }

  async onBankAccountFileChanged(event, field) {
    const loading = await this.loadingController.create({
      message: 'Envoi de votre fichier...'
    });
    await loading.present();
    try {
      const selectedFile = event.target.files[0];
      const value = {}
      value[field] = selectedFile
      this.bankAccountOtherDataForm.patchValue(value);
    } catch (error) {
      console.log(error)
      await this.notificationService.showToast({
        message: this.translate.instant('Erreur lors de l\'envoi du fichier'),
        duration: 2000,
        color: 'danger',
        position: 'top'
      });
    }
    await loading.dismiss();
  }

  fieldDocumentFrm(field) {
    return this.bankAccountOtherDataForm.get(field);
  }

  async editBankAccount() {
    this.initStripe();
    this.checkPayoutDay(this.bankAccountOtherDataForm.value.payoutDay);
    const loading = await this.loadingController.create({
      message: 'Envoi de vos informations...'
    });
    await loading.present();
    try {

      let data : any = {
        dob:JSON.stringify({
          day: this.bankAccountOtherDataForm.value.dobDay,
          month: this.bankAccountOtherDataForm.value.dobMonth,
          year: this.bankAccountOtherDataForm.value.dobYear,
        }),
        payoutDay: this.bankAccountOtherDataForm.value.payoutDay
      };

      if((this.bankAccountForm.value.account_holder_name || this.bankAccountForm.value.account_number)){
        if(!this.bankAccountForm.valid){
          await this.notificationService.showToast({
            message: this.translate.instant('Pour changer le numéro IBAN, veuillez remplir les champs suivants: Nom du titulaire du compte, IBAN'),
            duration: 2000,
            color: 'danger',
            position: 'top'
          });
          return;
        }
        const result = await this.stripe.createToken('bank_account', this.bankAccountForm.value);
        console.log(result);
        data.bankAccountToken = result.token.id;
      }

      loading.present();

      if(this.bankAccountOtherDataForm.value.tax_id){
        data.taxId = this.bankAccountOtherDataForm.value.tax_id
      }

      if(this.bankAccountOtherDataForm.value.companyDocFront){
        data.companyDocFront = this.bankAccountOtherDataForm.value.companyDocFront
      }

      if(this.bankAccountOtherDataForm.value.companyDocBack){
        data.companyDocBack = this.bankAccountOtherDataForm.value.companyDocBack
      }

      if(this.bankAccountOtherDataForm.value.personDocFront){
        data.personDocFront = this.bankAccountOtherDataForm.value.personDocFront
      }
      
      if(this.bankAccountOtherDataForm.value.personDocBack){
        data.personDocBack = this.bankAccountOtherDataForm.value.personDocBack;
      }
      
      await this.userService.updateStripeAccount(data);
      await this.notificationService.showToast({
        message: this.translate.instant('SUCCESS_EDIT_BANK_ACCOUNT'),
        duration: 2000,
        color: 'success',
        position: 'bottom'
      });
      
      this.profile = (await this.userService.getProfile())['user'];
    } catch (error) {
      console.log(error);
      await this.notificationService.showToast({
        message: this.translate.instant('Erreur lors de l\'envoi de vos informations bancaires'),
        duration: 2000,
        color: 'danger',
        position: 'top'
      });
    }
    loading.dismiss();
  }

  initStripe(){
    if(!this.stripe){
      this.stripe = Stripe(environment.apiKeyStripe);
    }
  }
}
