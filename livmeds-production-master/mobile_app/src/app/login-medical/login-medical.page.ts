import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/User';
import { NgForm, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { MainEventService } from '../main-event.service';
import { UserRole } from '../enums/UserRole';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Address } from '../models/Address';
import { environment } from 'src/environments/environment';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SessionService } from '../session.service';
import { NotificationService } from '../notification.service';

declare var window: any;

@Component({
  selector: 'app-login-medical',
  templateUrl: './login-medical.page.html',
  styleUrls: ['./login-medical.page.scss'],
})
export class LoginMedicalPage implements OnInit {
  private rememberMeOption = false;

  private address: Address;
  private role: String;
  public loginFormGroup: FormGroup;
  public registerFormGroup : FormGroup;
  private iti;
  public checkValid = false;

  constructor(private userService: UserService, 
    private router: NavController, 
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private session: SessionService,
    private iab: InAppBrowser,
    private eventEmitter: MainEventService,
    public notification: NotificationService) {
    this.loginFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });

    this.registerFormGroup = new FormBuilder().group({
      mail: ['',Validators.compose([Validators.required])],
      name: ['',Validators.compose([Validators.required])],
      lastName: ['',Validators.compose([Validators.required])],
      siret: ['', Validators.compose([Validators.required])],
      phoneNumber: ['',Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      passwordConfirm: ['', Validators.compose([Validators.required])]
    },
    { updateOn: "blur" });
  }

  fieldsRegisterFrm(field) {
    return this.registerFormGroup.get(field);
  }

  ngOnInit() {
    this.role = this.route.snapshot.queryParams.role as String;
    if(!this.role){
      this.router.navigateBack('/choice');
    }
    this.loginFormGroup.controls['mail'].setValue(localStorage.getItem('mail'));
    this.loginFormGroup.controls['password'].setValue(localStorage.getItem('pass'));
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/choice');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_LOGIN').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(true);
    const input = document.querySelector("#phone-input-login-medical");
    if(input) {
        this.iti = window.intlTelInput(input, {
            autoPlaceholder: "aggressive",
            utilsScript: "./assets/js/utils-8.4.6.js",
            initialCountry: "fr"
        });
    }
    (document.getElementById('loginPasswordMedical') as any ).value =localStorage.getItem('pass');
  }

  async submit() {
    if (this.loginFormGroup.valid) {
      const loading = await this.loadingController.create({
        message: await this.translate.get('CONNECT_IN_PROGRESS').toPromise()
      });
      await loading.present();
      try{
        const loginParams : User = this.loginFormGroup.value as User;
        loginParams.role = this.role;
        loginParams.connectionType = "MAIL";
        await this.userService.login(loginParams);
        await this.userService.sendNotificationToken();
        if(this.rememberMeOption){
          this.session.setPasswordInMemory(loginParams.password);
        }else{
          localStorage.removeItem('pass');
        }
        await loading.dismiss();
        this.router.navigateRoot(['home-customer', {}]);
      }catch(error){
        await this.notification.showToast({
          message: await this.translate.get(error.error.tradCode).toPromise(),
          duration: 3000,
          color: 'danger',
          position: 'bottom'
        });
        await loading.dismiss();
      }
    } else {
      await this.notification.showToast({
        message: 'Un de vos champs est invalide',
        duration: 5000,
        color: 'danger'
      });
    }
  }

  async submitRegister(){
    if (this.registerFormGroup.valid) {
      try{
        let user = this.registerFormGroup.value as User;
        user.role = this.role;
        user.favoriteAddress = this.address;
        user.phoneNumber = this.iti.getNumber(window.intlTelInputUtils.numberFormat.E164);
        await this.userService.register(user);
        await this.userService.sendNotificationToken();
        this.router.navigateRoot(['home-customer', {}]);
      }catch(error){
        await this.notification.showToast({
          message: error.error.message,
          duration: 5000,
          color: 'danger'
        });
      }
    } else {
      this.validateAllFormFields(this.registerFormGroup);
      await this.notification.showToast({
        message: 'Un de vos champs est invalide',
        duration: 5000,
        color: 'danger'
      });
    }
  }

  setAddress(data){
    console.log("setAddress")
    console.log(data);
    this.address = data;
  }

  lostPassword(){
    this.router.navigateForward('/reset-password',{
      queryParams: {
        backPage: "/login-medical"
      }
    })
  }

  openCgu(){
    const browser = this.iab.create(environment.urlFront + '/cgu', '_system');
  }

  rememberMe(value){
    this.rememberMeOption = value;
  }
  
  validateAllFormFields(formGroup: FormGroup) {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);             
      if (control instanceof FormControl) {            
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
    this.checkValid = true;
  }
}
