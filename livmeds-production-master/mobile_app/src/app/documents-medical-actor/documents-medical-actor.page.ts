import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { FileService } from '../file.service';
import { LoadingController, ToastController, NavController, AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { SessionService } from '../session.service';
import { UserRole } from '../enums/UserRole';
import { NotificationService } from '../notification.service';
import { Camera, CameraSource, CameraResultType, ImageOptions } from '@capacitor/camera';

declare var window: any;
declare var requestAnimFrame: any;

@Component({
  selector: 'app-documents-medical-actor',
  templateUrl: './documents-medical-actor.page.html',
  styleUrls: ['./documents-medical-actor.page.scss'],
})
export class DocumentsMedicalActorPage implements OnInit {
  documentsFormGroup;
  ribUrl;
  kbisUrl;
  ciFrontUrl;
  ciBackUrl;
  criminalRecordUrl;

  @ViewChild('canvasElement') canvasElement: ElementRef;

  constructor(private eventEmitter: MainEventService,
    public loadingController: LoadingController,
    private translate: TranslateService,
    private userService: UserService,
    private notificationService: NotificationService,
    private router: NavController,
    public session: SessionService,
    private alertController: AlertController,
    private document: DocumentViewer,
    private fileService: FileService) {
    this.documentsFormGroup = new FormBuilder().group({
      rib: ['', Validators.compose([Validators.required])],
      kbis: ['', Validators.compose([Validators.required])],
    });
  }

  async ngOnInit() {
    if (this.session.currentUser().role === UserRole.ROLE_DELIVERER) {
      this.documentsFormGroup = new FormBuilder().group({
        rib: ['', Validators.compose([Validators.required])],
        kbis: ['', Validators.compose([Validators.required])],
        ciBack: ['', Validators.compose([Validators.required])],
        ciFront: ['', Validators.compose([Validators.required])],
        // criminalRecord: ['', Validators.compose([])],
      });
    } else if (this.session.currentUser().role === UserRole.ROLE_NURSE) {
      this.documentsFormGroup = new FormBuilder().group({
        rib: ['', Validators.compose([Validators.required])],
        kbis: ['', Validators.compose([Validators.required])],
      });
    } else {
      this.documentsFormGroup = new FormBuilder().group({
        rib: ['', Validators.compose([])],
        kbis: ['', Validators.compose([])],
      });
    }

  }

  ngAfterViewInit() {

    let canvas = this.canvasElement.nativeElement;
    let ctx = canvas.getContext("2d");
    ctx.strokeStyle = "#222222";
    ctx.lineWidth = 4;

    let drawing = false;
    let mousePos = {
      x: 0,
      y: 0
    };
    let lastPos = mousePos;

    canvas.addEventListener("mousedown", function (e) {
      drawing = true;
      lastPos = getMousePos(canvas, e);
    }, false);

    canvas.addEventListener("mouseup", function (e) {
      drawing = false;
    }, false);

    canvas.addEventListener("mousemove", function (e) {
      mousePos = getMousePos(canvas, e);
    }, false);

    // Add touch event support for mobile
    canvas.addEventListener("touchstart", function (e) {

    }, false);

    canvas.addEventListener("touchmove", function (e) {
      var touch = e.touches[0];
      var me = new MouseEvent("mousemove", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(me);
    }, false);

    canvas.addEventListener("touchstart", function (e) {
      mousePos = getTouchPos(canvas, e);
      var touch = e.touches[0];
      var me = new MouseEvent("mousedown", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(me);
    }, false);

    canvas.addEventListener("touchend", function (e) {
      var me = new MouseEvent("mouseup", {});
      canvas.dispatchEvent(me);
    }, false);

    function getMousePos(canvasDom, mouseEvent) {
      var rect = canvasDom.getBoundingClientRect();
      return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
      }
    }

    function getTouchPos(canvasDom, touchEvent) {
      var rect = canvasDom.getBoundingClientRect();
      return {
        x: touchEvent.touches[0].clientX - rect.left,
        y: touchEvent.touches[0].clientY - rect.top
      }
    }

    function renderCanvas() {
      if (drawing) {
        ctx.moveTo(lastPos.x, lastPos.y);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
        lastPos = mousePos;
      }
    }

    // Prevent scrolling when touching the canvas
    document.body.addEventListener("touchstart", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchend", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);
    document.body.addEventListener("touchmove", function (e) {
      if (e.target == canvas) {
        e.preventDefault();
      }
    }, false);

    (function drawLoop() {
      window.requestAnimationFrame(drawLoop);
      renderCanvas();
    })();
  }

  async ionViewWillEnter() {
    this.eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/home-deliverer' : '/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_DOCUMENTS').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideModalLoader(false);
    this.eventEmitter.sendEventToHideMenuButton(false);

    if (this.session.currentUser().status === 'ACTIVE') {
      const response = await this.userService.getProfile();
      this.documentsFormGroup.patchValue(response['user']);
      for (let property in this.documentsFormGroup.value) {
        if (response['user'][property]) {
          this[property + 'Url'] = await this.fileService.getSignedUrl(response['user'][property]);
        }
      }
    }

    this.eventEmitter.sendEventToHideModalLoader(true);
  }

  async changeDocumentFile(tradCode, field) {

    const loading = await this.loadingController.create({
      message: await this.translate.get(tradCode).toPromise()
    });
    await loading.present();
    
    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: false,
      width: 600,
      height: 600,
      correctOrientation: true,
      source: CameraSource.Camera,
      resultType: CameraResultType.DataUrl,
    } as ImageOptions);

    const base64Image = image.dataUrl;
    const response: any = await this.fileService.upload(base64Image, 'document');
    let params = {};
    params[field] = response.idFile
    this.documentsFormGroup.patchValue(params);
    this[field + 'Url'] = await this.fileService.getSignedUrl(response.idFile);
    loading.dismiss();
  }

  async changeFile(event, tradCode, field) {
    const selectedFile = event.target.files[0];
    const loading = await this.loadingController.create({
      message: await this.translate.get(tradCode).toPromise()
    });
    
    if (selectedFile) {
      try {
        await loading.present();
        const uploadData = new FormData();
        uploadData.append('file', selectedFile, selectedFile.name);
        let response = await this.fileService.uploadForm(uploadData, 'document');
        let params = {};
        params[field] = response['idFile']
        this.documentsFormGroup.patchValue(params);
        this[field + 'Url'] = await this.fileService.getSignedUrl(response['idFile']);
        loading.dismiss();
      } catch (error) {
        loading.dismiss();
      }
    }
  }

  async showContract() {
    const loading = await this.loadingController.create({
      message: 'Téléchargement de votre contrat...'
    });

    const options: DocumentViewerOptions = {
      title: 'My contract'
    }
    try {
      loading.present();
      const url = await this.userService.generateContract();
      this.document.viewDocument(url, 'application/pdf', options)
      loading.dismiss();
    } catch (error) {
      console.log(error);
      await this.notificationService.showToast({
        message: "Erreur lors du chargement de votre contrat",
        duration: 3000,
        color: 'danger',
        position: 'top'
      });
      loading.dismiss();
    }

  }

  async sign() {
    if (this.documentsFormGroup.valid) {
      const loading = await this.loadingController.create({
        message: 'Validation de vos documents...'
      });
      try {
        loading.present();
        await this.userService.sign();
        const params = this.documentsFormGroup.value;
        params.signImage = this.canvasElement.nativeElement.toDataURL().replace('data:image/png;base64,', '');
        await this.userService.sendDocuments(params);
        await this.userService.getProfile();
        await this.notificationService.showToast({
          message: "Votre compte a été validé, vous pouvez maintenant utiliser pleinement l'application !",
          duration: 4000,
          color: 'success',
          position: 'top'
        });
        if (this.session.currentUser().role === UserRole.ROLE_DELIVERER) {
          this.router.navigateBack('/home-deliverer')
        } else {
          this.router.navigateBack('/home-customer')
        }

        await loading.dismiss();
      } catch (error) {
        console.log(error);

        await this.notificationService.showToast({
          message: "Erreur lors de la validation de vos documents",
          duration: 3000,
          color: 'danger',
          position: 'top'
        })
      }
      loading.dismiss();
    }
  }

  clearCanvas() {
    this.canvasElement.nativeElement.width = this.canvasElement.nativeElement.width;
  }

  async update() {
    if (this.documentsFormGroup.valid) {
      const loading = await this.loadingController.create({
        message: 'Mis à jour de vos informations...'
      });
      try {
        loading.present();
        const params = this.documentsFormGroup.value;
        await this.userService.updateDocuments(params);
        await this.userService.getProfile();
        await this.notificationService.showToast({
          message: await this.translate.get('SUCCESS_UPDATE_DOCUMENTS').toPromise(),
          duration: 4000,
          color: 'success',
          position: 'top'
        });
        loading.dismiss();
        if (this.session.currentUser().role === UserRole.ROLE_DELIVERER) {
          this.router.navigateBack('/home-deliverer')
        } else {
          this.router.navigateBack('/home-customer')
        }
      } catch (error) {
        console.log(error);
        loading.dismiss();
        await this.notificationService.showToast({
          message: await this.translate.get('FAIL_UPDATE_DOCUMENTS').toPromise(),
          duration: 3000,
          color: 'danger',
          position: 'top'
        });
      }
    }
  }
}
