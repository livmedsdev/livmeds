import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { DeliveryService } from '../delivery.service';

declare var config: any;
declare var config1: any;
declare var config2: any;
declare var config3: any;
declare var Chart: any;
declare var $: any;
import * as moment from 'moment';
import { FileService } from '../file.service';

@Component({
  selector: 'app-sales-history',
  templateUrl: './sales-history.page.html',
  styleUrls: ['./sales-history.page.scss'],
})
export class SalesHistoryPage implements OnInit {

  public deliveries : any = [];
  public limit = 5;
  public total = 0;
  public totalGain;
  public stats : any = {totalGain: 0};

  constructor(private eventEmitter: MainEventService, 
    private deliveryService: DeliveryService,
    public fileService: FileService,
    private translate : TranslateService) { }

  ngOnInit() {
    // let win = window as any;
    // var ctxCanvas = document.getElementById('canvas') as HTMLCanvasElement;
    // let ctx = ctxCanvas.getContext('2d');
    // win.myLine = new Chart(ctx, config);
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/wallet');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_TRIP_HISTORY').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.stats = await this.deliveryService.getStats();
    this.refreshList();
  }

  async loadMore(){
    this.limit += 5;
    this.refreshList();
  }

  async refreshList(){
    const response = await this.deliveryService.listMyDeliveriesByDeliverer({limit: this.limit});
    this.deliveries = response['data'];
    moment.locale(localStorage.getItem('lang'));
    this.deliveries = this.deliveries.map((delivery)=>{
      delivery.formatDate = moment(delivery.insertAt).format('DD/MM/YYYY HH:mm');
      return delivery;
    })
    this.total = response['total'];
  }
}
