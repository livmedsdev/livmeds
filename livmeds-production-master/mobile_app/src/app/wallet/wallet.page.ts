import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {

  constructor(private _eventEmitter: MainEventService, private translate: TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this._eventEmitter.setBackButtonUrl('/home-deliverer');
    this._eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_MY_WALLET').toPromise());
    this._eventEmitter.sendEventToHideMenuButton(false);
    this._eventEmitter.sendEventToHideTopBar(false);
    this._eventEmitter.sendEventToHideModalLoader(true);
  }

}
