import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { User } from './models/User';
import { NavController } from '@ionic/angular';
import { MainEventService } from './main-event.service';
import { SessionService } from './session.service';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { EventPollingService } from './event-polling.service';
import { TchatService } from './tchat.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient,
    private router: NavController,
    // private transfer: FileTransfer, 
    private eventPolling: EventPollingService,
    private file: File,
    private tchatService: TchatService,
    private _eventEmiter: MainEventService,
    private sessionService: SessionService) { }

  async register(user: any) {
    const formData = new FormData();
    if(user.mutual && (typeof user.mutual === 'string' || user.mutual instanceof String) ){
      let res = await fetch(user.mutual);
      let blob = await res.blob();
  
      formData.append('mutual', blob, "mutual.jpg");
      delete user.mutual;
    }

    if(user.socialSecurityNumber && (typeof user.socialSecurityNumber === 'string' || user.socialSecurityNumber instanceof String) ){
      let res = await fetch(user.socialSecurityNumber);
      let blob = await res.blob();
  
      formData.append('socialSecurityNumber', blob, "socialSecurityNumber.jpg");
      delete user.socialSecurityNumber;
    }

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    let response = await this.http.post(environment.url + '/register',
    this.objToForm(formData, user), { headers: headers }).toPromise();
    
    localStorage.setItem("token", response['token']);
    await this.getProfile();
  }

  async login(user: User) {
    let response = await this.http.post(environment.url + "/login", user).toPromise();
    localStorage.setItem("token", response['token']);
    await this.getProfile();
  }

  async socialLogin(userId,accessToken){
    localStorage.setItem("token", accessToken);
    await this.getProfile();
  }

  async getProfile() {
    const response = await this.http.get(environment.url + "/profile").toPromise();
    this.sessionService.saveInSession(response['user']);
    return response;
  }

  async listUsers(filter) {
    console.log("FILTER" + filter)
    let response = await this.http.post(environment.url + "/users", filter).toPromise();
    return response as unknown as Array<User>;
  }

  async checkToken() {
    return await this.http.get(environment.url + "/checkToken").toPromise();  
  }

  async getInfoFacebook(accessToken){
    return await this.http.get(environment.url + "/info-facebook", {params: {
      accessToken: accessToken
    }}).toPromise(); 
  }

  async logout() {
    this.http.post(environment.url + "/logout",{}).toPromise();  
    this.eventPolling.stopAllInterval();
    // this.tchatService.stopEventListener();
    await this.router.navigateRoot(['/choice', {}])
    this._eventEmiter.sendEventToHideTopBar(true);
    this._eventEmiter.sendEventToHideSidebar(true);
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('name');
    localStorage.removeItem('connectionType');
    localStorage.removeItem('referralCode');
  }

  async getMyAddresses(filter) {
    return await this.http.get(environment.url + "/addresses", {params: filter}).toPromise();  
  }

  async addAddress(address) {
    return await this.http.post(environment.url + "/address", address).toPromise();
  }

  getFavoriteAddress(addresses){
    for(let address of addresses){
      if(address.favoriteAddress){
        return address;
      }
    }
  }

  async changeAvatar(idAvatar){
    return await this.http.put(environment.url + "/avatar", {avatar:idAvatar}).toPromise();
  }


  async sendDocuments(body){
    return await this.http.post(environment.url + "/documents", body).toPromise();
  }

  async updateDocuments(body){
    return await this.http.put(environment.url + "/documents", body).toPromise();
  }

  async sign(){
    return await this.http.put(environment.url + "/sign", {}).toPromise();
  }

  async generateContract(){

    // const fileTransfer: FileTransferObject = this.transfer.create();
    // const entry = await fileTransfer.download(environment.url + "/user/contract", this.file.dataDirectory + 'file.pdf',false,{
    //   headers: {
    //       "Authorization": `Bearer ${localStorage.getItem('token')}`
    //   }
    // }
    // )
    // console.log('download complete: ' + entry.toURL());
    // return entry.toURL();
    return "test";
  }

  async forgetPassword(mail){
    return await this.http.post(environment.url + "/lostPassword", {
      mail : mail
    }).toPromise();
  }
  
  async editSecurity(data){
    return await this.http.put(environment.url + "/security", data).toPromise();
  }

  async editProfile(data){
    const formData = new FormData();
    if(data.socialSecurityNumber && (typeof data.socialSecurityNumber === 'string' || data.socialSecurityNumber instanceof String)  && (data.socialSecurityNumber as string).startsWith('data:image/jpeg;base64,')){
      let res = await fetch(data.socialSecurityNumber);
      let blob = await res.blob();
  
      formData.append('socialSecurityNumber', blob, "socialSecurityNumber.jpg");
      delete data.socialSecurityNumber;
    }else{
      delete data.socialSecurityNumber;
    }

    if(data.mutual && (typeof data.mutual === 'string' || data.mutual instanceof String) && (data.mutual as string).startsWith('data:image/jpeg;base64,')){
      let res = await fetch(data.mutual);
      let blob = await res.blob();
  
      formData.append('mutual', blob, "mutual.jpg");
      delete data.mutual;
    }else{
      delete data.mutual;
    }

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.put(environment.url + '/profile',
    this.objToForm(formData, data), { headers: headers }).toPromise();
  }

  async listTypesDeliverer(){
    return await this.http.get(environment.url + "/typesDeliverer").toPromise();
  }

  objToForm(formData, data){
    for(let key of Object.keys(data)){
      if(['favoriteAddress'].includes(key)){
        formData.append(key, JSON.stringify(data[key]));
      } else if(['socialSecurityNumber'].includes(key) && data[key].name){
        formData.append(key, data[key], data[key].name);
      }else{
        formData.append(key, data[key]);
      }
    }
    return formData;
  }

  async sendNotificationToken(){
    if(this.sessionService.getTokenFcm() !== 'NO_TOKEN'){
      return await this.http.post(environment.url + "/notification-token", {
        tokenFcm: this.sessionService.getTokenFcm()
      }).toPromise();
    }
  }


  async storeInformation(data){
    return await this.http.put(environment.url + "/user-information-third-party", data).toPromise();
  }

  async setOnline(online){
    return await this.http.put(environment.url + "/deliverer/online", {online}).toPromise();
  }

  async getVersions() {
    return await this.http.get(environment.url + "/versions").toPromise();  
  }


  async getAccount() {
    return await this.http.get(environment.url + `/bank-account`).toPromise();
  }

  async createStripeAccount(data) {
    const formData = new FormData();
    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    let response = await this.http.post(environment.url + `/bank-account`,
      this.objToFormStripeAccount(formData, data), { headers: headers }).toPromise();
    return response;
  }

  async updateStripeAccount(data) {
    const formData = new FormData();
    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    let response = await this.http.put(environment.url + `/bank-account`,
      this.objToFormStripeAccount(formData, data), { headers: headers }).toPromise();
    return response;
  }

  objToFormStripeAccount(formData, data) {
    for (let key of Object.keys(data)) {
        formData.append(key, data[key]);
    }
    return formData;
  }
}
