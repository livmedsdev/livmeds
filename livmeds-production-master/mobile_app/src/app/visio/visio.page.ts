import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../enums/UserRole';
import { MainEventService } from '../main-event.service';
import { SessionService } from '../session.service';
import { TchatService } from '../tchat.service';

import { CometChat } from "@cometchat-pro/cordova-ionic-chat"
import { LoadingController, NavController } from '@ionic/angular';
import { FileService } from '../file.service';

@Component({
  selector: 'app-visio',
  templateUrl: './visio.page.html',
  styleUrls: ['./visio.page.scss'],
})
export class VisioPage implements OnInit {
  private sessionId;
  private type: String;
  public avatar: String;
  private receiverId: String;
  private deliveryId;
  private token: String;
  public message;
  public debugMessage;
  private outgoingCall;
  private listnerID;

  constructor(
    private eventEmitter: MainEventService,
    public router: NavController,
    private route: ActivatedRoute,
    private translate: TranslateService,
    public session: SessionService,
    private tchatService: TchatService,
    public loadingController: LoadingController,
    public fileService: FileService
  ) { }

  ngOnInit() { }

  async ionViewWillEnter() {
    this.receiverId = this.route.snapshot.queryParams.receiverId as String;
    this.avatar = this.route.snapshot.queryParams.avatar as String;
    this.deliveryId = this.route.snapshot.queryParams.deliveryId as String;
    this.sessionId = this.route.snapshot.queryParams.sessionId as String;
    this.type = this.route.snapshot.queryParams.type as String;
    this.eventEmitter.sendEventToHideModalLoader(false);
    this.eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/home-deliverer' : '/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_VISIO').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.eventEmitter.sendEventToHideModalLoader(true);

    // const response: any = await this.tchatService.getTchatToken();
    // this.token = response.token;
    this.message = "CALL_SEND";
    if(this.receiverId){
      await this.initializeListener();
      await this.initiateCall();
    }else if(this.sessionId){
      this.startCall(this.sessionId);
    }
  }

  ionViewWillLeave(){
    // CometChat.removeCallListener(this.listnerID);
    // CometChat.rejectCall(this.outgoingCall.sessionId, CometChat.CALL_STATUS.CANCELLED);
    // CometChat.logout();
  }

  async initiateCall() {
    const loading = await this.loadingController.create({
      message: await this.translate.get('INIT_YOUR_VIDEO_CALL').toPromise()
    });
    try{
      await loading.present();

      // console.log(this.token)
      // console.log(this.deliveryId)
      // console.log(this.receiverId)
      // let callType = CometChat.CALL_TYPE.VIDEO;
      // let receiverType = CometChat.RECEIVER_TYPE.USER;
  
      // let call = new CometChat.Call(this.receiverId, callType, receiverType);
      // await CometChat.login(this.token);
      // this.outgoingCall = await CometChat.initiateCall(call);
      // console.log(this.outgoingCall)
      // await this.tchatService.createChatSession({
      //   sessionId: this.outgoingCall.sessionId,
      //   delivery: this.deliveryId,
      //   receiverId: this.receiverId,
      //   authToken: this.token,
      //   type: this.outgoingCall.type
      // });
      // setTimeout(() => { this.message = "CALL_TAKE_TOO_LONG" }, 300*1000);
      await loading.dismiss();
    }catch(error){
      console.log(error);
      await loading.dismiss();
    }
  }

  async initializeListener() {
    this.listnerID = "OUTGOING_CALL_" + this.receiverId + '_' + new Date().getTime();
    // CometChat.addCallListener(
    //   this.listnerID,
    //   new CometChat.CallListener({
    //     onOutgoingCallAccepted: (call) => {
    //       console.log("Outgoing call accepted:", call);
    //       this.message = "CALL_ACCEPTED";
    //       // Outgoing Call Accepted
    //       try{
    //         var sessionId = call.sessionId;
    //         var callType = call.type;
    //         var callListener = new CometChat.OngoingCallListener({
    //           onUserJoined: user => {
    //             console.log('User joined call:', user);
    //           },
    //           onUserLeft: user => {
    //             console.log('User left call:', user);
    //           },
    //           onUserListUpdated: userList => {
    //             console.log("user list:", userList);
    //           },
    //           onCallEnded: call => {
    //             console.log('Call ended listener', call);
    //             this.router.navigateForward('/home-customer')
    //           },
    //           onAudioModesUpdated: (audioModes) => {
    //             console.log("audio modes:", audioModes);
    //           }
    //         });
    //         var callSettings = new CometChat.CallSettingsBuilder()
    //           .setSessionID(sessionId)
    //           .enableDefaultLayout(true)
    //           .setIsAudioOnlyCall(callType == 'audio' ? true : false)
    //           .setCallEventListener(callListener)
    //           .build();
    //         CometChat.startCall(callSettings);
    //       }catch(error){
    //         this.debugMessage = error;
    //       }
    //     },
    //     onOutgoingCallRejected: (call) => {
    //       console.log("Outgoing call rejected:", call);
    //       this.message = "CALL_REJECTED";
    //       // Outgoing Call Rejected
    //     },
    //     onIncomingCallCancelled: (call)=> {
    //       console.log("Incoming call calcelled:", call);
    //     }
    //   })
    // );
  }

  async startCall(sessionId){
    try{
      // await CometChat.login(this.token);
      // const call = await CometChat.acceptCall(sessionId)
      // let callType = this.type;
      // let callListener = new CometChat.OngoingCallListener({
      //   onUserJoined: user => {
      //     console.log('User joined call:', user);
      //   },
      //   onUserLeft: user => {
      //     console.log('User left call:', user);
      //   },
      //   onUserListUpdated: userList => {
      //     console.log("user list:", userList);
      //   },
      //   onCallEnded: call => {
      //     console.log('Call ended listener', call);
      //     this.router.navigateForward('/home-customer')
      //   },
      //   onAudioModesUpdated: (audioModes) => {
      //     console.log("audio modes:", audioModes);
      //   }
      // });
      // let callSettings = new CometChat.CallSettingsBuilder()
      //   .setSessionID(sessionId)
      //   .enableDefaultLayout(true)
      //   .setIsAudioOnlyCall(callType == 'audio' ? true : false)
      //   .setCallEventListener(callListener)
      //   .build();
      // CometChat.startCall(callSettings);
    }catch(error){
      console.log(error);
    }
  }

  returnToHome(){
    this.router.navigateBack(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/home-deliverer' : '/home-customer')
  }
}
