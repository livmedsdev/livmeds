import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  async upload(file, type, body = []) {
    let res = await fetch(file);
    let blob = await res.blob();

    const formData = new FormData();
    formData.append('file', blob, "avatar.jpg");

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });

    return await this.http.post(environment.url + '/file/upload/' + type,
      formData, { headers: headers }).toPromise();
  }

  async uploadForm(formData, type) {
    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });

    return await this.http.post(environment.url + '/file/upload/' + type,
      formData, { headers: headers }).toPromise();
  }


  async getSignedUrl(idFile) {
    const data = JSON.parse(idFile);
    return (await this.http.post(environment.url + '/file/url', data).toPromise())['url'];
  }

  // async uploadFile(filePath, filename) {
    // let options: FileUploadOptions = {
    //   fileKey: 'file',
    //   fileName: filename,
    //   headers: {
    //     "Authorization": `Bearer ${localStorage.getItem('token')}`
    //   }
    // }
    // const fileTransfer: FileTransferObject = this.transfer.create();

    // return await fileTransfer.upload(filePath, environment.url + '/file/upload', options)
  // }

  getPublicUrl(image){
    const data = JSON.parse(image);
    return environment.publicBucketUrl + data.key;
  }

  // async downloadLocally(localPath, id) {
    // const fileTransfer: FileTransferObject = this.transfer.create();
    // const entry = await fileTransfer.download(environment.url + "/file/" + id, localPath, false, {
    //   headers: {
    //     "Authorization": `Bearer ${localStorage.getItem('token')}`
    //   }
    // }
    // )
    // console.log('download complete: ' + entry.toURL());
    // return entry.toURL();
  // }

  async getPdfBlob(dataImage) {
    const data = JSON.parse(dataImage);
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/pdf');

    return new Promise(async (resolve, reject) => {
      const response = await this.http.post(environment.url + "/file/download-as", data, { headers: headers, responseType: 'blob' }).toPromise();

      const myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        let base64Image: any = myReader.result;
        resolve(base64Image);
      }
      myReader.readAsDataURL(response);
    })
  }
}
