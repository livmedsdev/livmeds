import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add-new-bank-account',
  templateUrl: './add-new-bank-account.page.html',
  styleUrls: ['./add-new-bank-account.page.scss'],
})
export class AddNewBankAccountPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/add-payout-methods');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('BANK_INFORMATION').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }
}
