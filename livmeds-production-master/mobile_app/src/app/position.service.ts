import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor(private http: HttpClient) { }
  
  async sendPosition(latitude, longitude){
    return await this.http.post(environment.url + "/deliverer/position", {latitude, longitude}).toPromise();
  }

  async getDeliverers(){
    return await this.http.get(environment.url + "/customer/deliverers").toPromise();
  }
}
