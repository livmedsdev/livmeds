import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from './notification.service';

declare var Stripe;

@Injectable({
  providedIn: 'root'
})
export class CardService {
  stripe = Stripe(environment.apiKeyStripe);
  cardRegistration;

  constructor(private http: HttpClient,
    private notificationService: NotificationService,
    private translate: TranslateService,
    private loadingController: LoadingController) { }

  async list(){ 
    return await this.http.get(environment.url + "/cards").toPromise();
  }

  async setDefaultCard(cardId){
    return await this.http.put(environment.url + "/defaultCard/"+cardId, {}).toPromise();
  }

  async delete(cardId){
    return await this.http.delete(environment.url + "/card/"+cardId, {}).toPromise();
  }

  async askAddCardIntent(){
    return await this.http.get(environment.url + '/add-card-intent').toPromise();
  }


  async setupStripe(cardElement, cardErrors, formId,  callbackSuccess, callbackError) {
    await this.reloadCardRegistration();
    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    const card = elements.create('card', { style: style });

    card.mount(cardElement);

    card.addEventListener('change', event => {
      var displayError = document.getElementById(cardErrors);
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });

    const handler = async event => {
      event.preventDefault();


      const loading = await this.loadingController.create({
        message: 'Validation en cours...'
      });

      await loading.present();
      const cardSetup = await this.stripe.confirmCardSetup(
        this.cardRegistration,
        {
          payment_method: {
            card: card,
            billing_details: {
              name: "card",
            },
          },
        });
      const errorElement = document.getElementById(cardErrors);
      if (cardSetup.error) {
        await loading.dismiss();
        errorElement.textContent = cardSetup.error.message;
        callbackError();
      } else {
        console.log(cardSetup);
        errorElement.textContent = "";
        loading.dismiss();
        await this.notificationService.showToast({
          message: await this.translate.get('SUCCESS_CARD_ADDED').toPromise(),
          duration: 3000,
          color: 'success'
        })
        card.clear();
        callbackSuccess();
      }
    }

    const form = document.getElementById(formId);
    form.removeEventListener('submit', handler);
    form.addEventListener('submit', handler);
  }

  async reloadCardRegistration(){
    this.cardRegistration = (await this.askAddCardIntent())['client_secret'];
  }
}
