import { Injectable, EventEmitter } from '@angular/core';
import { DeliveryService } from './delivery.service';
import { PositionService } from './position.service';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class EventPollingService {

  //GET INFORMATION CUSTOMER
  eventStartGetDeliveryInformationCustomer = new EventEmitter();
  eventGetDeliveryInformationCustomer = new EventEmitter();
  eventStopGetDeliveryInformationCustomer = new EventEmitter();
  intervalGetInformation;

  // REDRAW MAP CUSTOMER
  eventStartRedrawMapCustomer = new EventEmitter();
  eventRedrawMapCustomer = new EventEmitter();
  eventStopRedrawMapCustomer = new EventEmitter();
  intervalRedrawMap;

  //GET DELIVERERS
  eventStartGetDeliverers = new EventEmitter();
  eventGetDeliverers = new EventEmitter();
  eventStopGetDeliverers = new EventEmitter();
  intervalGetDeliverers;

  // GET DELIVERIES VALID
  eventStartGetDeliveriesValid = new EventEmitter();
  eventGetDeliveriesValid = new EventEmitter();
  eventStopGetDeliveriesValid = new EventEmitter();
  intervalGetDeliveriesValid;

  // Check status
  eventStartCheckStatusDelivery = new EventEmitter();
  eventCheckStatusDelivery = new EventEmitter();
  eventStopCheckStatusDelivery = new EventEmitter();
  intervalCheckStatusDelivery;

  // GET DELIVERIES VALID
  eventStartRefreshDeliveryDeliverer = new EventEmitter();
  eventRefreshDeliveryDeliverer = new EventEmitter();
  eventStopRefreshDeliveryDeliverer = new EventEmitter();
  intervalRefreshDeliveryDeliverer;

  // REFRESH TCHAT
  eventStartTchat = new EventEmitter();
  eventRefreshTchat = new EventEmitter();
  eventStopTchat = new EventEmitter();
  intervalRefreshTchat;
  messagesInMemory = [];

  asyncIntervals: any;
  runAsyncInterval;
  setAsyncInterval;
  clearAsyncInterval;

  constructor(
    private deliveryService: DeliveryService,
    private positionService: PositionService,
    private messageService: MessageService
  ) {

    this.asyncIntervals = [];

    this.runAsyncInterval = async (cb, interval, intervalIndex) => {
      await cb();
      if (this.asyncIntervals[intervalIndex]) {
        setTimeout(() => this.runAsyncInterval(cb, interval, intervalIndex), interval);
      }
    };

    this.setAsyncInterval = (cb, interval) => {
      if (cb && typeof cb === "function") {
        const intervalIndex = this.asyncIntervals.length;
        this.asyncIntervals.push(true);
        this.runAsyncInterval(cb, interval, intervalIndex);
        return intervalIndex;
      } else {
        throw new Error('Callback must be a function');
      }
    };

    this.clearAsyncInterval = (intervalIndex) => {
      if (this.asyncIntervals[intervalIndex]) {
        this.asyncIntervals[intervalIndex] = false;
      }
    };

    this.eventStartGetDeliveryInformationCustomer.subscribe(data => {
      if (this.intervalGetInformation === undefined) {
        this.intervalGetInformation = this.setAsyncInterval(async () => {
          try {
            const delivery = await this.deliveryService.getDeliveryByIdWithCustomer(data.delivery._id);
            this.eventGetDeliveryInformationCustomer.emit(delivery);
          } catch (error) {

          }
        }, 1000);
      }
    });


    this.eventStopGetDeliveryInformationCustomer.subscribe(data => {
      if (this.intervalGetInformation !== undefined) {
        this.clearAsyncInterval(this.intervalGetInformation);
        this.intervalGetInformation = undefined;
      }
    });



    // GET DELIVERERS
    this.eventStartGetDeliverers.subscribe(data => {
      if (this.intervalGetDeliverers === undefined) {
        this.intervalGetDeliverers = setInterval(async () => {
          try {
            this.eventGetDeliverers.emit({ deliverers: await this.positionService.getDeliverers() });
          } catch (error) { }
        }, 20000);
      }
    });


    this.eventStopGetDeliverers.subscribe(data => {
      if (this.intervalGetDeliverers !== undefined) {
        clearInterval(this.intervalGetDeliverers);
        this.intervalGetDeliverers = undefined;
      }
    });


    // GET DELIVERIES VALID
    this.eventStartGetDeliveriesValid.subscribe(data => {
      console.log("start interval")
      if (this.intervalGetDeliveriesValid === undefined) {
        this.intervalGetDeliveriesValid = setInterval(async () => {
          try {
            this.eventGetDeliveriesValid.emit({deliveriesValid: await this.deliveryService.getDeliveriesAvailable(), deliveriesInProgress: await this.deliveryService.getMyDeliveryInProgress()});
          } catch (error) { }
        }, 1000);
      }
    });


    this.eventStopGetDeliveriesValid.subscribe(data => {
      if (this.intervalGetDeliveriesValid !== undefined) {
        clearInterval(this.intervalGetDeliveriesValid);
        this.intervalGetDeliveriesValid = undefined;
      }
    });


    // REFRESH MAP
    this.eventStartRedrawMapCustomer.subscribe(delivery => {
      if (this.intervalRedrawMap === undefined) {
        this.intervalRedrawMap = setInterval(async () => {
          this.eventRedrawMapCustomer.emit({});
        }, 10000);
      }
    });


    this.eventStopRedrawMapCustomer.subscribe(data => {
      if (this.intervalRedrawMap !== undefined) {
        clearInterval(this.intervalRedrawMap);
        this.intervalRedrawMap = undefined;
      }
    });

    // REFRESH DELIVERY
    this.eventStartRefreshDeliveryDeliverer.subscribe(delivery => {
      if (this.intervalRefreshDeliveryDeliverer === undefined) {
        this.intervalRefreshDeliveryDeliverer = setInterval(async () => {
          try {
            this.eventRefreshDeliveryDeliverer.emit(await this.deliveryService.getDeliveryById(delivery._id));
          } catch (error) { 
            if(error.error && error.error.tradCode){
              this.eventRefreshDeliveryDeliverer.emit("error");
            }
          }
        }, 1000);
      }
    });

    this.eventStopRefreshDeliveryDeliverer.subscribe(data => {
      if (this.intervalRefreshDeliveryDeliverer !== undefined) {
        clearInterval(this.intervalRefreshDeliveryDeliverer);
        this.intervalRefreshDeliveryDeliverer = undefined;
      }
    });


    // Check status
    this.eventStartCheckStatusDelivery.subscribe(delivery => {
      if (this.intervalCheckStatusDelivery === undefined) {
        this.intervalCheckStatusDelivery = setInterval(async () => {
          try {
            this.eventCheckStatusDelivery.emit(await this.deliveryService.getDeliveryById(delivery._id));
          } catch (error) { }
        }, 5000);
      }
    });

    this.eventStopCheckStatusDelivery.subscribe(data => {
      if (this.intervalCheckStatusDelivery !== undefined) {
        clearInterval(this.intervalCheckStatusDelivery);
        this.intervalCheckStatusDelivery = undefined;
      }
    });


    // REFRESH TCHAT
    this.eventStartTchat.subscribe(data => {
      if (this.intervalRefreshTchat === undefined) {
        this.intervalRefreshTchat = setInterval(async () => {
          try {
            let privateMessages = (await this.messageService.getPrivateMessages(data.authorType, data.delivery._id, data.typePrivateMessage))['data'];
            const changes = this.getChanges(this.messagesInMemory, privateMessages);
            this.messagesInMemory = privateMessages;

            this.eventRefreshTchat.emit({
              messages: privateMessages,
              changes
            });

          } catch (error) { }
        }, 1000);
      }
    });


    this.eventStopTchat.subscribe(data => {
      if (this.intervalRefreshTchat !== undefined) {
        clearInterval(this.intervalRefreshTchat);
        this.intervalRefreshTchat = undefined;
      }
    });
  }

  async refreshListTchat(data) {
    let privateMessages = (await this.messageService.getPrivateMessages(data.authorType, data.delivery._id, data.typePrivateMessage))['data'];
    this.messagesInMemory = privateMessages;

    this.eventRefreshTchat.emit({
      messages: privateMessages,
      changes: true
    });
  }

  getChanges(oldArray, newArray) {
    let changes, i, item, j, len;

    if (oldArray.length > 0 && oldArray.length < newArray.length) {
      return 'NEW';
    }

    if (oldArray.length === 0 && newArray.length > 0) {
      return 'INIT';
    }


    if (JSON.stringify(oldArray) === JSON.stringify(newArray)) {
      return false;
    }

    changes = [];
    for (i = j = 0, len = newArray.length; j < len; i = ++j) {
      item = newArray[i];
      if (JSON.stringify(item) !== JSON.stringify(oldArray[i])) {
        changes.push(item);
      }
    }
    return changes;
  }

  stopAllInterval() {

    if (this.intervalRedrawMap !== undefined) {
      clearInterval(this.intervalRedrawMap);
      this.intervalRedrawMap = undefined;
    }

    if (this.intervalGetInformation !== undefined) {
      this.clearAsyncInterval(this.intervalGetInformation);
      this.intervalGetInformation = undefined;
    }

    if (this.intervalGetDeliverers !== undefined) {
      clearInterval(this.intervalGetDeliverers);
      this.intervalGetDeliverers = undefined;
    }


    if (this.intervalGetDeliveriesValid !== undefined) {
      clearInterval(this.intervalGetDeliveriesValid);
      this.intervalGetDeliveriesValid = undefined;
    }


    if (this.intervalRefreshDeliveryDeliverer !== undefined) {
      clearInterval(this.intervalRefreshDeliveryDeliverer);
      this.intervalRefreshDeliveryDeliverer = undefined;
    }


    if (this.intervalRefreshTchat !== undefined) {
      clearInterval(this.intervalRefreshTchat);
      this.intervalRefreshTchat = undefined;
    }
  }
}
