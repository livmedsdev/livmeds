import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CometChat } from '@cometchat-pro/cordova-ionic-chat';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { DeliveryService } from './delivery.service';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class TchatService {
  private readonly listnerID = "COMETCHAT_CALL_GLOBAL";

  constructor(private http: HttpClient,
    private alertController: AlertController,
    private translate: TranslateService,
    public router: NavController,
    private session: SessionService,
    public loadingController: LoadingController,
    private deliveryService: DeliveryService,
    ) { }

  async getTchatToken() {
    return await this.http.get(environment.url + "/tchat/token", {}).toPromise();
  }

  async getChatSessionData(id) {
    return await this.http.get(environment.url + "/tchat/session/" + id, {}).toPromise();
  }

  async createChatSession(params) {
    return await this.http.post(environment.url + "/tchat/session", params).toPromise();
  }

  async setEventListener(){
    const response: any = await this.getTchatToken();
    await CometChat.login(response.token);
    CometChat.addCallListener(
      this.listnerID,
      new CometChat.CallListener({
        onIncomingCallReceived: async (call) => {
          console.log("Incoming call:", call);
          await new Promise(resolve => setTimeout(resolve, 5000));
          
          const response : any = await this.getChatSessionData(call.sessionId);
          const sessionData = response.data;

          const delivery : any = await this.deliveryService.getDeliveryByIdWithCustomer(sessionData.delivery);

          const loading = await this.loadingController.create({
            message: await this.translate.get('INIT_YOUR_VIDEO_CALL').toPromise()
          });

          const confirmScheduled = await this.alertController.create({
            backdropDismiss: false,
            header: await this.translate.get('TITLE_VIDEO_CALL_INCOMING').toPromise(),
            message: await this.translate.get('DESCRIPTION_VIDEO_CALL_INCOMING', {
              senderName: delivery.merchant.name,
              reference: delivery.reference
            }).toPromise(),
            buttons: [{
              text: await this.translate.get('BTN_ACCEPT').toPromise(),
              handler: async () => { 
                loading.present();
                await CometChat.acceptCall(call.sessionId);
                var sessionId = call.sessionId;
                var callType = call.type;
                var callListener = new CometChat.OngoingCallListener({
                  onUserJoined: user => {
                    console.log('User joined call:', user);
                  },
                  onUserLeft: user => {
                    console.log('User left call:', user);
                  },
                  onUserListUpdated: userList => {
                    console.log("user list:", userList);
                  },
                  onCallEnded: call => {
                    console.log('Call ended listener', call);
                    this.router.navigateForward('/home-customer')
                  },
                  onAudioModesUpdated: (audioModes) => {
                    console.log("audio modes:", audioModes);
                  }
                });
                var callSettings = new CometChat.CallSettingsBuilder()
                  .setSessionID(sessionId)
                  .enableDefaultLayout(true)
                  .setIsAudioOnlyCall(callType == 'audio' ? true : false)
                  .setCallEventListener(callListener)
                  .build();
                CometChat.startCall(callSettings);
                loading.dismiss();
              }
            },{
              text: await this.translate.get('BTN_DECLINE').toPromise(),
              handler: async () => { 
                await CometChat.rejectCall(call.sessionId, CometChat.CALL_STATUS.CANCELLED);
              }
            }
            ]
          });
          await confirmScheduled.present();
        },
        onIncomingCallCancelled(call) {
          console.log("Incoming call calcelled:", call);
        }
      })
    );
  }

  async stopEventListener(){
    try{
      await CometChat.removeCallListener(this.listnerID);
    }catch(error){
      console.log(error)
    }
  }
}
