import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { DeliveryService } from '../delivery.service';
import { ActivatedRoute } from '@angular/router';
import { FileService } from '../file.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-delivery-details',
  templateUrl: './delivery-details.page.html',
  styleUrls: ['./delivery-details.page.scss'],
})
export class DeliveryDetailsPage implements OnInit {

  public invoice : any = {
    products: [],
  };

  constructor(private eventEmitter: MainEventService,
     private translate : TranslateService,
     private deliveryService: DeliveryService,
     private route: ActivatedRoute,
     public fileService: FileService,
     public loadingController: LoadingController,
     ) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/history');
    this.eventEmitter.sendEventToChangeTitle(this.translate.instant('COMMANDS_DETAILS'));
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);

    const loading = await this.loadingController.create({
      message: this.translate.instant('LOADING_DETAILS_COMMAND')
    });
    await loading.present();

    const id = this.route.snapshot.queryParams.id;
    this.invoice = (await this.deliveryService.getDeliveryByIdWithCustomer(id))['invoice'];

    await loading.dismiss();
  }

  formatNumber(number) {
    if(number){
      return Number.parseFloat(number).toFixed(2);
    }
    return 0;
  }
}
