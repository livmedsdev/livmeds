import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { NavController, ToastController, NavParams } from '@ionic/angular';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../models/User';
import { UserRole } from '../enums/UserRole';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

  public resetFormGroup: FormGroup;
  constructor(private userService: UserService, 
    private router: NavController, 
    private eventEmitter: MainEventService,
    public route: ActivatedRoute,
    private translate: TranslateService,
    private notificationService: NotificationService) {
    this.resetFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.resetFormGroup.controls['mail'].setValue(localStorage.getItem('mail'));
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl(this.route.snapshot.queryParams.backPage);
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_RESET_PASSWORD').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(true);
  }

  async submit() {
    if (this.resetFormGroup.valid) {
      try{
        const response : any = await this.userService.forgetPassword(this.resetFormGroup.value.mail);
        await this.notificationService.showToast({
          message: await this.translate.get(response.tradCode).toPromise(),
          duration: 3000,
          color: 'success'
        });
        this.router.back()
      }catch(error){
        await this.notificationService.showToast({
          message: error.error.tradCode,
          duration: 3000,
          color: 'danger'
        });
      }
    } else {
      await this.notificationService.showToast({
        message: await this.translate.get('FIELD_INVALID').toPromise(),
        duration: 3000,
        color: 'danger'
      });
    }
  }
}
