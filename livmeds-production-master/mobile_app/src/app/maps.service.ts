import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

declare var google;

@Injectable({
  providedIn: 'root'
})
export class MapsService {

  public styles = {
    "light": new google.maps.StyledMapType(
      [
        {
          "featureType": "landscape",
          "stylers": [
            {
              "color": "#eef0f9"
            }
          ]
        },
        {
          "featureType": "landscape.natural.landcover",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#bde6c3"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#a3aabf"
            }
          ]
        },
        {
          "featureType": "transit",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#b3d9fe"
            }
          ]
        }
      ]),
    "dark": new google.maps.StyledMapType(
      [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#212121"
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#212121"
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#757575"
            },
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.country",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#40444f"
            }
          ]
        },
        {
          "featureType": "poi",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#181818"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#1b1b1b"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#2c2c2c"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#8a8a8a"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#292d38"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#20222b"
            }
          ]
        },
        {
          "featureType": "road.highway.controlled_access",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#4e4e4e"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "transit",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#415ba3"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#3d3d3d"
            }
          ]
        }
      ])
  }

  constructor(private translate: TranslateService) { }

  renderPath(response, map, stepsInMemory) {
    console.log(response);
    for (let step of stepsInMemory) {
      step.setMap(null);
    }
    stepsInMemory = [];
    let bounds = new google.maps.LatLngBounds();
    let lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 4
    };
    let polylineOptions = [{
      strokeColor: '#D8418F',
      strokeWeight: 5
    }, {
      strokeColor: '#D8418F',
      strokeWeight: 5
    }];
    let legs = response.routes[0].legs;
    for (let i = 0; i < legs.length; i++) {
      let steps = legs[i].steps;
      for (let j = 0; j < steps.length; j++) {
        let nextSegment = steps[j].path;
        let stepPolyline = new google.maps.Polyline(i > 0 ? polylineOptions[1] : polylineOptions[0]);
        for (let k = 0; k < nextSegment.length; k++) {
          stepPolyline.getPath().push(nextSegment[k]);
          bounds.extend(nextSegment[k]);
        }
        stepPolyline.setMap(map);
        stepsInMemory.push(stepPolyline);
      }
    }
    return stepsInMemory;
  }

  renderPathWithoutMemory(response, map) {
    let bounds = new google.maps.LatLngBounds();
    let lineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 1,
      scale: 4
    };
    let polylineOptions = [{
      strokeColor: '#66bb6a',
      strokeWeight: 5
    }, {
      strokeColor: '#66bb6a',
      strokeWeight: 5
    }];
    let legs = response.routes[0].legs;
    for (let i = 0; i < legs.length; i++) {
      let steps = legs[i].steps;
      for (let j = 0; j < steps.length; j++) {
        let nextSegment = steps[j].path;
        let stepPolyline = new google.maps.Polyline(i > 0 ? polylineOptions[1] : polylineOptions[0]);
        for (let k = 0; k < nextSegment.length; k++) {
          stepPolyline.getPath().push(nextSegment[k]);
          bounds.extend(nextSegment[k]);
        }
        stepPolyline.setMap(map);
      }
    }
  }

  generateContentWindows(merchant) {

   let content;
  if(merchant.ph_id){

     content = '<div id="iw-container">' +
      `<div class="iw-title"><B>${merchant.raison_sociale}</B></div>` +
      '<div class="iw-content">' +
      '<p style="position: absolute; right: 0">';
      if(merchant.isOpen === true){
        content += '<ion-chip *ngIf="merchant.isOpen === true" color="success">' +
        `<ion-label color="success">${this.translate.instant('OPEN')}</ion-label>` +
        '</ion-chip>';
      }else if(merchant.isOpen === false){
         content += 
         '<ion-chip *ngIf="merchant.isOpen === false" color="danger">' +
         `<ion-label color="danger">${this.translate.instant('CLOSE')}</ion-label>` +
         '</ion-chip>';
      }else{
        content += `<span *ngIf="merchant.isOpen === undefined" style="color: #D8418F;"> ${this.translate.instant('PLANNING_NOT_FILLED')}` +
        '</span>';
      }
      content +=' </p>' +
      `<img src="${merchant.urlAvatar ? merchant.urlAvatar : 'assets/images/stand.svg'}" height="115" width="115">` +
      '<div class="iw-subTitle">Information</div>' +
      `<p><strong>Téléphone:</strong> ${merchant.telephone}<br>` +
      //`<strong>E-Mail:</strong> ${merchant.mail}<br>` +
      `<strong>Adresse:</strong> ${merchant.rue}, ${merchant.code_postal} ${merchant.commune} </p>`;
      if(merchant.isOpen === false || merchant.isOpen === true){
        content += `<button id="btn-display-planning-${merchant._id}" style="margin-bottom: 5px; font-weight: 400; margin-top: 10px;" type="button"` +
      `class="btn btn-primary w-100 text-uppercase btn-accept">${this.translate.instant('PLANNING')}</button>`;
      }
      content += `<button id="btn-select-merchant-${merchant._id}" style="margin-bottom: 5px; font-weight: 400; margin-top: 10px;" type="button"` +
      `class="btn btn-primary w-100 text-uppercase btn-accept">${this.translate.instant('ORDER')}</button>` +
      '</div>' +
      '</div>';

      }
      else{
       content = '<div id="iw-container">' +
            `<div class="iw-title"><B>${merchant.name} </B></div>` +
            '<div class="iw-content">' +
            '<p style="position: absolute; right: 0">';
            if(merchant.isOpen === true){
              content += '<ion-chip *ngIf="merchant.isOpen === true" color="success">' +
              `<ion-label color="success">${this.translate.instant('OPEN')}</ion-label>` +
              '</ion-chip>';
            }else if(merchant.isOpen === false){
               content +=
               '<ion-chip *ngIf="merchant.isOpen === false" color="danger">' +
               `<ion-label color="danger">${this.translate.instant('CLOSE')}</ion-label>` +
               '</ion-chip>';
            }else{
              content += `<span *ngIf="merchant.isOpen === undefined" style="color: #D8418F;"> ${this.translate.instant('PLANNING_NOT_FILLED')}` +
              '</span>';
            }
            content +=' </p>' +
            `<img src="${merchant.urlAvatar ? merchant.urlAvatar : 'assets/images/stand.svg'}" height="115" width="115">` +
            '<div class="iw-subTitle">Information</div>' +
            `<p><strong>Téléphone:</strong> ${merchant.phoneNumber}<br>` +
            `<strong>E-Mail:</strong> ${merchant.mail}<br>` +
            `<strong>Adresse:</strong> ${merchant.favoriteAddress?.fullAddress}</p>`;
            if(merchant.isOpen === false || merchant.isOpen === true){
              content += `<button id="btn-display-planning-${merchant._id}" style="margin-bottom: 5px; font-weight: 400; margin-top: 10px;" type="button"` +
            `class="btn btn-primary w-100 text-uppercase btn-accept">${this.translate.instant('PLANNING')}</button>`;
            }
            content += `<button id="btn-select-merchant-${merchant._id}" style="margin-bottom: 5px; font-weight: 400; margin-top: 10px;" type="button"` +
            `class="btn btn-primary w-100 text-uppercase btn-accept">${this.translate.instant('ORDER')}</button>` +
            '</div>' +
            '</div>';

      }

      return content;
  }
}
