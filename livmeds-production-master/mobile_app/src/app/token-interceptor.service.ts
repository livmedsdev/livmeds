import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MainEventService } from './main-event.service';
import { NavController } from '@ionic/angular';
import { UserService } from './user.service';
import { SessionService } from './session.service';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private userService: UserService,
    private session: SessionService,
    private _eventEmiter: MainEventService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token') ? `Bearer ${localStorage.getItem('token')}` : undefined;
    const appVersion = this.session.getAppVersion() ? this.session.getAppVersion() : undefined;
    const deviceUuid = this.session.getDeviceUuid() ? this.session.getDeviceUuid() : undefined;

    const header: any = {}
    if (token) {
           header.Authorization = token;
    }

    if (deviceUuid) {
      header.device_uuid = deviceUuid;
    }

    if (appVersion) {
      header.appVersion = appVersion;
    }

    request = request.clone({
      setHeaders: header
    });

    return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
      if (error.status === 401 && this.session.currentUser() !== null) {
        this.userService.logout();
      }
      return throwError(error);
    }))
  }
}
