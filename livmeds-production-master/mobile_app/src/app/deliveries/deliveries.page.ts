import { Component, OnInit } from '@angular/core';
import { DeliveryService } from '../delivery.service';
import { NavController } from '@ionic/angular';
import { DeliveryStatus } from '../enums/DeliveryStatus';

@Component({
  selector: 'app-deliveries',
  templateUrl: './deliveries.page.html',
  styleUrls: ['./deliveries.page.scss'],
})
export class DeliveriesPage implements OnInit {

  public deliveries: any = [];
  public deliveryAccepted;

  constructor(private router: NavController, private deliveryService: DeliveryService) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.refreshList();
  }

  async acceptThisDelivery(id) {
    this.refreshList();
  }

  async refreshList() {
    this.deliveryAccepted = { accepted: false, id: null };
    this.deliveries = await this.deliveryService.getDeliveriesAvailable();
    this.deliveries = this.deliveries.map((delivery) => {
      if (delivery.status === 'IN_DELIVERY') {
        this.deliveryAccepted.accepted = true;
        this.deliveryAccepted.id = delivery._id;
      }
      delivery.statusClass = this.statusClass(delivery.status);
      return delivery
    })
  }

  async displayMap(id) {
    this.router.navigateRoot(['/home/maps', {id: id}]);
  }


  statusClass(status){
    switch(status){
      case DeliveryStatus.IN_DELIVERY:
        return "blue";
      case DeliveryStatus.DELIVERED:
      case DeliveryStatus.VALID:
        return "green";
      case DeliveryStatus.REFUSED:
        return "red-span";
      case DeliveryStatus.AWAITING_VALIDATION:
        return "yellow";
    }
    return "";
  }
}
