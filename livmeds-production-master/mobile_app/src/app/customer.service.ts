import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  async create(body){
    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.post(environment.url + "/medical/customer",
    this.objToForm(body), { headers: headers }).toPromise();
  }

  async edit(id, body){
    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    return await this.http.put(environment.url + "/medical/customer/"+id,
    this.objToForm(body), { headers: headers }).toPromise();
  }

  async list(filter){ 
    return await this.http.get(environment.url + "/medical/customers", {params: filter}).toPromise();
  }

  async get(id){ 
    return await this.http.get(environment.url + "/medical/customer/"+id).toPromise();
  }

  objToForm(data){
    const formData = new FormData();

    for(let key of Object.keys(data)){
      if(['address'].includes(key)){
        formData.append(key, JSON.stringify(data[key]));
      } else if(['socialSecurityNumber', 'mutual'].includes(key) && data[key].name){
        formData.append(key, data[key], data[key].name);
      }else{
        formData.append(key, data[key]);
      }
    }
    return formData;
  }
}
