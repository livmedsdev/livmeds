import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home-address',
  templateUrl: './home-address.page.html',
  styleUrls: ['./home-address.page.scss'],
})
export class HomeAddressPage implements OnInit {
  addresses;
  limit;
  total;

  constructor(private eventEmitter : MainEventService,
    private translate: TranslateService, private userService: UserService) { }

  ngOnInit() {
    this.addresses = [];
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_MY_ADDRESSES').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
    this.eventEmitter.sendEventToHideModalLoader(false);
    await this.refreshList();
    this.eventEmitter.sendEventToHideModalLoader(true);
  }

  async refreshList() {
    let filter: any = {
      limit: this.limit
    }

    const response = await this.userService.getMyAddresses(filter);
    this.addresses = response['data'];
    this.total = response['total'];
  }

  displayMoreAddresses() {
    this.limit += 5
    this.refreshList();
  }
}
