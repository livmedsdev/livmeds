import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { FormsModule } from '@angular/forms';
import { TokenInterceptorService } from './token-interceptor.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { PasswordFieldComponent } from './password-field/password-field.component';
import { PasswordFieldComponentModule } from './password-field/password-field.component.module';
import { LoginPageModule } from './login/login.module';
import { MainEventService } from './main-event.service';
import { PositionService } from './position.service';
import { MapsService } from './maps.service';
import { AddressInputComponent } from './address-input/address-input.component';
import { AddressInputComponentModule } from './address-input/address-input.component.module';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MessageService } from './message.service';
import { SessionService } from './session.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
// import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { EventPollingService } from './event-polling.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SupportTicketsService } from './support-tickets.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { NotificationService } from './notification.service';
import { Device } from '@ionic-native/device/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Appsflyer } from "@ionic-native/appsflyer/ngx";
import { AppVersion } from '@ionic-native/app-version/ngx';
import { TchatService } from './tchat.service';

export function appInitializerFactory(translate: TranslateService) {
  return () => {
    translate.setDefaultLang('fr');
    return translate.use('fr').toPromise();
  };
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, PasswordFieldComponentModule, AddressInputComponentModule],
  providers: [
    AppVersion,
    Appsflyer,
    Facebook,
    BackgroundMode,
    StatusBar,
    SplashScreen,
    Clipboard,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    NativeStorage,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    Geolocation,
    MainEventService,
    PositionService,
    MapsService,
    Globalization,
    EventPollingService,
    SupportTicketsService,
    MessageService,
    Device,
    AndroidPermissions,
    InAppBrowser,
    SessionService,
    NotificationService,
    { provide: LOCALE_ID, useValue: "fr-FR" },
    // FileTransfer,
    File,
    DocumentViewer,
    SocialSharing,
    TchatService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [TranslateService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
