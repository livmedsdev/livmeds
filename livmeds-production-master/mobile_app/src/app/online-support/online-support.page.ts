import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../enums/UserRole';

@Component({
  selector: 'app-online-support',
  templateUrl: './online-support.page.html',
  styleUrls: ['./online-support.page.scss'],
})
export class OnlineSupportPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/home-deliverer' :'/home-customer');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('ONLINE_SUPPORT').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }
}
