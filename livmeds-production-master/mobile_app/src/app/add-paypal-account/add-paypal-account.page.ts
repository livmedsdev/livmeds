import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from '../enums/UserRole';

@Component({
  selector: 'app-add-paypal-account',
  templateUrl: './add-paypal-account.page.html',
  styleUrls: ['./add-paypal-account.page.scss'],
})
export class AddPaypalAccountPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl(localStorage.getItem('role') === UserRole.ROLE_DELIVERER ? '/add-payout-methods' :'/payment-method');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_MY_ACCOUNT_PAYPAL').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }
}
