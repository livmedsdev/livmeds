import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login-customer', loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule) },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule) },
  { path: 'choice', loadChildren: () => import('./choice-type/choice-type.module').then(m => m.ChoiceTypePageModule) },
  // { path: '', redirectTo: 'choice' , pathMatch: 'full'},
  { path: 'sign-up-option', loadChildren: () => import('./sign-up-option/sign-up-option.module').then(m => m.SignUpOptionPageModule) },
  { path: 'sign-up-email-customer', loadChildren: () => import('./sign-up-email-customer/sign-up-email-customer.module').then(m => m.SignUpEmailCustomerPageModule) },
  { path: 'login-deliverer', loadChildren: () => import('./login-deliverer/login-deliverer.module').then(m => m.LoginDelivererPageModule) },
  { path: 'home-customer', loadChildren: () => import('./home-customer/home-customer.module').then(m => m.HomeCustomerPageModule) },
  { path: 'home-deliverer', loadChildren: () => import('./home-deliverer/home-deliverer.module').then(m => m.HomeDelivererPageModule) },
  { path: 'tchat', loadChildren: () => import('./tchat/tchat.module').then(m => m.TchatPageModule) },
  { path: 'apply-promo', loadChildren: () => import('./apply-promo/apply-promo.module').then(m => m.ApplyPromoPageModule) },
  { path: 'payment-method', loadChildren: () => import('./payment-method/payment-method.module').then(m => m.PaymentMethodPageModule) },
  { path: 'history', loadChildren: () => import('./history/history.module').then(m => m.HistoryPageModule) },
  { path: 'home-address', loadChildren: () => import('./home-address/home-address.module').then(m => m.HomeAddressPageModule) },
  { path: 'online-support', loadChildren: () => import('./online-support/online-support.module').then(m => m.OnlineSupportPageModule) },
  { path: 'settings', loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule) },
  { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule) },
  { path: 'wallet', loadChildren: () => import('./wallet/wallet.module').then(m => m.WalletPageModule) },
  { path: 'notifications', loadChildren: () => import('./notifications/notifications.module').then(m => m.NotificationsPageModule) },
  { path: 'deliverer-registration', loadChildren: () => import('./deliverer-registration/deliverer-registration.module').then(m => m.DelivererRegistrationPageModule) },
  { path: 'add-new-car', loadChildren: () => import('./add-new-car/add-new-car.module').then(m => m.AddNewCarPageModule) },
  { path: 'withdraw-history', loadChildren: () => import('./withdraw-history/withdraw-history.module').then(m => m.WithdrawHistoryPageModule) },
  { path: 'sales-history', loadChildren: () => import('./sales-history/sales-history.module').then(m => m.SalesHistoryPageModule) },
  { path: 'new-address', loadChildren: () => import('./new-address/new-address.module').then(m => m.NewAddressPageModule) },
  { path: 'add-credit-card', loadChildren: () => import('./add-credit-card/add-credit-card.module').then(m => m.AddCreditCardPageModule) },
  { path: 'add-paypal-account', loadChildren: () => import('./add-paypal-account/add-paypal-account.module').then(m => m.AddPaypalAccountPageModule) },
  { path: 'add-new-card', loadChildren: () => import('./add-new-card/add-new-card.module').then(m => m.AddNewCardPageModule) },
  { path: 'add-new-bank-account', loadChildren: () => import('./add-new-bank-account/add-new-bank-account.module').then(m => m.AddNewBankAccountPageModule) },
  { path: 'add-payout-methods', loadChildren: () => import('./add-payout-methods/add-payout-methods.module').then(m => m.AddPayoutMethodsPageModule) },
  { path: 'drivers-license', loadChildren: () => import('./drivers-license/drivers-license.module').then(m => m.DriversLicensePageModule) },
  { path: 'personal-id-card', loadChildren: () => import('./personal-id-card/personal-id-card.module').then(m => m.PersonalIdCardPageModule) },
  { path: 'login-medical', loadChildren: () => import('./login-medical/login-medical.module').then(m => m.LoginMedicalPageModule) },
  { path: 'manage-customer', loadChildren: () => import('./manage-customer/manage-customer.module').then(m => m.ManageCustomerPageModule) },
  { path: 'new-customer', loadChildren: () => import('./new-customer/new-customer.module').then(m => m.NewCustomerPageModule) },
  { path: 'documents', loadChildren: () => import('./documents-medical-actor/documents-medical-actor.module').then(m => m.DocumentsMedicalActorPageModule) },
  { path: 'reset-password', loadChildren: () => import('./reset-password/reset-password.module').then(m => m.ResetPasswordPageModule) },
  { path: 'delivery-details', loadChildren: () => import('./delivery-details/delivery-details.module').then(m => m.DeliveryDetailsPageModule) },
  {
    path: 'visio',
    loadChildren: () => import('./visio/visio.module').then( m => m.VisioPageModule)
  },
  {path: 'home-customer-sign',loadChildren: () => import('./home-customer-sign/home-customer-sign.module').then( m => m.HomeCustomerSignPageModule)},
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
