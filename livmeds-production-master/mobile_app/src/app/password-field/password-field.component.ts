import { Component, OnInit, ViewChild, ElementRef, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

declare var $: any;
declare var document: any;

@Component({
  selector: 'app-password-field',
  templateUrl: './password-field.component.html',
  styleUrls: ['./password-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordFieldComponent),
      multi: true
    } 
]})
export class PasswordFieldComponent implements OnInit, ControlValueAccessor {

  @ViewChild('input') input: ElementRef;
  @Input("placeholder") placeholder;
  @Input("idInput") idInput;
  @Input("required") required;
  invalid = false;

  val= "" // this is the updated value that the class accesses
  set value(val){ 
    this.val = val
    this.onChange(val)
    this.onTouch(val)
  }
  isDisabled : boolean = false;

  constructor() { }

  ngOnInit() {
  }

  clickShowPassword() {
    if (this.input.nativeElement.getAttribute('type') == 'text') {
      this.input.nativeElement.setAttribute('type', 'password');
    }
    else {
      this.input.nativeElement.setAttribute('type', 'text');
    }
  }

  writeValue(value: any): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  onChange: any = () => {}
  onTouch: any = () => {}

  validate(value){
    if(this.required === 'true' && value === ''){
      this.invalid = true;
    }else{
      this.invalid = false;
    }
  }

  @Input('checkValid') set checkValid(value: boolean) {
    if(this.input){
      this.validate(this.input.nativeElement.value);
    }
  }
}
