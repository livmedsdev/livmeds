import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DeliveryService } from '../delivery.service';

@Component({
  selector: 'app-mydeliveries',
  templateUrl: './mydeliveries.page.html',
  styleUrls: ['./mydeliveries.page.scss'],
})
export class MydeliveriesPage implements OnInit {
  ngOnInit(): void {
  }
  public deliveries: any = [];

  constructor(
    private router: NavController, private deliveryService: DeliveryService) { }

  async ionViewWillEnter() {
    await this.refreshList();
  }

  async refreshList(){
    this.deliveries = await this.deliveryService.getMyDeliveries();
    this.deliveries = this.deliveries.map((delivery) => {
      delivery.statusClass = this.statusClass(delivery.status);
      return delivery;
    });
  }

  createDelivery(){
    this.router.navigateRoot(['/home/addDelivery', {}]);
  }

  async displayMap(id) {
    this.router.navigateRoot(['/home/maps-customer', {id: id}]);
  }


  statusClass(status){
    switch(status){
      case 'IN_DELIVERY':
        return "blue";
      case 'DELIVERED':
      case 'VALID':
        return "green";
      case 'REFUSED':
        return "red-span";
      case 'AWAITING_VALIDATION':
        return "yellow";
    }
    return "";
  }
}
