import { Component, OnInit } from '@angular/core';
import { MainEventService } from '../main-event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add-payout-methods',
  templateUrl: './add-payout-methods.page.html',
  styleUrls: ['./add-payout-methods.page.scss'],
})
export class AddPayoutMethodsPage implements OnInit {

  constructor(private eventEmitter: MainEventService, private translate : TranslateService) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.eventEmitter.setBackButtonUrl('/wallet');
    this.eventEmitter.sendEventToChangeTitle(await this.translate.get('TITLE_ADD_PAYOUT_METHODS').toPromise());
    this.eventEmitter.sendEventToHideTopBar(false);
    this.eventEmitter.sendEventToHideMenuButton(false);
  }
}
