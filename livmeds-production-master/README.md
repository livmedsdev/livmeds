# Prérequis
 - NodeJS version 11
 - Docker (pour la base de donnée "MongoDB" préconfigurée)
 - npm install -g ionic
 - npm i -g native-run
 - npm i -g cordova
# Installation

Lancez tout d'abord la base de données "MongoDB" à l'aide de docker avec cette commande:
```
docker-compose up -d
```

Executez ensuite toutes les commandes suivantes dans cet ordre:
```
chmod +x init.sh
./init.sh
cd back_end
npm install pm2 -g
pm2 start npm -- start
```
Cela vous permettra d'initialiser les deux projets (front et back) en téléchargeant les modules.
De plus, l'API sera lancée sur le port 3001.

Pour lancez la partie front, exécutez les commandes suivantes dans un autre terminal:
```
cd front_end
npm install -g @angular/cli
ng serve
```
L'interface utilisateur est alors accessible par l'url: http//localhost:4200



Lancer mongodb:
docker-compose up

mongo -u root -p example --authenticationDatabase admin

lancer le back:
cd back_end
npm start

lancer pharma:
cd front_end
ng serve --port 4300

lancer admin:
cd admin_dashboard
ng serve

Build android app:
cd mobile_app
ionic cordova run android --device

C:\Users\X230\AppData\Local\Android\Sdk\extras\google\usb_driver
ng serve --host 0.0.0.0 --disable-host-check
ng serve --host 0.0.0.0 --disable-host-check --port 4300


Google API KEY
AIzaSyBzkbl89HPMW_EoYKNvF1957eJn-o5oqKs


IOS:
1) build project -> ionic cordova prepare ios
2) tu va sur xCode
3) t'appuye sur run



Launch with HTTPS:

Front_end:
screen -S pharma

ng serve --port 443 --ssl true --ssl-cert /etc/letsencrypt/live/livmeds.com/fullchain.pem --ssl-key /etc/letsencrypt/live/livmeds.com/privkey.pem  --host 0.0.0.0 --disable-host-check

admin:
screen -S admin

ng serve --port 80 --ssl true --ssl-cert /etc/letsencrypt/live/livmeds.com/fullchain.pem --ssl-key /etc/letsencrypt/live/livmeds.com/privkey.pem  --host 0.0.0.0 --disable-host-check



## Cards

4706750000000009
4706750000000033
4706750000000025
4706750000000017
