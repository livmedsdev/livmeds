const express = require('express');
const DeliveryController = require("./controllers/DeliveryController");
const PharmanityController = require("./controllers/PharmanityController");
const PrivateMessageController = require("./controllers/PrivateMessageController");
const BlogController = require("./controllers/BlogController");
const router = express.Router();
const Auth = require('./middlewares/authentification');
const ProductController = require("./controllers/ProductController");
const SupportController = require("./controllers/SupportController");
const PromoCodeController = require("./controllers/PromoCodeController");
const UserRole = require('./enums/UserRole');
const Multer = require("multer");
const asyncHandler = require('express-async-handler')

module.exports = () => {
    let deliveryController = new DeliveryController();
    let pharmanityController = new PharmanityController();
    let privateMessageController = new PrivateMessageController();
    let blogController = new BlogController();
    const productController = new ProductController();
    const supportController = new SupportController();
    const promoCodeController = new PromoCodeController();
    let auth = new Auth([UserRole.ROLE_CUSTOMER, UserRole.ROLE_NURSE, UserRole.ROLE_DOCTOR]);
    let authCustomer = new Auth([UserRole.ROLE_CUSTOMER]);

    // Delivery
    router.get('/delivery/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.getDelivery));
    router.post('/delivery', auth.getMiddleware.bind(auth), Multer({storage: Multer.memoryStorage()}).array("file"), asyncHandler(deliveryController.create.bind(deliveryController)));
    router.put('/delivery/cancel/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.cancelDelivery));
    router.put('/delivery/valid/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.validAmount.bind(deliveryController)));
    router.put('/delivery/refuse/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.refuseAmount.bind(deliveryController)));
    router.put('/delivery/received/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.packageIsReceived));
    router.put('/delivery/edit/:id', auth.getMiddleware.bind(auth), Multer({storage: Multer.memoryStorage()}).array("file"), asyncHandler(deliveryController.update));
    router.get('/deliveries', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.listMyDeliveries));
    router.get('/track/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.trackDelivery));
    router.get('/deliverers', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.getDeliverers));
    router.post('/rate',  auth.getMiddleware.bind(auth), asyncHandler(deliveryController.addScore));
    
    router.put('/delivery/card/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.changeCard));

    // Private message while delivery
    router.get('/privateMessages/:idDelivery', auth.getMiddleware.bind(auth), asyncHandler(privateMessageController.getPrivateMessages));
    router.post('/privateMessage', auth.getMiddleware.bind(auth), asyncHandler(privateMessageController.createPrivateMessage));

    // Blog
    router.get('/categories', auth.getMiddleware.bind(auth), asyncHandler(blogController.getCategories));
    router.get('/topics', auth.getMiddleware.bind(auth), asyncHandler(blogController.getTopics));
    router.get('/topic/:id', auth.getMiddleware.bind(auth), asyncHandler(blogController.getTopic));
    router.get('/messages/:id', auth.getMiddleware.bind(auth), asyncHandler(blogController.getMessages));
    router.post('/topic', auth.getMiddleware.bind(auth), asyncHandler(blogController.createTopic));
    router.post('/message', auth.getMiddleware.bind(auth), asyncHandler(blogController.createMessage));

    // Product
    router.get('/products', auth.getMiddleware.bind(auth), asyncHandler(productController.getAllProducts));


    router.get('/support-tickets', auth.getMiddleware.bind(auth), asyncHandler(supportController.getMySupportTicket));
    router.post('/support-tickets', auth.getMiddleware.bind(auth), asyncHandler(supportController.create));

    router.post('/referral-code', authCustomer.getMiddleware.bind(authCustomer), asyncHandler(promoCodeController.generateReferralCode));

    //Pharmanity
    router.get('/pharmanity', auth.getMiddleware.bind(auth), asyncHandler(pharmanityController.getPharmacy));
    router.get('/pharmanity/category', auth.getMiddleware.bind(auth), asyncHandler(pharmanityController.getCategories));
    router.get('/pharmanity/category/products/:id', auth.getMiddleware.bind(auth), asyncHandler(pharmanityController.getProductsById));
    router.get('/pharmanity/category/products/:id/:params/:page', auth.getMiddleware.bind(auth), asyncHandler(pharmanityController.getProducts));
    router.get('/pharmanity/category/products/:id/:params/:page/:ph_id', auth.getMiddleware.bind(auth), asyncHandler(pharmanityController.getProductsByPharma));
    router.get('/pharmanity/category/productsByName/:name', auth.getMiddleware.bind(auth), asyncHandler(pharmanityController.getProductsByName));

    return router;
};