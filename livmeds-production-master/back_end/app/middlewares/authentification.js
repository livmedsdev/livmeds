const jwt = require('jsonwebtoken');
const models = require("../models");
const FacebookAuth = require('../utils/FacebookAuth');
const UserStatus = require('../enums/UserStatus');

module.exports = class {

    constructor(role = []) {
        this.role = role;
    }

    async getMiddleware(req, res, next) {
        let token = req.headers.authorization;

        if (token) {
            token = token.replace(/^Bearer /, "");
            try {
                const decoded = jwt.verify(token, process.env.SECRET_JWT, { ignoreExpiration: false });
                if (this.role.length > 0 && !this.role.includes(decoded.role)) {
                    return res.status(403).send({ auth: false, message: 'Forbidden' });
                }
                models.User.findById(decoded.id, "-password")
                    .exec((err, user) => {
                        if (user !== null) {
                            req.currentUser = user;
                            next();
                        } else {
                            return res.status(401).send({ auth: false, message: 'User not exist' });
                        }
                    })
            } catch (err) {
                console.log(err);
                return res.status(401).send({ auth: false, message: 'No token provided.' });
            }

        } else {
            return res.status(401).send({ auth: false, message: 'No token provided.' });
        }
    };

    async getMiddlewareNotMandatory(req, res, next) {
        let token = req.headers.authorization;
        let socialToken = req.headers.social_token || 'false';
        if (token) {
            token = token.replace(/^Bearer /, "");
            try {
                const decoded = jwt.verify(token, process.env.SECRET_JWT, { ignoreExpiration: false });
                if (this.role.length > 0 && !this.role.includes(decoded.role)) {
                    return res.status(403).send({ auth: false, message: 'Forbidden' });
                }
                models.User.findById(decoded.id, "-password")
                    .exec((err, user) => {
                        if (user !== null) {
                            req.currentUser = user;
                            next();
                        } else {
                            next();
                        }
                    })
            } catch (err) {
                console.log(err);
                next();
            }
        } else {
            next();
        }
    };
}