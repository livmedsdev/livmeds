const express = require('express');
const CustomerController = require("./controllers/CustomerController");
const UserController = require("./controllers/UserController");
const router = express.Router();
const Auth = require('./middlewares/authentification');
const UserRole = require("./enums/UserRole");
const Multer  = require('multer');
const upload = Multer({storage: Multer.memoryStorage()}); 
const asyncHandler = require('express-async-handler')

module.exports = () => {
    let customerController = new CustomerController();
    let auth = new Auth([UserRole.ROLE_DOCTOR, UserRole.ROLE_NURSE]);


    // Delivery
    router.get('/customers', auth.getMiddleware.bind(auth), asyncHandler(customerController.list));
    router.get('/customer/:id', auth.getMiddleware.bind(auth), asyncHandler(customerController.get));
    router.post('/customer', auth.getMiddleware.bind(auth),upload.fields([{ name: 'socialSecurityNumber', maxCount: 1 }, { name: 'mutual', maxCount: 1 }]), asyncHandler(customerController.create));
    router.put('/customer/:id', auth.getMiddleware.bind(auth),upload.fields([{ name: 'socialSecurityNumber', maxCount: 1 }, { name: 'mutual', maxCount: 1 }]), asyncHandler(customerController.edit));
    router.delete('/customer/:id', auth.getMiddleware.bind(auth), asyncHandler(customerController.delete));



    return router;
};