const express = require('express');
const DeliveryController = require("./controllers/DeliveryController")
const UserController = require("./controllers/UserController")
const PrivateMessageController = require("./controllers/PrivateMessageController");
const router = express.Router();
const Auth = require('./middlewares/authentification');
const UserRole = require('./enums/UserRole');
const Multer = require("multer");
const asyncHandler = require('express-async-handler')

module.exports = () => {
    let deliveryController = new DeliveryController();
    let privateMessageController = new PrivateMessageController();
    let userController = new UserController();
    let auth = new Auth([UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY]);


    // Delivery
    router.post('/delivery', auth.getMiddleware.bind(auth),  Multer({storage: Multer.memoryStorage()}).single("file"),  asyncHandler(deliveryController.create.bind(deliveryController)));
    router.get('/deliveries', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.listDeliveries));
    router.get('/delivery/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.getDelivery));
    router.put('/delivery/valid/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.createInvoiceForDelivery.bind(deliveryController)));
    router.put('/delivery/refuse/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.refuseDeliveryByMerchant.bind(deliveryController)));
    router.put('/delivery/give/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.givePackageToDeliverer));

    // Private message while delivery
    router.get('/privateMessages/:idDelivery', auth.getMiddleware.bind(auth), asyncHandler(privateMessageController.getPrivateMessages));
    router.post('/privateMessage', auth.getMiddleware.bind(auth), asyncHandler(privateMessageController.createPrivateMessage));

    router.post('/openingTimes', auth.getMiddleware.bind(auth), asyncHandler(userController.updateOpeningTimes.bind(userController)));

    return router;
};