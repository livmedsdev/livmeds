const express = require('express');
const UserController = require("./controllers/UserController")
const DeliveryController = require("./controllers/DeliveryController");
const ContactController = require("./controllers/ContactController");
const router = express.Router();
const Auth = require('./middlewares/authentification');
const CardController = require('./controllers/CardController');
const ConfigController = require('./controllers/ConfigController');
const ChatController = require('./controllers/ChatController');
const { 
    userValidationRules, 
    validate, 
    userDetailsValidationRules,
    userChangeStatusRules ,
    userSecurityValidationRules
} = require('./validators/validator.js');
const UserRole = require('./enums/UserRole');
const Multer  = require('multer');
const upload = Multer({storage: Multer.memoryStorage()});
const asyncHandler = require('express-async-handler');
const mongoose = require('mongoose');

module.exports = () => {
    let userController = new UserController();
    let deliveryController = new DeliveryController();
    let contactController = new ContactController();
    let cardController = new CardController();
    let configController = new ConfigController();
    let chatController = new ChatController();
    let auth = new Auth();
    let authAdmin = new Auth([UserRole.ROLE_ADMIN, UserRole.ROLE_CUSTOMER]);
    let authCustomer = new Auth([UserRole.ROLE_CUSTOMER, UserRole.ROLE_NURSE, UserRole.ROLE_DOCTOR]);

    let authAppMobileRole = new Auth([UserRole.ROLE_CUSTOMER, UserRole.ROLE_DOCTOR, UserRole.ROLE_NURSE]);

    let authPayout = new Auth([UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY, UserRole.ROLE_OPTICIAN, UserRole.ROLE_DELIVERER]);

    let authPaymentAuthorized = new Auth([UserRole.ROLE_CUSTOMER, UserRole.ROLE_PHARMACY, UserRole.ROLE_OPTICIAN, UserRole.ROLE_VETERINARY, UserRole.ROLE_NURSE, UserRole.ROLE_DOCTOR]);

    router.get('/', (req,res) =>{
        if(mongoose.connection.readyState !== 1 ){
            return res.status(500).send();
        }
        return res.status(200).send();
    });

    // Version
    router.get('/versions', configController.getVersions);

    //User
    router.post('/login', userValidationRules(), validate,asyncHandler(userController.login));
    router.post('/register', upload.fields([{ name: 'socialSecurityNumber', maxCount: 1 }, { name: 'mutual', maxCount: 1 }]), asyncHandler(userController.register.bind(userController)));
    router.post('/logout', auth.getMiddleware.bind(auth), asyncHandler(userController.logout));
    router.get('/checkToken', asyncHandler(userController.checkToken));
    router.post('/lostPassword', asyncHandler(userController.lostPassword));
    router.post('/message', asyncHandler(contactController.create));
    router.post('/changePassword', asyncHandler(userController.changePasswordAfterLostPassword));
    router.get('/profile', auth.getMiddleware.bind(auth), asyncHandler(userController.profile));
    router.put('/security', auth.getMiddleware.bind(auth), userSecurityValidationRules(), validate, asyncHandler(userController.editSecurityInformation));
    router.put('/profile', auth.getMiddleware.bind(auth), upload.fields([{ name: 'socialSecurityNumber', maxCount: 1 }, { name: 'mutual', maxCount: 1 }]), asyncHandler(userController.editMyProfile.bind(userController)));
    router.put('/avatar', auth.getMiddleware.bind(auth), asyncHandler(userController.changeAvatar));

    router.get('/user/contract', auth.getMiddleware.bind(auth), asyncHandler(userController.generateContract));
    router.put('/user/status/:id', userChangeStatusRules(), validate, authAdmin.getMiddleware.bind(authAdmin), asyncHandler(userController.updateStatus.bind(userController)));

    // Addresses
    router.get('/all-addresses', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(userController.getAllAddresses));
    router.get('/addresses', authCustomer.getMiddleware.bind(authCustomer), asyncHandler(userController.getAddresses));
    router.post('/address', authCustomer.getMiddleware.bind(authCustomer), asyncHandler(userController.addAddress));
    
    router.get('/shops', authAppMobileRole.getMiddleware.bind(authAppMobileRole), asyncHandler(deliveryController.listShopAvailable.bind(deliveryController)));


    // CARD & PAYMENT
    router.get('/add-card-intent', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(cardController.addCardIntent));
    router.get('/card/:id', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(cardController.get));
    router.delete('/card/:id', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(cardController.delete));
    router.get('/cards', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(cardController.list));
    router.put('/defaultCard/:id', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(cardController.setDefaultCardId));


    // router.post('/bank-account', authPayout.getMiddleware.bind(authPayout), asyncHandler(cardController.addBankAccount));


    router.post('/documents', auth.getMiddleware.bind(auth), asyncHandler(userController.validAccount.bind(userController)));
    router.put('/documents', auth.getMiddleware.bind(auth), asyncHandler(userController.updateDocuments.bind(userController)));
    router.put('/sign', auth.getMiddleware.bind(auth), asyncHandler(userController.sendSmsToSign.bind(userController)));

    router.get('/typesDeliverer', asyncHandler(userController.listTypeDeliverer.bind(userController)));

    router.get('/info-facebook', asyncHandler(userController.getProfileFacebook.bind(userController)));

    router.put('/user-information-third-party', asyncHandler(userController.storeInformationAppleSignIn.bind(userController)));

    router.post('/notification-token', auth.getMiddleware.bind(auth), asyncHandler(userController.addTokenFcm.bind(userController)));
    router.get('/retrieve/contract', auth.getMiddleware.bind(auth), asyncHandler(userController.getMyContract.bind(userController)));

    //tchat
    router.get('/tchat/token', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(chatController.createTchatToken));
    router.get('/tchat/session/:sessionId', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(chatController.getChatSession));
    router.post('/tchat/session', authPaymentAuthorized.getMiddleware.bind(authPaymentAuthorized), asyncHandler(chatController.createSessionForDelivery));
    
    // Bank account
    router.post('/bank-account', upload.fields([{ name: 'personDocFront', maxCount: 1 }, { name: 'personDocBack', maxCount: 1 },{ name: 'companyDocFront', maxCount: 1 }, { name: 'companyDocBack', maxCount: 1 }]), authPayout.getMiddleware.bind(authPayout), asyncHandler(cardController.addBankAccount));
    router.put('/bank-account', upload.fields([{ name: 'personDocFront', maxCount: 1 }, { name: 'personDocBack', maxCount: 1 },{ name: 'companyDocFront', maxCount: 1 }, { name: 'companyDocBack', maxCount: 1 }]), authPayout.getMiddleware.bind(authPayout), asyncHandler(cardController.editBankAccount));
    router.get('/bank-account', authPayout.getMiddleware.bind(authPayout), asyncHandler(cardController.getConnectedAccount));

    return router;
};