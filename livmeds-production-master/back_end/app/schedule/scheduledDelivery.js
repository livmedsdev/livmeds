const models = require("../models");
const DeliveryStatus = require('../enums/DeliveryStatus');
const schedule = require('node-schedule');
const moment = require('moment');
const findDeliverer = require('../utils/findDeliverer');
const NotificationService = require('../utils/NotificationService');
const PaymentService = require('../utils/PaymentService');
const uuidv4 = require('uuid/v4');

module.exports = (object, keys) => {
    const j = schedule.scheduleJob('0 * * * * *', async () => {

        const response = await models.Delivery
            .updateOne({
                status: DeliveryStatus.SCHEDULED, deliveryDate: {
                    $lte: new Date()
                }
            }, { status: DeliveryStatus.AWAITING_VALIDATION })
            .exec();

    });

    // schedule.scheduleJob('0 * * * * *', async () => {
    //     const idTask = uuidv4();
    //     let minDate = moment(new Date()).subtract({ hours: 1 }).toDate();
    //     let deliveries = await models.Delivery
    //         .find({ status: DeliveryStatus.VALID, $or: [{ idTask: null, reservationDateByTask: null }, { reservationDateByTask: { $lt: minDate } }] }, "_id")
    //         .limit(10)
    //         .exec();
    //     deliveries = deliveries.map((elem) => elem._id);

    //     // Reserve
    //     await models.Delivery
    //         .updateMany({
    //             _id: { $in: deliveries },
    //             status: DeliveryStatus.VALID,
    //             $or: [{ idTask: null, reservationDateByTask: null }, { reservationDateByTask: { $lt: minDate } }]
    //         }
    //             , {
    //                 idTask: idTask,
    //                 reservationDateByTask: new Date()
    //             })
    //         .exec();

    //     deliveries = await models.Delivery
    //         .find({ idTask: idTask })
    //         .exec();

    //     try {

    //         const currentDate = moment(new Date());
    //         const updateDeliveries = [];
    //         for (const delivery of deliveries) {
    //             if (currentDate.diff(moment(delivery.lastUpdateDeliverer), 'minutes') >= 2) {
    //                 const delivererFound = await findDeliverer(delivery);
    //                 if (delivererFound) {
    //                     delivery.historyDeliverer.push(delivererFound);
    //                     const res = await models.Delivery.update({
    //                         _id: delivery._id,
    //                         status: DeliveryStatus.VALID,
    //                         idTask: idTask
    //                     }, {
    //                         deliverer: delivererFound._id,
    //                         lastUpdateDeliverer: new Date(),
    //                         historyDeliverer: delivery.historyDeliverer
    //                     }).exec();

    //                     if(res.nModified > 0){
    //                         const dataNotification = {
    //                             notificationType: 'DELIVERY_AVAILABLE'
    //                         }
    
    //                         const notificationService = new NotificationService();
    //                         await notificationService.notify(delivererFound._id, 'Nouvelle livraison disponible',
    //                             "Une nouvelle demande de livraison est disponible", dataNotification);
    //                     }
    //                 } else {
    //                     updateDeliveries.push(delivery._id);

    //                     if (delivery.preAuthorizationId) {
    //                         const paymentService = new PaymentService();
    //                         await paymentService.cancelPayment(delivery.preAuthorizationId);
    //                     }
    //                 }
    //             }
    //         }

    //         await models.Delivery.updateMany({
    //             _id: { $in: updateDeliveries },
    //             status: DeliveryStatus.VALID,
    //             idTask: idTask
    //         }, {
    //             status: DeliveryStatus.REFUSED_BY_DELIVERER
    //         }).exec();

    //     } catch (error) {
    //         console.log("ERROR_SCHEDULE: " + JSON.stringify(error));
    //     }

    //     // desassign task
    //     await models.Delivery.updateMany({
    //         idTask: idTask
    //     }, {
    //         idTask: null,
    //         reservationDateByTask: null
    //     }).exec();
    // });
}