const mongoose = require('mongoose');
const shortid = require('shortid');
const TypeBonus = require('../enums/TypeBonus');
const UserRole = require('../enums/UserRole');
const UserStatus = require('../enums/UserStatus');
const BonusStatus = require('../enums/BonusStatus');
const DeliveryStatus = require('../enums/DeliveryStatus');
const PromoCodeStatus = require('../enums/PromoCodeStatus');
const PromoCodeType = require('../enums/PromoCodeType');
const SizePackage = require('../enums/SizePackage');
const TypePrivateMessage = require('../enums/TypePrivateMessage');
const ProductStatus = require('../enums/ProductStatus');
const ProductCategory = require('../enums/ProductCategory');
const TypeDeliverer = require('../enums/TypeDeliverer');
const ConnectionType = require('../enums/ConnectionType');
const SupportTicketStatus = require('../enums/SupportTicketStatus');
const DeliveryType = require('../enums/DeliveryType');

// mongoose.set('debug', true);

let ChatSession = new mongoose.Schema({
    delivery: { type: mongoose.Schema.Types.ObjectId, ref: 'Delivery' },
    receiverId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    senderId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    sessionId: {
        type: String,
        index: { unique: true },
        required: true
    },
    type: {
        type: String,
        required: true
    },
});


let PartnerSchema = new mongoose.Schema({
    accessKey: {
        type: String,
        index: { unique: true },
        required: true
    },
    secretKey: {
        type: String,
        required: true
    },
    mail: {
        type: String
    },
    name: {
        type: String
    },
    phoneNumber: {
        type: String
    },
    address: { type: mongoose.Schema.Types.ObjectId, ref: 'Address' },
    siret: String,
    status: {
        type: String,
        enum: Object.values(UserStatus),
        default: UserStatus.ACTIVE,
        required: true
    },
    rib: {
        type: String
    },
    bic: {
        type: String
    },
    responsibleName: {
        type: String
    },
    responsibleLastname: {
        type: String
    },
    insertAt: { type: Date, default: Date.now },
    usedCode: [{ type: mongoose.Schema.Types.ObjectId, ref: 'PromoCode' }]
});

let ExternalCustomerSchema = new mongoose.Schema({
    externalReference: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    additionalAdress: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    mail: {
        type: String,
        required: true
    },
});

const pointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true
    },
    coordinates: {
        type: [Number],
        required: true
    }
});

let AddressSchema = new mongoose.Schema({
    label: {
        type: String,
        required: false
    },
    fullAddress: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    postalCode: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    latitude: {
        type: Number,
        required: true
    },
    longitude: {
        type: Number,
        required: true
    },
    favoriteAddress: {
        type: Boolean,
        required: true,
        default: false
    },
    compAddress: {
        type: String,
        required: false
    },
    doorbell: String,
    floor: String,
    doorCode: String,
    geo: {
        type: pointSchema,
        required: false
    }
});

let UserSchema = new mongoose.Schema({
    mail: { type: String, index: { unique: true }, required: true },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    lastName: {
        type: String
    },
    role: {
        type: String,
        default: UserRole.ROLE_CUSTOMER,
        enum: Object.values(UserRole),
        required: true
    },
    favoritePharma: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    pharmaInformation: { type: mongoose.Schema.Types.ObjectId, ref: 'PharmaInformation' },
    socialSecurityNumber: String,
    mutual: String,
    addresses: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Address' }],
    phoneNumber: {
        type: String,
        required: true
    },
    positions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Position' }],
    avatar: String,
    siret: String,
    status: {
        type: String,
        enum: Object.values(UserStatus),
        default: UserStatus.ACTIVE,
        required: true
    },
    rib: {
        type: String
    },
    responsibleName: {
        type: String
    },
    responsibleLastname: {
        type: String
    },
    deliveryCity: {
        type: String
    },
    typeOfProduct: {
        type: String
    },
    kbis: {
        type: String
    },
    ciBack: {
        type: String
    },
    ciFront: {
        type: String
    },
    criminalRecord: {
        type: String
    },
    tokenResetPassword: {
        type: String
    },
    tokenExpiredDate: {
        type: Date
    },
    typeDeliverer: {
        type: String,
        enum: Object.values(TypeDeliverer),
    },
    online: {
        type: Boolean
    },

    // Stripe
    customerIdPayment: {
        type: String,
        required: false
    },
    accountId: {
        type: String,
        required: false
    },
    defaultCardId: {
        type: String
    },
    bankAccountId:{
        type: String
    },

    // YouSign
    authenticationId: {
        type: String
    },
    contractFileId: {
        type: String
    },
    memberYouSign: {
        type: String
    },
    openingTime: { type: mongoose.Schema.Types.ObjectId, ref: 'OpeningTime' },
    totalScore: {
        type: Number
    },
    typeDeliverer: { type: mongoose.Schema.Types.ObjectId, ref: 'TypeDeliverer' },

    // SOCIAL
    connectionType: {
        type: String,
        enum: Object.values(ConnectionType),
        required: true,
        default: ConnectionType.MAIL
    },

    userIdSocial: {
        type: String
    },
    tokenFcm: {
        type: String
    },
    lastLogin: {
        type: Date
    },
    lastConnection: {
        type: Date
    },
    lastDevice: {
        type: String
    },
    insertAt: { type: Date, default: Date.now },

    usedCode: [{ type: mongoose.Schema.Types.ObjectId, ref: 'PromoCode' }],
    referralCode: {
        type: String,

        required: false, // only required for facebook users
        index: {
            unique: true,
            partialFilterExpression: { referralCode: { $type: 'string' } },
        },
        default: undefined,
    },
    referrer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },


    // For merchant
    location: {
        type: pointSchema,
        required: false
    }
});

let DeliverySchema = new mongoose.Schema({
    customer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    customerOfMerchant: { type: mongoose.Schema.Types.ObjectId, ref: 'Customer' },
    deliverer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    merchant: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    reference: {
        type: String,
        default: shortid.generate,
        index: { unique: true }
    },
    status: {
        type: String,
        enum: Object.values(DeliveryStatus),
        default: DeliveryStatus.AWAITING_VALIDATION
    },
    address: { type: mongoose.Schema.Types.ObjectId, ref: 'Address' },
    image: [String],
    packageGivenToDeliverer: {
        type: Boolean,
        default: false
    },
    recoveredPackageByDeliverer: {
        type: Boolean,
        default: false
    },
    delivererReachDestination: {
        type: Boolean,
        default: false
    },
    checkDeliverySuccessfullyByDeliverer: {
        type: Boolean,
        default: false
    },
    checkDeliverySuccessfullyByCustomer: {
        type: Boolean,
        default: false
    },
    latitude: String,
    longitude: String,
    phoneNumber: String,
    insertAt: { type: Date, default: Date.now },
    deliveryDate: { type: Date, default: Date.now },
    invoice: { type: mongoose.Schema.Types.ObjectId, ref: 'Invoice' },
    productsSelected: [{ /*type: mongoose.Schema.Types.ObjectId, ref: 'Product'*/    }],

    // if b2b
    sizePackage: {
        type: String,
        enum: Object.keys(SizePackage),
    },
    nbPackage: {
        type: Number
    },
    isBtoB: {
        type: Boolean
    },
    isForCustomer: {
        type: Boolean
    },
    nameCustomer: {
        type: String
    },
    lastnameCustomer: {
        type: String
    },
    phoneNumberCustomer: {
        type: String
    },
    tagLine: {
        type: String
    },

    // Payment,
    cardId: {
        type: String,
        required: true
    },
    preAuthorizationId: {
        type: String
    },

    historyDeliverer: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    lastUpdateDeliverer: {
        type: Date
    },
    codePromo: {
        type: String
    },
    idTask: {
        type: String,
        required: false,
        default: null
    },
    reservationDateByTask: {
        type: Date,
        required: false,
        default: null
    },


    // External
    partner: { type: mongoose.Schema.Types.ObjectId, ref: 'Partner' },
    type: {
        type: String,
        enum: Object.values(DeliveryType),
        default: DeliveryType.INTERNAL
    },
    externalCustomer: { type: mongoose.Schema.Types.ObjectId, ref: 'ExternalCustomer' },
    deliveryInstruction: {type: String},
    additionalInformation: {type: String},
    callbackUrlToNotify: { type: String },
    receptionDate: {
        type: Date,
        required: false,
        default: null
    }
});

let InvoiceSchema = new mongoose.Schema({
    delivery: { type: mongoose.Schema.Types.ObjectId, ref: 'Delivery' },
    amountCustomer: {
        required: true,
        type: Number
    },
    amountMerchant: {
        required: true,
        type: Number
    },
    amountDeliverer: {
        required: true,
        type: Number
    },
    platformFee: {
        required: true,
        type: Number
    },
    feeManagement: {
        type: Number
    },
    products: {
        type: [mongoose.Schema.Types.Mixed]
    },
    distanceText: {
        type: String,
        required: true
    },
    distanceInMeter: {
        type: String,
        required: true
    },
    rainPrimeActive: {
        type: Boolean,
        required: true
    },
    nightPrimeActive: {
        type: Boolean,
        required: true
    },
    productsParapharmacy: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
    isBtoB: {
        type: Boolean,
        default: false
    },

    amountCustomerBeforeCodePromo: {
        required: false,
        type: Number
    },
    codePromoInformation: {
        type: mongoose.Schema.Types.Mixed
    },
    promoAmount: {
        required: false,
        type: Number,
        default: 0
    },
    transferToMerchant: {
        type: String
    },
    transferToDeliverer: {
        type: String
    },
});

let TrackingSchema = new mongoose.Schema({
    delivery: { type: mongoose.Schema.Types.ObjectId, ref: 'Delivery' },
    latitude: Number,
    longitude: Number,
    insertAt: { type: Date, default: Date.now }
});

let PositionSchema = new mongoose.Schema({
    latitude: Number,
    longitude: Number,
    insertAt: { type: Date, default: Date.now },
    deliverer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    geo: {
        type: pointSchema,
        required: true
    }
});

// Notification models
let NotificationTokenSchema = new mongoose.Schema({
    token: { type: String, required: true },
    insertAt: { type: Date, default: Date.now },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

// Blog models
let TopicSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category: { type: mongoose.Schema.Types.ObjectId, ref: 'Category', required: true },
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    insertAt: { type: Date, default: Date.now },
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message', required: true }]
});

let CategorySchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    }
});

let MessageSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    insertAt: { type: Date, default: Date.now },
    topic: { type: mongoose.Schema.Types.ObjectId, ref: 'Topic', required: true },
});

// Bonus
let BonusSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: Object.values(TypeBonus),
        required: true
    },
    startFrom: {
        type: Date,
        default: Date.now,
        required: true
    },
    endAt: {
        type: Date,
        default: Date.now,
        required: true
    },
    placeId: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: BonusStatus.ACTIVE,
        enum: Object.values(BonusStatus),
        required: true
    }
});

let PromoCodeSchema = new mongoose.Schema({
    usedTimes: {
        type: Number,
        required: true,
        default: 0
    },
    usedTimeMax: {
        type: Number,
        required: true
    },
    promoCode: {
        type: String,
        required: true,
        unique: true
    },
    percentage: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        default: PromoCodeStatus.ENABLED,
        enum: Object.values(PromoCodeStatus),
        required: true
    },
    type: {
        type: String,
        enum: Object.values(PromoCodeType),
        default: PromoCodeType.REGULAR,
        required: true
    },
    referrerIs: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },
    insertAt: { type: Date, default: Date.now }
})

const PrivateMessageDeliverySchema = new mongoose.Schema({
    message: {
        type: String
    },
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    delivery: { type: mongoose.Schema.Types.ObjectId, ref: 'Delivery', required: true },
    insertAt: { type: Date, default: Date.now },
    typePrivateMessage: {
        type: String,
        enum: Object.values(TypePrivateMessage),
        required: true
    },
    audioFile: {
        type: String
    }
});

let ProductSchema = new mongoose.Schema({
    label: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category: {
        type: String,
        enum: Object.values(ProductCategory),
        required: true
    },
    image: {
        type: String,
        required: false
    },
    status: {
        type: String,
        enum: Object.values(ProductStatus),
        default: ProductStatus.ACTIVE,
        required: true
    }
})

TopicSchema.index({ title: 'text', description: 'text' });

UserSchema.index({ name: 'text' });

AddressSchema.index({ city: 'text', favoriteAddress: 1 });
AddressSchema.index({ postalCode: 'text', favoriteAddress: 1 });

UserSchema.index({ role: 1 });

AddressSchema.index({ geo: '2dsphere'})

PositionSchema.index({ geo: '2dsphere'})

UserSchema.index({ location: '2dsphere'})

let CustomerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    lastname: {
        type: String
    },
    socialSecurityNumber: String,
    mutual: String,
    address: { type: mongoose.Schema.Types.ObjectId, ref: 'Address' },
    phoneNumber: {
        type: String,
        required: true
    },
    merchant: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
});

let ConfigSchema = new mongoose.Schema({
    label: {
        type: String,
        required: true,
        unique: true
    },
    categoriesAvailable: {
        type: [String],
        enum: Object.values(UserRole)
    },
    fixedPriceDelivery: {
        type: Number
    },
    fixedPriceDeliveryTruck: {
        type: Number
    },
    fixedPriceForDeliverer: {
        type: Number
    },
    nbKmSliceDelivery: {
        type: Number
    },
    priceByKmSlice: {
        type: Number
    },
    percentPharmaProduct: {
        type: Number
    },
    multipleBonusMergedPercent: {
        type: Number
    },
    percentPrimeRain: {
        type: Number
    },
    percentPrimeNight: {
        type: Number
    },
    percentReferralCode: {
        type: Number,
        default: 20
    },
    versionIosMin: {
        type: String,
        default: "0.50"
    },
    versionAndroidMin: {
        type: String,
        default: "0.50"
    }
});

let SupportSchema = new mongoose.Schema({
    subject: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    delivery: { type: mongoose.Schema.Types.ObjectId, ref: 'Delivery', required: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    insertAt: { type: Date, default: Date.now },
    status: {
        type: String,
        enum: Object.values(SupportTicketStatus),
        required: true,
        default: SupportTicketStatus.UNPROCESSED
    }
});

let ContactSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    mail: {
        type: String,
        required: true
    },
    subject: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    insertAt: { type: Date, default: Date.now },
    status: {
        type: String,
        enum: Object.values(SupportTicketStatus),
        required: true,
        default: SupportTicketStatus.UNPROCESSED
    }
});


let OpeningTimeSchema = new mongoose.Schema({
    '0Start': {
        type: Date
    },
    '0End': {
        type: Date
    },
    '1Start': {
        type: Date
    },
    '1End': {
        type: Date
    },
    '2Start': {
        type: Date
    },
    '2End': {
        type: Date
    },
    '3Start': {
        type: Date
    },
    '3End': {
        type: Date
    },
    '4Start': {
        type: Date
    },
    '4End': {
        type: Date
    },
    '5Start': {
        type: Date
    },
    '5End': {
        type: Date
    },
    '6Start': {
        type: Date
    },
    '6End': {
        type: Date
    },
    merchant: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

let ScoreSchema = new mongoose.Schema({
    value: {
        type: Number,
        required: true
    },
    userSendScore: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User',
        required: true
    },
    delivery: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Delivery',
        required: true
    },
    forUser: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User',
        required: true
    },
});

// Bonus
let TypeDelivererSchema = new mongoose.Schema({
    label: {
        type: String,
        enum: Object.values(TypeDeliverer),
        required: true
    },
    price: {
        type: Number,
        required: true
    }
});

let UserInformationThirdSign = new mongoose.Schema({
    userId: {
        type: String,
        required: true,
        unique: true
    },
    data: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    raw: {
        type: mongoose.Schema.Types.Mixed,
        required: false
    }
});

let OperationStripeInProgress = new mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    delivery: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Delivery',
        required: false
    },
    insertAt: {
        type: Date,
        required: true
    },
    taskId: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    }
})

module.exports = {
    User: mongoose.model('User', UserSchema),
    Delivery: mongoose.model('Delivery', DeliverySchema),
    Tracking: mongoose.model('Tracking', TrackingSchema),
    Address: mongoose.model('Address', AddressSchema),
    Position: mongoose.model('Position', PositionSchema),
    Topic: mongoose.model('Topic', TopicSchema),
    Message: mongoose.model('Message', MessageSchema),
    NotificationToken: mongoose.model('NotificationToken', NotificationTokenSchema),
    Category: mongoose.model('Category', CategorySchema),
    Bonus: mongoose.model('Bonus', BonusSchema),
    PromoCode: mongoose.model('PromoCode', PromoCodeSchema),
    PrivateMessageDelivery: mongoose.model('PrivateMessageDelivery', PrivateMessageDeliverySchema),
    Invoice: mongoose.model('Invoice', InvoiceSchema),
    Product: mongoose.model('Product', ProductSchema),
    Customer: mongoose.model('Customer', CustomerSchema),
    Config: mongoose.model('Config', ConfigSchema),
    Contact: mongoose.model('Contact', ContactSchema),
    OpeningTime: mongoose.model('OpeningTime', OpeningTimeSchema),
    Score: mongoose.model('Score', ScoreSchema),
    TypeDeliverer: mongoose.model('TypeDeliverer', TypeDelivererSchema),
    Support: mongoose.model('SupportSchema', SupportSchema),
    UserInformationThirdSign: mongoose.model('UserInformationThirdSign', UserInformationThirdSign),
    OperationStripeInProgress: mongoose.model('OperationStripeInProgress', OperationStripeInProgress),
    Partner: mongoose.model('Partner', PartnerSchema),
    ExternalCustomer: mongoose.model('ExternalCustomer', ExternalCustomerSchema),
    ChatSession: mongoose.model('ChatSession', ChatSession)
};