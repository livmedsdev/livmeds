const express = require('express');
const DeliveryController = require("./controllers/DeliveryController")
const PrivateMessageController = require("./controllers/PrivateMessageController");
const router = express.Router();
const Auth = require('./middlewares/authentification');
const asyncHandler = require('express-async-handler');
const UserController = require("./controllers/UserController")

module.exports = () => {
    let deliveryController = new DeliveryController();
    let privateMessageController = new PrivateMessageController();
    const userController = new UserController();
    let auth = new Auth(['ROLE_DELIVERER']);

    // Delivery
    router.get('/delivery/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.getDelivery));
    router.get('/in-delivery', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.getMyDeliveryInProgress));
    router.get('/deliveries/valid', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.listDeliveriesAvailable));
    router.get('/deliveries', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.listMyDeliveriesDeliverer));
    router.put('/delivery/accept/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.acceptDelivery));
    router.post('/track/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.addTrack));
    router.post('/position', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.addPosition));
    router.put('/delivery/recovered/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.recoveredPackageByDeliverer));
    router.put('/delivery/delivered/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.packageDeliveredByDeliverer));
    router.put('/delivery/reachDestination/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.delivererReachDestination));
    router.put('/delivery/returnPackage/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.returnPackage));
    router.put('/delivery/refuse/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.refuseDeliveryByDeliverer.bind(deliveryController)));
    
    router.get('/stats', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.statDeliverer));
    // Private message while delivery
    router.get('/privateMessages/:idDelivery', auth.getMiddleware.bind(auth), asyncHandler(privateMessageController.getPrivateMessages));
    router.post('/privateMessage', auth.getMiddleware.bind(auth), asyncHandler(privateMessageController.createPrivateMessage));
    router.put('/online',  auth.getMiddleware.bind(auth), asyncHandler(userController.setStatusForDeliverer))

    router.get('/check-delivery/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.checkIfItsMyDeliveryAndCheckValidity));

    return router;
};