const models = require("../models");
const googleMapsClient = require('@google/maps').createClient({
  key: process.env.GOOGLE_MAPS_KEY,
  Promise: Promise
});

const TypeDeliverer = require('../enums/TypeDeliverer');
const SizePackage = require('../enums/SizePackage');
const moment = require('moment');
const UserStatus = require("../enums/UserStatus");
const geolib = require('geolib');

module.exports = async (delivery) => {
  let hrstart = process.hrtime()
  try {
    const maxNbKm = Number.parseInt(process.env.MAX_NB_KM) || 30;
    let filter = { role: 'ROLE_DELIVERER', online: true, status: UserStatus.ACTIVE, lastDevice: { $exists: true, $ne: null } };
    if (delivery.isBtoB && delivery.sizePackage === SizePackage["50X50"]) {
      const type = await models.TypeDeliverer.findOne({ label: TypeDeliverer.CARGO_BIKE }).exec();
      filter.typeDeliverer = type._id;
    } else if (delivery.isBtoB && delivery.sizePackage === SizePackage.SMALL) {
      const types = await models.TypeDeliverer.find({ label: { $in: [TypeDeliverer.BIKE, TypeDeliverer.ELECT_SCOOTER, TypeDeliverer.TRUCK] } }).exec();
      let typesDeliverer = [];
      for (const type of types) {
        typesDeliverer.push(type._id);
      }
      filter.typeDeliverer = { $in: typesDeliverer };
    }


    const deliveriesInDelivery = await models.Delivery
      .find({ status: { $in: ['VALID', 'IN_DELIVERY'] } })
      .lean()
      .exec();

    console.log("nb deliveries in progress:");
    console.log(deliveriesInDelivery.length);
    const notInOp = deliveriesInDelivery.map(el => el.deliverer).concat(delivery.historyDeliverer);

    filter._id = { $nin: notInOp };

    // Find deliverer
    let startDb = process.hrtime()
    const deliverers = await models.User
      .find(filter, {_id: 1})
      // .populate({ path: 'positions', options: { sort: { "insertAt": -1 } } })
      .populate('typeDeliverer')
      // .slice('positions', -1)
      .lean()
      .exec();
    let endDb = process.hrtime(startDb)
    console.info('DB - Execution time (hr): %ds %dms', endDb[0], endDb[1] / 1000000)

    startDb = process.hrtime()
    const dateToCompare = moment().subtract(3, 'days').toDate();
    const positions = await models.Position.aggregate(
      [
        { $match: { insertAt: { $gte: dateToCompare } } },
        { $sort: { insertAt: -1 } },
        { $group: { _id: "$deliverer", positions: { $push: { latitude: "$latitude", longitude: "$longitude", insertAt: "$insertAt" } } } },
        { $project: { positions: { $slice: ["$positions", 1] } } }
      ])
      .exec();
    endDb = process.hrtime(startDb)
    console.info('DB-POSITIONS - Execution time (hr): %ds %dms', endDb[0], endDb[1] / 1000000)



    console.log("nb deliverers:" + deliverers.length);
    // Find merchant
    const merchant = await models.User.findById(delivery.merchant)
      .populate('addresses')
      .exec();

    let merchantAddress = merchant.addresses[0];
    for (let address of merchant.addresses) {
      if (address.favoriteAddress) {
        merchantAddress = address;
      }
    }

    let listPositions = positions.map(pos =>{
      const delivFound = deliverers.find(elem => pos._id.equals(elem._id));
      if(delivFound){
        return { latitude: pos.positions[0].latitude, longitude: pos.positions[0].longitude, deliverer: delivFound }
      }
      return undefined;
    })
    // let listPositions = deliverers.map(elem => {
    //   const posFound = positions.find(pos => pos._id.equals(elem._id));
    //   if (posFound) {
    //     return { latitude: posFound.positions[0].latitude, longitude: posFound.positions[0].longitude, deliverer: elem }
    //   }
    //   return undefined
    // })
    
    listPositions = listPositions.filter(elem => elem)

    let orderStart = process.hrtime()
    const sortedPos = geolib.orderByDistance({ latitude: merchantAddress.latitude, longitude: merchantAddress.longitude }, listPositions);
    console.log("nb sorted pos: " + sortedPos.length)
    console.log("sortedPos: " + JSON.stringify(sortedPos))
    let orderEnd = process.hrtime(orderStart)
    console.info('ORDER: Execution time (hr): %ds %dms', orderEnd[0], orderEnd[1] / 1000000)
    let foundStart = process.hrtime()
    for (let pos of sortedPos) {
      if ((geolib.getDistance({ latitude: merchantAddress.latitude, longitude: merchantAddress.longitude }, pos) / 1000) < maxNbKm) {
        let hrend = process.hrtime(hrstart)
        let foundEnd = process.hrtime(foundStart)
        console.info('FIND - Execution time (hr): %ds %dms', foundEnd[0], foundEnd[1] / 1000000)
        console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000)
        return pos.deliverer;
      } else {
        let foundEnd = process.hrtime(foundStart)
        let hrend = process.hrtime(hrstart)
        console.info('FIND - Execution time (hr): %ds %dms', foundEnd[0], foundEnd[1] / 1000000)
        console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000)
        return null;
      }
    }

    let hrend = process.hrtime(hrstart)
    console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000)
    return null;
  } catch (error) {
    console.log("ERROR DURING PROCESS findDeliverer: " + error)
  }
}