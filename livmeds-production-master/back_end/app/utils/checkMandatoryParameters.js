module.exports = (object, keys) => {
    const target = {};
    const objectFiltered = Object.assign(target, object)
    let result = true;
    keys.forEach((key) => {
        const array = Object.keys(objectFiltered);
        const index = array.indexOf(key);
        if( index == -1 || object[key] === '' || object[key] === null || object[key] === undefined) {
            result = false;
        }
    });
    return result;
}