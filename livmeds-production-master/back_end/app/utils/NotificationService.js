const models = require("../models");
const FCM = require('fcm-node');
const fcm = new FCM(process.env.FCM_KEY);
const fetch = require('node-fetch');

// TODO: Class to notify specific user
module.exports = class {

    async notify(userId, title, messageContent, data = {}) {
        const user = await models.User.findById(userId).exec();
        return new Promise((resolve, reject) => {
            console.log(user.tokenFcm);
            if (user.tokenFcm) {
                const message = {
                    to: user.tokenFcm,
                    notification: {
                        title: title, //title of notification 
                        body: messageContent, //content of the notification
                        sound: "default",
                        icon: "ic_launcher" //default notification icon
                    },
                    data: data //payload you want to send with your notification
                };
    
                fcm.send(message, function (err, response) {
                    if (err) {
                        console.log("Notification not sent");
                        console.log(err)
                        resolve(err);
                    } else {
                        console.log("Successfully sent with response: ", response);
                        resolve(response);
                    }
                });
            }else{
                console.log("NO TOKEN FCM for " + userId)
                resolve();   
            }
        })
    }

    async notifyByTokenFcm(tokenFcm, title, messageContent, data = {}) {
        return new Promise((resolve, reject) => {
                const message = {
                    to: tokenFcm,
                    notification: {
                        title: title, //title of notification 
                        body: messageContent, //content of the notification
                        sound: "default",
                        icon: "ic_launcher" //default notification icon
                    },
                    data: data //payload you want to send with your notification
                };
    
                fcm.send(message, function (err, response) {
                    if (err) {
                        console.log("Notification not sent");
                        console.log(err)
                        resolve(err);
                    } else {
                        console.log("Successfully sent with response: ", response);
                        resolve(response);
                    }
                });
        })
    }

    async notifyPartner(deliveryId){
        const delivery = await models.Delivery.findById(deliveryId).exec();
        const callbackUrlToNotify = delivery.callbackUrlToNotify;
        try{
            if(callbackUrlToNotify && callbackUrlToNotify !== ''){
                const body = { 
                    deliveryId: deliveryId,
                    status: delivery.status,
                    receptionDate: delivery.receptionDate.getTime()
                };
                const res = await fetch(callbackUrlToNotify, {
                    method: 'post',
                    body:    JSON.stringify(body),
                    headers: { 'Content-Type': 'application/json' },
                });
            }
        }catch(error){
            console.log("Error to notify external partner: " + JSON.stringify(error))
        }
    }
}