const models = require("../models");

module.exports = class {

    formatString(str){
        if(str && (typeof str === "string")){
            return str.trim();
        }else{
            return str;
        }
    }

    firstCharUpperCase(str){
        if(str && (typeof str === "string")){
            return this.capitalize(str)
        }else{
            return str;
        }
    }

    capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}