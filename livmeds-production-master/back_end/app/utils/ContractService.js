const Fs = require('fs')
const Path = require('path')
const Util = require('util')
const Puppeteer = require('puppeteer')
const Handlebars = require('handlebars')
const ReadFile = Util.promisify(Fs.readFile)
const TranslateCode = require('../enums/TranslateCode');
const UserRole = require('../enums/UserRole');
const UserRoleFr = require('../enums/UserRoleFr');
const moment = require('moment');


// TODO: Class to notify specific user
module.exports = class {
    constructor(pathTemplate = null, params = null) {
        this.pathTemplate = pathTemplate;
        this.params = params;
    }
    async html() {
        try {

            const templatePath = this.pathTemplate;
            const content = await ReadFile(templatePath, 'utf8')

            // compile and render the template with handlebars
            const template = Handlebars.compile(content)

            return template(this.params)
        } catch (error) {
            console.log(error);
            throw new Error('Cannot create invoice HTML template.')
        }
    }

    async pdf() {
        const html = await this.html()

        const browser = await Puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']})
        const page = await browser.newPage()
        await page.setContent(html)

        return page.pdf({
            format: 'A4',
            displayHeaderFooter: true,
            margin: {top: 40, bottom: 40}
        })
    }

    async generateContract(user){
        const favoriteAddress = user.addresses.filter((elem) => elem.favoriteAddress)[0];
        switch (user.role) {
            case UserRole.ROLE_DELIVERER:
                this.params = {
                    name: user.name,
                    lastname: user.lastName,
                    fullAddress: favoriteAddress.fullAddress,
                    zipCode: favoriteAddress.postalCode,
                    city: favoriteAddress.city,
                    phoneNumber: user.phoneNumber,
                    mail: user.mail,
                    siret: user.siret,
                    taglineDate: moment(new Date()).format('DD/MM/YYYY')
                }
                this.pathTemplate = 'app/resources/contract/template/contract-deliverer-template.html';
                break;
            case UserRole.ROLE_DOCTOR:
            case UserRole.ROLE_NURSE:
            case UserRole.ROLE_OPTICIAN:
            case UserRole.ROLE_VETERINARY:
            case UserRole.ROLE_PHARMACY:
                this.params = {
                    shopName: user.name + ' ' + user.lastName,
                    name: user.name,
                    lastname: user.lastName,
                    fullAddress: favoriteAddress.fullAddress,
                    zipCode: favoriteAddress.postalCode,
                    city: favoriteAddress.city,
                    phoneNumber: user.phoneNumber,
                    mail: user.mail,
                    siret: user.siret,
                    taglineDate: moment(new Date()).format('DD/MM/YYYY'),
                    role: UserRoleFr[user.role]
                }

                case UserRole.ROLE_OPTICIAN:
                case UserRole.ROLE_PHARMACY:
                case UserRole.ROLE_VETERINARY:
                    this.params.shopName = user.name;
                    this.params.name = user.responsibleName;
                    this.params.lastname = user.responsibleLastname;

                this.pathTemplate = 'app/resources/contract/template/contract-medical-partner-template.html';
                break;
            default:
                throw TranslateCode.NOT_AUTHORIZED;
        }
        return await this.pdf();
    }
}