const fetch = require('node-fetch');

module.exports.getUser = async (code) => {
    const client_id = process.env.FACEBOOK_CLIENT_ID;
    const client_secret = process.env.FACEBOOK_CLIENT_SECRET;

    let appToken;
    let url = 'https://graph.facebook.com/oauth/access_token?client_id=' + client_id + '&client_secret=' + client_secret + '&grant_type=client_credentials';
    try {
        //login as a facebook app to get an "app token"
        let response = await fetch(url, { method: 'GET' });
        response = await response.json();
        appToken = response.access_token;
        //validate "social token", must pass the "app token"
        let responseInfoUser = await fetch('https://graph.facebook.com/debug_token?input_token=' + code + '&access_token=' + appToken, {
            method: 'GET',
        });
        responseInfoUser = await responseInfoUser.json();
        const { app_id, is_valid, user_id } = responseInfoUser.data
        if (app_id !== client_id) {
            throw new Error('invalid app id: expected [' + client_id + '] but was [' + app_id + ']');
        }
        //check if the token is valid
        if (!is_valid) {
            throw new Error('token is not valid');
        }

        return user_id;
    } catch (err) {
        throw new Error("error while authenticating facebook user: " + JSON.stringify(err));
    }

}


module.exports.getUserInfo = async (code) => {
    const client_id = process.env.FACEBOOK_CLIENT_ID;
    const client_secret = process.env.FACEBOOK_CLIENT_SECRET;

    let appToken;
    let url = 'https://graph.facebook.com/oauth/access_token?client_id=' + client_id + '&client_secret=' + client_secret + '&grant_type=client_credentials';
    try {
        //login as a facebook app to get an "app token"
        let response = await fetch(url, { method: 'GET' });
        response = await response.json();
        appToken = response.access_token;
        //validate "social token", must pass the "app token"
        let responseInfoUser = await fetch('https://graph.facebook.com/debug_token?input_token=' + code + '&access_token=' + appToken, {
            method: 'GET',
        });
        responseInfoUser = await responseInfoUser.json();
        const { app_id, is_valid, user_id } = responseInfoUser.data
        if (app_id !== client_id) {
            throw new Error('invalid app id: expected [' + client_id + '] but was [' + app_id + ']');
        }
        //check if the token is valid
        if (!is_valid) {
            throw new Error('token is not valid');
        }

        //get user profile using the app token
        let userInfo = await fetch( 'https://graph.facebook.com/v2.11/' + user_id + '?fields=id,first_name,last_name,email&access_token=' + appToken, {
                method: 'GET',
            } )
        userInfo = await userInfo.json();
        return {
            id: userInfo.id,
            name: userInfo.first_name,
            lastName: userInfo.last_name,
            mail: userInfo.email
        };

        // } )
        // .then( response => response.json() )
        // .then( response => {
        // // return the user profile
        //     const { id, picture, email, name } = response;
        //     let user = {
        //         name: name,
        //         pic: picture.data.url,
        //         id: id,
        //         email_verified: true,
        //         email: email
        //     }
        //     return user;
        // } )
        //throw an error if something goes wrong
    } catch (err) {
        throw new Error("error while authenticating facebook user: " + JSON.stringify(err));
    }

}