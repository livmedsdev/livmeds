const appleSignin = require("apple-signin");
const path = require("path");
const jsonwebtoken = require('jsonwebtoken');
const fs = require('fs');


const privateKeyPath = fs.readFileSync(path.join(__dirname, "../../certificates/AuthKey_BKNKP5D76D.p8"));

const jwksClient = require('jwks-rsa');


module.exports.getUser = async (code) => {
    const clientId = process.env.APPLE_CLIENT_ID;
    const teamId = process.env.APPLE_TEAM_ID;
    const keyIdentifier = process.env.APPLE_KEY_IDENTIFIER;
    const redirectUri = process.env.APPLE_REDIRECT_URI;

    try {
        const result = jsonwebtoken.decode(code, { complete: true })
        console.log(result);

        const client = jwksClient({
            strictSsl: true, // Default value
            jwksUri: 'https://appleid.apple.com/auth/keys'
        });

        const kid = result.header.kid;
        const publicKey = await new Promise((resolve, reject) => {
            client.getSigningKey(kid, (err, key) => {
                if (err) {
                    reject(err);
                }
                resolve(key.getPublicKey());
            });
        });

        const verifyResult = jsonwebtoken.verify(code, publicKey);
        console.log(verifyResult);
        return verifyResult.sub;

    } catch (err) {
        console.log(err);
        throw new Error('APPLE : token is not valid');
    }

}