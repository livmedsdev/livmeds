const fetch = require('node-fetch');
const FormData = require('form-data');
const UserRole = require('../enums/UserRole');


module.exports = class {

    async uploadFile(data){
        return (await fetch(process.env.YOUSIGN_URL+'/files', {
            method: 'post',
            body:    JSON.stringify(data),
            headers: {
                'Authorization': 'Bearer ' + process.env.YOUSIGN_API_KEY, 
                'Content-Type': 'application/json' },
        })).json();
    }

    async createProcedure(data){
        return (await fetch(process.env.YOUSIGN_URL+'/procedures', {
            method: 'post',
            body:    JSON.stringify(data),
            headers: {
                'Authorization': 'Bearer ' + process.env.YOUSIGN_API_KEY, 
                'Content-Type': 'application/json' },
        })).json();
    }

    async sendAuthentification(data){
        return (await fetch(process.env.YOUSIGN_URL+'/operations', {
            method: 'post',
            body:    JSON.stringify(data),
            headers: {
                'Authorization': 'Bearer ' + process.env.YOUSIGN_API_KEY, 
                'Content-Type': 'application/json' },
        })).json();
    }

    async authenticationSms(id, data){
        return (await fetch(process.env.YOUSIGN_URL+'/authentications/sms/'+id, {
            method: 'put',
            body:    JSON.stringify(data),
            headers: {
                'Authorization': 'Bearer ' + process.env.YOUSIGN_API_KEY, 
                'Content-Type': 'application/json' },
        })).json();
    }

    async downloadFile(idMember){
        return (await fetch(process.env.YOUSIGN_URL+idMember+'/proof', {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + process.env.YOUSIGN_API_KEY, 
                'Content-Type': 'application/json' },
        })).json();
    }
}