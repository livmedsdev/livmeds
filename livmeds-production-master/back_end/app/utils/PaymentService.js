const fetch = require('node-fetch');
const models = require("../models");
const FormData = require('form-data');
const UserRole = require('../enums/UserRole');
const TranslateCode = require('../enums/TranslateCode');
const FileService = require('./FileService');
const { model } = require('mongoose');
const AddressService = require('../utils/AddressService');
const addressService = new AddressService(models);

const fileService = new FileService();
const stripe = require('stripe')(process.env.STRIPE_API_KEY);

module.exports = class {

    async createUser() {
        return await stripe.customers.create();
    }

    async updateUser(user, addr) {

        if (!user.customerIdPayment) {
            return;
        }

        let name = user.lastName + ' ' + user.name;
        if (["ROLE_OPTICIAN", "ROLE_PHARMACY", "ROLE_VETERINARY"].includes(user.role)) {
            name = user.name;
        }

        return await stripe.customers.update(
            user.customerIdPayment,
            {
                email: user.mail,
                address: {
                    city: addr.city,
                    line1: addr.fullAddress
                },
                name: name,
                phone: user.phoneNumber,
                metadata: {
                    role: user.role
                },
                shipping: {
                    address: {
                        city: addr.city,
                        line1: addr.fullAddress
                    },
                    name: name,
                    phone: user.phoneNumber,
                }
            }
        );
    }

    async updateUserWithoutAddr(user) {

        if (!user.customerIdPayment) {
            return;
        }

        let name = user.lastName + ' ' + user.name;
        if (["ROLE_OPTICIAN", "ROLE_PHARMACY", "ROLE_VETERINARY"].includes(user.role)) {
            name = user.name;
        }

        return await stripe.customers.update(
            user.customerIdPayment,
            {
                email: user.mail,
                name: name,
                phone: user.phoneNumber,
                metadata: {
                    role: user.role
                },
            }
        );
    }

    async createSetupIntent(customerId) {
        return await stripe.setupIntents.create({
            customer: customerId,
        });
    }

    async listCardsByUser(userId) {
        return (await stripe.paymentMethods.list({
            customer: userId,
            type: 'card',
        }))['data'];
    }

    async verifyPreAuthorization(deliveryData, cardId, userId, amount) {
        let transferGroup = `${deliveryData.id}-ref${deliveryData.reference}`
        try {
            const paymentIntent = await stripe.paymentIntents.create({
                amount: Number.parseFloat(amount * 100).toFixed(0),
                currency: 'eur',
                customer: userId,
                payment_method: cardId,
                confirmation_method: 'manual',
                transfer_group: transferGroup
            });

            console.log(paymentIntent);
            return paymentIntent.id;
        } catch (err) {
            console.log(err);
            // Error code will be authentication_required if authentication is needed
            console.log('Error code is: ', err.code);
            const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
            console.log('PI retrieved: ', paymentIntentRetrieved.id);
            throw paymentIntentRetrieved.id;
        }
    }

    async confirmTransaction(preAuthorizationId, cardId) {
        try {
            const payment = await stripe.paymentIntents.confirm(
                preAuthorizationId, {
                off_session: 'true',
                payment_method: cardId
            }
            );
        } catch (error) {
            console.log("ERROR " + JSON.stringify(error));
            throw error;
        }
    }

    async deleteCard(cardId) {
        return await stripe.paymentMethods.detach(cardId);
    }

    async cancelPayment(id) {
        return await stripe.refunds.create({
            payment_intent: id,
        });
    }

    async createAccount(type, mail, addr, name, data) {
        let params = {
            type: "custom",
            email: mail,
            capabilities: {
                transfers: { requested: true },
            },
            settings: {
                payouts: {
                    schedule: {
                        monthly_anchor: data.payoutDay,
                        interval: 'monthly'
                    }
                }
            }
        };
        const paramsToken = {
            account: {
                tos_shown_and_accepted: true,
                business_type: type,
            },
        };
        switch (type) {
            case "company":
                paramsToken.account.company = {
                    address: {
                        city: addr.city,
                        line1: addr.fullAddress,
                        postal_code: addr.postalCode
                    },
                    name: name,
                    directors_provided: true,
                    executives_provided: true,
                    owners_provided: true,
                    tax_id: data.taxId,
                    verification: {
                        document: {
                            back: data.backFileId,
                            front: data.frontFileId
                        }
                    }
                };
                params.business_profile = {
                    url: "https://livmeds.com"
                };
                break;
        }
        const token = await stripe.tokens.create(paramsToken);
        params.account_token = token.id;
        return await stripe.accounts.create(params);
    }

    async updateAccount(accountId, mail, addr, name, data) {
        let params = {
            email: mail,
        };

        if (data.payoutDay) {
            params.settings = {
                payouts: {
                    schedule: {
                        monthly_anchor: data.payoutDay,
                        interval: 'monthly'
                    }
                }
            }
        }

        const paramsToken = {
            account: {
                company: {
                    address: {
                        city: addr.city,
                        line1: addr.fullAddress,
                        postal_code: addr.postalCode
                    },
                    name: name,
                }
            }
        };

        if (data.taxId) {
            paramsToken.account.company.tax_id = data.taxId
        }

        if (data.frontFileId || data.backFileId) {
            paramsToken.account.company.document = {};
        }

        if (data.frontFileId) {
            paramsToken.account.company.document.front = data.frontFileId;
        }

        if (data.backFileId) {
            paramsToken.account.company.document.back = data.backFileId;
        }

        const token = await stripe.tokens.create(paramsToken);
        params.account_token = token.id;
        return await stripe.accounts.update(accountId, params);
    }

    async createBankAccount(accountId, bankAccountToken) {
        return await stripe.accounts.createExternalAccount(
            accountId,
            {
                external_account: bankAccountToken,
                default_for_currency: true
            }
        );
    }

    async deleteBankAccount(accountId, bankAccountId) {
        return await stripe.accounts.deleteExternalAccount(
            accountId,
            bankAccountId,
          );
    }

    async payout(destId, amount, metadata) {
        return await stripe.transfers.create({
            amount: Number.parseFloat(amount * 100).toFixed(0),
            currency: 'eur',
            destination: destId,
            metadata: metadata
        });
    }

    async createPerson(accountId, lastname, name, addr, data) {
        const person = await stripe.accounts.createPerson(
            accountId,
            {
                first_name: name,
                last_name: lastname,
                relationship: {
                    representative: true,
                    executive: true,
                },
                address: {
                    city: addr.city,
                    postal_code: addr.postalCode,
                    line1: addr.fullAddress
                },
                dob: {
                    day: data.dob.day,
                    month: data.dob.month,
                    year: data.dob.year
                },
                verification: {
                    document: {
                        back: data.backPersonFileId,
                        front: data.frontPersonFileId
                    },
                    additional_document: {
                        back: data.backPersonFileId,
                        front: data.frontPersonFileId
                    },
                }
            }
        );
        return person;
    }

    async updatePerson(accountId, personId, lastname, name, addr, data) {
        const params = {
            first_name: name,
            last_name: lastname,
            address: {
                city: addr.city,
                postal_code: addr.postalCode,
                line1: addr.fullAddress
            },
        }

        if(data.dob){
            params.dob = {
                day: data.dob.day,
                month: data.dob.month,
                year: data.dob.year
            }
        }

        if(data.backPersonFileId || data.frontPersonFileId){
            params.verification = {
                document: {},
                additional_document: {}
            }
        }

        if(data.backPersonFileId){
            params.verification.document = {
                back: data.backPersonFileId,
            };
            params.verification.additional_document = {
                back: data.backPersonFileId,
            }
        }

        if(data.frontPersonFileId){
            params.verification.document = {
                front: data.frontPersonFileId
            };
            params.verification.additional_document = {
                front: data.frontPersonFileId
            }
        }

        const person = await stripe.accounts.updatePerson(
            accountId,
            personId,
            params
        );
        console.log(person)
        return person;
    }

    async getCapabilities(accountId) {
        return await stripe.accounts.listCapabilities(
            accountId
        );
    }

    async uploadFile(fileData, purpose) {
        // const parsedFile = JSON.parse(fileInformation);
        // const path = await fileService.getObject(parsedFile.bucket, parsedFile.key);
        // const fp = require('fs').readFileSync(path);
        console.log(fileData)
        const file = await stripe.files.create({
            purpose: purpose,
            file: {
                data: fileData.buffer,
                name: fileData.originalname,
                type: 'application/octet-stream',
            },
        });
        return file;
    }

    async payMultiPart(deliveryData, transferToMerchantData, transferToDelivererData) {
        try {
            let result = {};

            let transferGroup = `${deliveryData.id}-ref${deliveryData.reference}`

            const payment = await stripe.paymentIntents.retrieve(
                deliveryData.paymentId
            );

            if (transferToMerchantData && transferToMerchantData.accountId) {
                // Transfer to merchant
                result.transferToMerchant = await stripe.transfers.create({
                    amount: Number.parseFloat(transferToMerchantData.amount * 100).toFixed(0),
                    currency: 'eur',
                    destination: transferToMerchantData.accountId,
                    source_transaction: payment.charges.data[0].id,
                    transfer_group: transferGroup
                });
            }

            if (transferToDelivererData && transferToDelivererData.accountId) {
                // Transfer to deliverer
                result.transferToDeliverer = await stripe.transfers.create({
                    amount: Number.parseFloat(transferToDelivererData.amount * 100).toFixed(0),
                    currency: 'eur',
                    destination: transferToDelivererData.accountId,
                    source_transaction: payment.charges.data[0].id,
                    transfer_group: transferGroup
                });
            }

            return result;
        } catch (error) {
            console.log('ERROR TO PAY MULTIPART')
            console.log(error)
        }
    }

    async getAccount(accountId) {
        return await stripe.accounts.retrieve(accountId);
    }

    async getPerson(accountId) {
        return await stripe.accounts.listPersons(
            accountId,
            {
                limit: 1,
                relationship: {
                    representative: true,
                    executive: true,
                }
            }
        );
    }

    async addBankAccount(user, params, files){
        if(user.accountId || ![UserRole.ROLE_DELIVERER, UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY].includes(user.role)){
            return res.status(403).send({tradCode: TranslateCode.NOT_AUTHORIZED});
        }

        const companyDocFront = await this.uploadFile(files.companyDocFront[0], "account_requirement")
        const companyDocBack = await this.uploadFile(files.companyDocBack[0], "account_requirement")
        const personDocFront = await this.uploadFile(files.personDocFront[0], "identity_document")
        const personDocBack = await this.uploadFile(files.personDocBack[0], "identity_document")

        const data = {
            taxId: params.taxId,
            frontFileId: companyDocFront.id,
            backFileId: companyDocBack.id,
            frontPersonFileId: personDocFront.id,
            backPersonFileId: personDocBack.id,
            payoutDay: params.payoutDay,
            dob: params.dob 
        };

        let name = user.name;
        if(user.role === UserRole.ROLE_DELIVERER){
            name = user.lastName + ' ' + user.name;
        }

        const account = await this.createAccount("company", user.mail, addressService.getFavoriteAddress(user.addresses), name, data)
        console.log(account);

        let responsibleLastname = user.responsibleLastname;
        let responsibleName = user.responsibleName;
        if(user.role === UserRole.ROLE_DELIVERER){
            responsibleLastname = user.lastName;
            responsibleName = user.name;
        }

        const person = await this.createPerson(account.id, responsibleName, responsibleLastname, addressService.getFavoriteAddress(user.addresses), data);
        console.log(person)

        const bankAccount = await this.createBankAccount(account.id, params.bankAccountToken);
        console.log(bankAccount);

        return {
            bankAccountId: bankAccount.id,
            accountId: account.id
        }
    }

    async editBankAccount(user, params, files){
        let dataAccount = {};
        let dataPerson = {};
        let result = {}

        if (files && files.companyDocFront && files.companyDocFront[0]) {
            dataAccount.companyDocFront = (await this.uploadFile(files.companyDocFront[0], "account_requirement")).id
        }

        if (files && files.companyDocBack && files.companyDocBack[0]) {
            dataAccount.companyDocBack = (await this.uploadFile(files.companyDocBack[0], "account_requirement")).id
        }

        if (files && files.personDocFront && files.personDocFront[0]) {
            dataPerson.personDocFront = (await this.uploadFile(files.personDocFront[0], "identity_document")).id;
        }

        if (files && files.personDocBack && files.personDocBack[0]) {
            dataPerson.personDocBack = (await this.uploadFile(files.personDocBack[0], "identity_document")).id;
        }

        if (params.taxId) {
            dataAccount.taxId = params.taxId;
        }

        if (params.payoutDay) {
            dataAccount.payoutDay = params.payoutDay;
        }

        let name = user.name;
        if (user.role === UserRole.ROLE_DELIVERER) {
            name = user.lastName + ' ' + user.name;
        }

        // Edit account
        console.log("Edit account")
        await this.updateAccount(user.accountId, user.mail, addressService.getFavoriteAddress(user.addresses), name, dataAccount)

        // Edit person
        console.log("Edit person")
        let responsibleLastname = user.responsibleLastname;
        let responsibleName = user.responsibleName;
        if (user.role === UserRole.ROLE_DELIVERER) {
            responsibleLastname = user.lastName;
            responsibleName = user.name;
        }

        if (params.dob) {
            dataPerson.dob = params.dob;
        }

        const person = (await this.getPerson(user.accountId)).data[0];
        await this.updatePerson(user.accountId, person.id, responsibleLastname, responsibleName, addressService.getFavoriteAddress(user.addresses), dataPerson);

        if (params.bankAccountToken) {
            result.bankAccount = await this.createBankAccount(user.accountId, params.bankAccountToken);
            await this.deleteBankAccount(user.accountId, user.bankAccountId)
        }

        return result;
    }
}