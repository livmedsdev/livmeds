const sgMail = require('@sendgrid/mail');
const Fs = require('fs')
const Util = require('util')
const Handlebars = require('handlebars')
const ReadFile = Util.promisify(Fs.readFile)
sgMail.setApiKey(process.env.SENDGRID_API_KEY);


// TODO: Class to notify specific user
module.exports = class {

    async sendMail(to, subject, htmlBody) {
        const msg = {
            to: to,
            from: 'platform-livmeds@livmeds.com',
            subject: subject,
            html: htmlBody
        }
        await sgMail.send(msg);
    }

    async getEmailHtml(templatePath, params){
        try {
            const content = await ReadFile(templatePath, 'utf8')

            // compile and render the template with handlebars
            const template = Handlebars.compile(content)

            return template(params)
        } catch (error) {
            console.log(error);
            throw new Error('Cannot create invoice HTML template.')
        }
    }
} 