// Class to filter parameters receive by each endpoint
module.exports = (object, keys) => {
    const target = {};
    const objectFiltered = Object.assign(target, object)
    Object.keys(objectFiltered).forEach(function(key) {
        if(keys.indexOf(key) == -1) {
            delete objectFiltered[key];
        }
    });
    return objectFiltered;
}