
const mime = require('mime-types');
const AWS = require('aws-sdk');
const uuidv4 = require('uuid/v4');
const fs = require('fs')

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_S3_REGION
});

module.exports = class {


    /**
     * Upload medical file for specific user. Return json string that contain the bucket and the file name
     * @param {string} file 
     * @param {string} userId 
     * @param {string} field 
     * @returns {string}
     */
    async uploadMedicalData(file, userId, field) {

        const bucket = 'livmeds-medical-data';
        const path = '/userId/'.replace('userId', userId);
        const extension = mime.extension(file.mimetype);

        let key = path + field + '-' + uuidv4() + "." + extension;

        const paramsUpload = {
            Bucket: bucket,
            Key: key,
            Body: file.buffer,
            ACL: 'public-read'
        }

        try {
            const uploadResponse = await s3.putObject(paramsUpload).promise();
            console.log(uploadResponse);
            return JSON.stringify({ bucket: bucket, key: key });
        } catch (error) {
            console.log(error)
            throw error;
        }
    }

    async uploadOrder(file, deliveryId) {

        const bucket = 'livmeds-order';
        const path = '/deliveryId/'.replace('deliveryId', deliveryId);
        const extension = mime.extension(file.mimetype);

        let key = path + 'order-' + uuidv4() + "." + extension;

        const paramsUpload = {
            Bucket: bucket,
            Key: key,
            Body: file.buffer,
            ACL: 'public-read'
        }

        try {
            const uploadResponse = await s3.putObject(paramsUpload).promise();
            console.log(uploadResponse);
            return JSON.stringify({ bucket: bucket, key: key });
        } catch (error) {
            console.log(error)
            throw error;
        }
    }

    async deletes(bucket, keys) {
        var deleteParam = {
            Bucket: bucket,
            Delete: {
                Objects: keys
            }
        };

        try {
            const deleteResponse = await s3.deleteObjects(deleteParam).promise();
            console.log(deleteResponse);
        } catch (error) {
            console.log(error)
            throw error;
        }
    }

    getObject(bucket, key) {
        try {
            return new Promise((resolve, reject) => {
                let pathToSave = './tmp/test.png';
                let tempFile = fs.createWriteStream(pathToSave);
                let stream = s3.getObject({ Bucket: bucket, Key: key }).createReadStream().pipe(tempFile);
                let had_error = false;
                stream.on('error', function (err) {
                    console.log(err)
                    had_error = true;
                });
                stream.on('close', function () {
                    if (!had_error) {
                        resolve(pathToSave);
                    }
                });
            });
        } catch (error) {
            console.log(error)
            throw error;
        }
    }

    formatInternalFormatUrlToFormatAws(url) {
        return {
            Bucket: url.bucket,
            Key: url.key
        };
    }
}