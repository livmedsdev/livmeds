const models = require("../models");
const googleMapsClient = require('@google/maps').createClient({
  key: process.env.GOOGLE_MAPS_KEY,
  Promise: Promise
});

const TypeDeliverer = require('../enums/TypeDeliverer');
const SizePackage = require('../enums/SizePackage');
const moment = require('moment');
const UserStatus = require("../enums/UserStatus");

module.exports = async (delivery) => {
  var hrstart = process.hrtime()
  try {
    const maxNbKm = Number.parseInt(process.env.MAX_NB_KM) || 30;
    let filter = { role: 'ROLE_DELIVERER', online: true, status: UserStatus.ACTIVE, lastDevice: { $exists: true, $ne: null } };
    if (delivery.isBtoB && delivery.sizePackage === SizePackage["50X50"]) {
      const type = await models.TypeDeliverer.findOne({ label: TypeDeliverer.CARGO_BIKE }).exec();
      filter.typeDeliverer = type._id;
    } else if (delivery.isBtoB && delivery.sizePackage === SizePackage.SMALL) {
      const types = await models.TypeDeliverer.find({ label: { $in: [TypeDeliverer.BIKE, TypeDeliverer.ELECT_SCOOTER, TypeDeliverer.TRUCK] } }).exec();
      let typesDeliverer = [];
      for (const type of types) {
        typesDeliverer.push(type._id);
      }
      filter.typeDeliverer = { $in: typesDeliverer };
    }

    var hrstartMerchant = process.hrtime()
    // Find merchant
    const merchant = await models.User.findById(delivery.merchant)
      .populate('addresses')
      .exec();

    let merchantAddress = merchant.addresses[0];
    for (let address of merchant.addresses) {
      if (address.favoriteAddress) {
        merchantAddress = address;
      }
    }
    var hrendMerchant = process.hrtime(hrstartMerchant)

    var hrstartPositions = process.hrtime();
    let minDate = moment(new Date()).subtract({ days: 1 }).toDate();
    const positions = await models.Position.find({
      geo: {
        $nearSphere: {
          $geometry: { type: "Point", coordinates: [merchantAddress.longitude, merchantAddress.latitude] },
          $maxDistance: maxNbKm * 1000
        }
      },
      insertAt: {$gt: minDate}
    })
    .lean()
    .exec();

    console.log("NB POSITIONS")
    console.log(positions.length)
    var hrendPosition = process.hrtime(hrstartPositions)


    var hrstartVerify = process.hrtime()
    let notFarDeliverer = null;
    for (let i = 0; i < positions.length && notFarDeliverer == null; i++) {
      const position = positions[i];

      filter.$and = [{ _id: { $nin: delivery.historyDeliverer } }, { _id: position.deliverer }];

      // Find deliverer
      const deliverer = await models.User
        .findOne(filter, '-password -address')
        .populate('typeDeliverer')
        .exec();

      if (deliverer) {
        const deliveriesInDelivery = await models.Delivery
          .find({ $and: [{ 'deliverer': deliverer._id }, { status: { $in: ['VALID', 'IN_DELIVERY'] } }] })
          .exec();
        console.log(deliveriesInDelivery)
        if (deliveriesInDelivery.length === 0) {
          notFarDeliverer = deliverer;
        }
      }
    }
    var hrendVerify = process.hrtime(hrstartVerify)

    hrend = process.hrtime(hrstart)

    console.info('Execution time Merchant (hr): %ds %dms', hrendMerchant[0], hrendMerchant[1] / 1000000)
    console.info('Execution time Positions (hr): %ds %dms', hrendPosition[0], hrendPosition[1] / 1000000)
    console.info('Execution time Verify (hr): %ds %dms', hrendVerify[0], hrendVerify[1] / 1000000)

    console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000)
    return notFarDeliverer;
  } catch (error) {
    console.log("ERROR DURING PROCESS findDeliverer: " + error)
  }
}