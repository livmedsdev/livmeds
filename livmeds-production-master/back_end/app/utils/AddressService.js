const models = require("../models");
const { Client } = require("@googlemaps/google-maps-services-js");

module.exports = class {

  constructor(models) {
    this.models = models;
  }

  async getAddressDetails(place_id) {
    const placeDetails = await client.placeDetails({
      params: {
        place_id: place_id,
        fields: ['name', 'rating', 'address_component', 'formatted_address', 'geometry'],
        key: process.env.GOOGLE_MAPS_KEY,
      }
    })
    if (!placeDetails.data.result) {
      throw "No places found"
    }

    return this.toInternalAddress(placeDetails.data.result)
  }

  async getAddress(address) {
    const client = new Client({});

    try {
      const place = await client.placeQueryAutocomplete({
        params: {
          input: address,
          key: process.env.GOOGLE_MAPS_KEY,
        }
      })

      if (place.data.predictions.length < 0) {
        throw "No places found"
      }

      return place.data.predictions;
    } catch (error) {
      console.log(error)
      throw error
    }
  }

  toInternalAddress(address) {
    return {
      latitude: address.geometry.location.lat,
      longitude: address.geometry.location.lng,
      fullAddress: address.formatted_address,
      address: this.getInformationInAddressComponent("street_number", address.address_components) + ' ' +
        this.getInformationInAddressComponent("route", address.address_components),
      country: this.getInformationInAddressComponent("country", address.address_components),
      postalCode: this.getInformationInAddressComponent("postal_code", address.address_components),
      city: this.getInformationInAddressComponent("locality", address.address_components),
    }
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  async checkIfAddressAlreadyExistForUser(fullAddress, user) {
    const addressFound = await this.models.Address.find({
      _id: { $in: user.addresses },
      fullAddress: fullAddress
    }).lean().exec();

    if (addressFound.length === 0) {
      return null;
    } else {
      return addressFound[0];
    }
  }

  async createAddressIfNotExist(address, usr) {
    if (typeof address !== 'string') {
      const found = await this.checkIfAddressAlreadyExistForUser(address.fullAddress, usr);
      if (found === null) {
        // Create address
        const addressObj = new this.models.Address(address);
        addressObj.favoriteAddress = false;
        address = await addressObj.save();

        const user = await this.models.User.findOne({ _id: usr._id }).exec();
        user.addresses.push(address);
        await user.save();

        return address;
      } else {
        return found;
      }
    } else {
      return await models.Address.findOne({ _id: address }).exec();
    }
  }

  getFavoriteAddress(addresses) {
    for (let address of addresses) {
      if (address.favoriteAddress) {
        return address;
      }
    }
    return null;
  }
}