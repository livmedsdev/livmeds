const fetch = require('node-fetch');

module.exports = class {

    async createUser(user, withAuthToken) {
        let name = user.lastName + ' ' + user.name;
        if (["ROLE_OPTICIAN", "ROLE_PHARMACY", "ROLE_VETERINARY"].includes(user.role)) {
            name = user.name;
        }
        const url = 'https://api-eu.cometchat.io/v2.0/users';
        const options = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                "appId": process.env.TCHAT_APP_ID,
                "apiKey": process.env.TCHAT_API_KEY
            },
            body: JSON.stringify({
                uid: user._id,
                name: name,
                role: user.role,
                metadata: {
                    isProdData: process.env.NODE_ENV === 'PROD'
                },
                withAuthToken: withAuthToken,
                tags: [process.env.NODE_ENV]
            })
        };

        const res = await fetch(url, options);
        const response = await res.json();
        console.log(response);
        if (response.error) {
            throw response.error;
        }
        return response.data.authToken;
    }

    async getUser(userId) {
        const url = `https://api-eu.cometchat.io/v2.0/users/${userId}`;
        const options = {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                "appId": process.env.TCHAT_APP_ID,
                "apiKey": process.env.TCHAT_API_KEY
            }
        };

        try {
            const res = await fetch(url, options);
            const response = await res.json();
            console.log(response)
            return response.data;
        } catch (error) {
            console.log(error)
            return null;
        }
    }

    async getAuthToken(userId) {
        const url = `https://api-eu.cometchat.io/v2.0/users/${userId}/auth_tokens`;
        const options = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                "appId": process.env.TCHAT_APP_ID,
                "apiKey": process.env.TCHAT_API_KEY
            },
            body: JSON.stringify({ force: false })
        };

        const res = await fetch(url, options);
        const response = await res.json();
        return response.data.authToken;
    }

    async getAuthTokenData(userId, authToken) {
        const url = `https://api-eu.cometchat.io/v2.0/users/${userId}/auth_tokens/${authToken}`;
        const options = {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                "appId": process.env.TCHAT_APP_ID,
                "apiKey": process.env.TCHAT_API_KEY
            }
        };

        const res = await fetch(url, options);
        const response = await res.json();

        if (response.error) {
            throw response.error;
        }

        return response.data;
    }
}