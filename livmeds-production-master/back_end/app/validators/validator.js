const { body, validationResult } = require('express-validator');
const TranslateCode = require('../enums/TranslateCode');

const userValidationRules = () => {
  return [
    // username must be an email
    body('mail').optional(),
    // password must be at least 5 chars long
    body('password').optional(),
    body('role').exists(),
    body('connectionType').optional(),
    body('socialToken').optional()
  ]
}

const userSecurityValidationRules = () => {
  return [
    // username must be an email
    body('mail').isEmail(),
    // password must be at least 5 chars long
    body('currentPassword').optional(),
    body('newPassword').optional()
  ]
}

const userDetailsValidationRules = () => {
  return [
    // username must be an email
    body('mail').isEmail(),
    body('name').exists(),
    body('lastName').optional(),
    // password must be at least 5 chars long
    body('password').optional(),
    body('role').exists()
  ]
}

const userChangeStatusRules = () => {
  return [
    body('status').exists() 
  ]
}

const validate = (req, res, next) => {
  const errors = validationResult(req)
  if (errors.isEmpty()) {
    return next()
  }
  const extractedErrors = []
  errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

  return res.status(422).json({
    //errors: extractedErrors,
    tradCode: TranslateCode.INVALID_PARAMS,
    message: 'Invalid parameters'
  })
}

module.exports = {
  userValidationRules,
  validate,
  userDetailsValidationRules,
  userChangeStatusRules,
  userSecurityValidationRules
}