const express = require('express');
const DeliveryController = require("./controllers/DeliveryController");
const UserController = require("./controllers/UserController");
const BlogController = require("./controllers/BlogController");
const BonusController = require("./controllers/BonusController");
const PromoCodeController = require("./controllers/PromoCodeController");
const ProductController = require("./controllers/ProductController");
const ConfigController = require("./controllers/ConfigController");
const ContactController = require("./controllers/ContactController");
const SupportController = require("./controllers/SupportController");
const StatController = require("./controllers/StatController");
const PartnerController = require("./controllers/PartnerController");
const CardController = require("./controllers/CardController");
const router = express.Router();
const Auth = require('./middlewares/authentification');
const Multer  = require('multer');
const upload = Multer({storage: Multer.memoryStorage()});
const asyncHandler = require('express-async-handler')

module.exports = () => {
    let blogController = new BlogController();
    let bonusController = new BonusController();
    let deliveryController = new DeliveryController();
    const promoCodeController = new PromoCodeController();
    const productController = new ProductController();
    const configController = new ConfigController();
    let contactController = new ContactController();
    const supportController = new SupportController();
    const statController = new StatController();
    let userController = new UserController();
    let partnerController = new PartnerController();
    let cardController = new CardController();
    let auth = new Auth(['ROLE_ADMIN']);

    router.get('/deliveries', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.getAllDeliveries));
    router.put('/delivery/deliverer/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.updateDelivererByAdmin));
    router.put('/delivery/status/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.changeStatus));
    router.put('/delivery/status-delivered/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.changeStatusToDelivered));


    router.get('/deliverers', auth.getMiddleware.bind(auth), asyncHandler(userController.listUsersWithFilter));

    // Blog admin endpoint
    router.get('/categories', auth.getMiddleware.bind(auth), asyncHandler(blogController.getCategories));
    router.get('/topics', auth.getMiddleware.bind(auth), asyncHandler(blogController.getTopics));
    router.get('/topic/:id', auth.getMiddleware.bind(auth), asyncHandler(blogController.getTopic));
    router.put('/topic/:id', auth.getMiddleware.bind(auth), asyncHandler(blogController.editTopic));
    router.delete('/topic/:id', auth.getMiddleware.bind(auth), asyncHandler(blogController.deleteTopic));
    router.get('/messages/:id', auth.getMiddleware.bind(auth), asyncHandler(blogController.getMessages));
    router.delete('/message/:id', auth.getMiddleware.bind(auth), asyncHandler(blogController.deleteMessage));
    router.post('/topic', auth.getMiddleware.bind(auth), asyncHandler(blogController.createTopic));
    router.post('/message', auth.getMiddleware.bind(auth), asyncHandler(blogController.createMessage));
    router.get('/blog/stat', auth.getMiddleware.bind(auth), asyncHandler(blogController.getStat));

    // Bonus admin endpoint
    router.get('/prime/:id', auth.getMiddleware.bind(auth), asyncHandler(bonusController.getBonus));
    router.get('/primes', auth.getMiddleware.bind(auth), asyncHandler(bonusController.getPrimes));
    router.post('/prime', auth.getMiddleware.bind(auth), asyncHandler(bonusController.createBonus));
    router.put('/prime/:id', auth.getMiddleware.bind(auth), asyncHandler(bonusController.editBonus));
    router.put('/prime/status/:id', auth.getMiddleware.bind(auth), asyncHandler(bonusController.updateStatus));
    router.delete('/prime/:id', auth.getMiddleware.bind(auth), asyncHandler(bonusController.deleteBonus));

    // Promo Code
    router.get('/promoCode/:id', auth.getMiddleware.bind(auth), asyncHandler(promoCodeController.getPromoCode));
    router.get('/promoCodes', auth.getMiddleware.bind(auth), asyncHandler(promoCodeController.getPromoCodes));
    router.post('/promoCode', auth.getMiddleware.bind(auth), asyncHandler(promoCodeController.createPromoCode));
    router.put('/promoCode/:id', auth.getMiddleware.bind(auth), asyncHandler(promoCodeController.editPromoCode));
    router.put('/promoCode/enabled/:id', auth.getMiddleware.bind(auth), asyncHandler(promoCodeController.updateStatus));
    router.delete('/promoCode/:id', auth.getMiddleware.bind(auth), asyncHandler(promoCodeController.delete));

    // Products
    router.get('/product/:id', auth.getMiddleware.bind(auth), asyncHandler(productController.getProduct));
    router.get('/products', auth.getMiddleware.bind(auth), asyncHandler(productController.getProducts));
    router.post('/product', auth.getMiddleware.bind(auth), asyncHandler(productController.create));
    router.put('/product/:id', auth.getMiddleware.bind(auth), asyncHandler(productController.edit));
    router.put('/product/status/:id', auth.getMiddleware.bind(auth), asyncHandler(productController.updateStatus));

    // Config
    router.get('/config', auth.getMiddleware.bind(auth), asyncHandler(configController.get));
    router.put('/config', auth.getMiddleware.bind(auth), asyncHandler(configController.edit));

    // Contact
    router.get('/contacts', auth.getMiddleware.bind(auth), asyncHandler(contactController.list));
    router.get('/contact/:id', auth.getMiddleware.bind(auth), asyncHandler(contactController.get));
    router.delete('/contact/:id', auth.getMiddleware.bind(auth), asyncHandler(contactController.delete));
    router.put('/contact/status/:id', auth.getMiddleware.bind(auth), asyncHandler(contactController.updateStatus));

    // Delivery
    router.put('/delivery/cancel/:id', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.cancelDelivery.bind(deliveryController)));

    // Support
    router.get('/support-tickets', auth.getMiddleware.bind(auth), asyncHandler(supportController.list));
    router.get('/support-tickets-ticketstact/:id', auth.getMiddleware.bind(auth), asyncHandler(supportController.get));
    router.post('/support-tickets/:id', auth.getMiddleware.bind(auth), asyncHandler(supportController.answer));
    router.put('/support-tickets/status/:id', auth.getMiddleware.bind(auth), asyncHandler(supportController.updateStatus));
    router.delete('/support-tickets/:id', auth.getMiddleware.bind(auth), asyncHandler(supportController.delete));

    // Stats
    router.get('/stats', auth.getMiddleware.bind(auth), asyncHandler(statController.get));

    // User
    router.get('/users', auth.getMiddleware.bind(auth), asyncHandler(userController.listUsers));
    router.get('/user/:id', auth.getMiddleware.bind(auth), asyncHandler(userController.getUser));
    router.post('/user', upload.fields([{ name: 'socialSecurityNumber', maxCount: 1 }, { name: 'mutual', maxCount: 1 }]),auth.getMiddleware.bind(auth), asyncHandler(userController.register.bind(userController)));
    router.put('/user/:id', upload.fields([{ name: 'socialSecurityNumber', maxCount: 1 }, { name: 'mutual', maxCount: 1 }]), auth.getMiddleware.bind(auth), asyncHandler(userController.editUser.bind(userController)));
    router.put('/openingTimes/:id', auth.getMiddleware.bind(auth), asyncHandler(userController.updateOpeningTimes.bind(userController)));
    router.put('/avatar/:id', auth.getMiddleware.bind(auth), asyncHandler(userController.changeAvatarByAdmin));
    router.get('/contract/:id', auth.getMiddleware.bind(auth), asyncHandler(userController.generateContractByUserId));
    router.put('/unlock/:id', auth.getMiddleware.bind(auth), asyncHandler(userController.unlock.bind(userController)));

    router.get('/bills', auth.getMiddleware.bind(auth), asyncHandler(deliveryController.listDelivered));

    // Partner
    router.get('/partners', auth.getMiddleware.bind(auth), asyncHandler(partnerController.list));
    router.post('/partner', auth.getMiddleware.bind(auth), asyncHandler(partnerController.add));
    router.get('/partner/:id', auth.getMiddleware.bind(auth), asyncHandler(partnerController.get));
    router.put('/partner/:id', auth.getMiddleware.bind(auth), asyncHandler(partnerController.update));
    router.put('/partner-key/:id', auth.getMiddleware.bind(auth), asyncHandler(partnerController.generateSecretKey));
    router.put('/partner-status/:id', auth.getMiddleware.bind(auth), asyncHandler(partnerController.updateStatus));

    // Payment
    router.post('/bank-account/:id', upload.fields([{ name: 'personDocFront', maxCount: 1 }, { name: 'personDocBack', maxCount: 1 },{ name: 'companyDocFront', maxCount: 1 }, { name: 'companyDocBack', maxCount: 1 }]), auth.getMiddleware.bind(auth), asyncHandler(cardController.addBankAccountByAdmin));
    router.put('/bank-account/:id', upload.fields([{ name: 'personDocFront', maxCount: 1 }, { name: 'personDocBack', maxCount: 1 },{ name: 'companyDocFront', maxCount: 1 }, { name: 'companyDocBack', maxCount: 1 }]), auth.getMiddleware.bind(auth), asyncHandler(cardController.editBankAccountByAdmin));
    router.post('/transfer/:id', auth.getMiddleware.bind(auth), asyncHandler(cardController.transfer));
    router.get('/bank-account/:id', auth.getMiddleware.bind(auth), asyncHandler(cardController.getConnectedAccountByAdmin));
    
    return router;
};