const TranslateCode = require('../enums/TranslateCode');
const UserRole = require('../enums/UserRole');
const uuidv4 = require('uuid/v4');
const filterKeys = require('../utils/filterKeys');
const mime = require('mime-types');
const AWS = require('aws-sdk');
const models = require("../models");
const sharp = require('sharp');

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_S3_REGION
});

const buckets = {
    'livmeds-public': {
        types: ['avatar'],
        path: '/',
        enableResize: true,
        resizeParameters: {
            width: 200,
            height: 200,
            fit: sharp.fit.cover
        }
    },
    'livmeds-order': {
        types: ['order', 'tchat', 'receipt'],
        path: '/deliveryId/'
    },
    'livmeds-documents': {
        types: ['document'],
        path: '/userId/'
    },
    'livmeds-medical-data': {
        types: [],
        path: '/userId/'
    }
}

// class qui gere transfer file sur aws s3
module.exports = class {
// depause file sur s3
    async upload(req, res) {
        const type = req.params.type;
        const params = filterKeys(req.query, ['deliveryId']);
        const file = req.file;

        let bucketPath;
        for (let bucket of Object.keys(buckets)) {
            const value = buckets[bucket];
            if (value.types.includes(type)) {
                bucketPath = bucket;
                break;
            }
        }
        if (!bucketPath) {
            res.status(400).send({ tradCode: TranslateCode.NOT_AUTHORIZED })
            return;
        }

        let key = buckets[bucketPath].path;
        for (let param of Object.keys(params)) {
            key = key.replace(param, params[param]);
        }
        key = key.replace('userId', req.currentUser._id);
        const extension = mime.extension(file.mimetype);
        key += type + '-' + uuidv4() + "." + extension;

        let resizedBuffer = file.buffer;
        if (buckets[bucketPath].enableResize) {
            resizedBuffer = await sharp(file.buffer)
                .resize(buckets[bucketPath].resizeParameters).toBuffer();
        }

        const paramsUpload = {
            Bucket: bucketPath,
            Key: key,
            Body: resizedBuffer,
            ContentType: file.mimetype,
            ACL: 'private'
        }

        if (bucketPath === 'livmeds-public') {
            paramsUpload.ACL = 'public-read';
        }

        try {
            const uploadResponse = await s3.putObject(paramsUpload).promise();
            console.log(uploadResponse);
            res.json({ error_code: 0, err_desc: null, idFile: JSON.stringify({ bucket: bucketPath, key: key }) });
        } catch (error) {
            console.log(error);
            res.status(500).send({ tradCode: TranslateCode.UPLOAD_FAILED })
        }
    }
// cree un url et nom file
    async file(req, res) {
        const params = filterKeys(req.body, ['bucket', 'key']);
        try {
            await this.checkPermission(params, req.currentUser);

            const paramsUrl = { Bucket: params.bucket, Key: params.key, Expires: 604800 };

            if (params.bucket === 'livmeds-public') {
                let url = 'https://livmeds-public.s3.eu-west-3.amazonaws.com/' + params.key;
                return res.status(200).send({ url: url });
            } else {
                const response = await new Promise((resolve, reject) => {
                    s3.getSignedUrl('getObject', paramsUrl, (err, url) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(url);
                        }
                    });
                });
                return res.status(200).send({ url: response });
            }
        } catch (error) {
            console.log(error)
            res.status(400).send({ tradCode: TranslateCode.NOT_AUTHORIZED })
            return;
        }
    }
// telecharger fichier sur s3
    async downloadAs(req, res) {
        const params = filterKeys(req.body, ['bucket', 'key']);
        try {
            await this.checkPermission(params, req.currentUser);

            const paramsUrl = { Bucket: params.bucket, Key: params.key };

            await new Promise((resolve, reject) => {
                s3.getObject(paramsUrl, (err, url) => {
                    if (err) {
                        reject(err)
                    } else {
                        res.attachment(params.key);
                        s3.getObject(paramsUrl)
                            .createReadStream()
                            .pipe(res);
                        resolve();
                    }
                });
            });
        } catch (error) {
            console.log(error)
            res.status(400).send({ tradCode: TranslateCode.NOT_AUTHORIZED })
            return;
        }
    }
// verifier les permision role utilisateur
    async checkPermission(params, currentUser) {
        switch (params.bucket) {
            case 'livmeds-documents':
                if (currentUser === undefined) {
                    throw TranslateCode.NOT_AUTHORIZED;
                } else if (currentUser && currentUser.role !== UserRole.ROLE_ADMIN) {
                    const expectedKey = buckets['livmeds-documents'].path.replace('userId', currentUser._id);
                    if (!params.key.startsWith(expectedKey)) {
                        throw TranslateCode.NOT_AUTHORIZED;
                    }
                }
                break;
            case 'livmeds-medical-data':
                if (currentUser === undefined) {
                    throw TranslateCode.NOT_AUTHORIZED;
                } else if (currentUser) {
                    if ([UserRole.ROLE_CUSTOMER, UserRole.ROLE_DOCTOR, UserRole.ROLE_NURSE].includes(currentUser.role)) {
                        const expectedKey = buckets['livmeds-medical-data'].path.replace('userId', currentUser._id);
                        if (!params.key.startsWith(expectedKey)) {
                            throw TranslateCode.NOT_AUTHORIZED;
                        }
                    } else if (![UserRole.ROLE_VETERINARY, UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY].includes(currentUser.role)) {
                        throw TranslateCode.NOT_AUTHORIZED;
                    }
                } else {
                    throw TranslateCode.NOT_AUTHORIZED;
                }
                break;
            case 'livmeds-order':
                if (currentUser === undefined) {
                    throw TranslateCode.NOT_AUTHORIZED;
                } else if (currentUser && currentUser.role !== UserRole.ROLE_ADMIN) {
                    // if order
                    if (params.key.includes('order-')) {
                        switch (currentUser.role) {
                            case UserRole.ROLE_CUSTOMER:
                                // Check if customer is the customer
                                const countNbDeliveryCustomer = await models.Delivery.count({
                                    image: JSON.stringify(params),
                                    customer: currentUser._id
                                }).exec();

                                if (countNbDeliveryCustomer === 0) {
                                    throw TranslateCode.NOT_AUTHORIZED;
                                }
                                break;
                            case UserRole.ROLE_VETERINARY:
                            case UserRole.ROLE_PHARMACY:
                            case UserRole.ROLE_OPTICIAN:
                                // Check if pharmacy is the shop
                                const countNbDeliveryMerchant = await models.Delivery.count({
                                    image: JSON.stringify(params),
                                    merchant: currentUser._id
                                }).exec();

                                if (countNbDeliveryMerchant === 0) {
                                    throw TranslateCode.NOT_AUTHORIZED;
                                }
                                break;
                            default:
                                throw TranslateCode.NOT_AUTHORIZED;
                        }
                    }

                    // if receipt
                    if (params.key.includes('receipt-')) {
                        switch (currentUser.role) {
                            case UserRole.ROLE_VETERINARY:
                            case UserRole.ROLE_PHARMACY:
                            case UserRole.ROLE_OPTICIAN:
                                // Check if pharmacy are the shop
                                const count = await models.Delivery.count({
                                    tagLine: JSON.stringify(params),
                                    merchant: currentUser._id
                                }).exec();

                                if (count === 0) {
                                    throw TranslateCode.NOT_AUTHORIZED;
                                }
                                break;
                            default:
                                throw TranslateCode.NOT_AUTHORIZED;
                        }
                    }
                }
            default:
                break;
        }
    }
}
