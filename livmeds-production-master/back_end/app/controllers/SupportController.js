const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypeBonus = require('../enums/TypeBonus');
const BonusStatus = require('../enums/BonusStatus');
const MailService = require('../utils/MailService');
const moment = require('moment');
const momentTimezone = require('moment-timezone');
require('moment/locale/fr');

const mailService = new MailService();

module.exports = class {
// la liste des element 
    async list(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;

        const filter = {}

        const messages = await models.Support.find(filter)
            .populate('delivery', '_id reference')
            .populate('user', '_id lastName name mail')
            .skip(page * limit)
            .limit(limit)
            .sort({ insertAt: 'desc' })
            .exec();

        const count = await models.Support.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: messages.length,
            data: messages
        });
        return;
    }

// recuper les elements de la DB
    async get(req, res) {
        const idMessage = req.params.id;
        const message = await models.Support.findOne({ _id: idMessage })
            .exec();
        if (message) {
            res.status(200).send(message);
        } else {
            res.status(404).send({ tradCode: TranslateCode.SUPPORT_NOT_FOUND });
        }
        return;
    }
// cree un element de la DB
    async create(req, res) {
        const params = filterKeys(req.body, ['subject', 'message', 'delivery']);
        const messageObj = new models.Support(params);
        messageObj.user = req.currentUser._id;

        const messageCreated = await messageObj.save();

        if (process.env.NODE_ENV === 'PROD') {
            try {
                const delivery = await models.Delivery.findOne({ _id: params.delivery }, 'reference').exec();
                const date = moment(new Date());
                date.tz('Europe/Paris');
                const topicStaff = "Nouveau ticket d'intervention";
                const htmlStaff = await mailService.getEmailHtml('app/resources/email/notif-new-support-ticket-admin.html', {
                    date: date.format('DD/MM/YYYY HH:mm'),
                    reference: delivery.reference,
                    subject: messageCreated.subject,
                    message: messageCreated.message,
                });

                mailService.sendMail(process.env.NOTIFICATION_MAIL_ADMIN, topicStaff, htmlStaff);

            } catch (error) {
                console.log('ERROR TO SEND MAIL: ' + error)
            }
        }

        return res.status(200).send({ data: messageCreated, tradCode: TranslateCode.MESSAGE_CREATED_SUCCESS });
    }
// modifier un element de la DB
    async updateStatus(req, res) {
        const params = filterKeys(req.body, ['status']);
        const id = req.params.id;
        const supportTicket = await models.Support.findById(id).exec();
        if (supportTicket) {
            await models.Support.updateOne({ _id: supportTicket._id }, params).exec();
            res.status(200).send();
        } else {
            res.status(404).send({ tradCode: TranslateCode.SUPPORT_NOT_FOUND });
            return;
        }
        // send email
    }
// repondre a un sujet et meesage
    async answer(req, res) {
        const params = filterKeys(req.body, ['subject', 'message']);
        const id = req.params.id;
        const supportTicket = await models.Support.findById(id).exec();
        if (supportTicket) {

        } else {
            res.status(404).send({ tradCode: TranslateCode.SUPPORT_NOT_FOUND });
            return;
        }
        // send email
        return;
    }
// supprimer un element avec un id message
    async delete(req, res) {
        const idMessage = req.params.id;
        const message = await models.Support.findById(idMessage).exec();
        if (message) {
            await message.remove();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.SUPPORT_NOT_FOUND });
            return;
        }
    }
// recuper le ticket d un element 
    async getMySupportTicket(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;

        const filter = { delivery: req.query.delivery, user: req.currentUser._id };

        const messages = await models.Support.find(filter)
            .populate('delivery', '_id reference')
            .skip(page * limit)
            .limit(limit)
            .exec();

        const count = await models.Support.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: messages.length,
            data: messages
        });
        return;
    }
}
