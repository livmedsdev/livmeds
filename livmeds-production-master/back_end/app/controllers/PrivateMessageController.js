const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypePrivateMessage = require('../enums/TypePrivateMessage');
const NotificationService = require('../utils/NotificationService');

module.exports = class {

    // recuper les message prive
    async getPrivateMessages(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;
        const idDelivery = req.params.idDelivery;
        const typePrivateMessage = req.query.typePrivateMessage;
        const filter = {delivery: idDelivery, typePrivateMessage: typePrivateMessage};
        const privateMessages = await models.PrivateMessageDelivery.find(filter)
            .populate('author', 'name lastName role avatar')
            .sort({insertAt: 'asc'})
            .exec();

        const count = await models.PrivateMessageDelivery.countDocuments(filter).exec();
        
        res.status(200).send({
            total: count,
            pageSize: privateMessages.length,
            data: privateMessages
        });
        return;
    }
// cree un message prive
    async createPrivateMessage(req, res) {
        const params = filterKeys(req.body, ['delivery', 'message', 'typePrivateMessage', 'audioFile']);
        const delivery = await models.Delivery.findOne({_id: params.delivery})
        .populate('merchant', '_id')
        .populate('customer', '_id')
        .populate('deliverer', '_id')
        .exec();
        
        if (delivery) {
            const privateMessageObj = new models.PrivateMessageDelivery(params);
            privateMessageObj.author = req.currentUser._id;
            const privateMessageCreated = await privateMessageObj.save();

            const notificationService = new NotificationService();

            const data =  {
              notificationType: 'NEW_MESSAGES',
              delivery: delivery._id,
              typePrivateMessage: params.typePrivateMessage,
            };
            if(params.typePrivateMessage === TypePrivateMessage.TO_PHARMACY){
              console.log("to pharma")
              if(delivery.customer._id.toString() === req.currentUser._id.toString()){
                console.log('to merchant')
                data.receiver = delivery.merchant._id;
                await notificationService.notify(delivery.merchant._id, 'Nouveau message', privateMessageCreated.message, data);
              }else if(delivery.merchant._id.toString() === req.currentUser._id.toString()){
                console.log('to customer')
                data.receiver = delivery.customer._id;
                await notificationService.notify(delivery.customer._id, 'Nouveau message', privateMessageCreated.message, data);
              }
            }else if(params.typePrivateMessage === TypePrivateMessage.TO_DELIVERER){
              console.log("to deliverer")
              if(delivery.customer._id.toString() === req.currentUser._id.toString()){
                console.log("to deliverer")
                data.receiver = delivery.deliverer._id;
                await notificationService.notify(delivery.deliverer._id, 'Nouveau message', privateMessageCreated.message, data);
              }else if(delivery.deliverer._id.toString() === req.currentUser._id.toString()){
                console.log("to customer")
                data.receiver = delivery.customer._id;
                await notificationService.notify(delivery.customer._id, 'Nouveau message', privateMessageCreated.message, data);
              }
            }

            res.status(200).send(privateMessageCreated);
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
            return;
        }
    }
}
