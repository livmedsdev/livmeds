const models = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const TranslateCode = require('../enums/TranslateCode');
const UserStatus = require('../enums/UserStatus');
const filterKeys = require('../utils/filterKeys');
const checkMandatoryParameters = require('../utils/checkMandatoryParameters');
const UserRole = require('../enums/UserRole');


const ContractService = require('../utils/ContractService');
const MailService = require('../utils/MailService');
const uuidv4 = require('uuid/v4');
const YouSign = require('../utils/YouSign');
const FileService = require('../utils/FileService');
const FacebookAuth = require('../utils/FacebookAuth');
const AppleAuth = require('../utils/AppleAuth');
const ConnectionType = require('../enums/ConnectionType');
const moment = require('moment');
const PromoCodeStatus = require('../enums/PromoCodeStatus');
const voucher_codes = require('voucher-code-generator');
const PromoCodeType = require('../enums/PromoCodeType');

const FormatService = require('../utils/FormatService');
const formatService = new FormatService();

const PaymentService = require('../utils/PaymentService');
const paymentService = new PaymentService();

const AddressService = require('../utils/AddressService');
const addressService = new AddressService();
// la class qui gere les utilisateur et autentification

module.exports = class {
// se connecter 
    async login(req, res) {
        const role = req.body.role;
        let mail = (req.body.mail) ? req.body.mail.toLowerCase() : '';
        const password = req.body.password;
        const connectionType = req.body.connectionType || ConnectionType.MAIL;
        const socialToken = req.body.socialToken || '';
        const deviceUuid = req.headers.device_uuid || undefined;

        switch (connectionType) {
            case ConnectionType.FACEBOOK:
                try {
                    console.log(socialToken);
                    const userId = await FacebookAuth.getUser(socialToken);
                    const user = await models.User.findOne({ userIdSocial: userId, connectionType }, "-password")
                        .exec();
                    if (user !== null) {
                        const token = jwt.sign({ id: user._id, role: user.role }, process.env.SECRET_JWT, {
                            expiresIn: process.env.JWT_EXPIRATION_TIME // expires in 24 hours
                        });
                        if (deviceUuid) {
                            const currentDate = moment(new Date());
                            if (!user.lastDevice || deviceUuid === user.lastDevice
                                || currentDate.diff(moment(user.lastLogin), 'seconds') >= process.env.JWT_EXPIRATION_TIME) {
                                await models.User.updateOne({ _id: user._id }, {
                                    lastLogin: new Date(),
                                    lastConnection: new Date(),
                                    lastDevice: deviceUuid
                                }).exec();
                            } else {
                                return res.status(400).send({ auth: false, tradCode: TranslateCode.ALREADY_CONNECTED });
                            }
                        }
                        return res.status(200).send({ auth: true, token: token });
                    } else {
                        return res.status(404).send({ auth: false, tradCode: TranslateCode.USER_NOT_EXIST });
                    }
                } catch (error) {
                    console.log(error);
                    return res.status(400).send({ auth: false, tradCode: TranslateCode.INVALID_PARAMS });
                }
            case ConnectionType.APPLE:
                try {
                    const userId = await AppleAuth.getUser(socialToken);
                    const user = await models.User.findOne({ userIdSocial: userId, connectionType }, "-password")
                        .exec();
                    if (user !== null) {
                        const token = jwt.sign({ id: user._id, role: user.role }, process.env.SECRET_JWT, {
                            expiresIn: process.env.JWT_EXPIRATION_TIME // expires in 24 hours
                        });
                        if (deviceUuid) {
                            const currentDate = moment(new Date());
                            if (!user.lastDevice || deviceUuid === user.lastDevice
                                || currentDate.diff(moment(user.lastLogin), 'seconds') >= process.env.JWT_EXPIRATION_TIME) {
                                await models.User.updateOne({ _id: user._id }, {
                                    lastLogin: new Date(),
                                    lastConnection: new Date(),
                                    lastDevice: deviceUuid
                                }).exec();
                            } else {
                                return res.status(400).send({ auth: false, tradCode: TranslateCode.ALREADY_CONNECTED });
                            }
                        }
                        res.status(200).send({ auth: true, token: token });
                        return
                    } else {
                        res.status(404).send({ auth: false, tradCode: TranslateCode.USER_NOT_EXIST });
                        return
                    }
                } catch (error) {
                    console.log(error);
                    return res.status(404).send({ auth: false, tradCode: TranslateCode.INVALID_PARAMS });
                }
                break;
            case ConnectionType.MAIL:
                mail = mail.trim();
                models.User.findOne({ mail: mail, role: { $in: role } }).exec(async (err, user) => {
                    if (user !== null) {

                        // Check userIdSocial and ConnectionType
                        switch (user.connectionType) {
                            case ConnectionType.FACEBOOK:
                                return res.status(400).send({ auth: false, tradCode: TranslateCode.MAIL_USED_WITH_FACEBOOK })
                            case ConnectionType.APPLE:
                                return res.status(400).send({ auth: false, tradCode: TranslateCode.MAIL_USED_WITH_APPLE })
                        }

                        if (user.status === UserStatus.DELETED) {
                            res.status(403).send({ auth: false, tradCode: TranslateCode.ACCOUNT_DELETED });
                            return;
                        } else if (user.status === UserStatus.BLOCKED) {
                            res.status(403).send({ auth: false, tradCode: TranslateCode.ACCOUNT_BLOCKED });
                            return;
                        }

                        const valid = await bcrypt.compare(password, user.password);
                        if (valid) {
                            const token = jwt.sign({ id: user._id, role: user.role }, process.env.SECRET_JWT, {
                                expiresIn: process.env.JWT_EXPIRATION_TIME // expires in 24 hours
                            });
                            if (deviceUuid) {
                                const currentDate = moment(new Date());
                                if (!user.lastDevice || deviceUuid === user.lastDevice
                                    || currentDate.diff(moment(user.lastLogin), 'seconds') >= process.env.JWT_EXPIRATION_TIME) {
                                    await models.User.updateOne({ _id: user._id }, {
                                        lastLogin: new Date(),
                                        lastConnection: new Date(),
                                        lastDevice: deviceUuid
                                    }).exec();
                                } else {
                                    return res.status(400).send({ auth: false, tradCode: TranslateCode.ALREADY_CONNECTED });
                                }
                            }
                            return res.status(200).send({ auth: true, token: token });
                        } else {
                            return res.status(400).send({ auth: false, message: "Invalid password", tradCode: TranslateCode.INVALID_PASSWORD });
                        }
                    } else {
                        return res.status(404).send({ auth: false, message: "User not exist", tradCode: TranslateCode.USER_NOT_EXIST });
                    }
                });
                break;
        }
    }
// s enregister sur la db
    async register(req, res) {
        const deviceUuid = req.headers.device_uuid || undefined;
        let userWillBeCreated;
        try {
            userWillBeCreated = this.filterUserInput(req.body, false, req.currentUser);
            userWillBeCreated.mail = userWillBeCreated.mail.toLowerCase();
            userWillBeCreated.connectionType = userWillBeCreated.connectionType || ConnectionType.MAIL;

            if (userWillBeCreated.referralCodeReferrer && userWillBeCreated.referralCodeReferrer !== 'null') {
                // check exist
                const referrer = await models.User.findOne({ referralCode: userWillBeCreated.referralCodeReferrer }, '_id');
                if (!referrer) {
                    throw TranslateCode.INVALID_REFERRAL_CODE;
                }
                userWillBeCreated.referrer = referrer._id;
            }

            switch (userWillBeCreated.connectionType) {
                case ConnectionType.FACEBOOK:
                    if (userWillBeCreated.userIdFb && userWillBeCreated.accessTokenFb) {
                        const userId = await FacebookAuth.getUser(userWillBeCreated.accessTokenFb)
                        if (userId !== userWillBeCreated.userIdFb) {
                            throw TranslateCode.INVALID_PARAMS;
                        }

                        userWillBeCreated.password = "PASSWORD_NOT_USED";
                        userWillBeCreated.userIdSocial = userId;
                    }
                    break;
                case ConnectionType.APPLE:
                    const userId = await AppleAuth.getUser(userWillBeCreated.identityToken);
                    userWillBeCreated.password = "PASSWORD_NOT_USED";
                    userWillBeCreated.userIdSocial = userId;
                    console.log(userId);
                    break;
                case ConnectionType.MAIL:
                    break;
                default:
                    throw TranslateCode.INVALID_PARAMS;
            }


        } catch (error) {
            console.log(error);
            res.status(400).send({ tradCode: error });
            return;
        }

        if (userWillBeCreated.role === 'ROLE_ADMIN' && (!req.currentUser || req.currentUser.role !== UserRole.ROLE_ADMIN)) {
            res.status(400).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
            return;
        }

        models.User.findOne({ mail: userWillBeCreated.mail }).exec(async (err, user) => {
            if (user === null) {
                console.log(userWillBeCreated);
                let userObj = new models.User(userWillBeCreated);
                if (userWillBeCreated.favoriteAddress) {
                    const addressObj = new models.Address(userWillBeCreated.favoriteAddress);
                    addressObj.label = 'Main';
                    addressObj.favoriteAddress = true;
                    addressObj.geo = { type: 'Point', coordinates: [userWillBeCreated.favoriteAddress.longitude, userWillBeCreated.favoriteAddress.latitude] }
                    const address = await addressObj.save();
                    userObj.addresses = [address];

                    if([UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY].includes(req.body.role)){
                        userObj.location = { type: 'Point', coordinates: [userWillBeCreated.favoriteAddress.longitude, userWillBeCreated.favoriteAddress.latitude] }
                    }
                }else{
                    userObj.addresses = [];
                }

                // Create user on mangopay
                const paymentService = new PaymentService();
                const userCreated = await paymentService.createUser();
                userObj.customerIdPayment = (userCreated) ? userCreated.id : null;

                userObj.password = await bcrypt.hash(userObj.password, parseInt(process.env.SALTROUNDS));
                const user = await userObj.save();
                
                let token = jwt.sign({ id: user._id, role: user.role }, process.env.SECRET_JWT, {
                    expiresIn: process.env.JWT_EXPIRATION_TIME
                });

                if (user.role === UserRole.ROLE_CUSTOMER) {
                    try {
                        // Upload images
                        const fileService = new FileService();
                        if (req.files) {
                            if (req.files.socialSecurityNumber) {
                                const image = await fileService.uploadMedicalData(req.files.socialSecurityNumber[0], user._id, 'socialSecurityNumber');
                                await models.User.updateOne({ _id: user._id }, {
                                    socialSecurityNumber: image
                                }).exec();
                            }

                            if (req.files.mutual) {
                                const image = await fileService.uploadMedicalData(req.files.mutual[0], user._id, 'mutual');
                                await models.User.updateOne({ _id: user._id }, {
                                    mutual: image
                                }).exec();
                            }
                        }
                    } catch (error) {
                        console.log(error)
                        await models.User.deleteOne({ _id: user._id }).exec();
                        res.status(400).send({ tradCode: error });
                        return
                    }
                }

                if (deviceUuid) {
                    await models.User.updateOne({ _id: user._id }, {
                        lastLogin: new Date(),
                        lastConnection: new Date(),
                        lastDevice: deviceUuid
                    }).exec();
                }

                if(userObj.addresses.length > 0){
                    await paymentService.updateUser(user, userObj.addresses[0]);
                }else{
                    await paymentService.updateUserWithoutAddr(user);
                }

                const mailService = new MailService();
                let topic = "Bienvenue sur votre application Livmed’s.";
                let html = await mailService.getEmailHtml('app/resources/email/welcome.html', {
                    title: '« Plus besoin de déplacements on vous livre vos médicaments. »',
                    footer: 'L’équipe Lived’s est heureuse de vous compter parmi nous.'
                });

                switch (userWillBeCreated.role) {
                    case UserRole.ROLE_OPTICIAN:
                    case UserRole.ROLE_PHARMACY:
                    case UserRole.ROLE_VETERINARY:
                        topic = 'Bienvenue cher partenaire';
                        html = await mailService.getEmailHtml('app/resources/email/welcome-pro.html', {});
                        break;
                    case UserRole.ROLE_DELIVERER:
                        topic = "Votre inscription sur LIVMED’S";
                        html = await mailService.getEmailHtml('app/resources/email/welcome-deliverer.html', {});
                        break;
                }
                mailService.sendMail(userWillBeCreated.mail, topic, html);

                if (user.referrer) {
                    const codePromo = await this.generateCodePromo(userWillBeCreated.referrer);
                    if (codePromo) {
                        const referralPromoTopic = "Votre code promotion de parrainage";
                        const referralHtml = await mailService.getEmailHtml('app/resources/email/referral-code.html', {
                            codePromo
                        });
                        mailService.sendMail(userWillBeCreated.mail, referralPromoTopic, referralHtml);
                    }
                }

                res.status(200).send({ auth: true, token: token, message: "User created with success" });
            } else {
                res.status(400).send({ tradCode: TranslateCode.MAIL_ALREADY_USED });
            }
        });
    }
// se deconnecter
    async logout(req, res) {
        await models.User.updateOne({ _id: req.currentUser._id }, {
            lastLogin: null,
            lastConnection: null,
            lastDevice: null,
            tokenFcm: null,
            online: false
        }).exec();
        return res.status(200).send({ valid: true, tradCode: TranslateCode.LOGOUT_SUCESSFULL });
    }
// verifier le token jwt par utilisateur
    async checkToken(req, res) {
        let token = req.headers.authorization;
        let socialToken = req.headers.social_token || 'false';
        const deviceUuid = req.headers.device_uuid || undefined;
        if (token) {
            token = token.replace(/^Bearer /, "");
            try {
                const decoded = jwt.verify(token, process.env.SECRET_JWT, { ignoreExpiration: false });
                models.User.findById(decoded.id, "-password")
                    .exec(async (err, user) => {
                        if (user !== null) {
                            if (deviceUuid) {
                                if (user.lastDevice === null) {
                                    return res.status(400).send({ valid: false });
                                }
                            }

                            req.currentUser = user;
                            return res.status(200).send({ valid: true, user });
                        } else {
                            return res.status(400).send({ valid: false });
                        }
                    })
            } catch (err) {
                return res.status(400).send({ valid: false, message: "Token expired" });
            }
        } else {
            return res.status(401).send({ valid: false, token: "No token provided." });
        }
    }
// profil utilisateur
    async profile(req, res) {
        const reponse = await models.User.findOne({ _id: req.currentUser._id }, "-password")
            .populate('addresses')
            .populate('openingTime')
            .exec();
        return res.status(200).send({ user: reponse });
    }
// liste drs utilisateur recuper les sur db
    async listUsers(req, res) {
        const role = req.query.role;
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 5;
        const text = req.query.text || undefined;
        const filter = { role: role };

        // Aggregation for city
        const match = [{role: role}];
        if (text) {
            // filter.$text = { $search: text }
            match.push({
                $or: [
                    {
                        $and: [
                            { "linkAddresses.city": { $regex: text, $options: "i" } },
                            { "linkAddresses.favoriteAddress": true }
                        ]
                    },
                    {
                        $and: [
                            { "linkAddresses.postalCode": { $regex: text, $options: "i" } },
                            { "linkAddresses.favoriteAddress": true }
                        ]
                    },
                    { name: { $regex: text, $options: "i" } },
                    { lastName: { $regex: text, $options: "i" } },
                    { mail: { $regex: text, $options: "i" } },
                    { responsibleName: { $regex: text, $options: "i" } },
                    { responsibleLastname: { $regex: text, $options: "i" } },
                ]
            })
        }


        const responseAggregation = await models.User.aggregate(
            [
                { $lookup: { from: "addresses", localField: "addresses", foreignField: "_id", as: "linkAddresses" } },
                { $unwind: "$linkAddresses" },
                {
                    $match: {
                        $and: match
                    }
                },
                { $group: { _id: "$_id", linkedAddresses: { $push: "$linkAddresses" } } },
                { $project: { _id: "$_id" } }],
        )
            .exec();


        filter._id = { $in: responseAggregation.filter((elem) => elem._id) }
        const response = await models.User.find(filter,
            '-password -tokenResetPassword')
            .populate('addresses')
            .populate('typeDeliverer')
            .populate('referrer', 'name lastName')
            .populate('openingTime')
            .sort({ insertAt: 'desc' })
            .skip(page * limit)
            .limit(limit)
            .exec();

        // if (text) {
        //     const tmp = [];
        //     for (let attr of ['name', 'lastName', 'mail', 'responsibleName', 'responsibleLastname']) {
        //         let sub = {};
        //         sub[attr] = { '$regex': text, $options: 'i' };
        //         tmp.push(sub)
        //     }
        //     filter['$or'] = tmp;
        // }

        // const response = await models.User.find(filter, "-password")
        //     .populate('addresses')
        //     .populate('typeDeliverer')
        //     .populate('referrer', 'name lastName')
        //     .populate('openingTime')
        //     .sort({ insertAt: 'desc' })
        //     .skip(page * limit)
        //     .limit(limit)
        //     .exec();

        const count = await models.User.countDocuments(filter).exec();

        return res.status(200).send({
            total: count,
            pageSize: response.length,
            data: response
        });
    }
// recuper un utilisateur par id
    async getUser(req, res) {
        const reponse = await models.User.findOne({ _id: req.params.id }, "-password")
            .populate("addresses").exec();
        return res.status(200).send(reponse);
    }
// recuper l adress
    async getAddresses(req, res) {

        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;
        const text = req.query.text;

        let filter = {}

        const response = await models.User.findById(req.currentUser._id, "-password")
            .populate('addresses')
            .skip(page * limit)
            .limit(limit)
            .exec();

        const count = await models.Customer.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: response.addresses.length,
            data: response.addresses
        });
        return;
    }

// recuper tous les adresse mail
    async getAllAddresses(req, res) {
        const response = await models.User.findById(req.currentUser._id, "-password")
            .populate('addresses')
            .exec();

        res.status(200).send(response.addresses);
        return;
    }
// ajouter une adress mail
    async addAddress(req, res) {
        const params = filterKeys(req.body, ['label', 'fullAddress', 'address', 'city', 'postalCode', 'country', 'latitude', 'longitude', 'favoriteAddress', 'doorbell', 'floor', 'doorCode', 'compAddress'])
        try {
            const addressObj = new models.Address(params);
            addressObj.favoriteAddress = false;
            addressObj.geo = { type: 'Point', coordinates: [params.longitude, params.latitude] }
            const address = await addressObj.save();
            const user = await models.User.findOne({ _id: req.currentUser._id }).exec();
            user.addresses.push(address);
            await user.save();
            return res.status(200).send(address);
        } catch (error) {
            return res.status(500).send({ message: 'Internal error' });
        }
    }
// modifier un utilisateur avec son id
    async editUser(req, res) {
        const id = req.params.id;
        let userWillBeCreated;
        try {
            userWillBeCreated = this.filterUserInput(req.body, true);
        } catch (error) {
            console.log(error);
            res.status(400).send({ tradCode: error });
            return;
        }

        delete userWillBeCreated.status;

        models.User.findOne({ _id: { $ne: id }, mail: userWillBeCreated.mail }).exec(async (err, user) => {
            if (user === null) {
                const userExist = await models.User.findOne({ _id: id }).exec();
                if (userWillBeCreated.password && userWillBeCreated.password !== ''
                    && userExist.connectionType === ConnectionType.MAIL) {
                    userWillBeCreated.password = await bcrypt.hash(userWillBeCreated.password, parseInt(process.env.SALTROUNDS));
                } else {
                    delete userWillBeCreated.password;
                }

                if (userWillBeCreated.favoriteAddress) {
                    await models.Address.updateMany({
                        _id: { $in: userExist.addresses }
                    }, { favoriteAddress: false }).exec();

                    const addressObj = new models.Address(userWillBeCreated.favoriteAddress);
                    addressObj.label = 'Main';
                    addressObj.favoriteAddress = true;
                    addressObj.geo = { type: 'Point', coordinates: [userWillBeCreated.favoriteAddress.longitude, userWillBeCreated.favoriteAddress.latitude] }
                    const address = await addressObj.save();
                    userWillBeCreated.addresses = userExist.addresses;
                    userWillBeCreated.addresses.push(address);

                    if([UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY].includes(userExist.role)){
                        userWillBeCreated.location = { type: 'Point', coordinates: [address.longitude, address.latitude] }
                    }
                } 
                
                if (userWillBeCreated.compAddress) {
                    const response = await models.Address.updateOne(
                        { _id: { $in: userExist.addresses }, favoriteAddress: true },
                        {
                            compAddress: userWillBeCreated.compAddress
                        }
                    ).exec();
                }


                if (req.currentUser.role === UserRole.ROLE_CUSTOMER) {
                    try {
                        // Upload images
                        const fileService = new FileService();
                        if (!userWillBeCreated.socialSecurityNumber) {
                            const image = await fileService.uploadMedicalData(req.files.socialSecurityNumber[0], req.currentUser._id, 'socialSecurityNumber');
                            userWillBeCreated.socialSecurityNumber = image;
                        }

                        if (!userWillBeCreated.mutual) {
                            const image = await fileService.uploadMedicalData(req.files.mutual[0], req.currentUser._id, 'mutual');
                            userWillBeCreated.mutual = image;
                        }
                    } catch (error) {
                        console.log(error)
                        res.status(400).send({ tradCode: error });
                        return
                    }
                }

                const response = await models.User.updateOne(
                    { _id: id },
                    userWillBeCreated
                ).exec();

                const userAfterUpdate = await models.User.findById(id)
                .populate('addresses')
                .lean()
                .exec();
                await paymentService.updateUser(userAfterUpdate, addressService.getFavoriteAddress(userAfterUpdate.addresses))

                return res.status(200).send({ tradCode: TranslateCode.SUCCESS_EDIT_USER });
            } else {
                return res.status(400).send({ tradCode: TranslateCode.MAIL_ALREADY_USED });
            }
        });
    }

    async updateStatus(req, res) {
        const id = req.params.id;
        const status = req.body.status;
        if (req.currentUser.id === id) {
            res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
            return;
        }
        let user = await models.User.findOne({ _id: id }).exec();
        if (user.status === UserStatus.DELETED) {
            res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
            return;
        }
        await models.User.updateOne(
            { _id: id },
            { status: status }
        ).exec();
        res.status(200).send({ tradCode: TranslateCode.SUCCESS_STATUS_CHANGE });
        return;
    }

    filterUserInput(body, admin = false, currentUser = undefined) {
        let userWillBeCreated;
        switch (body.role) {
            case UserRole.ROLE_ADMIN:
                userWillBeCreated = filterKeys(body, ['mail', 'name', 'lastName', 'password', 'phoneNumber']);
            case UserRole.ROLE_CUSTOMER:
                userWillBeCreated = filterKeys(body, ['socialSecurityNumber', 'mutual', 'mail', 'name', 'lastName', 'favoriteAddress', 'password', 'phoneNumber', 'userIdFb', 'accessTokenFb', 'connectionType', 'identityToken', 'authorizationCode', 'compAddress', 'referralCodeReferrer']);
                if (!admin) {
                    userWillBeCreated.favoriteAddress = (userWillBeCreated.favoriteAddress) ? JSON.parse(userWillBeCreated.favoriteAddress) : undefined;
                    if (userWillBeCreated.compAddress && userWillBeCreated.favoriteAddress) {
                        userWillBeCreated.favoriteAddress.compAddress = userWillBeCreated.compAddress;
                    }
                } else {
                    userWillBeCreated.favoriteAddress = (userWillBeCreated.favoriteAddress) ? JSON.parse(userWillBeCreated.favoriteAddress) : undefined;
                    if (userWillBeCreated.compAddress && userWillBeCreated.favoriteAddress) {
                        userWillBeCreated.favoriteAddress.compAddress = userWillBeCreated.compAddress;
                    }
                }
                break;
            case UserRole.ROLE_DELIVERER:
                userWillBeCreated = filterKeys(body, ['mail', 'name', 'lastName', 'favoriteAddress', 'password', 'phoneNumber', 'siret', 'typeDeliverer']);
                if (!admin) {
                    userWillBeCreated.status = UserStatus.AWAITING_VALIDATION;
                    userWillBeCreated.favoriteAddress = (userWillBeCreated.favoriteAddress) ? JSON.parse(userWillBeCreated.favoriteAddress) : undefined;
                }
                break;
            case UserRole.ROLE_NURSE:
            case UserRole.ROLE_DOCTOR:
                userWillBeCreated = filterKeys(body, ['mail', 'name', 'lastName', 'password', 'favoriteAddress', 'phoneNumber', 'siret']);
                if (!admin) {
                    userWillBeCreated.favoriteAddress = (userWillBeCreated.favoriteAddress) ? JSON.parse(userWillBeCreated.favoriteAddress) : undefined;
                    userWillBeCreated.status = UserStatus.AWAITING_VALIDATION;
                } else {
                    userWillBeCreated.favoriteAddress = (userWillBeCreated.favoriteAddress) ? JSON.parse(userWillBeCreated.favoriteAddress) : undefined;
                }
                break;
            case UserRole.ROLE_VETERINARY:
            case UserRole.ROLE_OPTICIAN:
            case UserRole.ROLE_PHARMACY:
                userWillBeCreated = filterKeys(body, ['mail', 'name', 'password', 'favoriteAddress', 'phoneNumber', 'siret', 'responsibleLastname', 'responsibleName']);
                if (!admin && (!currentUser || currentUser.role !== UserRole.ROLE_ADMIN)) {
                    userWillBeCreated.status = UserStatus.AWAITING_VALIDATION;
                } else {
                    userWillBeCreated.status = UserStatus.AWAITING_VALIDATION;
                    userWillBeCreated.favoriteAddress = (userWillBeCreated.favoriteAddress) ? JSON.parse(userWillBeCreated.favoriteAddress) : undefined;
                }
                break;
            default:
                throw TranslateCode.ROLE_NOT_SUPPORTED;
        }
        userWillBeCreated.role = body.role;
        userWillBeCreated.mail = formatService.formatString(userWillBeCreated.mail);
        userWillBeCreated.name = formatService.formatString(userWillBeCreated.name);
        userWillBeCreated.name = formatService.firstCharUpperCase(userWillBeCreated.name);
        if (userWillBeCreated.lastName) {
            userWillBeCreated.lastName = formatService.formatString(userWillBeCreated.lastName);
            userWillBeCreated.lastName = formatService.firstCharUpperCase(userWillBeCreated.lastName);
        }

        return userWillBeCreated;
    }
// ajouter notification du token
    async addNotificationToken(req, res) {
        const notificationToken = await models.NotificationToken.find({ token: req.body.notificationToken, user: req.currentUser._id });
        if (notificationToken === null) {
            const notifTokenObj = new models.NotificationToken({
                token: req.body.notificationToken,
                user: req.currentUser._id
            });
            await notifTokenObj.save();
            res.status(200).send({ tradCode: TranslateCode.NOTIFICATION_TOKEN_CREATED });
        } else {
            res.status(200).send({ tradCode: TranslateCode.NOTIFICATION_TOKEN_ALREADY_EXIST });
        }
        return;
    }
// modifer la securite des information des utilisateur
    async editSecurityInformation(req, res) {
        const params = filterKeys(req.body, ['mail', 'currentPassword', 'newPassword']);
        params.mail = params.mail.toLowerCase();
        const userWithThisMail = await models.User.findOne({ _id: { $ne: req.currentUser._id }, mail: params.mail }).exec();
        if (userWithThisMail === null) {
            const user = await models.User.findOne({ _id: req.currentUser._id }).exec();
            const updatedFields = { mail: params.mail };
            if (params.currentPassword && params.newPassword) {
                const valid = await bcrypt.compare(params.currentPassword, user.password);
                if (valid && userWithThisMail.connectionType === ConnectionType.MAIL) {
                    updatedFields.password = await bcrypt.hash(params.newPassword, parseInt(process.env.SALTROUNDS));
                } else {
                    res.status(400).send({ tradCode: TranslateCode.CURRENT_PASSWORD_INVALID });
                    return;
                }
            }

            const response = await models.User.updateOne(
                { _id: req.currentUser._id },
                updatedFields
            ).exec();
            console.log(response)

            res.status(200).send({ tradCode: TranslateCode.MAJ_SECURITY_PROFILE_SUCCESS });
            return;
        } else {
            res.status(400).send({ tradCode: TranslateCode.MAIL_ALREADY_USED });
            return;
        }
    }
// pour chaque utilisateur avec son id modifer son profile
    async editMyProfile(req, res) {
        const params = this.filterUserMyProfile(req.currentUser, req.body);

        if (req.currentUser.role === UserRole.ROLE_CUSTOMER) {
            try {
                // Upload images
                const fileService = new FileService();
                if (!params.socialSecurityNumber && req.files && req.files.socialSecurityNumber) {
                    const image = await fileService.uploadMedicalData(req.files.socialSecurityNumber[0], req.currentUser._id, 'socialSecurityNumber');
                    params.socialSecurityNumber = image;
                } else {
                    delete params.socialSecurityNumber;
                }

                if (!params.mutual && req.files && req.files.mutual) {
                    const image = await fileService.uploadMedicalData(req.files.mutual[0], req.currentUser._id, 'mutual');
                    params.mutual = image;
                } else {
                    delete params.mutual;
                }
            } catch (error) {
                console.log(error)
                res.status(400).send({ tradCode: error });
                return
            }
        }

        console.log(params)
        const response = await models.User.updateOne(
            { _id: req.currentUser._id },
            params
        ).exec();
        console.log(response)

        const userAfterUpdate = await models.User.findOne({_id: req.currentUser._id})
        .populate('addresses')
        .lean()
        .exec();
        
        try{
            await paymentService.updateUser(userAfterUpdate, addressService.getFavoriteAddress(userAfterUpdate.addresses))
        }catch(error){
            console.log(error)
        }


        return res.status(200).send({ tradCode: TranslateCode.MAJ_PROFILE_SUCCESS });
    }

    filterUserMyProfile(currentUser, body) {
        let params;
        switch (currentUser.role) {
            case 'ROLE_ADMIN':
                params = filterKeys(body, ['name', 'lastName', 'phoneNumber']);
            case UserRole.ROLE_CUSTOMER:
                params = filterKeys(body, ['socialSecurityNumber', 'mutual', 'mail', 'name', 'lastName', 'phoneNumber']);
                break;
            case UserRole.ROLE_DELIVERER:
                params = filterKeys(body, ['mail', 'name', 'lastName', 'phoneNumber', 'typeDeliverer', 'siret']);
                break;
            case UserRole.ROLE_NURSE:
            case UserRole.ROLE_DOCTOR:
                params = filterKeys(body, ['mail', 'name', 'lastName', 'phoneNumber', 'siret']);
                break;
            case UserRole.ROLE_VETERINARY:
            case UserRole.ROLE_OPTICIAN:
            case UserRole.ROLE_PHARMACY:
                params = filterKeys(body, ['name', 'lastName', 'phoneNumber', 'siret']);
                break;

            default:
                throw TranslateCode.ROLE_NOT_SUPPORTED;
        }

        if (params.mail) {
            params.mail = formatService.formatString(params.mail);
        }
        if (params.name) {
            params.name = formatService.formatString(params.name);
            params.name = formatService.firstCharUpperCase(params.name);
        }
        if (params.lastName) {
            params.lastName = formatService.formatString(params.lastName);
            params.lastName = formatService.firstCharUpperCase(params.lastName);
        }
        return params;
    }

    async changeAvatar(req, res) {
        const response = await models.User.updateOne(
            { _id: req.currentUser._id },
            { avatar: req.body.avatar }
        ).exec();

        res.status(200).send({ tradCode: TranslateCode.MAJ_PROFILE_SUCCESS });
        return;
    }

    async changeAvatarByAdmin(req, res) {
        await models.User.updateOne(
            { _id: req.params.id },
            { avatar: req.body.avatar }
        ).exec();

        return res.status(200).send({ tradCode: TranslateCode.MAJ_PROFILE_SUCCESS });
    }

    async validAccount(req, res) {
        try {
            if (req.currentUser.status !== UserStatus.AWAITING_VALIDATION) {
                throw TranslateCode.ALREADY_VALIDATED;
            }
            const params = this.filterUserDocument(req.currentUser, req.body);
            let mandatoryFieldIsFill;
            switch (req.currentUser.role) {
                case UserRole.ROLE_NURSE:
                    mandatoryFieldIsFill = checkMandatoryParameters(params, ['signImage', 'rib', 'kbis']);
                    break;
                case UserRole.ROLE_DELIVERER:
                    mandatoryFieldIsFill = checkMandatoryParameters(params, ['signImage', 'ciBack', 'ciFront', 'kbis', 'rib']);
                    break;
                case UserRole.ROLE_OPTICIAN:
                case UserRole.ROLE_PHARMACY:
                case UserRole.ROLE_VETERINARY:
                    mandatoryFieldIsFill = checkMandatoryParameters(params, ['signImage', 'rib', 'kbis']);
                    break;
                default:
                    mandatoryFieldIsFill = true;
                    break;
            }
            if (!mandatoryFieldIsFill) {
                throw TranslateCode.MANDATORY_FIELD_IS_MISSING;
            }

            // const bodyAuth = {
            //     "code": params.code,
            //     "signImage": params.signImage
            // }
            // try {
            //     const youSign = new YouSign();
            //     const authentificationSms = await youSign.authenticationSms(req.currentUser.authenticationId, bodyAuth);
            // } catch (error) {
            //     console.log(error);
            //     throw TranslateCode.AUTHENTICATION_SMS_FAILED
            // }


            params.status = UserStatus.ACTIVE;
            const response = await models.User.updateOne(
                { _id: req.currentUser._id },
                params
            ).exec();
            res.status(200).send({ tradCode: TranslateCode.ACCOUNT_ACTIVE })
            return;
        } catch (error) {
            if (error === TranslateCode.ROLE_NOT_SUPPORTED) {
                res.status(403).send({});
                return
            } else {
                res.status(400).send({ tradCode: error });
                return
            }
        }
    }

    async updateDocuments(req, res) {
        try {
            const params = this.filterUserDocument(req.currentUser, req.body);
            let mandatoryFieldIsFill;
            switch (req.currentUser.role) {
                case UserRole.ROLE_NURSE:
                    mandatoryFieldIsFill = checkMandatoryParameters(params, ['rib', 'kbis']);
                    break;
                case UserRole.ROLE_DELIVERER:
                    mandatoryFieldIsFill = checkMandatoryParameters(params, ['ciBack', 'ciFront', 'kbis', 'rib']);
                    break;
                case UserRole.ROLE_OPTICIAN:
                case UserRole.ROLE_PHARMACY:
                case UserRole.ROLE_VETERINARY:
                    mandatoryFieldIsFill = checkMandatoryParameters(params, ['rib', 'kbis']);
                    break;
                default:
                    mandatoryFieldIsFill = true;
                    break;
            }
            if (!mandatoryFieldIsFill) {
                throw TranslateCode.MANDATORY_FIELD_IS_MISSING;
            }

            const response = await models.User.updateOne(
                { _id: req.currentUser._id },
                params
            ).exec();
            res.status(200).send({ tradCode: TranslateCode.SUCCESS_UPDATE_DOCUMENTS })
            return;
        } catch (error) {
            if (error === TranslateCode.ROLE_NOT_SUPPORTED) {
                res.status(403).send({});
                return
            } else {
                res.status(400).send({ tradCode: error });
                return
            }
        }
    }

    filterUserDocument(currentUser, body) {
        let params;
        switch (currentUser.role) {
            case UserRole.ROLE_DELIVERER:
                params = filterKeys(body, ['code', 'signImage', 'ciBack', 'ciFront', 'kbis', 'rib', 'criminalRecord']);
                break;
            case UserRole.ROLE_NURSE:
                params = filterKeys(body, ['code', 'signImage', 'rib', 'kbis']);
                break;
            case UserRole.ROLE_DOCTOR:
                params = filterKeys(body, []);
                break;
            case UserRole.ROLE_VETERINARY:
            case UserRole.ROLE_OPTICIAN:
            case UserRole.ROLE_PHARMACY:
                params = filterKeys(body, ['rib', 'kbis', 'code', 'signImage']);
                break;
            default:
                throw TranslateCode.ROLE_NOT_SUPPORTED;
        }
        return params;
    }

    async generateContract(req, res) {
        const user = await models.User.findOne({ _id: req.currentUser._id }, "-password")
            .populate("addresses").exec();
        const contractService = new ContractService();
        try {
            const buffer = await contractService.generateContract(user);
            res.set('Content-Type', 'application/pdf')
            res.status(200).send(buffer);
        } catch (error) {
            console.log(error);
            res.status(400).send({ tradCode: error });
            return;
        }
    }

    async generateContractByUserId(req, res) {
        const user = await models.User.findOne({ _id: req.params.id }, "-password")
            .populate("addresses").exec();
        const contractService = new ContractService();
        try {
            const buffer = await contractService.generateContract(user);
            res.set('Content-Type', 'application/pdf')
            res.status(200).send(buffer);
        } catch (error) {
            console.log(error);
            res.status(400).send({ tradCode: error });
            return;
        }
    }


    async lostPassword(req, res) {
        const mail = (req.body.mail) ? req.body.mail.toLowerCase() : undefined;
        if (!mail) {
            return res.status(400).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
        }
        const user = await models.User.findOne({ mail: mail }).exec();
        if (user && !user.userIdSocial && user.connectionType === ConnectionType.MAIL) {
            const token = uuidv4();
            const date = new Date();
            await models.User.updateOne(
                { _id: user._id },
                {
                    tokenResetPassword: token,
                    tokenExpiredDate: new Date(date.setDate(date.getDate() + 1))
                }
            ).exec();

            const url = process.env.DNS_USED + '/reset-password?'
                + 'user=' + user._id
                + '&token=' + token;

            const mailService = new MailService();
            const html = await mailService.getEmailHtml('app/resources/email/simple-template.html', {
                title: 'Bonjour,',
                url: url,
                content: 'Vous avez demandé un changement de mot de passe. Si vous n\'êtes pas à l\'origine de cette demande, veuillez ignorer cet email.',
                btnTitle: 'Changer son mot de passe',
                footer: 'A bientôt !'
            });
            mailService.sendMail(mail, "Changement de mot de passe", html);
            return res.status(200).send({ tradCode: TranslateCode.MAIL_SEND_IF_ACCOUNT_EXIST })
        } else if (user && user.userIdSocial && user.connectionType !== ConnectionType.MAIL) {
            return res.status(403).send({ tradCode: TranslateCode.RESET_MAIL_USE_WITH_SOCIAL_ACCOUNT })
        } else {
            return res.status(200).send({ tradCode: TranslateCode.MAIL_SEND_IF_ACCOUNT_EXIST })
        }
    }

    async changePasswordAfterLostPassword(req, res) {
        const userId = req.body.user;
        const token = req.body.token;
        const password = req.body.password;

        const user = await models.User.findOne({ _id: userId, tokenResetPassword: token }).exec();
        if (user && user.tokenResetPassword) {
            if (user.tokenExpiredDate.getTime() < new Date().getTime()) {
                await models.User.updateOne(
                    { _id: user._id },
                    {
                        tokenResetPassword: null,
                        tokenExpiredDate: null
                    }
                ).exec();
                res.status(403).send({ tradCode: TranslateCode.RESET_PASSWORD_TOKEN_EXPIRED });
                return;
            } else {
                await models.User.updateOne(
                    { _id: user._id },
                    {
                        tokenResetPassword: null,
                        tokenExpiredDate: null,
                        password: await bcrypt.hash(password, parseInt(process.env.SALTROUNDS))
                    }
                ).exec();
                res.status(200).send({ tradCode: TranslateCode.PASSWORD_CHANGED })
            }
        } else {
            res.status(403).send({ tradCode: TranslateCode.RESET_PASSWORD_TOKEN_EXPIRED });
            return;
        }
    }


    async sendSmsToSign(req, res) {

        try {

            if (req.currentUser.status !== UserStatus.AWAITING_VALIDATION) {
                throw TranslateCode.ALREADY_VALIDATED;
            }

            const user = await models.User.findOne({ _id: req.currentUser._id }, "-password")
                .populate("addresses").exec();
            // const contractService = new ContractService();
            // const buffer = await contractService.generateContract(user);
            // const youSign = new YouSign();

            // const fileUpload = await youSign.uploadFile({
            //     name: 'contract',
            //     content: Buffer.from(buffer).toString('base64')
            // });

            // console.log(fileUpload);

            // let page = 4;
            // if (req.currentUser.role === UserRole.ROLE_DELIVERER) {
            //     page = 3;
            // }
            // const roleWithNoLastName = [UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY]
            // let params = {
            //     "name": "Contract",
            //     "description": "contract",
            //     "members": [
            //         {
            //             "firstname": user.name,
            //             "lastname": (roleWithNoLastName.includes(req.currentUser.role)) ? user.name : user.lastName,
            //             "email": user.mail,
            //             "phone": user.phoneNumber,
            //             "fileObjects": [
            //                 {
            //                     "file": fileUpload.id,
            //                     "page": page,
            //                     "position": "50,600,200,750",
            //                     "mention": "Signé et approuvé",
            //                 }
            //             ]
            //         }
            //     ]
            // };
            // const procedure = await youSign.createProcedure(params);
            // if (procedure.title === "An error occurred" && procedure.violations.length > 0) {
            //     throw procedure.violations[0];
            // }
            // params = {
            //     "mode": "sms",
            //     "type": "accept",
            //     "members": [
            //         procedure.members[0].id
            //     ],
            //     "metadata": {
            //     }
            // }

            // console.log(procedure)
            // const auth = await youSign.sendAuthentification(params);
            // console.log(auth)
            // await models.User.updateOne({ _id: req.currentUser._id }, {
            //     authenticationId: auth.authentication.id,
            //     contractFileId: fileUpload.id,
            //     memberYouSign: procedure.members[0].id
            // }).exec();
            res.status(200).send({ tradCode: TranslateCode.SMS_SEND });
        } catch (error) {
            console.log(error);
            if (error.propertyPath && error.propertyPath === "members[0].phone") {
                res.status(400).send({ tradCode: TranslateCode.PHONE_NUMBER_INVALID })
            } else {
                res.status(500).send({ tradCode: TranslateCode.ERROR_SMS_SEND })
            }
        }
    }

    async updateOpeningTimes(req, res) {
        let id = req.currentUser._id;
        if (req.currentUser.role === UserRole.ROLE_ADMIN) {
            id = req.params.id;
        }

        const params = filterKeys(req.body, [
            '0Start',
            '0End',
            '1Start',
            '1End',
            '2Start',
            '2End',
            '3Start',
            '3End',
            '4Start',
            '4End',
            '5Start',
            '5End',
            '6Start',
            '6End']);
        for (let time in params) {
            if (params[time]) {
                let newTime = new Date(params[time]);
                params[time] = newTime
            }
        }
        const openingTimeFound = await models.OpeningTime.findOne({
            merchant: id
        }).exec();
        if (!openingTimeFound) {
            const openingTime = new models.OpeningTime(params);
            openingTime.merchant = id;
            const saved = await openingTime.save();

            await models.User.updateOne({
                _id: id
            }, { openingTime: saved._id }).exec();
        } else {
            await models.OpeningTime.updateOne({
                _id: openingTimeFound._id
            }, params).exec();
        }

        return res.status(200).send({
            data: await models.OpeningTime.findOne({
                merchant: id
            }).exec(), tradCode: TranslateCode.MAJ_OPENING_TIMES_SUCCESS
        })
    }

    async listTypeDeliverer(req, res) {
        res.status(200).send(await models.TypeDeliverer.find({}, '_id label').exec());
    }

    async getProfileFacebook(req, res) {
        try {
            const userInfo = await FacebookAuth.getUserInfo(req.query.accessToken);
            res.status(200).send(userInfo);
        } catch (error) {
            console.log(error)
            res.status(400).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
        }
    }

    async setStatusForDeliverer(req, res) {
        await models.User.updateOne({ _id: req.currentUser._id }, {
            online: req.body.online
        }).exec();

        return res.status(200).send({ tradCode: TranslateCode.SUCCESS_STATUS_CHANGE })
    }

    async addTokenFcm(req, res) {
        await models.User.updateOne({ _id: req.currentUser._id }, {
            tokenFcm: req.body.tokenFcm
        }).exec();

        return res.status(200).send({ tradCode: TranslateCode.NOTIFICATION_TOKEN_ADDED })
    }

    async getMyContract(req, res) {
        const youSign = new YouSign();
        if (!req.currentUser.contractFileId) {
            return res.status(400).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
        }
        try {
            const responseYouSign = await youSign.downloadFile(req.currentUser.memberYouSign);
            return res.status(200).send({ file: responseYouSign });
        } catch (error) {
            console.log(error)
            return res.status(400).send({ tradCode: TranslateCode.DOCUMENT_NOT_FOUND });
        }
    }

    async storeInformationAppleSignIn(req, res) {
        const body = filterKeys(req.body, ['userId', 'data', 'raw']);

        let userInformation = await models.UserInformationThirdSign.findOne({ userId: body.userId });
        if (!userInformation) {
            const userInfoObj = new models.UserInformationThirdSign(body);
            const userInfo = await userInfoObj.save();
            return res.status(200).send(userInfo.data)
        } else {
            let data = userInformation.data;

            if (body.data) {
                if (body.data.mail !== '' && body.data.mail !== data['mail']) {
                    data['mail'] = body.data.mail;
                }

                if (body.data.lastName !== '' && body.data.lastName !== data['lastName']) {
                    data['lastName'] = body.data.lastName;
                }

                if (body.data.name !== '' && body.data.name !== data['name']) {
                    data['name'] = body.data.name;
                }

                await models.UserInformationThirdSign.updateOne({ _id: userInformation }, { data }).exec();

                userInformation = await models.UserInformationThirdSign.findOne({ userId: body.userId });
            }

            return res.status(200).send(userInformation.data)
        }
    }

    async unlock(req, res) {
        await models.User.updateOne({ _id: req.params.id }, {
            lastLogin: null,
            lastConnection: null,
            lastDevice: null,
            tokenFcm: null,
            online: false
        }).exec();

        return res.status(200).send({ tradCode: TranslateCode.SUCCESS_EDIT_USER })
    }

    async generateCodePromo(referrer) {
        try {
            let valid = false;
            const maxTry = 5;
            let nb = 0;
            const config = await models.Config.findOne({ label: 'DEFAULT' }).exec();
            do {
                let code = voucher_codes.generate({
                    length: 8,
                    count: 1,
                    charset: voucher_codes.charset("alphabetic"),
                    prefix: "PROMO-",
                });
                code = code[0].toUpperCase();

                const codePromoExist = await models.PromoCode.countDocuments({ promoCode: code }).exec();
                if (codePromoExist === 0) {
                    valid = true;

                    let codePromoObj = new models.PromoCode({
                        usedTimeMax: 1,
                        usedTimes: 0,
                        promoCode: code,
                        percentage: config.percentReferralCode,
                        status: PromoCodeStatus.ENABLED,
                        type: PromoCodeType.REFERRAL,
                        referrerIs: referrer
                    });

                    await codePromoObj.save();

                    return code;
                }
                nb++;
            } while (!valid && nb < maxTry);

            if (!valid) {
                throw "Impossible to generate an Code Promo";
            }
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    async listUsersWithFilter(req, res) {
        const page = 0; //for next page pass 1 here
        const limit = 5;

        const text = req.query.text || undefined;

        const filter = { role: UserRole.ROLE_DELIVERER };


        if (text) {
            const tmp = [];
            for (let attr of ['name', 'lastName', 'mail']) {
                let sub = {};
                sub[attr] = { '$regex': text, $options: 'i' };
                tmp.push(sub)
            }
            filter['$or'] = tmp;
        }

        let users = models.User.find(filter);

        if (page !== undefined && limit !== undefined) {
            users = await users
                .skip(page * limit)
                .limit(limit)
                .sort({ lastname: 'asc' })
                .exec();
        } else {
            users = await users
                .sort({ lastname: 'asc' })
                .exec();
        }

        const count = await models.User.countDocuments(filter).exec();

        return res.status(200).send({
            total: count,
            pageSize: users.length,
            data: users
        });
    }
}
