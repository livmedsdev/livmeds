const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const ProductStatus = require('../enums/ProductStatus');

module.exports = class {

    // recupere un Produits
    async getProducts(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;

        const filter = {}

        const products = await models.Product.find(filter)
            .skip(page * limit)
            .limit(limit)
            .exec();

        const count = await models.Product.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: products.length,
            data: products
        });
        return;
    }
//recuper tous les produits
    async getAllProducts(req, res) {
        const category = req.query.category || null;
        const text = req.query.text || null;

        const filter = { status: 'ACTIVE' };

        if (text) {
            filter.$or = [
                {
                    label: { $regex: text, $options: "i" }
                }, {
                    description: { $regex: text, $options: "i" }
                }
            ]
        }

        if(category){
            filter.category = req.query.category;
        }

        const products = await models.Product.find(filter)
            .exec();

        const count = await models.Product.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: products.length,
            data: products
        });
        return;
    }

    async getProduct(req, res) {
        const idProduct = req.params.id;
        const product = await models.Product.findOne({ _id: idProduct })
            .exec();
        if (product) {
            res.status(200).send(product);
        } else {
            res.status(404).send({ tradCode: TranslateCode.PRODUCT_NOT_FOUND });
        }
        return;
    }
// cree un produit
    async create(req, res) {
        const params = filterKeys(req.body, ['label', 'description', 'price', 'category', 'image']);
        const productObj = new models.Product(params);
        const productCreated = await productObj.save();
        res.status(200).send(productCreated);
        return;

    }
// modifier un produit
    async edit(req, res) {
        const idProduct = req.params.id;
        const params = filterKeys(req.body, ['label', 'description', 'price', 'category', 'image']);
        const response = await models.Product.updateOne(
            { _id: idProduct },
            params
        ).exec();
        res.status(200).send();
        return;
    }
// modifier le status du produit
    async updateStatus(req, res) {
        const id = req.params.id;
        const status = req.body.status;

        const statusExist = Object.values(ProductStatus).find((elem) => {
            return elem === status
        });
        if (statusExist) {

            if (statusExist === ProductStatus.DELETED) {
                await models.Product.deleteOne({ _id: id });
            } else {
                const response = await models.Product.updateOne(
                    { _id: id },
                    { status: status }
                ).exec();
            }

            res.status(200).send({ tradCode: TranslateCode.SUCCESS_STATUS_CHANGE });
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.PRODUCT_NOT_FOUND });
            return;
        }
    }

}
