var request = require('request');
// classe qui gere pharmancy
module.exports = class {

//recuper une pharmacy de l api
    async getPharmacy(req, res) {

        var options = {
          'method': 'GET',
          'url': 'http://myapi.pharmanity.com/livmeds/pharmacies/',
          'headers': {
            'Authorization': 'Basic bGl2bWVkczpheDRSSlN0R0xY'
          }
        };
          request(options, function (error, response) {
          if (error) throw new Error(error);
           //console.log(response.body);
           var bodyObj = JSON.parse(response.body);

           return res.status(200).send(bodyObj);
        });

    }

// recuper les categorie dans l api
    async getCategories(req, res) {

        var options = {
          'method': 'GET',
          'url': 'https://myapi.pharmanity.com/livmeds/references/',
          'headers': {
            'Authorization': 'Basic bGl2bWVkczpheDRSSlN0R0xY',
          }
        };

          request(options, function (error, response) {
          if (error) throw new Error(error);
           //console.log(response.body);
           var bodyObj = JSON.parse(response.body);


           return res.status(200).send(bodyObj);
        });


    }
    // recuper les produit de l api
        async getProducts(req, res) {
            console.log(req.params);
            var options = {
            'method': 'GET',
            'url': 'https://myapi.pharmanity.com/livmeds/products/?code='+req.params.id+'&type='+req.params.params+'&page='+req.params.page,
            'headers': {
            'Authorization': 'Basic bGl2bWVkczpheDRSSlN0R0xY',
            }
            };

          request(options, function (error, response) {
          if (error) throw new Error(error);
           //console.log(response.body);
           var bodyObj = JSON.parse(response.body);


           return res.status(200).send(bodyObj);
        });


        }
// recuper le produit par pharmacy dans l api
        async getProductsByPharma(req, res) {
            console.log(req.params);
            var options = {
            'method': 'GET',
            'url': 'https://myapi.pharmanity.com/livmeds/products/?code='+req.params.id+'&type='+req.params.params+'&page='+req.params.page+'&ph_id='+req.params.ph_id,
            'headers': {
            'Authorization': 'Basic bGl2bWVkczpheDRSSlN0R0xY',
            }
            };

          request(options, function (error, response) {
          if (error) throw new Error(error);
           //console.log(response.body);
           var bodyObj = JSON.parse(response.body);


           return res.status(200).send(bodyObj);
        });


        }

             async getProductsByName(req, res) {
            console.log(req.params);


            var options = {
              'method': 'GET',
              'url': 'http://myapi.pharmanity.com/livmeds/search/?txt='+req.params.name+'&autocomplete=true',
              'headers': {
                'Authorization': 'Basic bGl2bWVkczpheDRSSlN0R0xY',
              }
            };

          request(options, function (error, response) {
          if (error) throw new Error(error);
           //console.log(response.body);
           var bodyObj = JSON.parse(response.body);

           return res.status(200).send(bodyObj);
        });

        }
        // recuper le produit par id de l api
          async getProductsById(req, res) {
            console.log(req.params);

            var options = {
              'method': 'GET',
              'url': 'http://myapi.pharmanity.com/livmeds/product/?id='+req.params.id,
              'headers': {
                'Authorization': 'Basic bGl2bWVkczpheDRSSlN0R0xY',

              }
            };

          request(options, function (error, response) {
          if (error) throw new Error(error);
           //console.log(response.body);
           var bodyObj = JSON.parse(response.body);

           console.log(bodyObj);

           return res.status(200).send(bodyObj);
        });
        }
}

