const models = require("../models");
const filterKeys = require('../utils/filterKeys');
const findDeliverer = require('../utils/findDeliverer');
const FileService = require('../utils/FileService');
const googleMapsClient = require('@google/maps').createClient({
  key: process.env.GOOGLE_MAPS_KEY,
  Promise: Promise
});
const TranslateCode = require('../enums/TranslateCode');
const DeliveryStatus = require('../enums/DeliveryStatus');
const UserRole = require('../enums/UserRole');
const UserStatus = require('../enums/UserStatus');
const TypeBonus = require('../enums/TypeBonus');
const TypeDeliverer = require('../enums/TypeDeliverer');
const SizePackage = require('../enums/SizePackage');
const admin = require('firebase-admin');
const serviceAccount = require("../../admin-sdk.json");
const PaymentService = require('../utils/PaymentService');
const moment = require('moment');
const momentTimezone = require('moment-timezone');
require('moment/locale/fr');
const NotificationService = require('../utils/NotificationService');
const MailService = require('../utils/MailService');
const AddressService = require('../utils/AddressService');
const PromoCodeStatus = require('../enums/PromoCodeStatus');
const PromoCodeType = require('../enums/PromoCodeType');
const uuidv4 = require('uuid/v4');
const PaymentStatus = require("../enums/PaymentStatus");
const shortid = require('shortid');

const pharmanityController = require('./PharmanityController.js');
const mongoose = require('mongoose');
const maxNbKm = Number.parseInt(process.env.MAX_NB_KM) || 30;
// classe qui gere les commandes
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://delimeds.firebaseio.com"
});

const addressService = new AddressService(models);


module.exports = class {

  //customer
  async create(req, res) {
    const isForCustomer = req.body.isForCustomer;
    const ROLE_BTOB = [UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY, UserRole.ROLE_OPTICIAN];
    const customer = req.currentUser;
    const addressId = req.body.addressId ? JSON.parse(req.body.addressId) : undefined;
    const customerOfMerchant = req.body.customerOfMerchant ? JSON.parse(req.body.customerOfMerchant) : undefined;
    let merchant = req.body.merchant ? JSON.parse(req.body.merchant) : undefined;
    const products = req.body.products ? JSON.parse(req.body.products) : undefined;
    const isScheduled = req.body.isScheduled ? JSON.parse(req.body.isScheduled) : false;
    const deliveryDate = req.body.deliveryDate ? JSON.parse(req.body.deliveryDate) : undefined;
    const phoneNumber = req.body.phoneNumber ? JSON.parse(req.body.phoneNumber) : undefined;
    const cardId = req.body.cardId ? JSON.parse(req.body.cardId) : undefined;
    const codePromo = req.body.codePromo ? JSON.parse(req.body.codePromo) : undefined;


    console.log("----------------body-----------------------------");
    console.log(req.body);
     console.log("-------------------ICI--------------------------");

    if(!merchant._id) {

        let lat =  merchant['lng/10000'];
        let long = merchant['lat/10000'];
        console.log(lat);
        console.log(long);


/*
    const mConnection = await mongoose.connect('mongodb+srv://' + process.env.HOST_DB + '?retryWrites=true&w=majority', configBdd);
     const phDB = await mConnection.connection.db.collection('users').find({'location' : { $near : { 'coordinates' : [5.4508,43.5289]}, $maxDistance : 500}} );

     console.log(phDB);
*/
       //const pharmacyDatabase = await models.User.find({'location' : { $near : { 'coordinates' : [5.4508,43.5289]}, $maxDistance : 10 }} ).exec();


       const pharmacyDatabase = await models.User.find({ 'location': { $near: { $geometry: { type: "Point", coordinates: [5.4508, 43.5289] }, $maxDistance: 500 } } }).exec();
       console.log(pharmacyDatabase);

       merchant = pharmacyDatabase[0];

       console.log(merchant);

        }
     console.log("---------------------------------------------");

    let deliveryObj;

    const productsSelected = [];
    if (req.currentUser.status === UserStatus.AWAITING_VALIDATION) {
      res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
      return;
    }

    try {
      console.log(products)

      if (!ROLE_BTOB.includes(req.currentUser.role)) {

        if (!req.currentUser.socialSecurityNumber && req.files && req.files.length > 0) {
          throw TranslateCode.ERROR_DOC_NOT_SENDED
        }

        let addressFound = await addressService.createAddressIfNotExist(addressId, req.currentUser);
        if (addressFound === null) {
          res.status(404).send({ tradCode: TranslateCode.ADDRESS_NOT_FOUND });
          return;
        }
        for (const product of products) {
          /*
          let productFound = await models.Product.findOne({ _id: product }).exec();
          console.log(productFound);
          console.log(await models.Product.find().exec())
          */
         //console.log("product : "); console.log(product);
          let productFound = product; //  await pharmanityController.getProductsById(product);
          //console.log("product2 : "); console.log(productFound);

          if (productFound === null) {
            res.status(404).send({ tradCode: TranslateCode.PRODUCT_NOT_FOUND });
            return;
          }
          productsSelected.push(product);
        }

        deliveryObj = new models.Delivery({
          customer: customer._id,
          merchant,
          latitude: addressFound.latitude,
          longitude: addressFound.longitude,
          checkDeliverySuccessfullyByDeliverer: false,
          checkDeliverySuccessfullyByCustomer: false,
          address: addressFound,
          productsSelected: productsSelected,
          phoneNumber: phoneNumber,
          isBtoB: false,
          cardId,
          codePromo,
          type: "INTERNAL"
        });

        console.log("ici");
        console.log(deliveryObj);

        if (req.currentUser.role === UserRole.ROLE_DOCTOR || req.currentUser.role === UserRole.ROLE_NURSE) {
          deliveryObj.customerOfMerchant = customerOfMerchant;
        }

        if (!req.currentUser.usedCode) {
          req.currentUser.usedCode = []
        }
        await this.checkCodePromo(codePromo, req.currentUser, req.currentUser.usedCode, [PromoCodeType.REGULAR, PromoCodeType.REFERRAL]);

        if (isScheduled) {
          deliveryObj.status = DeliveryStatus.SCHEDULED
          deliveryObj.deliveryDate = deliveryDate;


          if (!(await this.checkIfMerchantIsOpen(merchant, new Date(deliveryDate)))) {
            throw TranslateCode.MERCHANT_CLOSED;
          }

          if (new Date(deliveryDate).getTime() <= new Date().getTime()) {
            res.status(400).send({ tradCode: TranslateCode.DATE_BEFORE_NOW });
            return;
          }
        } else {
          if (!(await this.checkIfMerchantIsOpen(merchant, new Date()))) {
            throw TranslateCode.MERCHANT_CLOSED;
          }
        }
      } else if (isForCustomer) {
        const params = filterKeys(req.body, ['nameCustomer', 'lastnameCustomer', 'phoneNumberCustomer', 'address', 'products', 'codePromo']);
        params.address = params.address ? JSON.parse(params.address) : undefined;
        params.nameCustomer = params.nameCustomer ? JSON.parse(params.nameCustomer) : undefined;
        params.lastnameCustomer = params.address ? JSON.parse(params.lastnameCustomer) : undefined;
        params.phoneNumberCustomer = params.phoneNumberCustomer ? JSON.parse(params.phoneNumberCustomer) : undefined;
        params.products = params.products ? JSON.parse(params.products) : [];
        params.codePromo = params.codePromo ? JSON.parse(params.codePromo) : undefined;
        let address;
        if (typeof params.address !== 'string') {
          // Create address
          const addressObj = new models.Address(params.address);
          addressObj.favoriteAddress = false;
          addressObj.label = 'btob';
          addressObj.geo = { type: 'Point', coordinates: [params.address.longitude, params.address.latitude] }
          address = await addressObj.save();

          const user = await models.User.findOne({ _id: req.currentUser._id }).exec();
          user.addresses.push(address);
          await user.save();
        } else {
          address = await models.Address.findOne({ _id: params.address }).exec();
        }

        await this.checkCodePromo(codePromo, null, null, [PromoCodeType.REGULAR]);

        deliveryObj = new models.Delivery({
          reference: shortid.generate(),
          nameCustomer: params.nameCustomer,
          lastnameCustomer: params.lastnameCustomer,
          phoneNumberCustomer: params.phoneNumberCustomer,
          merchant: req.currentUser._id,
          latitude: address.latitude,
          longitude: address.longitude,
          checkDeliverySuccessfullyByDeliverer: false,
          checkDeliverySuccessfullyByCustomer: false,
          address: address,
          productsSelected: productsSelected,
          phoneNumber: phoneNumber,
          isBtoB: false,
          isForCustomer: true,
          // status: DeliveryStatus.VALID,
          status: DeliveryStatus.STAND_BY,
          cardId,
          codePromo: params.codePromo,
          type: "INTERNAL"
        });

        // let notFarDeliverer = await findDeliverer(deliveryObj);
        // if (notFarDeliverer) {
        //   deliveryObj.deliverer = notFarDeliverer;
        //   deliveryObj.lastUpdateDeliverer = new Date();
        //   deliveryObj.historyDeliverer.push(notFarDeliverer);
        // } else {
        //   throw TranslateCode.DELIVERER_FOR_DELIVERY_NOT_FOUND;
        // }

        const merchant = await models.User.findById(req.currentUser._id)
          .populate('addresses')
          .exec();
        const data = await this.computeAmount(params, deliveryObj, this.getFavoriteAddress(merchant.addresses));
        const invoiceObj = new models.Invoice({
          amountCustomer: data.amountCustomer,
          amountMerchant: data.amountMerchant,
          feeManagement: data.feeManagement,
          amountDeliverer: data.amountDeliverer,
          platformFee: data.platformFee,
          nightPrimeActive: data.nightPrimeActive,
          rainPrimeActive: data.rainPrimeActive,
          distanceText: data.distanceText,
          distanceInMeter: data.distanceInMeter,
          isBtoB: false,
          products: data.products,
          codePromoInformation: (codePromo ? await models.PromoCode.findOne({ promoCode: codePromo.toUpperCase() }).exec() : undefined),
          amountCustomerBeforeCodePromo: data.amountCustomerBeforeCodePromo,
          promoAmount: data.promoAmount
        });
        const invoice = await invoiceObj.save();
        deliveryObj.invoice = invoice;


        if (deliveryObj.invoice.amountCustomer > parseFloat("0")) {
          const deliveryData = { id: "FOR_CUSTOMER", reference: deliveryObj.reference };
          const transferToMerchantData = {};
          const transferToDelivererData = {};
          const paymentService = new PaymentService();
          deliveryObj.preAuthorizationId = await paymentService.verifyPreAuthorization(deliveryData, deliveryObj.cardId,
            req.currentUser.customerIdPayment, deliveryObj.invoice.amountCustomer)

          try {
            await paymentService.confirmTransaction(deliveryData, deliveryObj.preAuthorizationId, deliveryObj.cardId, transferToMerchantData, transferToDelivererData);
          } catch (error) {
            throw TranslateCode.ERROR_CONFIRM_PAYMENT;
          }
        }

        const dataNotification = {
          notificationType: 'DELIVERY_AVAILABLE'
        }

        const notificationService = new NotificationService();
        // await notificationService.notify(notFarDeliverer._id, 'Nouvelle livraison disponible',
        //   "Une nouvelle demande de livraison est disponible", dataNotification);

      } else {
        const params = filterKeys(req.body, ['nbPackage', 'sizePackage', 'address']);
        params.address = params.address ? JSON.parse(params.address) : undefined;
        let address;
        if (typeof params.address !== 'string') {
          // Create address
          const addressObj = new models.Address(params.address);
          addressObj.favoriteAddress = false;
          addressObj.label = 'btob';
          addressObj.geo = { type: 'Point', coordinates: [params.address.longitude, params.address.latitude] }
          address = await addressObj.save();

          const user = await models.User.findOne({ _id: req.currentUser._id }).exec();
          user.addresses.push(address);
          await user.save();
        } else {
          address = await models.Address.findOne({ _id: params.address }).exec();
        }

        deliveryObj = new models.Delivery({
          reference: shortid.generate(),
          merchant: req.currentUser._id,
          nbPackage: JSON.parse(params.nbPackage),
          sizePackage: JSON.parse(params.sizePackage),
          latitude: address.latitude,
          longitude: address.longitude,
          checkDeliverySuccessfullyByDeliverer: false,
          checkDeliverySuccessfullyByCustomer: false,
          address: address,
          isBtoB: true,
          // status: DeliveryStatus.VALID,
          status: DeliveryStatus.STAND_BY,
          cardId,
          historyDeliverer: [],
          type: "INTERNAL"
        });

        // let notFarDeliverer = await findDeliverer(deliveryObj);
        // if (notFarDeliverer) {
        //   deliveryObj.deliverer = notFarDeliverer;
        //   deliveryObj.lastUpdateDeliverer = new Date();
        //   deliveryObj.historyDeliverer.push(notFarDeliverer);
        // } else {
        //   throw TranslateCode.DELIVERER_FOR_DELIVERY_NOT_FOUND;
        // }

        const merchant = await models.User.findById(req.currentUser._id)
          .populate('addresses')
          .exec();
        const data = await this.computeAmount(params, deliveryObj, this.getFavoriteAddress(merchant.addresses));
        const invoiceObj = new models.Invoice({
          amountCustomer: 0,
          amountMerchant: data.amountMerchant,
          amountDeliverer: data.amountDeliverer,
          platformFee: data.platformFee,
          nightPrimeActive: data.nightPrimeActive,
          rainPrimeActive: data.rainPrimeActive,
          distanceText: data.distanceText,
          distanceInMeter: data.distanceInMeter,
          isBtoB: true
        });
        const invoice = await invoiceObj.save();
        deliveryObj.invoice = invoice;

        if (deliveryObj.invoice.amountMerchant > parseFloat("0")) {
          const deliveryData = { id: "BTOB", reference: deliveryObj.reference };
          const transferToMerchantData = {};
          const transferToDelivererData = {};
          const paymentService = new PaymentService();
          deliveryObj.preAuthorizationId = await paymentService.verifyPreAuthorization(deliveryData, deliveryObj.cardId, req.currentUser.customerIdPayment, deliveryObj.invoice.amountMerchant)

          try {
            await paymentService.confirmTransaction(deliveryData, deliveryObj.preAuthorizationId, deliveryObj.cardId, transferToMerchantData, transferToDelivererData);
          } catch (error) {
            throw TranslateCode.ERROR_CONFIRM_PAYMENT;
          }
        }

        const dataNotification = {
          notificationType: 'DELIVERY_AVAILABLE'
        }

        const notificationService = new NotificationService();
        // await notificationService.notify(notFarDeliverer._id, 'Nouvelle livraison disponible',
        //   "Une nouvelle demande de livraison est disponible", dataNotification);

      }

      const delivery = await deliveryObj.save();

      try {
        let selectedMerchant = undefined;
        if(delivery.merchant){
          selectedMerchant = await models.User.findOne({_id: merchant})
            .exec();
        }

        const mailService = new MailService();
        if (process.env.NODE_ENV === 'PROD') {
          const date = moment(delivery.deliveryDate);
          date.tz('Europe/Paris');
          const topicStaff = "Nouvelle livraison";
          const htmlStaff = await mailService.getEmailHtml('app/resources/email/notif-new-order-admin.html', {
            address: delivery.address.fullAddress,
            date: date.format('DD/MM/YYYY HH:mm'),
            reference: delivery.reference,
            shop: selectedMerchant?selectedMerchant.name:'BtoB'
          });

          mailService.sendMail(process.env.NOTIFICATION_MAIL_ADMIN, topicStaff, htmlStaff);
        }

        if (merchant) {
          const topic = "Nouvelle livraison disponible sur LIVMED’S";
          const html = await mailService.getEmailHtml('app/resources/email/notif-new-order.html', {});

          mailService.sendMail(selectedMerchant.mail, topic, html);
        }
      } catch (error) {
        console.log('ERROR TO SEND MAIL: ' + error)
      }

      // Upload image
      const fileService = new FileService();
      if (req.files && req.files.length > 0) {
        let promises = []
        for (let file of req.files) {
          promises.push(fileService.uploadOrder(file, delivery._id));
        }

        const images = await Promise.all(promises)

        await models.Delivery.updateOne({ _id: delivery._id }, {
          image: images
        }).exec();
      }

      let deliveryResp = await models.Delivery.findOne({ _id: delivery._id })
        .populate('merchant', '-password')
        .populate('address')
        .populate({
          path: 'merchant',
          // Get friends of friends - populate the 'friends' array for every friend
          populate: { path: 'addresses' }
        })
        .exec();

      return res.status(200).send({ delivery: deliveryResp, tradCode: TranslateCode.CREATE_DELIVERY_SUCCESS });
    } catch (error) {
      console.log("GLOBAL ERROR: " + JSON.stringify(error));
      const isTradCode = Object.values(TranslateCode).find((elem) => {
        return elem === error
      });

      if (isTradCode) {
        return res.status(403).send({ tradCode: error });
      } else {
        return res.status(403).send({ tradCode: TranslateCode.ERROR_CREATE_DELIVERY });
      }
    }
  }

  async validAmount(req, res) {
    req.setTimeout(0);
    const taskId = uuidv4();

    // Check if delivery exist
    let delivery = await models.Delivery.findOne({
      _id: req.params.id,
      status: DeliveryStatus.AWAITING_VALIDATION_AMOUNT
    })
      .populate('customer', '-password')
      .populate('merchant', '-password')
      // .populate('deliverer', '-password')
      .populate('invoice')
      .exec();
    if (!delivery) {
      res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
      return;
    }

    const typeOperation = "PAYMENT";
    let operation = await models.OperationStripeInProgress.findOne({
      delivery: req.params.id,
      type: typeOperation,
      status: { $in: [PaymentStatus.IN_PROGRESS, PaymentStatus.SUCCESS] }
    });

    if (operation) {
      console.log(operation)
      return res.status(403).send({ tradCode: "Opération de paiement en cours..." })
    }

    let paymentInProgress = new models.OperationStripeInProgress({
      type: typeOperation,
      delivery: req.params.id,
      insertAt: new Date(),
      taskId: taskId,
      status: PaymentStatus.IN_PROGRESS
    });

    let operationObj = await paymentInProgress.save();

    try {
      // First check
      // let notFarDeliverer = await findDeliverer(delivery);
      // if (notFarDeliverer === null) {
      //   throw TranslateCode.DELIVERER_FOR_DELIVERY_NOT_FOUND;
      // }

      // Send "Accepted wait payment"
      res.status(200).send({ tradCode: TranslateCode.DELIVERY_AMOUNT_VALIDATE_SUCCESS });

      // Continue for payment
      // Payment
      const paymentService = new PaymentService();
      if (delivery.invoice.amountCustomer > parseFloat("0") && !delivery.preAuthorizationId) {
        const deliveryData = { id: delivery._id, reference: delivery.reference };
        const transferToMerchantData = { accountId: delivery.merchant.accountId, amount: delivery.invoice.amountMerchant };
        // const transferToDelivererData = {accountId: delivery.deliverer.accountId, amount: delivery.invoice.amountDeliverer};
        const transferToDelivererData = {};
        try {
          const preAuthorizationId = await paymentService.verifyPreAuthorization(deliveryData, delivery.cardId, req.currentUser.customerIdPayment, delivery.invoice.amountCustomer);
          await paymentService.confirmTransaction(preAuthorizationId, delivery.cardId);

          await models.Delivery
            .updateOne(
              { _id: req.params.id, status: DeliveryStatus.AWAITING_VALIDATION_AMOUNT },
              { preAuthorizationId })
            .exec();
        } catch (error) {
          console.log("Error_PAYMENT: " + error)
          throw TranslateCode.ERROR_CONFIRM_PAYMENT;
        }
      }


      // Second check & assign
      // let notFarDeliverer = await findDeliverer(delivery);
      // if (notFarDeliverer === null) {
      //   throw TranslateCode.DELIVERER_FOR_DELIVERY_NOT_FOUND;
      // }
      // const historyDeliverer = delivery.historyDeliverer;
      // historyDeliverer.push(notFarDeliverer);

      // const updatedParams = {
      //   status: DeliveryStatus.VALID,
      //   deliverer: notFarDeliverer._id,
      //   lastUpdateDeliverer: new Date(),
      //   historyDeliverer
      // }
      const updatedParams = {
        status: DeliveryStatus.STAND_BY
      }

      await models.Delivery
        .updateOne({ _id: req.params.id, status: DeliveryStatus.AWAITING_VALIDATION_AMOUNT },
          updatedParams)
        .exec();

      const dataNotification = {
        notificationType: 'DELIVERY_AVAILABLE'
      }


      const notificationService = new NotificationService();

      await notificationService.notify(req.currentUser._id, 'Livraison validée',
        "Votre paiement a été validé", dataNotification);

      // await notificationService.notify(notFarDeliverer._id, 'Nouvelle livraison disponible',
      //   "Une nouvelle demande de livraison est disponible", dataNotification);

      await models.OperationStripeInProgress.updateOne({
        _id: operationObj._id,
        taskId: taskId,
        delivery: req.params.id
      }, { status: PaymentStatus.SUCCESS }).exec();

      return;
    } catch (error) {
      console.log(error);

      if (error === TranslateCode.ERROR_CONFIRM_PAYMENT) {
        await models.OperationStripeInProgress.updateOne({
          _id: operationObj._id,
          taskId: taskId,
          delivery: req.params.id
        }, { status: PaymentStatus.FAILED }).exec();
      }

      if (error === TranslateCode.DELIVERER_FOR_DELIVERY_NOT_FOUND) {
        res.status(400).send({ tradCode: error });
      } else if (error === TranslateCode.ERROR_CONFIRM_PAYMENT) {
        res.status(400).send({ tradCode: error });
      } else {
        res.status(400).send({ tradCode: TranslateCode.ERROR_UPDATE_DELIVERY });
      }
    }
  }


  async refuseAmount(req, res) {
    const delivery = await models.Delivery.findOne({ _id: req.params.id }).exec();
    if (delivery) {

      const typeOperation = "PAYMENT";
      let operation = await models.OperationStripeInProgress.findOne({
        delivery: req.params.id,
        type: typeOperation,
        status: { $in: [PaymentStatus.IN_PROGRESS, PaymentStatus.SUCCESS] }
      });

      if (operation) {
        return res.status(403).send({ tradCode: "Opération de paiement en cours..." })
      }

      this.cancelCodePromo(delivery.codePromo, req.currentUser);
      await models.Delivery
        .updateOne({ _id: req.params.id }, { status: DeliveryStatus.REFUSED_AMOUNT })
        .exec();

      await models.ChatSession.deleteMany({ delivery: delivery._id }).exec();
      return res.status(200).send({ tradCode: TranslateCode.DELIVERY_AMOUNT_REFUSE_SUCCESS });
    } else {
      res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
      return;
    }
  }

  async listMyDeliveries(req, res) {
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);

    const filter = { customer: req.currentUser._id };

    let response = models.Delivery.find(filter)
      .populate('address')
      .populate('customerOfMerchant')
      .populate({
        path: 'merchant',
        select: '-password',
        // Get friends of friends - populate the 'friends' array for every friend
        populate: { path: 'addresses' }
      });

    if (page !== undefined && limit !== undefined) {
      response = response
        .skip(page * limit)
        .limit(limit)
    }

    const count = await models.Delivery.countDocuments(filter).exec();

    response = await response
      .lean()
      .sort({ deliveryDate: 'desc' })
      .exec();

    if (!page && !limit) {
      return res.status(200).send(response);
    }

    return res.status(200).send({
      total: count,
      pageSize: response.length,
      data: response
    });
  }

  async trackDelivery(req, res) {
    console.log(req.params.id);
    const response = await models.Tracking.find({ delivery: req.params.id }).sort({ insertAt: 'desc' }).limit(1).exec();
    return res.status(200).send(response);
  }


  async givePackageToDeliverer(req, res) {
    const delivery = await models.Delivery.findOne({ _id: req.params.id, merchant: req.currentUser._id }).exec();
    if (delivery) {
      await models.Delivery
        .updateOne({ _id: req.params.id }, { packageGivenToDeliverer: true })
        .exec();
      return res.status(200).send({ tradCode: TranslateCode.DELIVERED });
    } else {
      return res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
    }
  }

  async packageIsReceived(req, res) {
    const received = req.body.received;
    const delivery = await models.Delivery.findOne({ _id: req.params.id, customer: req.currentUser._id })
      .populate('invoice')
      .exec();
    if (delivery) {
      if (received) {
        await models.Delivery
          .updateOne({ _id: req.params.id }, { checkDeliverySuccessfullyByCustomer: true })
          .exec();
      } else {
        await models.Delivery
          .updateOne({ _id: req.params.id }, { status: DeliveryStatus.NOT_RECEIVED })
          .exec();
      }
      console.log(delivery.merchant);

      await models.ChatSession.deleteMany({ delivery: delivery._id }).exec();

      return res.status(200).send({ tradCode: TranslateCode.DELIVERED });
    } else {
      return res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
    }
  }

  async returnPackage(req, res) {
    const received = req.body.received;
    const delivery = await models.Delivery.findOne({ _id: req.params.id, deliverer: req.currentUser._id }).exec();
    if (delivery) {
      await models.Delivery
        .updateOne({ _id: req.params.id }, { status: DeliveryStatus.RETURN_PACKAGE })
        .exec();

      await models.ChatSession.deleteMany({ delivery: delivery._id }).exec();

      return res.status(200).send({ tradCode: TranslateCode.DELIVERED });
    } else {
      return res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
    }
  }

  async getDeliverers(req, res) {
    const dateToCompare = new Date();
    dateToCompare.setMinutes(dateToCompare.getMinutes() - 10);

    const response = await models.Position.aggregate(
      [
        { $match: { insertAt: { $gte: dateToCompare } } },
        { $sort: { insertAt: -1 } },
        { $group: { _id: "$deliverer", positions: { $push: { latitude: "$latitude", longitude: "$longitude", insertAt: "$insertAt" } } } },
        { $project: { positions: { $slice: ["$positions", 1] } } }
      ])
      .exec();
    return res.status(200).send(response);
  }

  //deliverer
  async listDeliveriesAvailable(req, res) {
    const reponse = await models.Delivery
      .find({ $and: [{ 'deliverer': req.currentUser._id }, { status: ['VALID'] }] })
      .populate('deliverer', '-password')
      .populate('merchant', '-password')
      .populate({
        path: 'merchant',
        // Get friends of friends - populate the 'friends' array for every friend
        populate: { path: 'addresses' }
      })
      .populate('customer', '-password')
      .populate('address')
      .populate('customerOfMerchant')
      .populate('invoice', 'distanceText amountDeliverer')
      .populate('externalCustomer')
      .populate({ path: 'partner', select: '-secretKey -accessKey', populate: { path: 'address' } })
      .exec();
    return res.status(200).send(reponse);
  }

  async acceptDelivery(req, res) {
    const id = req.params.id;
    try {
      let delivery = await models.Delivery.findOne({ _id: id, status: DeliveryStatus.VALID, deliverer: req.currentUser._id }).exec();
      if (delivery) {
        const currentDate = moment(new Date());
        if (currentDate.diff(moment(delivery.lastUpdateDeliverer), 'minutes') < 2) {
          const result = await models.Delivery.updateOne(
            { _id: req.params.id, deliverer: req.currentUser._id },
            { status: 'IN_DELIVERY' }
          ).exec();

          if (result.nModified > 0) {
            const reponse = await models.Delivery.find({ _id: id })
              .populate('deliverer', '-password')
              .populate('merchant', '-password')
              .populate({
                path: 'merchant',
                // Get friends of friends - populate the 'friends' array for every friend
                populate: { path: 'addresses' }
              })
              .populate('customer', '-password')
              .populate('address')
              .populate('externalCustomer')
              .populate({ path: 'partner', select: '-secretKey -accessKey', populate: { path: 'address' } })
              .exec();
            return res.status(200).send(reponse);
          } else {
            throw TranslateCode.DELIVERER_EXPIRED;
          }
        } else {
          throw TranslateCode.DELIVERER_EXPIRED;
        }
      } else {
        throw TranslateCode.DELIVERER_EXPIRED;
      }
    } catch (error) {
      console.log(error);
      res.status(403).send({ tradCode: error });
      return;
    }
  }

  async getDelivery(req, res) {
    const role = req.currentUser.role;

    let filter;
    if (role === 'ROLE_CUSTOMER' || role === UserRole.ROLE_NURSE || role == UserRole.ROLE_DOCTOR) {
      filter = { customer: req.currentUser._id }
    } else if (role === 'ROLE_DELIVERER') {
      filter = { deliverer: req.currentUser._id }
    } else if (role === UserRole.ROLE_PHARMACY || role === UserRole.ROLE_OPTICIAN) {
      filter = { merchant: req.currentUser._id }
    } else {
      res.boom.forbidden("Error");
      return;
    }

    filter._id = req.params.id;

    try {
      const reponse = await models.Delivery
        .findOne(filter)
        .populate('deliverer', '-password')
        .populate('merchant', '-password')
        .populate({
          path: 'merchant',
          populate: { path: 'addresses' }
        })
        .populate('customer', '-password')
        .populate('address')
        .populate('invoice')
        .populate('customerOfMerchant')
        //.populate('productsSelected')
        .populate('externalCustomer')
        .populate({ path: 'partner', select: '-secretKey -accessKey', populate: { path: 'address' } })
        .lean()
        .exec();

      if (!reponse) {
        throw "Not authorized";
      }
      console.log("-------------------------------------------------------------------");
      console.log(reponse);

      const operations = await models.OperationStripeInProgress.find({
        delivery: req.params.id
      })
        .lean()
        .exec();

      reponse.operations = operations;

      return res.status(200).send(reponse);
    } catch (error) {
        console.log("error"); console.log("-------------------------------------------------------------------");
        console.log(error);
      return res.status(402).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
    }
  }

  async addTrack(req, res) {
    let trackObj = new models.Tracking(req.body);
    trackObj.delivery = req.params.id;
    const track = await trackObj.save();
    return res.status(200).send(track);
  }

  async addPosition(req, res) {
    let positionObj = new models.Position(req.body);
    positionObj.geo = { type: 'Point', coordinates: [req.body.longitude, req.body.latitude] }
    positionObj.deliverer = req.currentUser._id;
    const position = await positionObj.save();
    req.currentUser.positions.push(position)
    await req.currentUser.save();
    return res.status(200).send(position);
  }

  //merchant
  async listDeliveries(req, res) {
    const reponse = await models.Delivery.find({
      merchant: req.currentUser._id,
      status: { $ne: DeliveryStatus.CANCELLED }
    })
      .populate('customer', '-password')
      .populate('address')
      .populate('customerOfMerchant')
      .populate('invoice')
      .sort({ deliveryDate: 'desc' })
      .exec();
    return res.status(200).send(reponse);
  }

  async listAllDeliveries(req, res) {
    const response = await models.Delivery.find({})
      .populate('deliverer', '-password')
      .populate('merchant', '-password')
      .populate({
        path: 'merchant',
        // Get friends of friends - populate the 'friends' array for every friend
        populate: { path: 'addresses' }
      })
      .populate('customer', '-password')
      .populate('address')
      .exec();
    return res.status(200).send(response);
  }

  async recoveredPackageByDeliverer(req, res) {

    try {
      let delivery = await models.Delivery.findOne({ _id: req.params.id, deliverer: req.currentUser._id }).exec();
      await models.Delivery.updateOne(
        { _id: req.params.id, deliverer: req.currentUser._id },
        { recoveredPackageByDeliverer: true }
      ).exec();
      return res.status(200).send({ message: 'Recovered package successfully' });
    } catch (error) {
      res.boom.forbidden("Error");
      return;
    }

  }

  async delivererReachDestination(req, res) {

    try {
      let delivery = await models.Delivery.findOne({ _id: req.params.id, deliverer: req.currentUser._id }).exec();
      await models.Delivery.updateOne(
        { _id: req.params.id, deliverer: req.currentUser._id },
        { delivererReachDestination: true }
      ).exec();
      return res.status(200).send({ message: 'Deliverer has reached destination' });
    } catch (error) {
      res.boom.forbidden("Error");
      return;
    }

  }

  async packageDeliveredByDeliverer(req, res) {
    try {
      let delivery = await models.Delivery.findOne({ _id: req.params.id, deliverer: req.currentUser._id })
        .populate('invoice')
        .populate('merchant')
        .populate('deliverer')
        .exec();
      if (!delivery) {
        return res.send(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
      }
      if (!delivery.isBtoB && !delivery.isForCustomer) {
        await models.Delivery.updateOne(
          { _id: req.params.id, deliverer: req.currentUser._id },
          { checkDeliverySuccessfullyByDeliverer: true, status: 'DELIVERED' }
        ).exec();

        (async () => {
          const deliveryData = { id: delivery._id, reference: delivery.reference, paymentId: delivery.preAuthorizationId };
          const transferToMerchantData = { accountId: delivery.merchant.accountId, amount: delivery.invoice.amountMerchant };
          const transferToDelivererData = { accountId: delivery.deliverer.accountId, amount: delivery.invoice.amountDeliverer};
          const paymentService = new PaymentService();
          const result = await paymentService.payMultiPart(deliveryData, transferToMerchantData, transferToDelivererData);

          let updateParams = {};
          if (result.transferToMerchant) {
            updateParams.transferToMerchant = result.transferToMerchant.id;
          }

          if (result.transferToDeliverer) {
            updateParams.transferToDeliverer = result.transferToDeliverer.id;
          }

          await models.Invoice.updateOne({ _id: delivery.invoice._id }, updateParams).exec();
        })()

      } else {
        await models.Delivery.updateOne(
          { _id: req.params.id, deliverer: req.currentUser._id },
          {
            checkDeliverySuccessfullyByDeliverer: true,
            status: 'DELIVERED',
            tagLine: req.body.tagLine,
            receptionDate: new Date()
          }
        ).exec();

        const notificationService = new NotificationService();
        notificationService.notifyPartner(delivery._id)

      }
      return res.status(200).send({ message: 'Recovered package successfully' });
    } catch (error) {
      return res.status(403).send({ tradCode: TranslateCode.ERROR_FINISH_DELIVERY });
    }
  }

  async createInvoiceForDelivery(req, res) {
    const params = filterKeys(req.body, ['products']);
    let delivery = await models.Delivery.findOne({ _id: req.params.id, merchant: req.currentUser._id })
      .populate('merchant', '-password')
      .populate({
        path: 'merchant',
        // Get friends of friends - populate the 'friends' array for every friend
        populate: { path: 'addresses' }
      })
      .exec();
    if (delivery) {
      const data = await this.computeAmount(params, delivery, this.getFavoriteAddress(delivery.merchant.addresses));
      const invoiceObj = new models.Invoice({
        amountCustomer: data.amountCustomer,
        amountMerchant: data.amountMerchant,
        amountDeliverer: data.amountDeliverer,
        feeManagement: data.feeManagement,
        platformFee: data.platformFee,
        products: data.products,
        distanceText: data.distanceText,
        distanceInMeter: data.distanceInMeter,
        nightPrimeActive: data.nightPrimeActive,
        rainPrimeActive: data.rainPrimeActive,
        codePromoInformation: (delivery.codePromo ? await models.PromoCode.findOne({ promoCode: delivery.codePromo.toUpperCase() }).exec() : undefined),
        amountCustomerBeforeCodePromo: data.amountCustomerBeforeCodePromo,
        promoAmount: data.promoAmount
      });

      const invoice = await invoiceObj.save();

      await models.Delivery
        .updateOne({ _id: req.params.id }, { invoice: invoice._id, status: DeliveryStatus.AWAITING_VALIDATION_AMOUNT })
        .exec();

      const dataNotification = {
        notificationType: 'MERCHANT_VALIDATED_AMOUNT',
        id: delivery._id
      }

      const notificationService = new NotificationService();
      await notificationService.notify(delivery.customer, 'Votre commande a été validée',
        "Connectez-vous sur l'application pour valider la facture", dataNotification);

      return res.status(200).send({ tradCode: TranslateCode.INVOICE_CREATED, message: 'Invoice created' });
    } else {
      res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
      return;
    }
  }


  async refuseDeliveryByMerchant(req, res) {
    let delivery = await models.Delivery.findOne({ _id: req.params.id, merchant: req.currentUser._id })
      .populate('customer')
      .exec();

    if (delivery) {
      this.cancelCodePromo(delivery.codePromo, delivery.customer);
      await models.Delivery
        .updateOne({ _id: req.params.id }, { status: 'REFUSED' })
        .exec();
      console.log(req.currentUser._id);
      return res.status(200).send({ tradCode: TranslateCode.REFUSE_DELIVERY, message: 'Delivery refused' });
    } else {
      res.boom.notFound("Not found");
      return;
    }
  }


  async refuseDeliveryByDeliverer(req, res) {
    let delivery = await models.Delivery.findOne({ _id: req.params.id, status: DeliveryStatus.VALID, deliverer: req.currentUser._id })
      .populate('customer')
      .exec();

    if (delivery) {
      await models.Delivery.update({
        _id: delivery._id,
        status: DeliveryStatus.VALID,
        deliverer: req.currentUser._id
      }, {
        deliverer: null,
        status: DeliveryStatus.STAND_BY
      }).exec();
      // const delivererFound = await findDeliverer(delivery);
      // if (delivererFound) {
      //   delivery.historyDeliverer.push(delivererFound);
      //   await models.Delivery.update({
      //     _id: delivery._id,
      //     status: DeliveryStatus.VALID,
      //     deliverer: req.currentUser._id
      //   }, {
      //     deliverer: delivererFound._id,
      //     lastUpdateDeliverer: new Date(),
      //     historyDeliverer: delivery.historyDeliverer
      //   }).exec();

      //   if (res.nModified > 0) {
      //     const dataNotification = {
      //       notificationType: 'DELIVERY_AVAILABLE'
      //     }

      //     const notificationService = new NotificationService();
      //     await notificationService.notify(delivererFound._id, 'Nouvelle livraison disponible',
      //       "Une nouvelle demande de livraison est disponible", dataNotification);
      //   }
      // } else {

      //   if (delivery.isForCustomer) {
      //     this.cancelCodePromo(delivery.codePromo, null);
      //   } else {
      //     this.cancelCodePromo(delivery.codePromo, delivery.customer);
      //   }

      //   if (delivery.preAuthorizationId) {
      //     const paymentService = new PaymentService();
      //     await paymentService.cancelPayment(delivery.preAuthorizationId);
      //   }

      //   await models.Delivery.updateOne({
      //     _id: delivery._id,
      //     status: DeliveryStatus.VALID,
      //     deliverer: req.currentUser._id
      //   }, {
      //     status: DeliveryStatus.REFUSED_BY_DELIVERER
      //   }).exec();
      // }
      return res.status(200).send({ tradCode: TranslateCode.REFUSE_DELIVERY, message: 'Delivery refused' });
    } else {
      return res.status(200).send({ tradCode: TranslateCode.DELIVERER_EXPIRED, message: 'Delivery refused' });
    }
  }

  async getAllDeliveries(req, res) {
    const page = parseInt(req.query.page) || 0; //for next page pass 1 here
    const limit = parseInt(req.query.limit) || 3;
    const type = req.query.type || "INTERNAL";

    const filter = {};

    if (type === 'INTERNAL') {
      filter.$or = [{ type: type }, { type: { $exists: false } }, { type: null }]
    } else {
      filter.type = type;
    }

    const deliveries = await models.Delivery.find(filter)
      .populate('deliverer', '-password')
      .populate('merchant', '-password')
      .populate({
        path: 'merchant',
        // Get friends of friends - populate the 'friends' array for every friend
        populate: { path: 'addresses' }
      })
      .populate('customer', '-password')
      .populate('address')
      .populate('invoice')
      .populate({ path: 'partner', select: '-secretKey -accessKey', populate: { path: 'address' } })
      .skip(page * limit)
      .limit(limit)
      .sort({ deliveryDate: 'desc' })
      .exec();
    const count = await models.Delivery.countDocuments(filter).exec();

    res.status(200).send({
      total: count,
      pageSize: deliveries.length,
      data: deliveries
    });
    return;
  }

  async listMyDeliveriesDeliverer(req, res) {
    const page = parseInt(req.query.page) || 0; //for next page pass 1 here
    const limit = parseInt(req.query.limit) || 3;

    const filter = { deliverer: req.currentUser._id, status: DeliveryStatus.DELIVERED };
    const deliveries = await models.Delivery.find(filter)
      .populate('deliverer', '-password')
      .populate('merchant', '-password')
      .populate('customerOfMerchant')
      .populate({
        path: 'merchant',
        // Get friends of friends - populate the 'friends' array for every friend
        populate: { path: 'addresses' }
      })
      .populate('customer', '-password')
      .populate('address')
      .populate('invoice')
      .populate({ path: 'partner', select: '-secretKey -accessKey', populate: { path: 'address' } })
      .skip(page * limit)
      .limit(limit)
      .exec();
    const count = await models.Delivery.countDocuments(filter).exec();

    res.status(200).send({
      total: count,
      pageSize: deliveries.length,
      data: deliveries
    });
    return;
  }

  async statDeliverer(req, res) {

    const deliveries = await models.Delivery.find({ deliverer: req.currentUser._id, status: DeliveryStatus.DELIVERED })
      .populate('invoice')
      .exec();
    let totalGain = 0;
    for (let delivery of deliveries) {
      totalGain += delivery.invoice.amountDeliverer;
    }

    res.status(200).send({
      totalGain: totalGain
    });
    return;
  }

  async listShopAvailable(req, res) {
    const limitNbMet = 15000;

    const page = parseInt(req.query.page) || 0; //for next page pass 1 here
    const limit = parseInt(req.query.limit) || undefined;
    const latitude = req.query.lat || undefined;
    const longitude = req.query.long || undefined;

    const text = req.query.text;
    const cat = req.query.category;

    const matchAfterPopulate = [];
    const match = [];

    if (text) {
      matchAfterPopulate.push({
        $or: [
          {
            $and: [
              { "linkAddresses.city": { $regex: text, $options: "i" } },
              { "linkAddresses.favoriteAddress": true }
            ]
          },
          {
            $and: [
              { "linkAddresses.postalCode": { $regex: text, $options: "i" } },
              { "linkAddresses.favoriteAddress": true }
            ]
          },
          { name: { $regex: text, $options: "i" } },
          { nameWithoutSpecialChar: { $regex: text, $options: "i" } }
        ]
      })
    }

    const config = await models.Config.findOne({ label: 'DEFAULT' }).exec();
    if (cat) {
      match.push({ $and: [{ role: { $in: config.categoriesAvailable } }, { role: cat }] })
    } else {
      match.push({ role: { $in: config.categoriesAvailable } })
    }

    const actualDate = new Date();
    const momentTargetDate = momentTimezone(actualDate);
    momentTargetDate.tz('Europe/Paris');
    const weekDay = momentTargetDate.toDate().getDay();
    const startField = `$openingTime.${weekDay}Start`;
    const endField = `$openingTime.${weekDay}End`;

    const h = Number(momentTargetDate.format('HH'));
    const m = Number(momentTargetDate.format('mm'));

    let aggregation = [];

    if (latitude && longitude) {
      aggregation.push(
        {
          $geoNear: {
            near: { type: "Point", coordinates: [Number(longitude), Number(latitude)] },
            distanceField: 'distcalculated',
            spherical: true,
            maxDistance: limitNbMet
          }
        }
      )

      match.push({ $expr: { $lte: ["$distcalculated", limitNbMet] } })
    }

    match.push({
      status: { $in: [UserStatus.ACTIVE] }
    });

    aggregation.push(
      {
        $match: {
          $and: match
        }
      },
      { $lookup: { from: "openingtimes", localField: "openingTime", foreignField: "_id", as: "linkOpeningTime" } },
      { $unwind: { path: "$linkOpeningTime", preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: "addresses",
          localField: "addresses",
          foreignField: "_id",
          as: "linkAddresses",
        }
      },
      { $unwind: "$linkAddresses" },
      {
        $addFields: {
          nameWithoutSpecialChar: { $replaceAll: { input: "$name", find: "é", replacement: "e" } }
        }
      },
      {
        $set: {
          nameWithoutSpecialChar: { $replaceAll: { input: "$nameWithoutSpecialChar", find: "è", replacement: "e" } }
        }
      },
      {
        $set: {
          nameWithoutSpecialChar: { $replaceAll: { input: "$nameWithoutSpecialChar", find: "ê", replacement: "e" } }
        }
      }
    )


    if (text) {
      aggregation.push({
        $match: {
          $and: matchAfterPopulate
        }
      })
    }

    aggregation.push(
      {
        $group: {
          _id: "$_id",
          name: { $first: "$name" },
          openingTime: { $first: "$linkOpeningTime" },
          avatar: { $first: "$avatar" },
          location: { $first: "$location" },
          phoneNumber: { $first: "$phoneNumber" },
          mail: { $first: "$mail" },
          linkedAddresses: { $push: "$linkAddresses" }
        }
      },
      {
        $addFields: {
          startMinutesStr: { $dateToString: { format: "%M", date: startField, timezone: "Europe/Paris" } },
          startHoursStr: { $dateToString: { format: "%H", date: startField, timezone: "Europe/Paris" } },
          endMinutesStr: { $dateToString: { format: "%M", date: endField, timezone: "Europe/Paris" } },
          endHoursStr: { $dateToString: { format: "%H", date: endField, timezone: "Europe/Paris" } },
        }
      },
      {
        $addFields: {
          startMinute: { $toInt: "$startMinutesStr" },
          startHour: { $toInt: "$startHoursStr" },
          endMinute: { $toInt: "$endMinutesStr" },
          endHour: { $toInt: "$endHoursStr" },
        },
      },
      {
        $project:
        {
          _id: 1,
          name: 1,
          addresses: "$linkedAddresses",
          openingTime: 1,
          avatar: 1,
          location: 1,
          phoneNumber: 1,
          mail: 1,
          haveOpeningTime: {
            $cond: [
              { $not: ["$openingTime"] }
              , false, true]
          },
          isOpen:
          {
            $cond: [
              {
                $or: [
                  {
                    $and: [
                      {
                        $or: [
                          { $lt: ["$startHour", h] },
                          {
                            $and: [
                              { $eq: ["$startHour", h] },
                              { $lte: ["$startMinute", m] },
                            ]
                          }
                        ]
                      },
                      {
                        $or: [
                          { $gt: ["$endHour", h] },
                          {
                            $and: [
                              { $eq: ["$endHour", h] },
                              { $gte: ["$endMinute", m] },
                            ]
                          }
                        ]
                      }
                    ]
                  },
                  { $not: ["$openingTime"] }
                ],
              },
              true, false
            ]
          }
        }
      },
      { $sort: { isOpen: -1, name: 1, haveOpeningTime: -1 } },
    );

    console.log("AGGREGATION: " + JSON.stringify(aggregation));

    // Add pagination
    const aggregationFilter = [...aggregation];

    if (limit) {
      aggregationFilter.push({ $skip: page * limit });
      aggregationFilter.push({ $limit: limit });
    }

    const responseAggregation = await models.User.aggregate(
      aggregationFilter
    )
      .exec();


    const aggregationCount = [...aggregation];
    aggregationCount.push({
      $count: "nb"
    });

    const countAggregation = await models.User.aggregate(
      aggregationCount
    )
      .exec();

    let total = 0
    if (countAggregation.length > 0) {
      total = countAggregation[0].nb;
    }
    console.log(countAggregation)

    return res.status(200).send({
      total: total,
      pageSize: responseAggregation.length,
      data: responseAggregation
    });
  }

  async cancelDelivery(req, res) {
    const delivery = await models.Delivery.findOne({ _id: req.params.id, customer: req.currentUser._id, status: DeliveryStatus.SCHEDULED }).exec();
    if (delivery) {
      await models.Delivery
        .updateOne({ _id: req.params.id }, { status: DeliveryStatus.CANCELLED })
        .exec();

      await models.ChatSession.deleteMany({ delivery: delivery._id }).exec();

      return res.status(200).send({ tradCode: TranslateCode.DELIVERY_CANCELLED })
    } else {
      return res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
    }

  }




  async update(req, res) {
    const id = req.params.id;
    const addressId = req.body.addressId ? JSON.parse(req.body.addressId) : undefined;
    const customerOfMerchant = req.body.customerOfMerchant ? JSON.parse(req.body.customerOfMerchant) : undefined;
    const merchant = req.body.merchant ? JSON.parse(req.body.merchant) : undefined;
    const products = req.body.products ? JSON.parse(req.body.products) : undefined;
    const isScheduled = req.body.isScheduled ? JSON.parse(req.body.isScheduled) : false;
    const deliveryDate = req.body.deliveryDate ? JSON.parse(req.body.deliveryDate) : undefined;
    const phoneNumber = req.body.phoneNumber ? JSON.parse(req.body.phoneNumber) : undefined;
    const cardId = req.body.cardId ? JSON.parse(req.body.cardId) : undefined;
    const deletedImages = req.body.deletedImages ? JSON.parse(req.body.deletedImages) : undefined;

    const productsSelected = [];

    try {
      if (req.currentUser.status === UserStatus.AWAITING_VALIDATION) {
        res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
        return;
      }

      const delivery = await models.Delivery.findOne({
        _id: id,
        customer: req.currentUser._id,
        status: DeliveryStatus.SCHEDULED
      }).exec();
      if (!delivery) {
        res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
        return;
      }

      let addressFound = await models.Address.findOne({ _id: addressId }).exec();
      if (addressFound === null) {
        res.status(404).send({ tradCode: TranslateCode.ADDRESS_NOT_FOUND });
        return;
      }
      console.log(products)
      for (const product of products) {
        console.log(product)
        let productFound = await models.Product.findOne({ _id: product }).exec();
        console.log(productFound);
        console.log(await models.Product.find().exec())
        if (productFound === null) {
          res.status(404).send({ tradCode: TranslateCode.PRODUCT_NOT_FOUND });
          return;
        }
        productsSelected.push(product);
      }

      const updatedData = {
        merchant,
        latitude: addressFound.latitude,
        longitude: addressFound.longitude,
        checkDeliverySuccessfullyByDeliverer: false,
        checkDeliverySuccessfullyByCustomer: false,
        address: addressFound,
        productsSelected: productsSelected,
        phoneNumber: phoneNumber,
        cardId
      };

      if (req.currentUser.role === UserRole.ROLE_DOCTOR || req.currentUser.role === UserRole.ROLE_NURSE) {
        updatedData.customerOfMerchant = customerOfMerchant;
      }

      if (isScheduled) {
        updatedData.status = DeliveryStatus.SCHEDULED
        updatedData.deliveryDate = deliveryDate;

        if (new Date(deliveryDate).getTime() <= new Date().getTime()) {
          res.status(400).send({ tradCode: TranslateCode.DATE_BEFORE_NOW });
          return;
        }
      } else {
        updatedData.status = DeliveryStatus.AWAITING_VALIDATION;
        updatedData.deliveryDate = new Date();
      }

      const fileService = new FileService();
      // delete images
      const imageInDb = delivery.image;
      const confirmedImageDeleted = []
      if (deletedImages && deletedImages.length > 0) {
        for (const image of deletedImages) {
          const indexFound = imageInDb.indexOf(image)
          if (indexFound !== -1) {
            confirmedImageDeleted.push(fileService.formatInternalFormatUrlToFormatAws(JSON.parse(image)));
            imageInDb.splice(indexFound, 1);
          }
        }

        if (confirmedImageDeleted.length > 0) {
          await fileService.deletes('livmeds-order', confirmedImageDeleted.map(elem => {
            return { Key: elem.Key }
          }));
        }

        updatedData.image = imageInDb;
      }

      // Upload image
      if (req.files) {
        if (!Array.isArray(imageInDb)) {
          const image = await fileService.uploadOrder(req.files, delivery._id);
          updatedData.image = image;
        } else if (req.files.length > 0) {
          let promises = []
          for (let file of req.files) {
            promises.push(fileService.uploadOrder(file, delivery._id));
          }

          const images = await Promise.all(promises)
          updatedData.image = imageInDb.concat(images);
        }
      }

      await models.Delivery.updateOne({
        _id: id
      }, updatedData).exec();

      let deliveryResp = await models.Delivery.findOne({ _id: delivery._id })
        .populate('merchant', '-password')
        .populate('address')
        .populate({
          path: 'merchant',
          // Get friends of friends - populate the 'friends' array for every friend
          populate: { path: 'addresses' }
        })
        .exec();

      res.status(200).send({ delivery: deliveryResp, tradCode: TranslateCode.UPDATE_DELIVERY_SUCCESS });
    } catch (error) {
      console.log(error);
      res.status(403).send({ tradCode: TranslateCode.ERROR_UPDDATE_DELIVERY });
    }
  }


  async computeAmount(params, delivery, address) {
    const config = await models.Config.findOne({ label: 'DEFAULT' }).exec();
    let amountDeliverer = config.fixedPriceForDeliverer;
    let rainPrimeActive = false;
    let nightPrimeActive = false;
    let productsAmount = 0;
    let productsPharma = 0;
    let deliveryFee = config.fixedPriceDelivery;
    const products = [];

    const distance = await googleMapsClient.distanceMatrix({
      origins: [{
        lat: address.latitude,
        lng: address.longitude,
      }],
      destinations: [{
        lat: delivery.latitude,
        lng: delivery.longitude
      }],
      mode: 'driving',
    }).asPromise();

    if (!delivery.isBtoB) {
      for (let product of params.products) {
        product = filterKeys(product, ['_id', 'description', 'quantity', 'unitAmount', 'image', 'status', 'comment']);
        if (!product.status || product.status !== 'DELETED') {
          if (product._id) {
            productsPharma += product.quantity * product.unitAmount;
          } else {
            productsAmount += product.quantity * product.unitAmount;
          }
        }
        products.push(product);
      }
      const nbKm = (distance.json.rows[0].elements[0].distance.value / 1000);
      if (nbKm >= config.nbKmSliceDelivery) {
        const nbKmRest = nbKm - config.nbKmSliceDelivery;
        deliveryFee += nbKmRest;

        // calcul for deliverer
        amountDeliverer += nbKmRest;
      }
      const distanceInMeter = distance.json.rows[0].elements[0].distance.value;
      const distanceText = distance.json.rows[0].elements[0].distance.text;

      let managementFees = productsPharma * (config.percentPharmaProduct / 100)

      const now = new Date();
      const bonus = await models.Bonus.find({
        startFrom: { "$gte": now },
        endAt: { "$lt": now },
        city: delivery.address.city
      })

      if (bonus.length >= 2) {
        amountDeliverer *= (1 + (config.multipleBonusMergedPercent / 100))
      } else {
        switch (bonus.type) {
          case TypeBonus.RAIN:
            amountDeliverer *= (1 + (config.percentPrimeRain / 100))
            rainPrimeActive = true;
            break;
          case TypeBonus.NIGHT:
            amountDeliverer *= (1 + (config.percentPrimeNight / 100))
            nightPrimeActive = true;
            break;
        }
      }

      const amountMerchant = Number.parseFloat(productsAmount + productsPharma).toFixed(2);
      deliveryFee = Number.parseFloat(deliveryFee).toFixed(2);
      managementFees = Number.parseFloat(managementFees).toFixed(2);

      let amountCustomer = Number.parseFloat(parseFloat(amountMerchant) + parseFloat(deliveryFee) + parseFloat(managementFees)).toFixed(2);
      amountDeliverer = Number.parseFloat(amountDeliverer).toFixed(2);


      let promoAmount = 0;
      let amountCustomerBeforeCodePromo = amountCustomer;
      if (delivery.codePromo) {
        const promoCode = await models.PromoCode.findOne({ promoCode: delivery.codePromo.toUpperCase() }).exec();
        if (promoCode && promoCode.status === PromoCodeStatus.ENABLED) {
          promoAmount = Number.parseFloat(Number.parseFloat(deliveryFee).toFixed(2) * (promoCode.percentage / 100)).toFixed(2);
        }
      }

      amountCustomer -= promoAmount;

      const platformFee = Number.parseFloat(parseFloat(amountCustomerBeforeCodePromo) - parseFloat(amountMerchant) - parseFloat(amountDeliverer)).toFixed(2);

      return {
        amountCustomerBeforeCodePromo: amountCustomerBeforeCodePromo,
        amountCustomer: amountCustomer,
        amountMerchant: amountMerchant,
        amountDeliverer: amountDeliverer,
        platformFee: platformFee,
        distanceInMeter: distanceInMeter,
        distanceText: Number.parseFloat(nbKm).toFixed(2) + ' km',
        productsAmount: Number.parseFloat(productsAmount).toFixed(2),
        productsPharma: Number.parseFloat(productsPharma).toFixed(2),
        rainPrimeActive: rainPrimeActive,
        nightPrimeActive: nightPrimeActive,
        products: products,
        feeManagement: managementFees,
        promoAmount: promoAmount
      }
    } else {
      let typeDeliverer = null;
      if (delivery.sizePackage === SizePackage["50X50"]) {
        const type = await models.TypeDeliverer.findOne({ label: TypeDeliverer.CARGO_BIKE }).exec();
        typeDeliverer = type;
      } else if (delivery.sizePackage === SizePackage.SMALL) {
        const type = await models.TypeDeliverer.findOne({ label: { $in: [TypeDeliverer.BIKE] } }).exec();
        typeDeliverer = type;
      }


      deliveryFee = delivery.nbPackage * typeDeliverer.price;
      if (deliveryFee >= 3) {
        deliveryFee -= deliveryFee * 0.7;
      } else if (deliveryFee == 2) {
        deliveryFee -= deliveryFee * 0.5;
      }
      const platformFee = deliveryFee - config.fixedPriceForDeliverer;

      const distanceInMeter = distance.json.rows[0].elements[0].distance.value;
      const distanceText = distance.json.rows[0].elements[0].distance.text;
      return {
        amountMerchant: Number.parseFloat(deliveryFee).toFixed(2),
        amountDeliverer: Number.parseFloat(config.fixedPriceForDeliverer).toFixed(2),
        platformFee: Number.parseFloat(platformFee).toFixed(2),
        rainPrimeActive,
        nightPrimeActive,
        distanceInMeter,
        distanceText
      }
    }
  }

  async addScore(req, res) {
    const params = filterKeys(req.body, ['delivery', 'value'])
    params.value = (params.value) ? params.value : 0;
    const alreadyVoted = await models.Score.findOne({
      userSendScore: req.currentUser._id,
      delivery: params.delivery
    }).exec();
    if (alreadyVoted) {
      res.status(403).send({ tradCode: TranslateCode.ALREADY_VOTED });
      return;
    }

    const value = parseInt(params.value);
    if (value > 5 && value < 1) {
      res.status(403).send({ tradCode: TranslateCode.INVALID_PARAMS });
      return;
    }

    const deliveryExist = await models.Delivery.findOne({
      _id: params.delivery,
      customer: req.currentUser._id
    })
      .populate('deliverer', '-password')
      .exec();
    if (!deliveryExist) {
      res.status(404).send({ tradCode: TranslateCode.DELIVERY_NOT_FOUND });
      return;
    }

    const scoreObj = new models.Score(params);
    scoreObj.userSendScore = req.currentUser._id;
    scoreObj.forUser = deliveryExist.deliverer._id;
    await scoreObj.save();

    const nbRate = await models.Score.count({
      forUser: deliveryExist.deliverer._id
    }).exec();

    let total = (!deliveryExist.scoreTotal) ? params.value : Math.round((deliveryExist.scoreTotal + params.value) / nbRate);

    await models.User.updateOne({
      _id: deliveryExist.deliverer._id
    }, { totalScore: total })

    res.status(200).send({ tradCode: TranslateCode.SUCCESS_GIVEN_SCORE });
  }


  getFavoriteAddress(addresses) {
    for (let address of addresses) {
      if (address.favoriteAddress) {
        return address;
      }
    }
  }

  async getMyDeliveryInProgress(req, res) {
    const delivery = await models.Delivery.findOne({ deliverer: req.currentUser._id, status: DeliveryStatus.IN_DELIVERY })
      .populate('deliverer', '-password')
      .populate('merchant', '-password')
      .populate({
        path: 'merchant',
        // Get friends of friends - populate the 'friends' array for every friend
        populate: { path: 'addresses' }
      })
      .populate('customer', '-password')
      .populate('address')
      .populate('externalCustomer')
      .populate({ path: 'partner', select: '-secretKey -accessKey', populate: { path: 'address' } })
      .exec();

    return res.status(200).send({
      delivery
    });
  }

  async cancelDelivery(req, res) {
    const delivery = await models.Delivery.findById(req.params.id)
      .populate('customer')
      .exec();
    if ([
      DeliveryStatus.REFUSED,
      DeliveryStatus.REFUSED_AMOUNT,
      DeliveryStatus.RETURN_PACKAGE,
      DeliveryStatus.DELIVERED,
      DeliveryStatus.CANCELLED
    ].includes(delivery.status)) {
      return res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
    }


    if (delivery.preAuthorizationId) {
      const paymentService = new PaymentService();
      await paymentService.cancelPayment(delivery.preAuthorizationId);
    }

    await models.Delivery.update({
      _id: req.params.id
    }, { status: DeliveryStatus.CANCELLED }).exec();

    if (delivery.codePromo && !delivery.isBtoB) {
      if (delivery.isForCustomer) {
        this.cancelCodePromo(delivery.codePromo, null);
      } else {
        this.cancelCodePromo(delivery.codePromo, delivery.customer);
      }
    }

    await models.ChatSession.deleteMany({ delivery: req.params.id }).exec();

    return res.status(200).send({ tradCode: TranslateCode.UPDATE_DELIVERY_SUCCESS });
  }

  async checkIfMerchantIsOpen(merchantId, targetDate) {
    const merchant = await models.User
      .findOne({ _id: merchantId, role: { $in: [UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY] } })
      .populate('openingTime')
      .exec();

    console.log('[OP_TIME] ' + JSON.stringify(merchant.openingTime))

    if (!merchant.openingTime) {
      return true;
    }

    return this.isOpen(merchant.openingTime, targetDate);
  }

  isOpen(openingTime, targetDate) {

    if (!openingTime) {
      return true;
    }

    const momentTargetDate = momentTimezone(targetDate);
    momentTargetDate.tz('Europe/Paris');
    const weekDay = momentTargetDate.day()
    console.log('[WEEK_DAY] ' + JSON.stringify(weekDay))
    const start = openingTime[weekDay + 'Start'];
    const end = openingTime[weekDay + 'End'];
    if (start && end) {
      const format = 'HH:mm'

      console.log('[TARGET_DATE] ')
      console.log(momentTargetDate)
      console.log("START_DATE_BEFORE: " + moment(start))
      console.log("END_DATE_BEFORE: " + moment(end))

      const time = momentTargetDate;

      const startMoment = momentTimezone(start, format);
      startMoment.tz('Europe/Paris');
      console.log('[START_DATE] ')
      console.log(startMoment)

      const endMoment = momentTimezone(end, format);
      endMoment.tz('Europe/Paris');
      console.log('[END_DATE] ')
      console.log(endMoment)

      const h = time.format('HH');
      const m = time.format('mm');

      const h1 = startMoment.format('HH');
      const m1 = startMoment.format('mm');

      const h2 = endMoment.format('HH');
      const m2 = endMoment.format('mm');

      console.log("heure actuelle : " + h)
      console.log("min actuelle : " + m)
      console.log("start h : " + h1)
      console.log("start m : " + m1)
      console.log("end h : " + h2)
      console.log("end m : " + m2)

      return (h1 < h || h1 == h && m1 <= m) && (h < h2 || h == h2 && m <= m2);
    } else {
      return false;
    }
  }

  async checkCodePromo(code, user, usedCode, authorizedType) {

    if (!code) {
      return;
    }

    // const session = await models.PromoCode.startSession();
    // session.startTransaction();
    try {
      // const opts = { session };
      const promoCode = await models.PromoCode.findOne({ promoCode: code.toUpperCase() })

      if (!promoCode) {
        throw TranslateCode.PROMO_CODE_INVALID;
      }

      if (user && usedCode.includes(promoCode._id)) {
        throw TranslateCode.ALREADY_USED_BY_THIS_ACCOUNT;
      }

      if (!authorizedType.includes(promoCode.type) || promoCode.status !== PromoCodeStatus.ENABLED) {
        throw TranslateCode.PROMO_CODE_INVALID;
      }

      if (promoCode.type === PromoCodeType.REFERRAL && user.referrer !== user.referrerIs) {
        throw TranslateCode.PROMO_CODE_INVALID;
      }

      const futurUsedTimes = promoCode.usedTimes + 1;

      if (futurUsedTimes <= promoCode.usedTimeMax && promoCode.status === PromoCodeStatus.ENABLED) {
        await models.PromoCode.updateOne({ _id: promoCode._id }, { usedTimes: futurUsedTimes }).exec();

        if (user && usedCode) {
          usedCode.push(promoCode._id);
          await models.User.updateOne({ _id: user._id }, { usedCode: usedCode }).exec();
        }
      } else {
        throw TranslateCode.PROMO_CODE_INVALID;
      }

      // await session.commitTransaction();
      // session.endSession();
    } catch (error) {
      console.log(error);
      // If an error occurred, abort the whole transaction and
      // undo any changes that might have happened
      // await session.abortTransaction();
      // session.endSession();
      throw error;
    }
  }

  async cancelCodePromo(code, user) {

    if (!code) {
      return;
    }

    const promoCode = await models.PromoCode.findOne({ promoCode: code.toUpperCase() }).exec();

    if (promoCode) {
      let futurUsedTimes = promoCode.usedTimes;
      if (promoCode.usedTimes > 0) {
        futurUsedTimes -= 1;
      }

      await models.PromoCode.updateOne({ _id: promoCode._id }, { usedTimes: futurUsedTimes }).exec();

      if (user) {
        console.log(user)
        let usedCode = user.usedCode.filter(e => !e.equals(promoCode._id));
        console.log(promoCode._id.toString())
        console.log(usedCode)
        await models.User.updateOne({ _id: user._id }, { usedCode: usedCode }).exec();
      }

    }

  }

  async listDelivered(req, res) {
    const page = parseInt(req.query.page) || undefined; //for next page pass 1 here
    const limit = parseInt(req.query.limit) || undefined;
    const type = req.query.type || 'INTERNAL';


    const filter = { status: DeliveryStatus.DELIVERED };

    if (type === 'INTERNAL') {
      filter.$or = [{ type: type }, { type: { $exists: false } }, { type: null }]
    } else {
      filter.type = type;
    }

    let deliveries = models.Delivery.find(filter)
      .populate('customer', 'name lastName')
      .populate('invoice')
      .populate('merchant', 'name')
      .populate({ path: 'partner', select: '-secretKey -accessKey', populate: { path: 'address' } })
      .populate('deliverer', 'name lastName');

    if (page !== undefined && limit !== undefined) {
      deliveries = deliveries
        .skip(page * limit)
        .limit(limit)
    }

    deliveries = await deliveries
      .sort({ deliveryDate: 'desc' })
      .lean()
      .exec();

    const count = await models.Delivery.countDocuments(filter).exec();

    res.status(200).send({
      total: count,
      pageSize: deliveries.length,
      data: deliveries
    });
    return;
  }

  async changeCard(req, res) {
    await models.Delivery.updateOne({ _id: req.params.id, customer: req.currentUser._id }, {
      cardId: req.body.cardId
    }).exec();
    return res.status(200).send({ tradCode: TranslateCode.SUCCESS_EDIT_CARD_DELIVERY })
  }

  async updateDelivererByAdmin(req, res) {

    const deliverer = await models.User.findOne({
      _id: req.body.deliverer,
      role: UserRole.ROLE_DELIVERER
    }).exec();

    if (!deliverer) {
      return res.status(404).send({ tradCode: TranslateCode.DELIVERER_NOT_FOUND });
    }

    // Check if deliverer have already an delivery
    const deliveriesInDelivery = await models.Delivery
      .find({ $and: [{ 'deliverer': deliverer._id }, { status: { $in: ['VALID', DeliveryStatus.IN_DELIVERY] } }] })
      .exec();
    if (deliveriesInDelivery.length > 0) {
      return res.status(400).send({ tradCode: TranslateCode.ALREADY_ASSIGNED_TO_ONE_DELIVERY });
    }

    await models.Delivery.updateOne({ _id: req.params.id, status: DeliveryStatus.STAND_BY }, {
      deliverer: req.body.deliverer,
      status: DeliveryStatus.IN_DELIVERY,
    }).exec();


    const dataNotification = {
      notificationType: 'DELIVERY_AVAILABLE'
    }

    const notificationService = new NotificationService();
    await notificationService.notify(deliverer._id, 'Nouvelle livraison disponible',
      "Une nouvelle demande de livraison est disponible", dataNotification);

    return res.status(200).send({ tradCode: TranslateCode.UPDATE_DELIVERY_SUCCESS });
  }

  async checkIfItsMyDeliveryAndCheckValidity(req, res) {
    const delivery = await models.Delivery
      .findOne({ $and: [{ _id: req.params.id }, { 'deliverer': req.currentUser._id }, { status: { $in: ['IN_DELIVERY'] } }] })
      .exec();
    let response = { valid: ((delivery) ? true : false) };
    return res.status(200).send(response);
  }

  async changeStatus(req, res) {
    await models.Delivery.updateOne({ _id: req.params.id, status: { $in: [DeliveryStatus.VALID, DeliveryStatus.IN_DELIVERY] } }, { status: DeliveryStatus.STAND_BY, deliverer: null }).exec();
    return res.status(200).send({ tradCode: TranslateCode.UPDATE_DELIVERY_SUCCESS });
  }

  async changeStatusToDelivered(req, res) {

    await models.Delivery.updateOne({ _id: req.params.id, status: { $in: [DeliveryStatus.IN_DELIVERY] } }, { status: DeliveryStatus.DELIVERED }).exec();


    (async () => {
      const delivery = await models.Delivery.findOne({ _id: req.params.id })
        .populate('invoice')
        .populate('merchant')
        .populate('deliverer')
        .exec();
      const deliveryData = { id: delivery._id, reference: delivery.reference, paymentId: delivery.preAuthorizationId };
      const transferToMerchantData = { accountId: delivery.merchant.accountId, amount: delivery.invoice.amountMerchant };
      const transferToDelivererData = { accountId: delivery.deliverer.accountId, amount: delivery.invoice.amountDeliverer};
      const paymentService = new PaymentService();
      const result = await paymentService.payMultiPart(deliveryData, transferToMerchantData, transferToDelivererData);

      let updateParams = {};
      if (result.transferToMerchant) {
        updateParams.transferToMerchant = result.transferToMerchant.id;
      }

      if (result.transferToDeliverer) {
        updateParams.transferToDeliverer = result.transferToDeliverer.id;
      }

      await models.Invoice.updateOne({ _id: delivery.invoice._id }, updateParams).exec();
    })()

    await models.ChatSession.deleteMany({ delivery: req.params.id }).exec();
    return res.status(200).send({ tradCode: TranslateCode.UPDATE_DELIVERY_SUCCESS });
  }
}
