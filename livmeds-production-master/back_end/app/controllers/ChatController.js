const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypePrivateMessage = require('../enums/TypePrivateMessage');
const TchatService = require('../utils/TchatService');
const NotificationService = require('../utils/NotificationService');
const mongoose = require('mongoose');

const notificationService = new NotificationService();
const tchatService = new TchatService();
// class pour les chats
module.exports = class {
// ajouter un chat avec token jwt
  async createTchatToken(req, res) {
    if (await tchatService.getUser(req.currentUser._id)) {
      const token = await tchatService.getAuthToken(req.currentUser._id)
      return res.status(200).send({ token });
    } else {
      const token = await tchatService.createUser(req.currentUser, true)
      return res.status(200).send({ token });
    }
  }
// ajouter une session de chat pour la commande
  async createSessionForDelivery(req, res) {
    const body = filterKeys(req.body, ['receiverId', 'sessionId', 'type', 'authToken', 'delivery']);

    try {
      const data = await tchatService.getAuthTokenData(req.currentUser._id, body.authToken);

      const newEntry = new models.ChatSession({
        sessionId: body.sessionId,
        type: body.type,
        delivery: body.delivery,
        receiverId: body.receiverId,
        senderId: req.currentUser._id,
      });

      await newEntry.save();

      const dataNotification = {
        notificationType: 'CALL_INCOMING',
        sessionId: body.sessionId,
        type: body.type
      }

      const notificationService = new NotificationService();

      await notificationService.notify(body.receiverId, 'Appel vidéo',
        "Votre pharmacien cherche à vous contacter", dataNotification);

      return res.status(200).send({ tradCode: TranslateCode.SUCCESS_CREATE_CHATSESSION });
    } catch (error) {
      console.log("DEBUG: " + error)
      return res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
    }
  }
// recuperer la session du chat
  async getChatSession(req, res) {

    const chatSession = await models.ChatSession.findOne({
      "sessionId": req.params.sessionId,
      $or: [{ "receiverId": new mongoose.Types.ObjectId(req.currentUser._id) }, { "senderId": new mongoose.Types.ObjectId(req.currentUser._id) }]
    }).lean().exec();

    if (chatSession) {
      return res.status(200).send({ data: chatSession });
    } else {
      return res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
    }
  }
}
