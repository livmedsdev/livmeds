const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypeBonus = require('../enums/TypeBonus');
const FileService = require('../utils/FileService');
const BonusStatus = require('../enums/BonusStatus');
// class qui gere les clients
module.exports = class {

    // recuper les client
    async list(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;
        const text = req.query.text;

        let filter = {merchant: req.currentUser._id}
        if (text) {
            // filter.$text = { $search: text }
            filter.$or = [{ name: { "$regex": text, "$options": "i" } }, { lastname: { "$regex": text, "$options": "i" } }]
        }
        const list = await models.Customer.find(filter)
            .populate('address')
            .sort({ type: 'asc' })
            .skip(page * limit)
            .limit(limit)
            .exec();

        const count = await models.Customer.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: list.length,
            data: list
        });
        return;
    }


    async get(req, res) {
        const id = req.params.id;
        const obj = await models.Customer.findOne({ _id: id })
            .populate('address')
            .exec();
        if (obj) {
            res.status(200).send(obj);
        } else {
            res.status(404).send({ tradCode: TranslateCode.CUSTOMER_NOT_FOUND });
        }
        return;
    }
// cree une client
    async create(req, res) {
        const params = filterKeys(req.body, ['name', 'lastname', 'address', 'phoneNumber']);
        params.address = (params.address) ? JSON.parse(params.address) : undefined;

        const obj = new models.Customer(params);
        obj.merchant = req.currentUser._id;
        // Add address
        const addressObj = new models.Address(params.address);
        addressObj.label = 'Main';
        addressObj.favoriteAddress = true;
        addressObj.geo = { type: 'Point', coordinates: [params.address.longitude, params.address.latitude] }
        const address = await addressObj.save();
        obj.address = address;

        const objCreated = await obj.save();


        try {
            if(!req.files){
                throw TranslateCode.SOCIAL_SECURITY_NUMBER_NOT_FILL;
            }
            // Upload images
            const fileService = new FileService();
            if (req.files.socialSecurityNumber) {
                const image = await fileService.uploadMedicalData(req.files.socialSecurityNumber[0], req.currentUser._id, 'socialSecurityNumber');
                await models.Customer.updateOne({ _id: objCreated._id }, {
                    socialSecurityNumber: image
                }).exec();
            } else {
                throw TranslateCode.SOCIAL_SECURITY_NUMBER_NOT_FILL;
            }

            if (req.files.mutual) {
                const image = await fileService.uploadMedicalData(req.files.mutual[0], req.currentUser._id, 'mutual');
                await models.Customer.updateOne({ _id: objCreated._id }, {
                    mutual: image
                }).exec();
            } else {
                throw TranslateCode.MUTUAL_NOT_FILL;
            }
        } catch (error) {
            console.log(error)
            await models.Customer.deleteOne({_id: user._id}).exec();
            res.status(400).send({ tradCode: error });
            return
        }


        res.status(200).send(objCreated);
        return;
    }
// modifier un client
    async edit(req, res) {
        const id = req.params.id;

        const params = filterKeys(req.body, ['name', 'lastname', 'mutual', 'socialSecurityNumber', 'address', 'phoneNumber']);
        params.address = (params.address) ? JSON.parse(params.address) : undefined;
        delete params.address['_id'];
        const customer = await models.Customer.findOne({ _id: id }).exec();

        if (customer) {

            if (params.address._id !== customer.address._id) {
                const oldAddress = await models.Address.findOne({ _id: customer.address._id }).exec();
                const addressObj = new models.Address(params.address);
                addressObj.label = 'Main';
                addressObj.favoriteAddress = true;
                addressObj.geo = { type: 'Point', coordinates: [params.address.longitude, params.address.latitude] }
                const address = await addressObj.save();
                params.address = address;
                if(oldAddress){
                    await oldAddress.remove();
                }
            }

            try {
                // Upload images
                const fileService = new FileService();
                if (!params.socialSecurityNumber) {
                    const image = await fileService.uploadMedicalData(req.files.socialSecurityNumber[0], req.currentUser._id, 'socialSecurityNumber');
                    params.socialSecurityNumber = image;
                }

                if (!params.mutual) {
                    const image = await fileService.uploadMedicalData(req.files.mutual[0], req.currentUser._id, 'mutual');
                    params.mutual = image;
                }
            } catch (error) {
                console.log(error)
                res.status(400).send({ tradCode: error });
                return
            }

            const response = await models.Customer.updateOne(
                { _id: id },
                params
            ).exec();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.CUSTOMER_NOT_FOUND });
        }
    }
// suprrimer un client
    async delete(req, res) {
        const id = req.params.id;
        const customer = await models.Customer.findOne({ _id: id, merchant: req.currentUser._id }).exec();
        if (customer) {
            await customer.remove();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.CUSTOMER_NOT_FOUND });
            return;
        }
    }

}
