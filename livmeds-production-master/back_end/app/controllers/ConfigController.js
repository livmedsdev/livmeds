const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypeBonus = require('../enums/TypeBonus');
const BonusStatus = require('../enums/BonusStatus');

module.exports = class {
// recuperer le langage 
    async get(req, res) {
        const obj = await models.Config.findOne({ label: 'DEFAULT' }).exec();
        if (obj) {
            res.status(200).send(obj);
        } else {
            res.status(404).send({ tradCode: TranslateCode.CONFIG_NOT_FOUND });
        }
        return;
    }
// modifier le langage
    async edit(req, res) {
        const params = filterKeys(req.body, ['categoriesAvailable', 'versionIosMin', 'versionAndroidMin']);
        const config = await models.Config.findOne({ label: 'DEFAULT' }).exec();

        if (config) {

            const response = await models.Config.updateOne(
                { _id: config._id },
                params
            ).exec();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.CONFIG_NOT_FOUND });
        }
    }
// recuper la version
    async getVersions(req, res){
        const config = await models.Config.findOne({ label: 'DEFAULT' }).exec();
        return res.status(200).send({
            versionIosMin: config.versionIosMin,
            versionAndroidMin: config.versionAndroidMin
        });
    }
}
