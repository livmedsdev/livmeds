const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypeBonus = require('../enums/TypeBonus');
const BonusStatus = require('../enums/BonusStatus');
const MailService = require('../utils/MailService');
const moment = require('moment');
const momentTimezone = require('moment-timezone');
require('moment/locale/fr');

const mailService = new MailService();

module.exports = class {
// afficher les compte
    async list(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;

        const filter = {}

        const messages = await models.Contact.find(filter)
            .skip(page * limit)
            .limit(limit)
            .sort({ insertAt: 'desc' })
            .exec();

        const count = await models.Contact.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: messages.length,
            data: messages
        });
        return;
    }

// recuper les comptes
    async get(req, res) {
        const idMessage = req.params.id;
        const message = await models.Contact.findOne({ _id: idMessage })
            .exec();
        if (message) {
            res.status(200).send(message);
        } else {
            res.status(404).send({ tradCode: TranslateCode.MESSAGE_NOT_FOUND });
        }
        return;
    }
// cree un compte
    async create(req, res) {
        const params = filterKeys(req.body, ['name', 'mail', 'subject', 'message']);
        const messageObj = new models.Contact(params);
        const messageCreated = await messageObj.save();

        if (process.env.NODE_ENV === 'PROD') {
            try {
                const date = moment(new Date());
                date.tz('Europe/Paris');
                const topicStaff = "Nouveau message";
                const htmlStaff = await mailService.getEmailHtml('app/resources/email/notif-new-message-contact-admin.html', {
                    date: date.format('DD/MM/YYYY HH:mm'),
                    mail: messageCreated.mail,
                    subject: messageCreated.subject,
                    message: messageCreated.message,
                });

                mailService.sendMail(process.env.NOTIFICATION_MAIL_ADMIN, topicStaff, htmlStaff);
            } catch (error) {
                console.log('ERROR TO SEND MAIL: ' + error)
            }
        }

        return res.status(200).send({ data: messageCreated, tradCode: TranslateCode.MESSAGE_CREATED_SUCCESS });
    }
// supprimer un compte
    async delete(req, res) {
        const idMessage = req.params.id;
        const message = await models.Contact.findById(idMessage).exec();
        if (message) {
            await message.remove();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.MESSAGE_NOT_FOUND });
            return;
        }
    }
// modifier le status du comptacte
    async updateStatus(req, res) {
        const params = filterKeys(req.body, ['status']);
        const id = req.params.id;
        const message = await models.Contact.findById(id).exec();
        if (message) {
            await models.Contact.updateOne({ _id: message._id }, params).exec();
            res.status(200).send();
        } else {
            res.status(404).send({ tradCode: TranslateCode.SUPPORT_NOT_FOUND });
            return;
        }
        // send email
    }
}
