const models = require("../models");
const bcrypt = require('bcrypt');
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const uuidv4 = require('uuid/v4');
const crypto = require("crypto")

// classe qui gere les partenaire
module.exports = class {
// ajouter un partenaire 
    async add(req, res) {
        const params = filterKeys(req.body, ['mail', 'name', 'phoneNumber', 'address', 'siret', 'rib', 'bic', 'responsibleName', 'responsibleLastname']);
        params.accessKey = uuidv4();
        params.secretKeyPlain = crypto.randomBytes(256).toString('hex');
        params.usedCode = []

        const addressObj = new models.Address(params.address);
        addressObj.label = 'Main';
        addressObj.favoriteAddress = true;
        addressObj.geo = { type: 'Point', coordinates: [params.address.longitude, params.address.latitude] }
        const address = await addressObj.save();
        params.address = address;
        
        // Hash pass
        params.secretKey = await bcrypt.hash(params.secretKeyPlain, parseInt(process.env.SALTROUNDS));

        const partner = new models.Partner(params);
        await partner.save();
        
        return res.status(200).send({data: {
            accessKey: params.accessKey,
            secretKey: params.secretKeyPlain,
        }})
    }
// modifier les donner du partenaire
    async update(req, res) {
        const id = req.params.id;
        const params = filterKeys(req.body, ['mail', 'name', 'phoneNumber', 'address', 'siret', 'rib', 'bic', 'responsibleName', 'responsibleLastname']);

        const part = await models.Partner.findById(id, '-secretKey')
            .exec();

        if(params.address){
            // Create new address
            const addressObj = new models.Address(params.address);
            addressObj.label = 'Main';
            addressObj.favoriteAddress = true;
            addressObj.geo = { type: 'Point', coordinates: [params.address.longitude, params.address.latitude] }
            const address = await addressObj.save();
            params.address = address;

            await models.Partner.deleteOne({_id: part.address}).exec();
        }

        await models.Partner.updateOne({_id: id}, params).exec()
    
        return res.status(200).send({});
    }
// afficher les partenaire
    async list(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;

        const filter = {}

        const data = await models.Partner.find(filter, '-secretKey')
            .populate('address')
            .skip(page * limit)
            .limit(limit)
            .exec();

        const count = await models.Partner.countDocuments(filter).exec();

        return res.status(200).send({
            total: count,
            pageSize: data.length,
            data: data
        });
    }

    async get(req, res) {
        const id = req.params.id;

        const data = await models.Partner.findById(id, '-secretKey')
            .populate('address')
            .exec();
        
        return res.status(200).send(data)
    }
// generer une cle secret 
    async generateSecretKey(req, res) {
        const id = req.params.id;
        const secretKeyPlain = crypto.randomBytes(256).toString('hex');
        const secretKey = await bcrypt.hash(secretKeyPlain, parseInt(process.env.SALTROUNDS));

        await models.Partner.updateOne({_id: id}, {
            secretKey
        }).exec()
    
        return res.status(200).send({data: {
            secretKey: secretKeyPlain,
        }})
    }


    async updateStatus(req, res) {
        const id = req.params.id;
        const params = filterKeys(req.body, ['status']);

        await models.Partner.updateOne({_id: id}, params).exec()
    
        return res.status(200).send({});
    }
}
