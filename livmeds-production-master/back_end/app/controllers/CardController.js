const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const UserRole = require('../enums/UserRole');

const PaymentService = require('../utils/PaymentService');
const paymentService = new PaymentService();

const AddressService = require('../utils/AddressService');
const addressService = new AddressService();
// classe qui permet de gerer les carte et compte bancaire
module.exports = class {
// afficher la cartes
    async list(req, res) {
        res.status(200).send({
            data: await paymentService.listCardsByUser(req.currentUser.customerIdPayment)
        });
        return;
    }
// recuper la cartes du db
    async get(req, res) {
        const cardId = req.params.id;
        const response = await paymentService.getCardById(cardId);
        if (response.UserId === req.currentUser.customerIdPayment) {
            res.status(200).send(response);
            return;
        } else {
            res.status(403).send();
            return
        }
    }
// supprimer la cartes du db
    async delete(req, res) {
        const cardId = req.params.id;
        try {
            const response = await paymentService.deleteCard(cardId);
            res.status(200).send({ tradCode: TranslateCode.SUCCESS_CARD_DELETED });
        } catch (error) {
            console.log(error)
            res.status(403).send();
            return
        }
    }
// recuper la cartes du db
    async setDefaultCardId(req, res) {
        const id = req.params.id;
        const response = await models.User.updateOne({
            _id: req.currentUser._id
        }, {
            defaultCardId: id
        }).exec();
        res.status(200).send();
        return;
    }
// cree une carte local
    async addCardIntent(req, res) {
        const intent = await paymentService.createSetupIntent(req.currentUser.customerIdPayment)
        res.status(200).send({ client_secret: intent.client_secret });
    }

    async getConnectedAccountByAdmin(req, res) {
        const params = filterKeys(req.body, ['bankAccountToken', 'taxId', 'dob']);
        console.log(params)
        try {
            const user = await models.User.findOne({ _id: req.params.id })
                .populate("addresses")
                .exec();

            if (!user.accountId || ![UserRole.ROLE_DELIVERER, UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY].includes(user.role)) {
                return res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
            }

            const account = await paymentService.getAccount(user.accountId);
            const person = (await paymentService.getPerson(user.accountId)).data[0];

            return res.status(200).send({
                data: {
                    account_holder_name: account.external_accounts.data[0].account_holder_name,
                    last4: account.external_accounts.data[0].last4,
                    payoutDay: account.settings.payouts.schedule.monthly_anchor,
                    companyFront: account.company.verification.document.front,
                    companyBack: account.company.verification.document.back,
                    responsibleFront: person.verification.document.front,
                    responsibleBack: person.verification.document.back,
                    responsibleDob: person.dob
                }
            });
        } catch (error) {
            console.log(error);
            return res.status(500).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
        }
    }
// ajouter compte bancaire par l admin
    async addBankAccountByAdmin(req, res) {
        const params = filterKeys(req.body, ['bankAccountToken', 'taxId', 'dob', 'payoutDay']);
        params.dob = filterKeys(JSON.parse(req.body.dob), ['day', 'month', 'year']);
        console.log(params)
        try {
            const user = await models.User.findOne({ _id: req.params.id })
                .populate("addresses")
                .exec();

            const result = await paymentService.addBankAccount(user, params, req.files);

            await models.User.updateOne({
                _id: user._id
            }, {
                bankAccountId: result.bankAccountId,
                accountId: result.accountId
            });
            return res.status(200).send({ tradCode: TranslateCode.SUCCESS_ADD_BANK_ACCOUNT })
        } catch (error) {
            console.log(error);
            return res.status(500).send({ tradCode: TranslateCode.ERROR_ADD_BANK_ACCOUNT });
        }
    }
// modifier compte bancaire par admin
    async editBankAccountByAdmin(req, res) {
        const params = filterKeys(req.body, ['bankAccountToken', 'taxId', 'dob', 'payoutDay']);
        params.dob = filterKeys(JSON.parse(req.body.dob), ['day', 'month', 'year']);
        console.log(params)
        try {
            const user = await models.User.findOne({ _id: req.params.id })
                .populate("addresses")
                .exec();

            const result = await paymentService.editBankAccount(user, params, req.files);

            if (result.bankAccount) {
                await models.User.updateOne({
                    _id: user._id
                }, {
                    bankAccountId: result.bankAccount.id
                });
            }

            return res.status(200).send({ tradCode: TranslateCode.SUCCESS_EDIT_BANK_ACCOUNT })
        } catch (error) {
            console.log(error);
            return res.status(500).send({ tradCode: TranslateCode.ERROR_EDIT_BANK_ACCOUNT });
        }
    }
//  ajouter compte bancaire
    async addBankAccount(req, res) {
        const params = filterKeys(req.body, ['bankAccountToken', 'taxId', 'dob', 'payoutDay']);
        params.dob = filterKeys(JSON.parse(req.body.dob), ['day', 'month', 'year']);
        console.log(params)
        try {
            const user = await models.User.findOne({ _id: req.currentUser._id })
                .populate("addresses")
                .exec();

            const result = await paymentService.addBankAccount(user, params, req.files);

            await models.User.updateOne({
                _id: user._id
            }, {
                bankAccountId: result.bankAccountId,
                accountId: result.accountId
            });
            return res.status(200).send({ tradCode: TranslateCode.SUCCESS_ADD_BANK_ACCOUNT })
        } catch (error) {
            console.log(error);
            return res.status(500).send({ tradCode: TranslateCode.ERROR_ADD_BANK_ACCOUNT });
        }
    }
// modifier compte bancaire
    async editBankAccount(req, res) {
        const params = filterKeys(req.body, ['bankAccountToken', 'taxId', 'dob', 'payoutDay']);
        params.dob = filterKeys(JSON.parse(req.body.dob), ['day', 'month', 'year']);
        console.log(params)
        try {
            const user = await models.User.findOne({ _id: req.currentUser._id })
                .populate("addresses")
                .exec();

            const result = await paymentService.editBankAccount(user, params, req.files);

            if (result.bankAccount) {
                await models.User.updateOne({
                    _id: user._id
                }, {
                    bankAccountId: result.bankAccount.id
                });
            }

            return res.status(200).send({ tradCode: TranslateCode.SUCCESS_EDIT_BANK_ACCOUNT })
        } catch (error) {
            console.log(error);
            return res.status(500).send({ tradCode: TranslateCode.ERROR_EDIT_BANK_ACCOUNT });
        }
    }
// recuperer la connection du compte
    async getConnectedAccount(req, res) {
        const params = filterKeys(req.body, ['bankAccountToken', 'taxId', 'dob']);
        console.log(params)
        try {
            const user = await models.User.findOne({ _id: req.currentUser._id })
                .populate("addresses")
                .exec();

            if (!user.accountId || ![UserRole.ROLE_DELIVERER, UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY, UserRole.ROLE_VETERINARY].includes(user.role)) {
                return res.status(403).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
            }

            const account = await paymentService.getAccount(user.accountId);
            const person = (await paymentService.getPerson(user.accountId)).data[0];

            return res.status(200).send({
                data: {
                    account_holder_name: account.external_accounts.data[0].account_holder_name,
                    last4: account.external_accounts.data[0].last4,
                    payoutDay: account.settings.payouts.schedule.monthly_anchor,
                    companyFront: account.company.verification.document.front,
                    companyBack: account.company.verification.document.back,
                    responsibleFront: person.verification.document.front,
                    responsibleBack: person.verification.document.back,
                    responsibleDob: person.dob
                }
            });
        } catch (error) {
            console.log(error);
            return res.status(500).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
        }
    }
}
