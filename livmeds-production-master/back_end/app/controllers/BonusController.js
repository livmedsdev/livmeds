const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypeBonus = require('../enums/TypeBonus');
const BonusStatus = require('../enums/BonusStatus');

module.exports = class {

    // Bonus cree une prime 
    async getPrimes(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;
        const type = req.query.type;

        const filter = {}
        if (type) {
            filter.type = type;
        }

        const bonus = await models.Bonus.find(filter)
            .sort({type: 'asc'})
            .skip(page * limit)
            .limit(limit)
            .exec();

        const count = await models.Bonus.countDocuments(filter).exec();

        res.status(200).send({
            total: count,
            pageSize: bonus.length,
            data: bonus
        });
        return;
    }

    // recuperet une promotion sur la DB
    async getBonus(req, res){
        const idBonus = req.params.id;
        const bonus = await models.Bonus.findOne({ _id: idBonus })
        .exec();
        if(bonus){
            res.status(200).send(bonus);
        }else{
            res.status(404).send({ tradCode: TranslateCode.BONUS_NOT_FOUND });
        }
        return;
    }
// cree une promotion 
    async createBonus(req, res) {
        const bonus = filterKeys(req.body, ['type', 'startFrom', 'endAt', 'placeId', 'city', 'timeToUse']);
        const typeExist = Object.values(TypeBonus).find((elem) =>{
            return elem === bonus.type
        });
        if (typeExist) {
            const bonusObj = new models.Bonus(bonus);
            bonusObj.status = BonusStatus.ENABLED;
            const bonusCreated = await bonusObj.save();
            res.status(200).send(bonusCreated);
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.TYPE_BONUS_NOT_FOUND });
            return;
        }

    }
// modifier une promotion 
    async editBonus(req, res) {
        const idBonus = req.params.id;
        const bonus = filterKeys(req.body, ['type', 'startFrom', 'endAt', 'placeId', 'city']);
        const typeExist = Object.values(TypeBonus).find((elem) =>{
            return elem === bonus.type
        });
        if (typeExist) {
            const response = await models.Bonus.updateOne(
                { _id: idBonus },
                bonus
            ).exec();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.TYPE_BONUS_NOT_FOUND });
            return;
        }
    }
// modifier l etat de la une promotion 
    async updateStatus(req, res) {
        const id = req.params.id;
        const status = req.body.status;
        const response = await models.Bonus.updateOne(
            { _id: id },
            {status: status}
        ).exec();
        res.status(200).send({ tradCode: TranslateCode.SUCCESS_STATUS_CHANGE });
        return;
    }
// supprrimer une promotion 
    async deleteBonus(req, res) {
        const idBonus = req.params.id;
        const bonus = await models.Bonus.findOne({ _id: idBonus }).exec();
        if (bonus) {
            await bonus.remove();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.BONUS_NOT_FOUND });
            return;
        }
    }

}
