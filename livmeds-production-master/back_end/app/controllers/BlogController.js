const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
// class qui gere le blog
module.exports = class {
// permet de recuperer un sujet dans le blog
    async getTopics(req, res){
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;
        const text = req.query.text;
        const category = req.query.category;

        const filter = {}
        if(text){
            filter.$text = {$search: text}
        }
        if(category){
            filter.category = category;
        }

        const topics = await models.Topic.find(filter)
        .populate('author', 'lastName name avatar')
        .populate('category')
        .skip(page * limit)
        .limit(limit)
        .sort({ insertAt: 'desc' }).exec();

        const count = await models.Topic.countDocuments(filter).exec();
        
        res.status(200).send({
            total: count,
            pageSize: topics.length,
            data: topics
        });
        return;
    }
// permet de recuperer un messeage dans le blog
    async getMessages(req, res){
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;

        const idTopic = req.params.id;
        const messages = await models.Message.find({ topic: idTopic })
        .populate('author', 'lastName name avatar')
        .skip(page * limit)
        .limit(limit)
        .sort({ insertAt: 'asc' }).exec();

        const count = await models.Message.countDocuments().exec();

        res.status(200).send({
            total: count,
            pageSize: messages.length,
            data: messages
        });
        return;
    }
// permet de recuperer un topic dans le blog
    async getTopic(req, res){
        const idTopic = req.params.id;
        const topic = await models.Topic.findOne({ _id: idTopic })
        .populate('author', 'lastName name avatar')
        .populate('category')
        .populate({
            path: 'messages',
            populate: { path: 'author', select: 'lastName name avatar'}
        })
        .exec();
        if(topic){
            res.status(200).send(topic);
        }else{
            res.status(404).send({ tradCode: TranslateCode.TOPIC_NOT_FOUND });
        }
        return;
    }
// permet de recuperer un sujet dans le blog
    async createTopic(req, res){
        const topic = filterKeys(req.body, ['title', 'description', 'category']);
        topic.author = req.currentUser._id;
        const topicObj = new models.Topic(topic);
        topicObj.messages = [];
        const topicCreated = await topicObj.save();
        res.status(200).send(topicCreated);
        return;
    }
// permet de cree un message dans le blog
    async createMessage(req, res){
        const message = filterKeys(req.body, ['content', 'topic']);
        message.author = req.currentUser._id;

        const topic = await models.Topic.findOne({ _id: message.topic }).exec();
        if(topic){
            const messageObj = new models.Message(message);
            const messageCreated = await messageObj.save();
            topic.messages.push(messageCreated);
            await topic.save();

            res.status(200).send(messageCreated);
            return;
        }else{
            res.status(404).send({ tradCode: TranslateCode.TOPIC_NOT_FOUND });
            return;
        }
    }
// permet de recuperer un Categorie dans le blog
    async getCategories(req, res){
        const categories = await models.Category.find().exec();
        res.status(200).send(categories);
        return;
    }
// permet de supprimer un sujet dans le blog
    async deleteTopic(req, res){
        const idTopic = req.params.id;
        const topic = await models.Topic.findOne({ _id: idTopic }).exec();
        if(topic){
            await models.Topic.deleteMany({_id : { $in: topic.messages }})
            await topic.remove();
            res.status(200).send();
            return;
        }else{
            res.status(404).send({ tradCode: TranslateCode.TOPIC_NOT_FOUND });
            return;
        }
    }
// permet de modifier un sujet dans le blog
    async editTopic(req, res){
        const idTopic = req.params.id;
        const topic = filterKeys(req.body, ['title', 'description', 'category']);
        const response = await models.Topic.updateOne(
            { _id: idTopic },
            topic
        ).exec();
        res.status(200).send();
        return;
    }
// permet de supprimer un message dans le blog
    async deleteMessage(req, res){
        const idMessage = req.params.id;
        const message = await models.Message.findOne({ _id: idMessage }).exec();
        if(message){
            const response = await models.Topic.updateOne(
                { _id: message.topic },
                { $pull : { messages :  idMessage} }
            ).exec();
            message.remove();
            res.status(200).send();
            return;
        }else{
            res.status(404).send({ tradCode: TranslateCode.MESSAGE_NOT_FOUND });
            return;
        }
    }
// permet de conter nbr de message dans le blog
    async getStat(req, res){
        const countMessage = await models.Message.countDocuments().exec();
        return res.status(200).send({nbMessages: countMessage});
    }
}
