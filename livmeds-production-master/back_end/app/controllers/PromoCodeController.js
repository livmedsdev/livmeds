const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const PromoCodeStatus = require('../enums/PromoCodeStatus');
const voucher_codes = require('voucher-code-generator');

module.exports = class {

    // recuper un code promo de la db
    async getPromoCodes(req, res) {
        const page = parseInt(req.query.page) || 0; //for next page pass 1 here
        const limit = parseInt(req.query.limit) || 3;

        const promoCodes = await models.PromoCode.find()
            .sort({ insertAt: 'desc' })
            .skip(page * limit)
            .limit(limit)
            .exec();

        const count = await models.PromoCode.countDocuments().exec();

        res.status(200).send({
            total: count,
            pageSize: promoCodes.length,
            data: promoCodes
        });
        return;
    }

// recuperer un code promo
    async getPromoCode(req, res) {
        const idPromoCode = req.params.id;
        const promoCode = await models.PromoCode.findOne({ _id: idPromoCode })
            .exec();
        if (promoCode) {
            res.status(200).send(promoCode);
        } else {
            res.status(404).send({ tradCode: TranslateCode.PROMO_CODE_NOT_FOUND });
        }
        return;
    }
// cree un code promo
    async createPromoCode(req, res) {
        const params = filterKeys(req.body, ['usedTimeMax', 'promoCode',
            'percentage']);

        params.promoCode = params.promoCode.toUpperCase();
        if (params.percentage < 0 && params.percentage > 100) {
            res.status(400).send({ tradCode: TranslateCode.PERCENTAGE_NOT_VALID });
            return;
        }
        const promoCodeObj = new models.PromoCode(params);
        promoCodeObj.status = PromoCodeStatus.ENABLED;
        const promoCodeCreated = await promoCodeObj.save();
        res.status(200).send(promoCodeCreated);
        return;

    }
// modifier un code promo
    async editPromoCode(req, res) {
        const idPromoCode = req.params.id;
        const params = filterKeys(req.body, ['usedTimeMax', 'promoCode',
            'percentage']);

        if (params.percentage < 0 && params.percentage > 100) {
            res.status(400).send({ tradCode: TranslateCode.PERCENTAGE_NOT_VALID });
            return;
        }

        const response = await models.PromoCode.updateOne(
            { _id: idPromoCode },
            params
        ).exec();
        res.status(200).send();
        return;
    }

    async updateStatus(req, res) {
        const id = req.params.id;
        const status = req.body.status;
        const response = await models.PromoCode.updateOne(
            { _id: id },
            { status: status }
        ).exec();
        res.status(200).send({ tradCode: TranslateCode.SUCCESS_STATUS_CHANGE });
        return;
    }
// suprimer un code promo aved id
    async delete(req, res) {
        const idPromoCode = req.params.id;
        const promoCode = await models.PromoCode.findOne({ _id: idPromoCode }).exec();
        if (promoCode) {
            await promoCode.remove();
            res.status(200).send();
            return;
        } else {
            res.status(404).send({ tradCode: TranslateCode.PROMO_CODE_NOT_FOUND });
            return;
        }
    }
// cree une reference de code promo acec parinage
    async generateReferralCode(req, res) {
        if (!req.currentUser.referralCode) {
            let valid = false;
            do {
                let code = voucher_codes.generate({
                    length: 6,
                    count: 1,
                    charset: voucher_codes.charset("alphabetic"),
                    prefix: "PARRAIN-",
                });
                code = code[0].toUpperCase();

                const userWithThisCode = await models.User.countDocuments({ referralCode: code }).exec();
                if (userWithThisCode === 0) {
                    valid = true;
                    await models.User.updateOne({ _id: req.currentUser._id }, {
                        referralCode: code
                    }).exec();
                    return res.status(200).send({ code: code });
                }
            } while (!valid);
        } else {
            // return referral code already generated
            return res.status(200).send({ code: req.currentUser.referralCode });
        }
    }

    // async addPromoCode(req, res) {

    //     const session = await PromoCode.startSession();
    //     try {
    //         const opts = { session };
    //         const promoCode = await models.PromoCode.findById(req.body.idPromoCode)
    //         let enteredCode = []
    //         let usedCode = [];
    //         // add to account
    //         if (req.currentUser.enteredCode) {
    //             enteredCode = req.currentUser.enteredCode;
    //         }

    //         if(req.currentUser.usedCode){
    //             usedCode = req.currentUser.usedCode;
    //         }

    //         if(usedCode.includes(promoCode._id) || enteredCode.includes(promoCode._id)){
    //             throw TranslateCode.ALREADY_USED_BY_THIS_ACCOUNT;
    //         }

    //         const futurUsedTimes = promoCode.usedTimes + 1;

    //         if(futurUsedTimes <= promoCode.usedTimeMax && promoCode.status === PromoCodeStatus.ENABLED){
    //             enteredCode.push(promoCode._id);
    //             await models.PromoCode.updateOne({_id: promoCode._id}, {usedTimes: futurUsedTimes, usedCode: usedCode}, opts).exec();
    //             await models.User.updateOne({_id: req.currentUser._id}, {enteredCode: enteredCode}, opts).exec();
    //         }else{
    //             throw TranslateCode.PROMO_CODE_INVALID;
    //         }

    //         await session.commitTransaction();
    //         session.endSession();
    //         return res.status(200).send({tradCode: TranslateCode.CODE_ADDED_SUCCESSFULLY});
    //     } catch (error) {
    //         // If an error occurred, abort the whole transaction and
    //         // undo any changes that might have happened
    //         await session.abortTransaction();
    //         session.endSession();
    //         return res.status(400).send({tradCode: error});
    //     }
    // }
}
