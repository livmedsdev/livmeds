const models = require("../models");
const TranslateCode = require('../enums/TranslateCode');
const filterKeys = require('../utils/filterKeys');
const TypeBonus = require('../enums/TypeBonus');
const BonusStatus = require('../enums/BonusStatus');
const UserRole = require('../enums/UserRole');
const SupportTicketStatus = require('../enums/SupportTicketStatus');
const ProductStatus = require('../enums/ProductStatus');

module.exports = class {
// recuper le statu stable des modules utilier 
    async get(req, res) {
        return res.status(200).send({
            nbCustomer: await models.User.countDocuments({role: UserRole.ROLE_CUSTOMER}).exec(),
            nbDelivery: await models.Delivery.countDocuments().exec(),
            nbPharmacy: await models.User.countDocuments({role: UserRole.ROLE_PHARMACY}).exec(),
            nbDeliverer: await models.User.countDocuments({role: UserRole.ROLE_DELIVERER}).exec(),
            nbDoctor: await models.User.countDocuments({role: UserRole.ROLE_DOCTOR}).exec(),
            nbNurse: await models.User.countDocuments({role: UserRole.ROLE_NURSE}).exec(),
            nbOptician: await models.User.countDocuments({role: UserRole.ROLE_OPTICIAN}).exec(),
            nbVeterinary: await models.User.countDocuments({role: UserRole.ROLE_VETERINARY}).exec(),
            nbSupportTicketUnProcessed: await models.Support.countDocuments({status: SupportTicketStatus.UNPROCESSED}).exec(),
            nbPartner: await models.Partner.countDocuments({}).exec(),
            nbMessageReceivedAndUnProcessed: await models.Contact.countDocuments({status: SupportTicketStatus.UNPROCESSED}).exec(),
            nbProductsOnSale: await models.Product.countDocuments({status: ProductStatus.ACTIVE}).exec(),
        })
    }
}
