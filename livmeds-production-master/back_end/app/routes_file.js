const express = require('express');
const FileController = require("./controllers/FileController")
const router = express.Router();
const Auth = require('./middlewares/authentification');
const Multer = require("multer");
const UserRole = require('./enums/UserRole');

module.exports = () => {
    let fileController = new FileController();
    let auth = new Auth();
    let authAdminCustomer = new Auth(['ROLE_ADMIN', 'ROLE_CUSTOMER', UserRole.ROLE_NURSE, UserRole.ROLE_DOCTOR, UserRole.ROLE_DELIVERER, UserRole.ROLE_OPTICIAN, UserRole.ROLE_PHARMACY,UserRole.ROLE_VETERINARY]);
    let authLimitedToMerchant = new Auth([UserRole.ROLE_PHARMACY, UserRole.ROLE_OPTICIAN,UserRole.ROLE_VETERINARY]);
    router.post('/upload/:type', authAdminCustomer.getMiddleware.bind(authAdminCustomer),Multer({storage: Multer.memoryStorage()}).single("file"), fileController.upload);
    router.post('/url', auth.getMiddlewareNotMandatory.bind(auth), fileController.file.bind(fileController));

    router.post('/download-as', authAdminCustomer.getMiddlewareNotMandatory.bind(authAdminCustomer), fileController.downloadAs.bind(fileController));

    return router;
};