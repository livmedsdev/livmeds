const TypeDeliverer = {
    BIKE: "BIKE",
    TRUCK: "TRUCK",
    CARGO_BIKE: "CARGO_BIKE",
    ELECT_SCOOTER: "ELECT_SCOOTER"
};

module.exports = TypeDeliverer;