const SupportTicketStatus = {
    UNPROCESSED: "UNPROCESSED",
    IN_PROCESS: "IN_PROCESS",
    PROCESSED: "PROCESSED"
};

module.exports = SupportTicketStatus;