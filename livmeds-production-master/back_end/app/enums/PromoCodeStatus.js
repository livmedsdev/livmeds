const PromoCodeStatus = {
    ENABLED: "ENABLED",
    DISABLED: "DISABLED"
};

module.exports = PromoCodeStatus;