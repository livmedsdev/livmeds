const PromoCodeType = {
    REGULAR: "REGULAR",
    REFERRAL: "REFERRAL"
};

module.exports = PromoCodeType;