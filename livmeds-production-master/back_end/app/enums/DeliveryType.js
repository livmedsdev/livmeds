const DeliveryType = {
    INTERNAL: "INTERNAL",
    EXTERNAL: "EXTERNAL"
};

module.exports = DeliveryType;