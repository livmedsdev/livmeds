const ConnectionType = {
    MAIL: "MAIL",
    FACEBOOK: "FACEBOOK",
    APPLE: "APPLE"
};

module.exports = ConnectionType;