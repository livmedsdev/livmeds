const UserStatus = {
    ROLE_ADMIN: 'Administrateur',
    ROLE_OPTICIAN: 'Opticien',
    ROLE_PHARMACY: 'Pharmacien',
    ROLE_DELIVERER: 'Livreur',
    ROLE_CUSTOMER: 'Client',
    ROLE_VETERINARY:  'Vétérinaire',
    ROLE_NURSE: 'Infirmier',
    ROLE_DOCTOR: 'Médecin'
};

module.exports = UserStatus;