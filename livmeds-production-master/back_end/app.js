require('dotenv').config()
const fs = require('fs');
const https = require('https');
const express = require('express')
const app = express()
const mongoose = require('mongoose');
const publicDir = require('path').join(__dirname, '/public');
const bodyParser = require('body-parser');
const boom = require('express-boom');
const scheduleDelivery = require('./app/schedule/scheduledDelivery');
const TranslateCode = require('./app/enums/TranslateCode');
const Cabin = require('cabin');
const cabin = new Cabin();

// const basicAuth = require('express-basic-auth');
// const client = require('prom-client')

// Monitoring
// const promBundle = require("express-prom-bundle");
// const metricsMiddleware = promBundle({includeMethod: true, includePath: true, includeStatusCode: true,
//     autoregister: true,
//     promClient: {
//       collectDefaultMetrics: {
//       }
//     }},
//     );

//Configuration of database
const configBdd = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    user: process.env.USER_DB,
    pass: process.env.PASSWORD_DB,
    dbName: process.env.NAME_DB
};

const AddressService = require('./app/utils/AddressService');
const addressService = new AddressService();

// (async() =>{
//     console.log(await addressService.getAddress("1 avenue de suède nice"))
// })()

if (process.env.NODE_ENV === 'DEV') {
    mongoose.connect('mongodb://' + process.env.USER_DB + ':' + process.env.PASSWORD_DB +
        '@' + process.env.HOST_DB + ':' + process.env.PORT_DB + '/' + process.env.NAME_DB
        + '?authSource=admin', configBdd)
        .then(async () => {
            console.log("Connected with database in dev");


            // const findDeliverer = require('./app/utils/findDeliverer');
            // const deliverer = await findDeliverer({
            //     isBtoB: false,
            //     merchant: '5d9d24ba66015d04c92a66e0',
            //     historyDeliverer: []
            // });
            // console.log("deliverer found: " + deliverer._id)
        })
        .catch((error) => {
            console.log("ERROR with database connection : " + error)
            process.exit(1);
        });
} else if (process.env.NODE_ENV === 'PROD') {
    console.log('PROD');
    console.log('Version 1');
    // const certFileBuf = fs.readFileSync('./certificates/rds-combined-ca-bundle.pem');
    // configBdd.sslCA = certFileBuf;
    mongoose.connect(
        'mongodb+srv://' + process.env.HOST_DB + '?retryWrites=true&w=majority', configBdd)
        .then(async () => {
            console.log("Connected with database");
            console.log("No reset database");

            // Test zone
            // const findDeliverer = require('./app/utils/findDeliverer');
            // const deliverer = await findDeliverer({
            //     isBtoB: false,
            //     merchant: '5d9d24ba66015d04c92a66e0',
            //     historyDeliverer: []
            // });
            // console.log("deliverer found: " + deliverer._id)

            // const findDeliverer = require('./app/utils/findDeliverer_mongogeo');
            // let deliverer = await findDeliverer({
            //     isBtoB: false,
            //     merchant: '60376df1c6cbb7001b138057',
            //     historyDeliverer: []
            // });
            // console.log("PHARMABEST (LYON) deliverer found: " + (deliverer?deliverer._id:"NOT FOUND"))

            // deliverer = await findDeliverer({
            //     isBtoB: false,
            //     merchant: '6040abbe2c4844001bf975ba',
            //     historyDeliverer: []
            // });
            // console.log("Bievre (PARIS) deliverer found: " + (deliverer?deliverer._id:"NOT FOUND"))

            // deliverer = await findDeliverer({
            //     isBtoB: false,
            //     merchant: '603f60b1c6cbb7001b13a135',
            //     historyDeliverer: []
            // });
            // console.log("PHARMACIE HENRI IV (PARIS) deliverer found: " + (deliverer?deliverer._id:"NOT FOUND"))
        })
        .catch((error) => {
            console.log("ERROR with database connection : " + error)
            process.exit(1);
        });
}

// app.use(cabin.middleware); 
app.use(boom());
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers, enctype, social_token, device_uuid, appversion");
    next();
});

// app.use('/metrics',basicAuth({
//     users: { 'admin': 'admin' }
// }));

// app.use(metricsMiddleware);

app.use(function (err, req, res, next) {
    console.error(err.stack);
    return res.status(500).send({ tradCode: TranslateCode.NOT_AUTHORIZED });
});

// Routes
const userEndpoint = require("./app/routes.js")();
const fileEndpoint = require("./app/routes_file.js")();
const delivererEndpoint = require("./app/route_deliverer.js")();
const customerEndpoint = require("./app/route_customer.js")();
const merchantEndpoint = require("./app/route_merchant.js")();
const adminEndpoint = require("./app/route_admin.js")();
const medicalEndpoint = require("./app/route_medical_actor.js")();
app.use('/', userEndpoint);
app.use('/file', fileEndpoint);
app.use('/deliverer', delivererEndpoint);
app.use('/customer', customerEndpoint);
app.use('/merchant', merchantEndpoint);
app.use('/medical', medicalEndpoint);
app.use('/admin', adminEndpoint);

app.listen(process.env.PORT, '0.0.0.0', () => {
    console.log('API on port ' + process.env.PORT);
})

if (process.env.ENABLE_SSL === 'true') {
    const privateKey = fs.readFileSync(process.env.SSL_KEY_PATH, 'utf8');
    const certificate = fs.readFileSync(process.env.SSL_CERT_PATH, 'utf8');
    const credentials = { key: privateKey, cert: certificate };
    const httpsServer = https.createServer(credentials, app);

    if (process.env.PORT_SSL) {
        httpsServer.listen(process.env.PORT_SSL, () => {
            console.log('SSL: API on port ' + process.env.PORT_SSL);
        })
    } else {
        httpsServer.listen(8443);
    }
}

// Launch schedule
scheduleDelivery();
