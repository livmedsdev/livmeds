import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { FormBuilder, Validators } from '@angular/forms';
import { PromoCode } from '../models/PromoCode';
import { TranslateService } from '@ngx-translate/core';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { Admin } from '../models/Admin';
import { FileService } from '../file.service';
import { environment } from 'src/environments/environment';
import { CategoryProduct } from '../enums/CategoryProduct';
import { ContactService } from '../contact.service';
import { SupportTicketsService } from '../support-tickets.service';
@Component({
  selector: 'app-support-tickets',
  templateUrl: './supports-tickets.component.html',
  styleUrls: ['./supports-tickets.component.scss']
})
export class SupportTicketsComponent implements OnInit {

  public messagesList: any[] = [];

  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public type: number;

  constructor(private supportService: SupportTicketsService) {
  }

  async ngOnInit() {
    this.type = null;
    this.page = 0;
    this.limit = 5;
    this.messagesList = [];
    await this.refreshList();
  }
  
  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    const response: any = await this.supportService.list(filter);
    this.messagesList = response.data;
    this.nbTotal = response.total;
  }

  setSelectedType(type) {
    this.type = type;
    this.refreshList();
  }

  async deleteElement(id) {
    try {
      await this.supportService.delete(id);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }


  async updateStatus(id, status) {
    try {
      await this.supportService.updateStatus(id, status);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }
}
