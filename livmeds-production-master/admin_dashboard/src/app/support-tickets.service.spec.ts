import { TestBed } from '@angular/core/testing';

import { SupportTicketsService } from './support-tickets.service';

describe('SupportTicketsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SupportTicketsService = TestBed.get(SupportTicketsService);
    expect(service).toBeTruthy();
  });
});
