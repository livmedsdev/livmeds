import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { FormBuilder, Validators } from '@angular/forms';
import { PromoCode } from '../models/PromoCode';
import { TranslateService } from '@ngx-translate/core';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { Admin } from '../models/Admin';
import { FileService } from '../file.service';
import { environment } from 'src/environments/environment';
import { CategoryProduct } from '../enums/CategoryProduct';
import { ContactService } from '../contact.service';
import { DeliveryService } from '../delivery.service';
import { ExcelService } from '../excel.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormatService } from '../format.service';
@Component({
  selector: 'app-bills',
  templateUrl: './bills.component.html',
  styleUrls: ['./bills.component.scss']
})
export class BillsComponent implements OnInit {

  public messagesList: any[] = [];
  public title;
  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public type: string;

  constructor(private deliveryService: DeliveryService,
    private excelService: ExcelService,
    private route: ActivatedRoute,
    public formatService: FormatService) {
  }

  async ngOnInit() {
    this.page = 0;
    this.limit = 5;
    this.messagesList = [];
    this.route.queryParams.subscribe(param => {
      this.type = param.type ? param.type : "INTERNAL";
      this.title = `TITLE_BILLS_${this.type.toUpperCase()}`;
      this.refreshList()
    });
  }

  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit,
      type: this.type
    };

    const response: any = await this.deliveryService.getListDelivered(filter);
    this.messagesList = response.data;
    this.nbTotal = response.total;
  }

  async generateExcel() {
    let header;

    if (this.type === 'INTERNAL') {
      header = [
        'Date de création',
        'Référence',
        'Client',
        'Montant pour le client',
        'Marchand',
        'Montant pour le marchand',
        'Livreur',
        'Montant pour le livreur',
        'Montant pour la plateforme',
        'Montant code promotion'
      ]
    } else {
      header = [
        'Date de création',
        'Référence',
        'Montant',
        'Partenaire',
        'Livreur',
        'Montant pour le livreur',
        'Montant pour la plateforme',
        'Montant code promotion'
      ]
    }

    const data = [];

    const response: any = await this.deliveryService.getListDelivered({});

    for (let delivery of response.data) {
      let row;
      if (this.type === 'INTERNAL') {
        let customer = "N/A";
        if(delivery.customer){
          customer = delivery.customer.lastName + ' ' + delivery.customer.name;
        }

        row = [
          new DatePipe('fr-FR').transform(delivery.deliveryDate, 'dd LLLL yyyy HH:mm'),
          delivery.reference,
          customer,
          (this.formatService.formatNumber(delivery.invoice.amountCustomer)),
          delivery.merchant.name,
          (this.formatService.formatNumber(delivery.invoice.amountMerchant)),
          delivery.deliverer.lastName + ' ' + delivery.deliverer.name,
          (this.formatService.formatNumber(delivery.invoice.amountDeliverer)),
          (this.formatService.formatNumber(delivery.invoice.platformFee)),
          (this.formatService.formatNumber(delivery.invoice.promoAmount))
        ]
      } else {
        row = [
          new DatePipe('fr-FR').transform(delivery.deliveryDate, 'dd LLLL yyyy HH:mm'),
          delivery.reference,
          (this.formatService.formatNumber(delivery.invoice.amountCustomer)),
          delivery.partner.name,
          delivery.deliverer.lastName + ' ' + delivery.deliverer.name,
          (this.formatService.formatNumber(delivery.invoice.amountDeliverer)),
          (this.formatService.formatNumber(delivery.invoice.platformFee)),
            (this.formatService.formatNumber(delivery.invoice.promoAmount))
        ]
      }
      data.push(row);
    }

    this.excelService.generateExcel("Factures", header, data);
  }
}
