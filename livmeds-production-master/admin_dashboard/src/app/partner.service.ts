import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  
  constructor(private http: HttpClient) { }

  async list(filter){
    return await this.http.get(environment.url + '/admin/partners', {
      params: filter
    }).toPromise();
  }

  async get(id){
    return await this.http.get(environment.url + '/admin/partner/' + id).toPromise();
  }

  async updateStatus(id, status){
    return await this.http.put(environment.url + '/admin/partner-status/' + id, {
      status
    }).toPromise();
  }

  async add(body){
    return await this.http.post(environment.url + '/admin/partner', body).toPromise();
  }

  async update(id, body){
    return await this.http.put(environment.url + '/admin/partner/' + id, body).toPromise();
  }

  async generateKey(id){
    return await this.http.put(environment.url + '/admin/partner-key/' + id, {}).toPromise();
  }
}
