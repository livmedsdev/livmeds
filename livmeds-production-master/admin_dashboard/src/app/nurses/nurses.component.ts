import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../user.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../session.service';
import { SseService } from '../sse.service';
import { UserRole } from '../enums/UserRole';
import { FormBuilder, Validators } from '@angular/forms';

declare var google;

@Component({
  selector: 'app-nurses',
  templateUrl: './nurses.component.html',
  styleUrls: ['./nurses.component.scss']
})
export class NursesComponent implements OnInit {
  userSelected;
  listUsers = [];
  role;

  hideList;
  hideEditForm;
  titleForm;
  submitForm;
  error;
  errorList;
  successList;
  passwordMandatory;
  titleDetails;
  hideDetails;
  selectedForShowDetails;


  private address;
  @ViewChild("address", { static: false })
  public addressElement: ElementRef;

  public formGroup;

  // list
  public page: number;
  public limit: number;
  public nbTotal: number;
  public text: string;

  constructor(public userService: UserService,
    private translate: TranslateService,
    public session: SessionService) {
    this.initFrm();
  }

  initFrm(type = 'ADD') {
    switch (type) {
      case 'ADD':
        this.formGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          siret: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          password: [null, Validators.compose([Validators.required])],
          passwordConfirm: [null, Validators.compose([Validators.required])]
        });
      case 'EDIT':
        this.formGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          siret: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          password: ['', Validators.compose([])],
          passwordConfirm: ['', Validators.compose([])]
        });
    }
  }

  async ngOnInit() {
    this.hideList = false;
    this.hideEditForm = true;
    this.hideDetails = true;
    this.titleForm = {};
    this.address = null;
    this.role = UserRole.ROLE_NURSE;
    this.userSelected = {} as User;
    this.page = 0;
    this.limit = 5;
    this.refreshList();
  }


  async addAdmin() {
        this.closeDetails();
    window.scrollTo(0, 0);
    this.resetError();
    this.formGroup.reset();
    this.initFrm('ADD');
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: await this.translate.get('FIELD_NURSE').toPromise() }
    }
    this.hideList = false;
    this.hideEditForm = false;
    this.submitForm = this.submitAdd;
    this.userSelected = { password: '' } as User;
    this.addressElement.nativeElement.value = "";
    this.address = null;
    this.passwordMandatory = true;
    console.log(this.address)
    console.log(!this.address)
  }


  async submitAdd() {
    this.resetError();
    if (!this.formGroup.valid || !this.formGroup.value.password || !this.formGroup.value.passwordConfirm)
      return
    const value = Object.assign({}, this.formGroup.value);
    value.favoriteAddress = this.address;
    value.role = this.role;
    try {
      await this.userService.register(value);
      this.hideEditForm = true;
      await this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async editAdmin(admin) {
        this.closeDetails();
    window.scrollTo(0, 0);
    window.scrollTo(0, 0);
    this.resetError();
    this.formGroup.reset();
    this.initFrm('EDIT');
    this.submitForm = this.submitEdit;
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: admin.name
      }
    }
    this.hideEditForm = false;
    const data: any = await this.userService.getUser(admin._id);
    this.userSelected = data;
    this.formGroup.patchValue(data);
    this.passwordMandatory = false;
    this.addressElement.nativeElement.value = this.userService.getFavoriteAddress(this.userSelected.addresses).fullAddress;
    this.formGroup.patchValue({
      compAddress: this.userService.getFavoriteAddress(data.addresses).compAddress
    })
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  closeDetails() {
    this.selectedForShowDetails = undefined;
  }

  showDetails(user) {
    this.hideEditForm = true;
    window.scrollTo(0, 0);
    this.titleDetails = {
      label: 'TITLE_DETAILS',
      params: {
        name: user.name + ' ' + user.lastName
      }
    }
    this.selectedForShowDetails = user;
  }

  async submitEdit() {
    this.resetError();
    try {
      const value = Object.assign({}, this.formGroup.value);
      value._id = this.userSelected._id;
      value.role = this.role;
      value.favoriteAddress = this.address;
      const response = await this.userService.editUser(value);
      this.hideEditForm = true;
      await this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async changeStatus(adminSelected, status) {
    this.resetError();
    try {
      const response: any = await this.userService.changeStatus(adminSelected._id, status);
      this.successList = response.tradCode;
      await this.refreshList();
    } catch (error) {
      this.errorList = error.error.tradCode;
    }
  }

  resetError() {
    this.successList = null;
    this.error = null;
    this.errorList = null;
  }


  ngAfterViewInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    autocomplete.setComponentRestrictions({ 'country': ['fr'] });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      let place = autocomplete.getPlace();
      this.address = this.retrieveAddressFromPlace(place);
      this.userSelected.favoriteAddress = this.address;
    });
  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }


  async unlock(user){
    this.resetError();
    try {
      const response : any = await this.userService.unlock(user._id);
      this.successList = response.tradCode;
      await this.refreshList();
    } catch (error) {
      this.errorList = error.error.tradCode;
    }
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    if (this.role)
      filter.role = this.role;

    if(this.text && this.text !== ''){
      filter.text = this.text;
    }

    const response: any = await this.userService.listUsers(filter);
    this.listUsers = response.data;
    this.nbTotal = response.total;
  }
  
  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }
}
