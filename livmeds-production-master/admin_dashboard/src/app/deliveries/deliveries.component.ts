import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PrimeService } from '../prime.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Prime } from '../models/Prime';
import { TypeBonus } from '../models/TypeBonus';
import { DeliveryService } from '../delivery.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/internal/operators";
import { UserService } from '../user.service';
import { ToasterService } from 'angular2-toaster';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { FormatService } from '../format.service';

declare var google;
@Component({
  selector: 'app-deliveries',
  templateUrl: './deliveries.component.html',
  styleUrls: ['./deliveries.component.scss']
})
export class DeliveriesComponent implements OnInit {
  public title="";
  public deliveries: any[] = [];
  public deliverySelected;
  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public type: number;
  public deliveryType: string;
  public hideEditForm;
  public deliveryFormGroup;
  public titleForm;
  public submitForm;
  public primeSelected;
  public titleDetails;
  private address;
  @ViewChild("address", { static: false })
  public addressElement: ElementRef;

  private selectedDeliverer;
  private selectedDeliveryForUpdate;

  constructor(private deliveryService: DeliveryService,
    private userService: UserService,
    private toasterService: ToasterService,
    private TranslateService: TranslateService,
    private route: ActivatedRoute,
    public formatService: FormatService,
    private modalService: NgbModal) {
    this.deliveryFormGroup = new FormBuilder().group({
      startFrom: ['', Validators.compose([Validators.required])],
      endAt: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required])],
      placeId: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])]
    });


    this.modelChanged
      .pipe(debounceTime(1000))
      .subscribe(async (model) => {
        // api call
        this.data = (await this.userService.listDelivererWithFilter({limit:5, page:0, text: model}))['data'];
      });
  }

  async ngOnInit() {
    this.hideEditForm = true;
    this.titleForm = {};
    this.type = null;
    this.page = 0;
    this.limit = 5;
    this.deliveries = [];
    this.route.queryParams.subscribe(param => {
        this.deliveryType = param.type?param.type:"INTERNAL";
        this.title = `TITLE_MANAGE_${this.deliveryType.toUpperCase()}_DELIVERIES`;
        this.refreshList()
    });
  }

  ngAfterViewInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    autocomplete.setComponentRestrictions({ 'country': ['fr'] });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      let place = autocomplete.getPlace();
      this.address = this.retrieveAddressFromPlace(place);
      this.deliveryFormGroup.patchValue({
        placeId: this.address.placeId,
        city: this.address.city
      });
    });
    
  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      placeId: place.place_id,
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit,
      type: this.deliveryType
    };

    if (this.type)
      filter.type = this.type;

    const response: any = await this.deliveryService.getList(filter);
    this.deliveries = response.data;
    this.nbTotal = response.total;
  }

  setSelectedType(type) {
    this.type = type;
    this.refreshList();
  }

  displayAddForm() {
  }


  async displayEditForm(prime) {
  }

  async submitAddForm() {
  }

  async submitEditForm() {
  }

  async deleteElement(id) {
  }

  async cancel(id) {
    await this.deliveryService.cancel(id);
    await this.refreshList();
  }

  async changeStatusToDelivered(delivery) {
    await this.deliveryService.changeStatusToDelivered(delivery._id)
    await this.refreshList();
  }

  async changeStatus(delivery) {
    await this.deliveryService.changeStatus(delivery._id)
    await this.refreshList();
  }

  showDetails(delivery) {
    this.titleDetails = {
      label: 'TITLE_DETAILS',
      params: {
        name: delivery.reference
      }
    }
    this.deliverySelected = delivery;
  }

  closeDetails() {
    this.deliverySelected = undefined;
  }

  async open(content, delivery) {
    this.modalService.open(content);
    this.selectedDeliveryForUpdate = delivery._id;
    this.data = (await this.userService.listDelivererWithFilter({page: 0, limit: 5}))['data'];
  }


  modelChanged: Subject<string> = new Subject<string>();

  keyword = 'name';
  data = [];


  selectEvent(item) {
    // do something with selected item
    this.selectedDeliverer = item.target.value;
  }

  onChangeSearch(event) {
    this.modelChanged.next(event.target.value);
  }

  onFocused(e) {
    // do something when input is focused
  }

  async submit(){
    try{
      await this.deliveryService.updateDelivererOfDelivery(this.selectedDeliveryForUpdate, this.selectedDeliverer);
      this.modalService.dismissAll();
      await this.refreshList();
      this.toasterService.pop('success', "Succès", "Livreur modifié pour cette livraison")
    }catch(error){
      this.toasterService.pop('error', "Erreur", this.TranslateService.instant(error.error.tradCode))
    }
  }
}
