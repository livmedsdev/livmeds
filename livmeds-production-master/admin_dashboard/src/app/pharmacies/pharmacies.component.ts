import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../user.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../session.service';
import { UserRole } from '../enums/UserRole';
import { FileService } from '../file.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster';
import * as moment from 'moment';
import { NgxSpinnerService } from "ngx-spinner";
import { NgxPicaService } from '@digitalascetic/ngx-pica';

declare var google;

@Component({
  selector: 'app-pharmacies',
  templateUrl: './pharmacies.component.html',
  styleUrls: ['./pharmacies.component.scss']
})
export class PharmaciesComponent implements OnInit {
  userSelected;
  listUsers = [];
  role;

  hideList;
  hideEditForm;
  titleForm;
  submitForm;
  error;
  errorList;
  successList;
  passwordMandatory;
  titleDetails;
  hideDetails;
  selectedForShowDetails;

  ribUrl;
  kbisUrl;
  selectedMerchant;
  
  private address;
  @ViewChild("address", { static: false })
  public addressElement: ElementRef;

  formGroup;


  public openingTimesForm;
  public days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
  public openingTimes;
  public checkValid = false;
  public userSelectedForOpeningTimes;

  
  // list
  public page: number;
  public limit: number;
  public nbTotal: number;
  public text: string;

  constructor(public userService: UserService,
    private translate: TranslateService,
    private sanitizer: DomSanitizer,
    private toasterService: ToasterService,
    public fileService: FileService,
    private spinner: NgxSpinnerService,
    private _ngxPicaService: NgxPicaService,
    private modalService: NgbModal,
    public session: SessionService) { 
      this.initFrm();
    }
  
    initFrm(type = 'ADD') {
      switch (type) {
        case 'ADD':
          this.formGroup = new FormBuilder().group({
            mail: ['', Validators.compose([Validators.required])],
            name: ['', Validators.compose([Validators.required])],
            responsibleLastname: ['', Validators.compose([Validators.required])],
            responsibleName: ['', Validators.compose([Validators.required])],
            siret: ['', Validators.compose([Validators.required])],
            phoneNumber: ['', Validators.compose([Validators.required])],
            password: [null, Validators.compose([Validators.required])],
            passwordConfirm: [null, Validators.compose([Validators.required])]
          });
        case 'EDIT':
          this.formGroup = new FormBuilder().group({
            mail: ['', Validators.compose([Validators.required])],
            name: ['', Validators.compose([Validators.required])],
            responsibleLastname: ['', Validators.compose([Validators.required])],
            responsibleName: ['', Validators.compose([Validators.required])],
            siret: ['', Validators.compose([Validators.required])],
            phoneNumber: ['', Validators.compose([Validators.required])],
            password: ['', Validators.compose([])],
            passwordConfirm: ['', Validators.compose([])]
          });
      }
    }

  async ngOnInit() {
    this.openingTimesForm = new FormBuilder().group({
      '0Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '0End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '1Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '1End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '2Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '2End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '3Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '3End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '4Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '4End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '5Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '5End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '6Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '6End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '7Start': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])],
      '7End': ['', Validators.compose([Validators.pattern('^(?:[01]\\d|2[0123]):(?:[012345]\\d)$')])]
    }, { updateOn: "blur" });

    this.hideList = false;
    this.hideEditForm = true;
    this.hideDetails = true;
    this.titleForm = {};
    this.address = null;
    this.role = UserRole.ROLE_PHARMACY;
    this.userSelected = {} as User;
    this.page = 0;
    this.limit = 5;
    this.refreshList();
  }


  async addAdmin() {
        this.closeDetails();
    window.scrollTo(0, 0);
    window.scrollTo(0, 0);
    this.resetError();
    this.formGroup.reset();
    this.initFrm('ADD');
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: await this.translate.get('FIELD_MERCHANT').toPromise() }
    }
    this.hideList = false;
    this.hideEditForm = false;
    this.submitForm = this.submitAdd;
    this.userSelected = {password:''} as User;
    this.addressElement.nativeElement.value = "";
    this.address = null;
    this.passwordMandatory = true;
    console.log(this.address)
    console.log(!this.address)
  }


  async submitAdd() {
    this.resetError();
    if (!this.formGroup.valid || !this.formGroup.value.password || !this.formGroup.value.passwordConfirm)
      return
    const value = Object.assign({}, this.formGroup.value);
    value.favoriteAddress = this.address;
    value.role = this.role;
    try {
      await this.userService.register(value);
      this.hideEditForm = true;
      await this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async editAdmin(admin) {
        this.closeDetails();
    window.scrollTo(0, 0);
    window.scrollTo(0, 0);
    this.resetError();
    this.formGroup.reset();
    this.initFrm('EDIT');
    this.submitForm = this.submitEdit;
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: admin.name
      }
    }
    this.hideEditForm = false;
    const data: any = await this.userService.getUser(admin._id);
    this.userSelected = data;
    this.formGroup.patchValue(data);
    this.passwordMandatory = false;
    this.addressElement.nativeElement.value = this.userService.getFavoriteAddress(this.userSelected.addresses).fullAddress;
    this.formGroup.patchValue({
      compAddress: this.userService.getFavoriteAddress(data.addresses).compAddress
    });
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  closeDetails(){
    this.selectedForShowDetails = undefined;
  }

  async showDetails(user){
    this.hideEditForm = true;
    this.titleDetails = {
      label: 'TITLE_DETAILS',
      params: {
        name: user.name
      }
    }
    this.selectedForShowDetails = user;
    if(user.rib){
      this.ribUrl = await this.fileService.getSignedUrl(user.rib);
    }else{
      this.ribUrl = undefined;
    }

    if(user.kbis){
      this.kbisUrl = await this.fileService.getSignedUrl(user.kbis);
    }else{
      this.kbisUrl = undefined;
    }
  }

  async submitEdit() {
    this.resetError();
    try {
      const value = Object.assign({}, this.formGroup.value);
      value._id = this.userSelected._id;
      value.role = this.role;
      value.favoriteAddress = this.address;
      const response = await this.userService.editUser(value);
      this.hideEditForm = true;
      await this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async changeStatus(adminSelected, status){
    this.resetError();
    try {
      const response : any = await this.userService.changeStatus(adminSelected._id, status);
      this.successList = response.tradCode;
      await this.refreshList();
    } catch (error) {
      this.errorList = error.error.tradCode;
    }
  }

  resetError(){
    this.successList = null;
    this.error = null;
    this.errorList = null;
  }

  
  ngAfterViewInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    autocomplete.setComponentRestrictions({'country': ['fr']});
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      let place = autocomplete.getPlace();
      this.address = this.retrieveAddressFromPlace(place);
      this.userSelected.favoriteAddress = this.address;
    });
  }

  retrieveAddressFromPlace(place){
    console.log(place);

    const addressComponents = place.address_components
    

    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents){
    for(let element of addressComponents){
      for(let type of element.types){
        if(elementName === type){
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }
  
  async generateContract(id){
    window.open((URL.createObjectURL(await this.userService.generateContract(id)) as any), "_blank");
  }
  

  async open(content, user) {
    this.modalService.open(content);
    this.userSelectedForOpeningTimes = user;
    this.userSelectedForOpeningTimes.avatar = (this.userSelectedForOpeningTimes.avatar)? await this.fileService.getSignedUrl(this.userSelectedForOpeningTimes.avatar): undefined;
    let openingTime = this.dateToString(user.openingTime);
    if(openingTime){
      this.openingTimesForm.patchValue(openingTime);
      if(openingTime['0Start']){
        this.openingTimesForm.patchValue({
          '7Start': openingTime['0Start']
        })
      }
      if(openingTime['0End']){
        this.openingTimesForm.patchValue({
          '7End': openingTime['0End']
        })
      }
    }
    this.openingTimes = this.openingTimesForm.value;
  }

  
  async submitOpeningTime(){
    if(!this.openingTimesForm.valid){
      this.validateAllFormFields(this.openingTimesForm);
      return;
    }
    console.log(this.openingTimesForm.value)
    try {
      let params = this.openingTimesForm.value;
      params['0Start'] = params['7Start']
      params['0End'] = params['7End']
      params = this.convertValueToDate(params);
      await this.userService.updateOpeningTimes(this.userSelectedForOpeningTimes._id, params);
      this.openingTimes = params;
      this.toasterService.pop('success', 'Succès', await this.translate.get("Les horaires de la bout ont été mis à jour!").toPromise());
      await this.refreshList();
      this.modalService.dismissAll();
    } catch (error) {
      console.log(error);
      this.toasterService.pop('error', 'Erreur', 'Erreur lors de la modification de vos horaires');
    }
  }

  convertValueToDate(values){
    const newValues = {};
    for(let index in values){
      if(values[index] && values[index] !== ''){
        const date = new Date();
        date.setHours(values[index].split(':')[0]);
        date.setMinutes(values[index].split(':')[1])
        newValues[index] = date;
      }else{
        newValues[index] = values[index];
      }
    }
    return newValues;
  }

  dateToString(values){
    const newValues = {};
    for(let index in values){
      if(values[index] && values[index] !== ''){
        const date = new Date();
        date.setHours(values[index].split(':')[0]);
        date.setMinutes(values[index].split(':')[1])
        newValues[index] = moment(values[index]).format('HH:mm');
      }else{
        newValues[index] = values[index];
      }
    }
    return newValues;
  }

  fieldsRegisterFrm(field) {
    return this.openingTimesForm.get(field);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
    this.checkValid = true;
  }

  async onFileChanged(event){
    this.spinner.show()
    try{
      const selectedFile = event.target.files[0];
      const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 400, 400, {aspectRatio: {
        keepAspectRatio: true
      }}).toPromise();
                
      const uploadData = new FormData();
      uploadData.append('file', imageResized, selectedFile.name);
      const response : any = await this.fileService.uploadForm(uploadData, 'avatar');
      await this.userService.changeAvatarByAdmin(this.userSelectedForOpeningTimes._id, response.idFile);
      this.toasterService.pop('success', 'Succès', 'Mis à jour de l\'avatar effectuée avec succès')
      this.spinner.hide()
      this.modalService.dismissAll();
      await this.refreshList();
    }catch(error){
      console.log(error)
      this.toasterService.pop('error', 'Erreur', 'Erreur lors de la mise à jour de l\'avatar')
      this.spinner.hide()
    }
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    if (this.role)
      filter.role = this.role;

    if(this.text && this.text !== ''){
      filter.text = this.text;
    }

    const response: any = await this.userService.listUsers(filter);
    this.listUsers = response.data;
    this.nbTotal = response.total;
  }
  
  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }
}
