import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { User } from './models/User';
import { SessionService } from './session.service';
import { UserRole } from './enums/UserRole';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private session: SessionService) { }

  async register(user){
      const formData = new FormData();
      if(user.mutual){
        let res = await fetch(user.mutual);
        let blob = await res.blob();
    
        formData.append('mutual', blob, "mutual.jpg");
        delete user.mutual;
      }
  
      if(user.socialSecurityNumber){
        let res = await fetch(user.socialSecurityNumber);
        let blob = await res.blob();
    
        formData.append('socialSecurityNumber', blob, "socialSecurityNumber.jpg");
        delete user.socialSecurityNumber;
      }
  
      const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
      let response = await this.http.post(environment.url + '/admin/user',
      this.objToForm(formData, user), { headers: headers }).toPromise();
      return response;
  }

  async registerJson(user){
    let response = await this.http.post(environment.url + '/admin/user', user).toPromise();
    return response;
}

  async login(user: User){
    user.role = [UserRole.ROLE_ADMIN];
    user.fullAddress ='lol';
    let response = await this.http.post(environment.url + "/login", user).toPromise();
    localStorage.setItem("token", response['token']);
    this.getProfile();
  }

  async listUsers(filter){
    let response = await this.http.get(environment.url + "/admin/users", {
      params: filter
    }).toPromise();
    return response as unknown as Array<User>;
  }

  async getProfile() {
    const response = await this.http.get(environment.url + "/profile").toPromise();
    this.session.saveInSession(response['user']);
    return response;
  }

  async editUser(user){
    const formData = new FormData();
    if(user.mutual && (user.mutual as string).startsWith('data:image/jpeg;base64,')){
      let res = await fetch(user.mutual);
      let blob = await res.blob();
  
      formData.append('mutual', blob, "mutual.jpg");
      delete user.mutual;
    }

    if(user.socialSecurityNumber && (user.socialSecurityNumber as string).startsWith('data:image/jpeg;base64,')){
      let res = await fetch(user.socialSecurityNumber);
      let blob = await res.blob();
  
      formData.append('socialSecurityNumber', blob, "socialSecurityNumber.jpg");
      delete user.socialSecurityNumber;
    }

    const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
    let response = await this.http.put(environment.url +  "/admin/user/" + user._id,
    this.objToForm(formData, user), { headers: headers }).toPromise();
    return response;
  }


  async editUserJson(user){
    let response = await this.http.put(environment.url +  "/admin/user/" + user._id, user).toPromise();
    return response;
  }

  async unlock(user){
    let response = await this.http.put(environment.url +  "/admin/unlock/" + user, {}).toPromise();
    return response;
  }

  async changeStatus(id, status){
    return await this.http.put(environment.url + "/user/status/" + id, {status}).toPromise();
  }

  async getUser(id){
    return await this.http.get(environment.url + "/admin/user/" + id).toPromise();
  }

  async editSecurityInformation(params){
    return await this.http.put(environment.url + "/security", params).toPromise();
  }

  async editMyProfile(params){
    return await this.http.put(environment.url + "/profile", params).toPromise();
  }
  
  async changeAvatar(idAvatar){
    return await this.http.put(environment.url + "/avatar", {avatar:idAvatar}).toPromise();
  }
  
  async changeAvatarByAdmin(idUser, idAvatar){
    return await this.http.put(environment.url + `/admin/avatar/${idUser}`, {avatar:idAvatar}).toPromise();
  }

  getFavoriteAddress(addresses){
    for(let address of addresses){
      if(address.favoriteAddress){
        return address;
      }
    }
  }

  objToForm(formData, data){
    for(let key of Object.keys(data)){
      if(['favoriteAddress'].includes(key)){
        formData.append(key, JSON.stringify(data[key]));
      } else if(['socialSecurityNumber'].includes(key) && data[key].name){
        formData.append(key, data[key], data[key].name);
      }else{
        formData.append(key, data[key]);
      }
    }
    return formData;
  }

  async listTypesDeliverer(){
    return await this.http.get(environment.url + "/typesDeliverer").toPromise();
  }

  async generateContract(id) {
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/pdf');
    return await this.http.get(environment.url + "/admin/contract/"+id, { headers: headers, responseType: 'blob' }).toPromise();
  }

  async listDelivererWithFilter(params){
    return await this.http.get(environment.url + "/admin/deliverers", {params}).toPromise();
  }

  async updateOpeningTimes(id, body) {
    return await this.http.put(environment.url + `/admin/openingTimes/${id}`, body).toPromise();
  }
}
