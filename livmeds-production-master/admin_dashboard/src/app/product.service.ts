import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { environment } from 'src/environments/environment';
import { PromoCode } from './models/PromoCode';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient, private session: SessionService) { }

  async getList(filter) {
    return await this.http.get(environment.url + '/admin/products', {
      params: filter
    }).toPromise();
  }
  
  async get(promoCodeId) {
    return await this.http.get(environment.url + '/admin/product/' + promoCodeId).toPromise();
  }

  async create(promoCode) {
    return await this.http.post(environment.url + '/admin/product', promoCode).toPromise();
  }

  async edit(promoCodeId, promoCode ) {
    return await this.http.put(environment.url + '/admin/product/' + promoCodeId, promoCode).toPromise();
  }

  async delete(id){
    return await this.http.put(environment.url + "/admin/product/status/" + id, {status: 'DELETED'}).toPromise();
  }
}
