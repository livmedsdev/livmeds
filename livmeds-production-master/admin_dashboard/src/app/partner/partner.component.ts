import { Component, OnInit, ElementRef, ViewChild, TemplateRef } from '@angular/core';
import { UserService } from '../user.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../session.service';
import { SseService } from '../sse.service';
import { User } from '../models/User';
import { UserRole } from '../enums/UserRole';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { FileService } from '../file.service';
import { PartnerService } from '../partner.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

declare var google;

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {
  public page: number;
  public limit: number;
  public nbTotal: number;

  itemSelected;
  list;

  hideList;
  hideEditForm;
  titleForm;
  submitForm;
  error;
  errorList;
  successList;
  titleDetails;
  hideDetails;
  selectedForShowDetails;

  responseAfterCreation;

  private address;
  @ViewChild("address", { static: false })
  public addressElement: ElementRef;

  public formGroup;

  @ViewChild("content", { static: false })
  public contentModalPopup: TemplateRef<any>;

  constructor(public partnerService: PartnerService,
    private translate: TranslateService,
    private _ngxPicaService: NgxPicaService,
    private fileService: FileService,
    private modalService: NgbModal,
    public session: SessionService) {

    this.initFrm();
  }

  initFrm(type = 'ADD') {
    switch (type) {
      case 'ADD':
        this.formGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          siret: ['', Validators.compose([Validators.required])],
          rib: ['', Validators.compose([Validators.required])],
          bic: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          responsibleName: ['', Validators.compose([Validators.required])],
          responsibleLastname: ['', Validators.compose([Validators.required])],
          addressDisplay: ['', Validators.compose([Validators.required])],
        });
      case 'EDIT':
        this.formGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          siret: ['', Validators.compose([Validators.required])],
          rib: ['', Validators.compose([Validators.required])],
          bic: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          responsibleName: ['', Validators.compose([Validators.required])],
          responsibleLastname: ['', Validators.compose([Validators.required])],
          addressDisplay: ['', Validators.compose([])],
        });
    }
  }

  async ngOnInit() {
    this.hideList = false;
    this.hideEditForm = true;
    this.hideDetails = true;
    this.titleForm = {};
    this.address = null;
    this.itemSelected = {};
    this.page = 0;
    this.limit = 5;
    this.list = [];
    this.refreshList();
  }


  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    const response = (await this.partnerService.list(filter)) as any;
    this.list = response.data;
    this.nbTotal = response.total;
  }

  async add() {
    this.closeDetails();
    window.scrollTo(0, 0);
    this.resetError();
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: {
        role: this.translate.instant('PARTNER')
      }
    }
    this.hideList = false;
    this.hideEditForm = false;
    this.submitForm = this.submitAdd;
    this.itemSelected = { password: '' } as User;
    this.addressElement.nativeElement.value = "";
    this.address = null;
    this.formGroup.reset();
  }


  async submitAdd() {
    this.resetError();
    if (!this.formGroup.valid)
      return
    const value = Object.assign({}, this.formGroup.value);
    value.address = this.address;
    try {
      const response = await this.partnerService.add(value);
      this.responseAfterCreation = response['data'];
      this.modalService.open(this.contentModalPopup);
      this.hideEditForm = true;
      this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async edit(item) {
    this.closeDetails();
    window.scrollTo(0, 0);
    this.resetError();
    this.formGroup.reset();
    this.initFrm('EDIT');
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: item.name
      }
    }
    this.hideEditForm = false;
    const data : any = await this.partnerService.get(item._id);
    this.itemSelected = data;
    this.formGroup.patchValue(data);
    this.submitForm = this.submitEdit;
    this.addressElement.nativeElement.value = this.itemSelected.address.fullAddress;
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  closeDetails() {
    this.selectedForShowDetails = undefined;
  }

  showDetails(partner) {
    this.hideEditForm = true;
    this.titleDetails = {
      label: 'TITLE_DETAILS',
      params: {
        name: partner.name
      }
    }
    this.selectedForShowDetails = partner;
  }

  async submitEdit() {
    this.resetError();
    try {
      const value = Object.assign({}, this.formGroup.value);
      value._id = this.itemSelected._id;
      if(this.address){
        value.address = this.address;
      }
      const response = await this.partnerService.update(value._id, value);
      this.hideEditForm = true;
      this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async changeStatus(itemSelected, status) {
    this.resetError();
    try {
      const response: any = await this.partnerService.updateStatus(itemSelected._id, status);
      this.successList = response.tradCode;
      this.refreshList();
    } catch (error) {
      this.errorList = error.error.tradCode;
    }
  }

  resetError() {
    this.successList = null;
    this.error = null;
    this.errorList = null;
  }


  ngAfterViewInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    autocomplete.setComponentRestrictions({ 'country': ['fr'] });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      let place = autocomplete.getPlace();
      this.address = this.retrieveAddressFromPlace(place);
      this.itemSelected.address = this.address;
    });
  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  async generateKey(itemSelected){
    this.resetError();
    try {
      const response = await this.partnerService.generateKey(itemSelected._id);
      this.responseAfterCreation = response['data'];
      this.responseAfterCreation.accessKey = itemSelected.accessKey;
      this.modalService.open(this.contentModalPopup);
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }
}
