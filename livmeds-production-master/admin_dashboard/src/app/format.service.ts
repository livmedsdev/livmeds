import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  constructor() { }


  formatNumber(number) {
    if(number){
      return Number.parseFloat(number).toFixed(2);
    }else{
      return "";
    }
  }
}
