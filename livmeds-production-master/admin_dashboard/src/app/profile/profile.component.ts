import { Component, OnInit } from '@angular/core';
import { SessionService } from '../session.service';
import { UserService } from '../user.service';
import { Admin } from '../models/Admin';
import { FormBuilder, Validators } from '@angular/forms';
import { FileService } from '../file.service';
import { NgxPicaService } from '@digitalascetic/ngx-pica';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  private myProfile : Admin;
  private JSON : JSON;

  // Security form
  public securityFormGroup;
  public errorSecurity;
  public successSecurity;

  
  // Information form
  public informationFormGroup;
  public errorInformation;
  public successInformation;

  constructor(public session: SessionService, private userService: UserService, 
    private fileService: FileService, 
    private _ngxPicaService: NgxPicaService) { 
    this.JSON = JSON;
    this.securityFormGroup = new FormBuilder().group({
      mail: ['', Validators.compose([Validators.required])],
      currentPassword: ['', Validators.compose([])],
      newPassword: ['', Validators.compose([])]
    });

    this.informationFormGroup = new FormBuilder().group({
      lastName: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      phoneNumber: ['', Validators.compose([Validators.required])]
    });
  }

  async ngOnInit() {
    this.myProfile = (await this.userService.getProfile())['user'] as Admin;
    console.log(this.myProfile);
    this.informationFormGroup.patchValue(this.myProfile);
  }

  async submitSecurityForm(){
    this.resetMessage();
    if(this.securityFormGroup.valid){
      try{
        const response : any = await this.userService.editSecurityInformation(this.securityFormGroup.value);
        this.myProfile = await this.userService.getProfile() as Admin;
        this.successSecurity = response.tradCode;
        this.securityFormGroup.reset();
      }catch(error){
        this.errorSecurity = error.error.tradCode;
      }
    }
  }

  async submitInformationForm(){
    this.resetMessage();
    if(this.informationFormGroup.valid){
      try{
        const response : any = await this.userService.editMyProfile(this.informationFormGroup.value);
        this.myProfile = await this.userService.getProfile() as Admin;
        this.successInformation = response.tradCode;
        this.informationFormGroup.patchValue(this.myProfile);
      }catch(error){
        this.errorInformation = error.error.tradCode;
      }
    }
  }

  async onFileChanged(event){
    const selectedFile = event.target.files[0];
    const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 400, 400, {aspectRatio: {
      keepAspectRatio: true
    }}).toPromise();
              
    const uploadData = new FormData();
    uploadData.append('file', imageResized, selectedFile.name);
    const response : any = await this.fileService.uploadForm(uploadData, 'avatar');
    await this.userService.changeAvatar(response.idFile);
    this.myProfile = await this.userService.getProfile() as Admin;
  }

  resetMessage(){
    this.successSecurity = null;
    this.errorSecurity = null;
  }
}
