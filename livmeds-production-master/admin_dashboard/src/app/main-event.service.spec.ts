import { TestBed } from '@angular/core/testing';

import { MainEventService } from './main-event.service';

describe('MainEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MainEventService = TestBed.get(MainEventService);
    expect(service).toBeTruthy();
  });
});
