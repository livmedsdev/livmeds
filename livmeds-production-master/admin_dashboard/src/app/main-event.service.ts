import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainEventService {
  
  hideTopBar = new EventEmitter();
  hideModalLoader = new EventEmitter();
  title = new EventEmitter();
  hideSideBar = new EventEmitter();
  hidePadding = new EventEmitter();

  constructor() { }

  sendEventToHideTopBar(data: boolean) {
    this.hideTopBar.emit(data);
  }

  sendEventToHideModalLoader(data: boolean) {
    this.hideModalLoader.emit(data);
  }

  sendEventToChangeTitle(title: String){ 
    this.title.emit(title);
  }

  sendEventToHideSidebar(data){
    this.hideSideBar.emit(data);
  }


  sendEventToHidePadding(data){
    this.hidePadding.emit(data);
  }
}
