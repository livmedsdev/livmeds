import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EventSourcePolyfill } from 'event-source-polyfill';

@Injectable({
  providedIn: 'root'
})
export class SseService {

  constructor(private _ngZone: NgZone) { }


  private getEventSource(url: string) : EventSource{
    return new EventSource(url, {
      // withCredentials: true,
      // headers: `Bearer ${localStorage.getItem('token')}`
    });
  }

  public getServerSentEvent(path: string) {
    return Observable.create(observer =>{
      const eventSource = this.getEventSource(environment.url + path);
      eventSource.onmessage = event =>{
        console.log(event);
        this._ngZone.run(() =>{
          observer.next(event);
        });
      }

      eventSource.onerror = error =>{
        console.log(error);
        this._ngZone.run(() =>{
          observer.error(error);
        });
      }
    })
  }
}
