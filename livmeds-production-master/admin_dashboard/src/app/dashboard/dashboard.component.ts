import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { DeliveryService } from '../delivery.service';
import { MainEventService } from '../main-event.service';
import { UserRole } from '../enums/UserRole';
import { StatsService } from '../stats.service';

declare var $: any;
declare var screenfull: any;
declare var Waves: any;
declare var google;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public user: User;
  private userWillBeCreate: User;
  private displayDashboard;
  private listUsers: User[] = [];
  private displayList;
  private titleList;
  private displayAddButton;
  private roleSelected;
  private displayListDeliveries;
  private listDeliveries;
  private address;
  private typeSelected;
  private evtSource;

  public stats;

  // @ViewChild("address", { static: false })
  // public addressElement: ElementRef;

  constructor(private event: MainEventService, 
    private userService: UserService,
    private statsService: StatsService,
    private deliveryService: DeliveryService) {

  }

  async ngOnInit() {
    this.event.sendEventToHideTopBar(false);
    this.event.sendEventToHideSidebar(false);
    this.event.sendEventToHidePadding(false);

    this.address = null;
    this.displayDashboard = true;
    this.displayList = false;
    this.displayListDeliveries = false;
    $(".page-loader-wrapper").fadeOut();

    this.userWillBeCreate = {} as User;

    this.stats = await this.statsService.get();
  }

  async addAdmin() {
    if (!this.userWillBeCreate.name || !this.userWillBeCreate.mail || !this.userWillBeCreate.password)
      return
    this.userWillBeCreate.role = this.roleSelected;
    this.userWillBeCreate.fullAddress = 'coucou';
    await this.userService.register(this.userWillBeCreate);
    this.listUsers = await this.userService.listUsers({ role: this.roleSelected, type: this.typeSelected } as User);
  }


  showDashboard() {
    this.displayList = false;
    this.displayDashboard = true;
    this.displayListDeliveries = false;
  }

  statusClass(status){
    switch(status){
      case 'IN_DELIVERY':
        return "blue";
      case 'DELIVERED':
      case 'VALID':
        return "green";
      case 'REFUSED':
        return "red-span";
      case 'AWAITING_VALIDATION':
        return "yellow";
    }
    return "";
  }

  ngAfterViewInit() {
    // let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    // autocomplete.setComponentRestrictions({'country': ['fr']});
    // google.maps.event.addListener(autocomplete, 'place_changed', () => {
    //   let place = autocomplete.getPlace();
    //   this.address = this.retrieveAddressFromPlace(place);
    // });
  }

  retrieveAddressFromPlace(place){
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' + 
                this.getInformationInAddressComponent("route", addressComponents),
      country:this.getInformationInAddressComponent("country", addressComponents),
      postalCode:this.getInformationInAddressComponent("postal_code", addressComponents),
      city:this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents){
    for(let element of addressComponents){
      for(let type of element.types){
        if(elementName === type){
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

}
