import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpticiansComponent } from './opticians.component';

describe('PharmaciesComponent', () => {
  let component: OpticiansComponent;
  let fixture: ComponentFixture<OpticiansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpticiansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpticiansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
