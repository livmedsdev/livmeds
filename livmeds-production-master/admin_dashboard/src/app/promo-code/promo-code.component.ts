import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TypeBonus } from '../models/TypeBonus';
import { PromoCodeService } from '../promo-code.service';
import { PromoCode } from '../models/PromoCode';

declare var google;

@Component({
  selector: 'app-promo-code',
  templateUrl: './promo-code.component.html',
  styleUrls: ['./promo-code.component.scss']
})
export class PromoCodeComponent implements OnInit {

  public promoCodeList:any[] = [];

  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public type: number;

  public hideEditForm;
  public promoCodeFormGroup;
  public titleForm;
  public submitForm;
  public promoCodeSelected;

  constructor(private promoCodeService: PromoCodeService) {
    this.promoCodeFormGroup = new FormBuilder().group({
      usedTimeMax: ['', Validators.compose([Validators.required])],
      promoCode: ['', Validators.compose([Validators.required])],
      percentage: ['', Validators.compose([Validators.required])]
    });
  }

  async ngOnInit() {
    this.hideEditForm = true;
    this.titleForm = {};
    this.type = null;
    this.page = 0;
    this.limit = 5;
    this.promoCodeList = [];
    await this.refreshList();
  }
  
  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    const response: any = await this.promoCodeService.getList(filter);
    this.promoCodeList = response.data;
    this.nbTotal = response.total;
  }

  setSelectedType(type) {
    this.type = type;
    this.refreshList();
  }

  displayAddForm() {
    this.promoCodeFormGroup.reset();
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: "bonus" }
    }
    this.hideEditForm = false;
    this.submitForm = this.submitAddForm;
  }


  async displayEditForm(promoCode) {
    this.promoCodeFormGroup.reset();
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: 'bonus'
      }
    }
    this.hideEditForm = false;
    this.promoCodeSelected = await this.promoCodeService.get(promoCode._id);
    this.submitForm = this.submitEditForm;
    this.promoCodeFormGroup.patchValue(this.promoCodeSelected);
  }

  async submitAddForm() {
    console.log(this.promoCodeFormGroup.value)
    if (this.promoCodeFormGroup.valid) {
      try {
        await this.promoCodeService.create(this.promoCodeFormGroup.value as PromoCode);
        this.promoCodeFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async submitEditForm() {
    if (this.promoCodeFormGroup.valid) {
      try {
        await this.promoCodeService.edit(this.promoCodeSelected._id, this.promoCodeFormGroup.value as PromoCode);
        this.promoCodeFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async deleteElement(id) {
    try {
      await this.promoCodeService.delete(id);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }

  async changeStatus(promoCodeSelected, status){
    try {
      const response : any = await this.promoCodeService.changeStatus(promoCodeSelected._id, status);
      await this.refreshList();
    } catch (error) {
      console.log(error)
    }
  }
}
