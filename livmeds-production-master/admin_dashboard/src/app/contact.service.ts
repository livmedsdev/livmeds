import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  async list(filter){
    return await this.http.get(environment.url + '/admin/contacts', {
      params: filter
    }).toPromise();
  }

  async get(id){
    return await this.http.get(environment.url + '/admin/contact/' + id).toPromise();
  }

  async delete(id){
    return await this.http.delete(environment.url + '/admin/contact/' + id).toPromise();
  }

  async updateStatus(id, status){
    return await this.http.put(environment.url + '/admin/contact/status/' + id, {
      status
    }).toPromise();
  }

}
