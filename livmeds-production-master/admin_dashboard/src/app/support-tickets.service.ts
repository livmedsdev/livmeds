import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SupportTicketsService {

  
  constructor(private http: HttpClient) { }

  async list(filter){
    return await this.http.get(environment.url + '/admin/support-tickets', {
      params: filter
    }).toPromise();
  }

  async get(id){
    return await this.http.get(environment.url + '/admin/support-tickets/' + id).toPromise();
  }

  async updateStatus(id, status){
    return await this.http.put(environment.url + '/admin/support-tickets/status/' + id, {
      status
    }).toPromise();
  }

  async answer(id, body){
    return await this.http.post(environment.url + '/admin/support-tickets/' + id, body).toPromise();
  }

  async delete(id){
    return await this.http.delete(environment.url + '/admin/support-tickets/' + id).toPromise();
  }
}
