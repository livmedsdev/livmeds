import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { UserService } from '../user.service';
import { TranslateService } from '@ngx-translate/core';
import { SessionService } from '../session.service';
import { SseService } from '../sse.service';
import { User } from '../models/User';
import { UserRole } from '../enums/UserRole';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { FileService } from '../file.service';

declare var google;

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  userSelected;
  listUsers = [];
  role;

  hideList;
  hideEditForm;
  titleForm;
  submitForm;
  error;
  errorList;
  successList;
  passwordMandatory;
  titleDetails;
  hideDetails;
  selectedForShowDetails;

  private address;
  @ViewChild("address", { static: false })
  public addressElement: ElementRef;

  public formGroup;
  public socialSecurityNumberUrl;
  mutualUrl;

  // list
  public page: number;
  public limit: number;
  public nbTotal: number;
  public text: string;

  constructor(public userService: UserService,
    private translate: TranslateService,
    private _ngxPicaService: NgxPicaService,
    private fileService: FileService,
    public session: SessionService) {

    this.initFrm();
  }

  initFrm(type = 'ADD') {
    switch (type) {
      case 'ADD':
        this.formGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          socialSecurityNumber: ['', Validators.compose([Validators.required])],
          mutual: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          password: ['', Validators.compose([Validators.required])],
          passwordConfirm: ['', Validators.compose([Validators.required])],
          compAddress: ['', Validators.compose([])],
        });
      case 'EDIT':
        this.formGroup = new FormBuilder().group({
          mail: ['', Validators.compose([Validators.required])],
          name: ['', Validators.compose([Validators.required])],
          lastName: ['', Validators.compose([Validators.required])],
          socialSecurityNumber: ['', Validators.compose([Validators.required])],
          mutual: ['', Validators.compose([Validators.required])],
          phoneNumber: ['', Validators.compose([Validators.required])],
          password: ['', Validators.compose([])],
          passwordConfirm: ['', Validators.compose([])],
          compAddress: ['', Validators.compose([])],
        });
    }
  }

  async ngOnInit() {
    this.hideList = false;
    this.hideEditForm = true;
    this.hideDetails = true;
    this.titleForm = {};
    this.address = null;
    this.role = UserRole.ROLE_CUSTOMER;
    this.userSelected = {} as User;
    this.page = 0;
    this.limit = 5;
    await this.refreshList();
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    if (this.role)
      filter.role = this.role;

    if(this.text && this.text !== ''){
      filter.text = this.text;
    }

    const response: any = await this.userService.listUsers(filter);
    this.listUsers = response.data;
    this.nbTotal = response.total;
  }

  async addAdmin() {
    this.closeDetails();
    window.scrollTo(0, 0);
    this.resetError();
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: await this.translate.get('FIELD_CUSTOMER').toPromise() }
    }
    this.hideList = false;
    this.hideEditForm = false;
    this.submitForm = this.submitAdd;
    this.userSelected = { password: '' } as User;
    this.addressElement.nativeElement.value = "";
    this.address = null;
    this.passwordMandatory = true;
    this.mutualUrl = undefined;
    this.socialSecurityNumberUrl = undefined;
    this.formGroup.reset();
  }


  async submitAdd() {
    this.resetError();
    if (!this.formGroup.valid)
      return
    const value = Object.assign({}, this.formGroup.value);
    value.role = this.role;
    value.favoriteAddress = this.address;
    try {
      await this.userService.register(value);
      this.hideEditForm = true;
      this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async editAdmin(admin) {
    this.closeDetails();
    window.scrollTo(0, 0);
    this.resetError();
    this.formGroup.reset();
    this.initFrm('EDIT');
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: admin.name
      }
    }
    this.hideEditForm = false;
    const data : any = await this.userService.getUser(admin._id);
    this.userSelected = data;
    this.formGroup.patchValue(data);
    this.submitForm = this.submitEdit;
    this.passwordMandatory = false;
    this.addressElement.nativeElement.value = this.userService.getFavoriteAddress(data.addresses).fullAddress;
    this.formGroup.patchValue({
        compAddress: this.userService.getFavoriteAddress(data.addresses).compAddress
    })
    this.mutualUrl = undefined;
    this.socialSecurityNumberUrl = undefined;
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  closeDetails() {
    this.selectedForShowDetails = undefined;
  }

  showDetails(user) {
    this.hideEditForm = true;
    this.titleDetails = {
      label: 'TITLE_DETAILS',
      params: {
        name: user.name + ' ' + user.lastName
      }
    }
    this.selectedForShowDetails = user;
  }

  async submitEdit() {
    this.resetError();
    try {
      const value = Object.assign({}, this.formGroup.value);
      value._id = this.userSelected._id;
      value.role = this.role;
      value.favoriteAddress = this.address;
      const response = await this.userService.editUser(value);
      this.hideEditForm = true;
      this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async changeStatus(adminSelected, status) {
    this.resetError();
    try {
      const response: any = await this.userService.changeStatus(adminSelected._id, status);
      this.successList = response.tradCode;
      this.refreshList();
    } catch (error) {
      this.errorList = error.error.tradCode;
    }
  }

  resetError() {
    this.successList = null;
    this.error = null;
    this.errorList = null;
  }


  ngAfterViewInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    autocomplete.setComponentRestrictions({ 'country': ['fr'] });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      let place = autocomplete.getPlace();
      this.address = this.retrieveAddressFromPlace(place);
      this.userSelected.favoriteAddress = this.address;
    });
  }

  retrieveAddressFromPlace(place) {
    console.log(place);

    const addressComponents = place.address_components


    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      address: this.getInformationInAddressComponent("street_number", addressComponents) + ' ' +
        this.getInformationInAddressComponent("route", addressComponents),
      country: this.getInformationInAddressComponent("country", addressComponents),
      postalCode: this.getInformationInAddressComponent("postal_code", addressComponents),
      city: this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents) {
    for (let element of addressComponents) {
      for (let type of element.types) {
        if (elementName === type) {
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }

  changeMutual(base64Image) {
    this.mutualUrl = base64Image;
    this.formGroup.patchValue({
      mutual: base64Image
    });
  }

  changeSocialSecurityNumber(base64Image) {
    this.socialSecurityNumberUrl = base64Image;
    this.formGroup.patchValue({
      socialSecurityNumber: base64Image
    });
  }

  async onFileChanged(event, methodToChange) {
    const selectedFile = event.target.files[0];
    const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 600, 600, {
      aspectRatio: {
        keepAspectRatio: true
      }
    }).toPromise();

    this[methodToChange](await this.toBase64(imageResized));
  }

  toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  async unlock(user){
    this.resetError();
    try {
      const response : any = await this.userService.unlock(user._id);
      this.successList = response.tradCode;
      this.refreshList();
    } catch (error) {
      this.errorList = error.error.tradCode;
    }
  }

  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }
}
