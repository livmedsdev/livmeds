import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  async uploadForm(formData, type) {
    return await this.http.post(environment.url + '/file/upload/'+type,
      formData).toPromise();
  }


  async getSignedUrl(idFile) {
    const data = JSON.parse(idFile);
    return (await this.http.post(environment.url + '/file/url', data).toPromise())['url'];
  }
}
