import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from '../config.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {
  public formGroup;
  public config :any = {};
  public successList;
  public successListVersions;
  public categories = [{
    label: 'ROLE_PHARMACY',
    value: 'ROLE_PHARMACY'
  }, {
    label: 'ROLE_OPTICIAN',
    value: 'ROLE_OPTICIAN'
  }, {
    label: 'ROLE_VETERINARY',
    value: 'ROLE_VETERINARY'
  }]

  public categoriesSelected: Array<any> = [];

  constructor(private configService: ConfigService) { }

  async ngOnInit() {
    this.formGroup = new FormBuilder().group({
      versionIosMin: ['', Validators.compose([Validators.required])],
      versionAndroidMin: ['', Validators.compose([Validators.required])]
    });
    
    this.config = await this.configService.get();
    this.categoriesSelected = this.config.categoriesAvailable;
    

    this.formGroup.patchValue(this.config)
  }

  onCheckChange(event) {
    console.log(event);
    /* Selected */
    if(event.target.checked){
      // Add a new control in the arrayForm
      this.categoriesSelected.push(event.target.value);
    }
    /* unselected */
    else{
      // find the unselected element
      let i: number = 0;
  
      this.categoriesSelected.forEach((cat: any) => {

        if(cat === event.target.value) {
          // Remove the unselected element from the arrayForm
          this.categoriesSelected.splice(i,1);
          return;
        }
  
        i++;
      });
      console.log(this.categoriesSelected);
    }
  }

  async submit(){
    try{
      await this.configService.edit({categoriesAvailable: this.categoriesSelected});
      this.successList = "Configuration modifiée !"
    }catch(error){
      console.log("error");
    }
  }

  async submitForm(){
    try{
      await this.configService.edit(this.formGroup.value);
      this.successListVersions = "Configuration modifiée !"
    }catch(error){
      console.log("error");
    }
  }
}
