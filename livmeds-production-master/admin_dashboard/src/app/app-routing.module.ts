import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { BlogComponent } from './blog/blog.component';
import { PrimeComponent } from './prime/prime.component';
import { ProfileComponent } from './profile/profile.component';
import { DeliveriesComponent } from './deliveries/deliveries.component';
import { DeliverersComponent } from './deliverers/deliverers.component';
import { PharmaciesComponent } from './pharmacies/pharmacies.component';
import { CustomersComponent } from './customers/customers.component';
import { PromoCodeComponent } from './promo-code/promo-code.component';
import { ProductComponent } from './product/product.component';
import { OpticiansComponent } from './opticians/opticians.component';
import { VeterinariesComponent } from './veterinaries/veterinaries.component';
import { NursesComponent } from './nurses/nurses.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { ConfigComponent } from './config/config.component';
import { MessagesComponent } from './messages/messages.component';
import { SupportTicketsComponent } from './support-tickets/supports-tickets.component';
import { BillsComponent } from './bills/bills.component';
import { PartnerComponent } from './partner/partner.component';


const routes: Routes = [{
  path: 'login',
  component: LoginComponent
},{
  path: 'dashboard',
  component: DashboardComponent
},{
  path: "",
  redirectTo: "login",
  pathMatch: "full"
},{
  path: "admin",
  component: AdminComponent
},
{
  path: 'blog',
  component: BlogComponent
},
{
  path: 'profile',
  component: ProfileComponent
},
{
  path: 'prime',
  component: PrimeComponent
},{
  path: 'deliveries',
  component: DeliveriesComponent
},{
  path: 'bills',
  component: BillsComponent
},{
  path: 'deliverers',
  component: DeliverersComponent
},{
  path: 'pharmacies',
  component: PharmaciesComponent
},{
  path: 'customers',
  component: CustomersComponent
},{
  path: 'promo-code',
  component: PromoCodeComponent
},{
  path: 'product',
  component: ProductComponent
},{
  path: 'opticians',
  component: OpticiansComponent
},{
  path: 'veterinaries',
  component: VeterinariesComponent
},{
  path: 'nurses',
  component: NursesComponent
},{
  path: 'doctors',
  component: DoctorsComponent
},{
  path: 'config',
  component: ConfigComponent
},{
  path: 'messages',
  component: MessagesComponent
},{
  path: 'support-tickets',
  component: SupportTicketsComponent
},{
  path: 'partners',
  component: PartnerComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
