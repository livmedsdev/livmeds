import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { FormBuilder, Validators } from '@angular/forms';
import { PromoCode } from '../models/PromoCode';
import { TranslateService } from '@ngx-translate/core';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { Admin } from '../models/Admin';
import { FileService } from '../file.service';
import { environment } from 'src/environments/environment';
import { CategoryProduct } from '../enums/CategoryProduct';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public productList: any[] = [];

  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public type: number;

  public hideEditForm;
  public productFormGroup;
  public titleForm;
  public submitForm;
  public productSelected;

  public urlImage;
  public categoriesProduct = [];

  constructor(private translate: TranslateService,
    private fileService: FileService,
    private _ngxPicaService: NgxPicaService, private productService: ProductService) {
    this.productFormGroup = new FormBuilder().group({
      label: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      category: ['', Validators.compose([Validators.required])],
      required: ['', Validators.compose([])],
      image: ['', Validators.compose([])]
    });
  }

  async ngOnInit() {
    this.hideEditForm = true;
    this.titleForm = {};
    this.type = null;
    this.page = 0;
    this.limit = 5;
    this.productList = [];
    this.categoriesProduct = Object.keys(CategoryProduct);
    await this.refreshList();
  }
  
  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    const response: any = await this.productService.getList(filter);
    this.productList = response.data;
    this.nbTotal = response.total;
  }

  setSelectedType(type) {
    this.type = type;
    this.refreshList();
  }

  async displayAddForm() {
    this.productFormGroup.reset();
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: await this.translate.get('PRODUCT').toPromise() }
    }
    this.hideEditForm = false;
    this.submitForm = this.submitAddForm;
    this.urlImage = undefined;
  }


  async displayEditForm(promoCode) {
    window.scrollTo(0, 0);
    this.productFormGroup.reset();
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: await this.translate.get('PRODUCT').toPromise()
      }
    }
    this.hideEditForm = false;
    this.productSelected = await this.productService.get(promoCode._id);
    this.submitForm = this.submitEditForm;
    this.productFormGroup.patchValue(this.productSelected);
    if(this.productSelected.image){
      this.urlImage = await this.fileService.getSignedUrl(this.productFormGroup.value.image);
    }else{
      this.urlImage = undefined;
    }
    console.log(this.urlImage)
  }

  async submitAddForm() {
    console.log(this.productFormGroup.value)
    if (this.productFormGroup.valid) {
      try {
        await this.productService.create(this.productFormGroup.value);
        this.productFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async submitEditForm() {
    if (this.productFormGroup.valid) {
      try {
        await this.productService.edit(this.productSelected._id, this.productFormGroup.value);
        this.productFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async deleteElement(id) {
    try {
      await this.productService.delete(id);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }

  async onFileChanged(event){
    const selectedFile = event.target.files[0];
    const imageResized: File = await this._ngxPicaService.resizeImages([selectedFile], 200, 200, {aspectRatio: {
      keepAspectRatio: true
    }}).toPromise();
              
    const uploadData = new FormData();
    uploadData.append('file', imageResized, selectedFile.name);
    const response : any = await this.fileService.uploadForm(uploadData, 'avatar');
    this.productFormGroup.patchValue({
      image: response.idFile
    });
    this.urlImage = await this.fileService.getSignedUrl(this.productFormGroup.value.image);
  }
}
