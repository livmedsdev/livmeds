import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PrimeService } from '../prime.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Prime } from '../models/Prime';
import { TypeBonus } from '../models/TypeBonus';

declare var google;

@Component({
  selector: 'app-prime',
  templateUrl: './prime.component.html',
  styleUrls: ['./prime.component.scss']
})
export class PrimeComponent implements OnInit {
  public primes: any[] = [];
  public types;

  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public type: number;

  public hideEditForm;
  public primeFormGroup;
  public titleForm;
  public submitForm;
  public primeSelected;
  
  private address;
  @ViewChild("address", { static: false })
  public addressElement: ElementRef;

  constructor(private primeService: PrimeService) {
    this.primeFormGroup = new FormBuilder().group({
      startFrom: ['', Validators.compose([Validators.required])],
      endAt: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      placeId: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])]
    });
  }

  async ngOnInit() {
    this.hideEditForm = true;
    this.titleForm = {};
    this.type = null;
    this.page = 0;
    this.limit = 5;
    this.primes = [];
    this.types= Object.keys(TypeBonus);
    console.log(this.types)
    await this.refreshList();
  }

  ngAfterViewInit() {
    let autocomplete = new google.maps.places.Autocomplete(this.addressElement.nativeElement);
    autocomplete.setComponentRestrictions({'country': ['fr']});
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      let place = autocomplete.getPlace();
      this.address = this.retrieveAddressFromPlace(place);
      this.primeFormGroup.patchValue({
        placeId: this.address.placeId, 
        city : this.address.city});
    });
  }

  retrieveAddressFromPlace(place){
    console.log(place);

    const addressComponents = place.address_components
    

    const address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      fullAddress: place.formatted_address,
      placeId: place.place_id,
      city:this.getInformationInAddressComponent("locality", addressComponents),
    };
    return address;
  }

  getInformationInAddressComponent(elementName, addressComponents){
    for(let element of addressComponents){
      for(let type of element.types){
        if(elementName === type){
          return element.long_name;
        }
      }
    }
    return "ERROR_GMAPS_NOT_FOUND";
  }
  
  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    if (this.type)
      filter.type = this.type;

    const response: any = await this.primeService.getPrimes(filter);
    this.primes = response.data;
    this.nbTotal = response.total;
  }

  setSelectedType(type) {
    this.type = type;
    this.refreshList();
  }

  displayAddForm() {
    this.primeFormGroup.reset();
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: "bonus" }
    }
    this.hideEditForm = false;
    this.submitForm = this.submitAddForm;
  }


  async displayEditForm(prime) {
    this.primeFormGroup.reset();
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: 'bonus'
      }
    }
    this.hideEditForm = false;
    this.primeSelected = await this.primeService.getPrime(prime._id);
    this.submitForm = this.submitEditForm;
    this.primeFormGroup.patchValue(this.primeSelected);
    this.addressElement.nativeElement.value = this.primeSelected.city;
  }

  async submitAddForm() {
    console.log(this.primeFormGroup.value)
    if (this.primeFormGroup.valid) {
      try {
        await this.primeService.createPrime(this.primeFormGroup.value as Prime);
        this.primeFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async submitEditForm() {
    if (this.primeFormGroup.valid) {
      try {
        await this.primeService.editPrime(this.primeSelected._id, this.primeFormGroup.value as Prime);
        this.primeFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async deleteElement(id) {
    try {
      await this.primeService.deletePrime(id);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }

  async changeStatus(primeSelected, status){
    try {
      const response : any = await this.primeService.changeStatus(primeSelected._id, status);
      await this.refreshList();
    } catch (error) {
      console.log(error)
    }
  }
}
