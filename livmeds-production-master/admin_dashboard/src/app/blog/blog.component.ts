import { Component, OnInit } from '@angular/core';
import { BlogService } from '../blog.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Topic } from '../models/Topic';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  public topics: any[] = [];
  public categories: any[] = [];

  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public selectedCategory;
  public searchText;

  // Pagination messages
  public pageMessages: number;
  public limitMessages: number;
  public nbTotalMessages: number;

  public hideEditForm;
  public topicFormGroup;
  public messageFormGroup;
  public titleForm;
  public submitForm;
  public topicSelected;
  public selectedTopicMessages;
  public hideManageMessage;
  public topicForManageMessage;

  constructor(private blogService: BlogService) {
  }

  async ngOnInit() {
    this.hideManageMessage = true;
    this.hideEditForm = true;
    this.titleForm = {};
    this.selectedTopicMessages = [];
    this.topicFormGroup = new FormBuilder().group({
      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      category: ['', Validators.compose([Validators.required])]
    });
    this.messageFormGroup = new FormBuilder().group({
      content: ['', Validators.compose([Validators.required])]
    });
    this.selectedCategory = null;
    this.searchText = null;
    this.page = 0;
    this.limit = 5;
    this.pageMessages = 0;
    this.limitMessages = 5;
    this.topics = [];
    this.categories = [];
    await this.refreshList();
    this.categories = await this.blogService.getCategories();
    this.nbTotalMessages = (await this.blogService.getStat())['nbMessages'];
  }

  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }
  
  async changePageMessages(newValue) {
    this.pageMessages = newValue;
    await this.refreshListMessages();
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  closeManageMessage() {
    this.hideManageMessage = true;
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    if (this.selectedCategory)
      filter.category = this.selectedCategory._id;

    if (this.searchText) {
      filter.text = this.searchText;
    }

    const response: any = await this.blogService.getTopics(filter);
    this.topics = response.data;
    this.nbTotal = response.total;
  }

  async refreshListMessages() {
    const filter: any = {
      page: this.pageMessages,
      limit: this.limitMessages
    };

    const response: any = await this.blogService.getMessages(this.topicForManageMessage._id, filter);
    this.selectedTopicMessages = response.data;
    this.nbTotalMessages = response.total;
    this.nbTotalMessages = (await this.blogService.getStat())['nbMessages'];
  }

  setSelectedCategory(category) {
    this.selectedCategory = category
    this.refreshList();
  }

  setSearchText(event) {
    this.searchText = event.target.value;
    this.refreshList();
  }

  displayAddForm() {
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: "topic" }
    }
    this.hideEditForm = false;
    this.submitForm = this.submitAddForm;
  }


  async displayEditForm(topic) {
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: topic.title
      }
    }
    this.hideEditForm = false;
    this.topicSelected = await this.blogService.getTopic(topic._id);
    this.topicSelected.category = this.topicSelected.category._id;
    this.submitForm = this.submitEditForm;
    this.topicFormGroup.patchValue(this.topicSelected);
  }

  async submitAddForm() {
    if (this.topicFormGroup.valid) {
      try {
        const newTopic: any = await this.blogService.createTopic(this.topicFormGroup.value as Topic);
        this.topicFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async submitEditForm() {
    if (this.topicFormGroup.valid) {
      try {
        const newTopic: any = await this.blogService.editTopic(this.topicSelected._id, this.topicFormGroup.value as Topic);
        this.topicFormGroup.reset();
        this.hideEditForm = true;
        await this.refreshList();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async deleteElement(id) {
    try {
      await this.blogService.deleteTopic(id);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }

  async deleteMessage(id) {
    try {
      await this.blogService.deleteMessage(id);
      await this.refreshListMessages();
    } catch (error) {
      console.log(error);
    }
  }

  
  async submitAddMessage() {
    try {
      await this.blogService.createMessage(this.topicForManageMessage._id, this.messageFormGroup.value.content);
      await this.refreshListMessages();
    } catch (error) {
      console.log(error);
    }
  }

  async displayManageMessage(topic) {
    try {
      this.topicForManageMessage = topic;
      await this.refreshListMessages();
      this.hideManageMessage = false;
      this.pageMessages = 0;
      this.limitMessages = 5;
    } catch (error) {
      console.log(error);
    }
  }
  
  
}
