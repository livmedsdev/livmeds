import { Injectable } from '@angular/core';
import { User } from './models/User';
import { Admin } from './models/Admin';
import { environment } from 'src/environments/environment';
import { FileService } from './file.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private readonly langKey = "lang"; 

  constructor(private fileService: FileService) { }

  currentAdmin(): Admin{
    return {
      _id: localStorage.getItem('id'),
      name: localStorage.getItem('name'),
      role: localStorage.getItem('role'),
      mail: localStorage.getItem('mail'),
      avatar: localStorage.getItem('avatar'),
      lastName: localStorage.getItem('lastName'),
      phoneNumber: localStorage.getItem('phoneNumber')
    } as Admin;
  }

  async saveInSession(data){
    localStorage.setItem("id", data._id);
    localStorage.setItem("name", data.name);
    localStorage.setItem("lastName", data.lastName);
    localStorage.setItem("role", data.role);
    localStorage.setItem("mail", data.mail);
    localStorage.setItem("avatar", (data.avatar)? await this.fileService.getSignedUrl(data.avatar):'undefined');
    localStorage.setItem("phoneNumber", data.phoneNumber)
  }

  setLang(value){
    localStorage.setItem(this.langKey, value);
  }
}
