export class PromoCode {
    usedTimes: Number;
    usedTimeMax: Number;
    promoCode: String;
    percentage: Number;
    enabled: Boolean;
    insertAt: Date;
}