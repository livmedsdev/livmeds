export class User {
    mail: string;
    name: string;
    lastName: string;
    password: string;
    role: any;
    fullAddress: string;
    favoriteAddress: any;
    type: string;
    avatar:string;
}