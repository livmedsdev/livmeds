export class Admin {
    _id: string;
    mail: string;
    name: string;
    lastName: string;
    password: string;
    role: string;
    avatar:string;
    phoneNumber: string;
}