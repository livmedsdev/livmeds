import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {
  
  constructor(private http: HttpClient, private session: SessionService) { }

  async getList(filter) {
    return await this.http.get(environment.url + '/admin/deliveries', {
      params: filter
    }).toPromise();
  }

  async getListDelivered(filter) {
    return await this.http.get(environment.url + '/admin/bills', {
      params: filter
    }).toPromise();
  }
  
  async get(deliveryId) {
    return await this.http.get(environment.url + '/admin/delivery/' + deliveryId).toPromise();
  }

  async create(delivery) {
    return await this.http.post(environment.url + '/admin/delivery', delivery).toPromise();
  }

  async edit(deliveryId, delivery) {
    return await this.http.put(environment.url + '/admin/delivery/' + deliveryId, delivery).toPromise();
  }

  async delete(deliveryId) {
    return await this.http.delete(environment.url + '/admin/delivery/' + deliveryId).toPromise();
  }

  async changeStatus(id){
    return await this.http.put(environment.url + "/admin/delivery/status/" + id, {}).toPromise();
  }

  async changeStatusToDelivered(id){
    return await this.http.put(environment.url + "/admin/delivery/status-delivered/" + id, {}).toPromise();
  }

  async updateDelivererOfDelivery(id, deliverer){
    return await this.http.put(environment.url + "/admin/delivery/deliverer/" + id, {deliverer}).toPromise();
  }

  async cancel(id){
    return await this.http.put(environment.url + "/admin/delivery/cancel/" + id, {}).toPromise();
  }
}
