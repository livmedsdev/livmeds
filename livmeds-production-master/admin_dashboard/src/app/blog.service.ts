import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Topic } from './models/Topic';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient, private session: SessionService) { }

  async getTopics(filter){
    return await this.http.get(environment.url + '/admin/topics',{
      params: filter
    }).toPromise();
  }

  async getTopic(topicId){
    return await this.http.get(environment.url + '/admin/topic/'+ topicId).toPromise();
  }

  async getStat(){
    return await this.http.get(environment.url + '/admin/blog/stat/').toPromise();
  }

  async getMessages(topicId, filter){
    return await this.http.get(environment.url + '/admin/messages/'+topicId,{
      params: filter
    }).toPromise();
  }
  
  async createTopic(topic: Topic){
    return await this.http.post(environment.url + '/admin/topic', topic).toPromise();
  }
  
  async editTopic(topicId, topic: Topic){
    return await this.http.put(environment.url + '/admin/topic/'+ topicId, topic).toPromise();
  }

  async createMessage(topic : String, content : String){
    return await this.http.post(environment.url + '/admin/message', {topic, content}).toPromise();
  }

  async getCategories(){
    return await this.http.get(environment.url + '/admin/categories').toPromise() as [];
  }

  async deleteTopic(topicId){
    return await this.http.delete(environment.url + '/admin/topic/'+ topicId).toPromise();
  }

  async deleteMessage(messageId){
    return await this.http.delete(environment.url + '/admin/message/'+ messageId).toPromise();
  }
}
