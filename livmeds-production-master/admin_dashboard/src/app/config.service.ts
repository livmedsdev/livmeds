import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private http: HttpClient, private session: SessionService) { }
  
  async get() {
    return await this.http.get(environment.url + '/admin/config').toPromise();
  }


  async edit(body) {
    return await this.http.put(environment.url + '/admin/config', body).toPromise();
  }
}
