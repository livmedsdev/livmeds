import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { PromoCode } from './models/PromoCode';

@Injectable({
  providedIn: 'root'
})
export class PromoCodeService {

  constructor(private http: HttpClient, private session: SessionService) { }

  async getList(filter) {
    return await this.http.get(environment.url + '/admin/promoCodes', {
      params: filter
    }).toPromise();
  }
  
  async get(promoCodeId) {
    return await this.http.get(environment.url + '/admin/promoCode/' + promoCodeId).toPromise();
  }

  async create(promoCode: PromoCode) {
    return await this.http.post(environment.url + '/admin/promoCode', promoCode).toPromise();
  }

  async edit(promoCodeId, promoCode: PromoCode) {
    return await this.http.put(environment.url + '/admin/promoCode/' + promoCodeId, promoCode).toPromise();
  }

  async delete(promoCodeId) {
    return await this.http.delete(environment.url + '/admin/promoCode/' + promoCodeId).toPromise();
  }

  async changeStatus(id, status){
    return await this.http.put(environment.url + "/admin/promoCode/enabled/" + id, {status}).toPromise();
  }
}
