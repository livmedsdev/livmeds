import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { MainEventService } from '../main-event.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public user: User = {} as User;
  private isConnected;
  public error;

  constructor(private userService: UserService, private router: Router, @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2, private event: MainEventService) { }

  ngOnInit(): void {
    this.event.sendEventToHideTopBar(true);
    this.event.sendEventToHideSidebar(true);
    this.event.sendEventToHidePadding(true);
    $(".navbar-toggler").on('click', function () {
      $("html").toggleClass("nav-open");
    });
    //=============================================================================
    $('.form-control').on("focus", function () {
      $(this).parent('.input-group').addClass("input-group-focus");
    }).on("blur", function () {
      $(this).parent(".input-group").removeClass("input-group-focus");
    });
    this.renderer.addClass(this.document.body, 'authentication');
    this.renderer.addClass(this.document.body, 'sidebar-collapse');
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'authentication');
    this.renderer.removeClass(this.document.body, 'sidebar-collapse');
  }

  async submitLogin() {
    try{
      await this.userService.login(this.user);
      this.router.navigateByUrl("/dashboard", {})
    }catch(error){
      console.log(error);
      this.error = error.error.message;
    }
  }

  async submitRegister() {
    await this.userService.register(this.user);
  }
}
