import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { UserService } from './user.service';
import { TokenInterceptorService } from './token-interceptor.service';
import { DeliveryService } from './delivery.service';
import { AdminComponent } from './admin/admin.component';
import { MainEventService } from './main-event.service';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BlogComponent } from './blog/blog.component';
import { BlogService } from './blog.service';
import { PrimeComponent } from './prime/prime.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PromoCodeComponent } from './promo-code/promo-code.component';
import { ProfileComponent } from './profile/profile.component';
import { FileService } from './file.service';
import { NgxPicaModule } from '@digitalascetic/ngx-pica';
import { DeliveriesComponent } from './deliveries/deliveries.component';
import { CustomersComponent } from './customers/customers.component';
import { DeliverersComponent } from './deliverers/deliverers.component';
import { PharmaciesComponent } from './pharmacies/pharmacies.component';
import { ProductComponent } from './product/product.component';
import { OpticiansComponent } from './opticians/opticians.component';
import { VeterinariesComponent } from './veterinaries/veterinaries.component';
import { NursesComponent } from './nurses/nurses.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { ConfigComponent } from './config/config.component';
import { MessagesComponent } from './messages/messages.component';
import { SupportTicketsComponent } from './support-tickets/supports-tickets.component';
import { BillsComponent } from './bills/bills.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToasterModule } from 'angular2-toaster';
import { NgxSpinnerModule } from "ngx-spinner";
import { TooltipDirective } from './tooltip.directive';
import { PartnerService } from './partner.service';
import { PartnerComponent } from './partner/partner.component';
import { APP_BASE_HREF, PlatformLocation } from '@angular/common';
import { environment } from 'src/environments/environment';
import { FormatService } from './format.service';

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function createTranslateLoader(http: HttpClient) {
  // return new TranslateHttpLoader(http, environment.baseHref + "assets/i18n/", '.json');
  return new TranslateHttpLoader(http, "./assets/i18n/", '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    AdminComponent,
    BlogComponent,
    PrimeComponent,
    PromoCodeComponent,
    ProfileComponent,
    PartnerComponent,
    DeliveriesComponent,
    CustomersComponent,
    DeliverersComponent,
    BillsComponent,
    PharmaciesComponent,
    ProductComponent,
    OpticiansComponent,
    VeterinariesComponent,
    NursesComponent,
    DoctorsComponent,
    ConfigComponent,
    MessagesComponent,
    SupportTicketsComponent,
    TooltipDirective,
  ],
  imports: [
    ToasterModule.forRoot(),
    BrowserModule,
    NgxPicaModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    AutocompleteLibModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    HttpClientModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [UserService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  },
    DeliveryService,
    BlogService,
    MainEventService,
    { provide: LOCALE_ID, useValue: "fr-FR" },
    FileService,
    PartnerService,
    {
      provide: APP_BASE_HREF,
      useFactory: (s: PlatformLocation) => s.getBaseHrefFromDOM(),
      deps: [PlatformLocation]
    },
    FormatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
