import { Injectable } from '@angular/core';
import { Prime } from './models/Prime';
import { environment } from 'src/environments/environment';
import { SessionService } from './session.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PrimeService {

  constructor(private http: HttpClient, private session: SessionService) { }

  async getPrimes(filter) {
    return await this.http.get(environment.url + '/admin/primes', {
      params: filter
    }).toPromise();
  }
  
  async getPrime(primeId) {
    return await this.http.get(environment.url + '/admin/prime/' + primeId).toPromise();
  }

  async createPrime(prime: Prime) {
    return await this.http.post(environment.url + '/admin/prime', prime).toPromise();
  }

  async editPrime(primeId, prime: Prime) {
    return await this.http.put(environment.url + '/admin/prime/' + primeId, prime).toPromise();
  }

  async deletePrime(primeId) {
    return await this.http.delete(environment.url + '/admin/prime/' + primeId).toPromise();
  }

  async changeStatus(id, status){
    return await this.http.put(environment.url + "/admin/prime/status/" + id, {status}).toPromise();
  }

}
