import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Admin } from '../models/Admin';
import { SessionService } from '../session.service';
import { SseService } from '../sse.service';
import { UserRole } from '../enums/UserRole';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  adminSelected;
  listUsers = [];
  role;

  hideList;
  hideEditForm;
  titleForm;
  submitForm;
  error;
  errorList;
  successList;
  passwordMandatory;
  titleDetails;
  hideDetails;
  selectedForShowDetails;

  // list
  public page: number;
  public limit: number;
  public nbTotal: number;
  public text: string;
  
  constructor(private userService: UserService,
    public session: SessionService, private sseService: SseService) { }

  async ngOnInit() {
    this.hideList = false;
    this.hideEditForm = true;
    this.hideDetails = true;
    this.titleForm = {};
    this.role = UserRole.ROLE_ADMIN;
    this.adminSelected = {} as Admin;
    this.page = 0;
    this.limit = 5;
    this.refreshList();
  }


  async addAdmin() {
    this.closeDetails();
    window.scrollTo(0, 0);
    this.resetError();
    this.titleForm = {
      label: 'TITLE_ADD_FORM',
      params: { role: "admin" }
    }
    this.hideList = false;
    this.hideEditForm = false;
    this.submitForm = this.submitAdd;
    this.adminSelected = {password:''} as Admin;
    this.passwordMandatory = true;
  }


  async submitAdd() {
    this.resetError();
    if (!this.adminSelected.name || !this.adminSelected.mail || !this.adminSelected.password)
      return
    this.adminSelected.role = this.role;
    try {
      await this.userService.register(this.adminSelected);
      this.hideEditForm = true;
      await this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async editAdmin(admin) {
    
    this.closeDetails();
    window.scrollTo(0, 0);
    this.resetError();
    this.titleForm = {
      label: 'TITLE_EDIT_FORM',
      params: {
        name: admin.name
      }
    }
    this.hideEditForm = false;
    this.adminSelected = await this.userService.getUser(admin._id);
    this.submitForm = this.submitEdit;
    this.passwordMandatory = false;
  }

  closeEdit() {
    this.hideEditForm = true;
  }

  closeDetails(){
    this.selectedForShowDetails = undefined;
  }

  showDetails(user){
    this.hideEditForm = true;
    this.titleDetails = {
      label: 'TITLE_DETAILS',
      params: {
        name: user.name + ' ' + user.lastName
      }
    }
    this.selectedForShowDetails = user;
  }

  async submitEdit() {
    this.resetError();
    try {
      const response = await this.userService.editUser(this.adminSelected);
      this.hideEditForm = true;
      await this.refreshList();
    } catch (error) {
      this.error = error.error.tradCode;
    }
  }

  async changeStatus(adminSelected, status){
    this.resetError();
    try {
      const response : any = await this.userService.changeStatus(adminSelected._id, status);
      this.successList = response.tradCode;
      await this.refreshList();
    } catch (error) {
      this.errorList = error.error.tradCode;
    }
  }

  resetError(){
    this.successList = null;
    this.error = null;
    this.errorList = null;
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    if (this.role)
      filter.role = this.role;

    if(this.text && this.text !== ''){
      filter.text = this.text;
    }

    const response: any = await this.userService.listUsers(filter);
    this.listUsers = response.data;
    this.nbTotal = response.total;
  }

  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }
}
