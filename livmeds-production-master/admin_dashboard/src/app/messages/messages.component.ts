import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { FormBuilder, Validators } from '@angular/forms';
import { PromoCode } from '../models/PromoCode';
import { TranslateService } from '@ngx-translate/core';
import { NgxPicaService } from '@digitalascetic/ngx-pica';
import { Admin } from '../models/Admin';
import { FileService } from '../file.service';
import { environment } from 'src/environments/environment';
import { CategoryProduct } from '../enums/CategoryProduct';
import { ContactService } from '../contact.service';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  public messagesList: any[] = [];

  // Pagination topics
  public page: number;
  public limit: number;
  public nbTotal: number;
  public type: number;

  constructor(private contactService: ContactService) {
  }

  async ngOnInit() {
    this.type = null;
    this.page = 0;
    this.limit = 5;
    this.messagesList = [];
    await this.refreshList();
  }
  
  async changePage(newValue) {
    this.page = newValue;
    await this.refreshList();
  }

  async refreshList() {
    const filter: any = {
      page: this.page,
      limit: this.limit
    };

    const response: any = await this.contactService.list(filter);
    this.messagesList = response.data;
    this.nbTotal = response.total;
  }

  setSelectedType(type) {
    this.type = type;
    this.refreshList();
  }

  async deleteElement(id) {
    try {
      await this.contactService.delete(id);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }

  async updateStatus(id, status) {
    try {
      await this.contactService.updateStatus(id, status);
      await this.refreshList();
    } catch (error) {
      console.log(error);
    }
  }
}
