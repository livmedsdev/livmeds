#!/bin/sh
set -e

url="$(echo "$API_URL" | sed 's/\//\\\//g')"
sed -i -e "s/__URL_ENV_PROD__/$url/g" /usr/share/nginx/html$FOLDER_WWW/main*.js > /usr/share/nginx/html$FOLDER_WWW/main*.js
